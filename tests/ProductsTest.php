<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductsTest extends BrowserKitTestCase
{
    use DatabaseTransactions;

    private $lastProductId;
    private $lastShopId;

    /**
     * Inicia Prueba unitaria
     */
    public function testUsersCrud()
    {
        Auth::loginUsingId(1, true);

        $this->getLastShopId();

        $this->registerData();

        $this->getLastProductId();

        $this->updateData();

        $this->deleteData();
    }

    private function registerData()
    {
    	$this->visit('/products/create')
        	->setDataForm()
            ->see( trans('process.success',['model' => trans('models.product.article') , 'process' => trans('process.created') ]) );
    }

    private function updateData()
    {    	
        return $this->visit('/products/'.$this->lastProductId)
            ->type('nombre cambiado', 'name')
            ->press(trans('process.update'))
            ->see( trans('process.success',['model' => trans('models.product.article') , 'process' => trans('process.updated') ]) );

    }

    private function deleteData()
    {
        return $this->delete('/products/'.$this->lastProductId)
	        ->followRedirects()
            ->see( trans('process.success',['model' => trans('models.product.article') , 'process' => trans('process.deleted') ]) );
    }

    private function getLastProductId()
    {
        $this->lastProductId = DB::table('products')->select('id')->orderBy('id', 'DESC')->first()->id;
    }

    private function getLastShopId()
    {
    	$this->lastShopId = DB::table('shops')->select('id')->orderBy('id', 'DESC')->first()->id;
    }
    
    /**
     * rellena formulario
     *
     * @return $this->visit
      **/
    private function setDataForm()
    {
        return $this->type($this->lastShopId, 'shop_id')
            ->type('nombre', 'name')
            ->type('1000', 'price')
            ->forceSelect('1','category_id')
            ->forceSelect('1','mp_category_id')
            ->type('abc1', 'code')
            ->type('Esto es un articulo de prueba', 'description')
            ->type(true, 'active')
            ->press(trans('process.create'));
    }

}
