<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CategoriesTest extends BrowserKitTestCase
{
    use DatabaseTransactions;

    private $lastCategoryId;

    /**
     * Inicia Prueba unitaria
     */
    public function testUsersCrud()
    {
        Auth::loginUsingId(1, true);

        $this->registerData();

        $this->getLastUserId();

        $this->updateData();

        $this->deleteData();
    }

    private function registerData()
    {
    	$this->visit('/categories/create')
        	->setDataForm() 
            ->see( trans('process.success',['model' => trans('models.category.article') , 'process' => trans('process.created') ]) );
    }

    private function updateData()
    {    	
        return $this->visit('/categories/'.$this->lastCategoryId)
            ->type('description new', 'description')
            ->press(trans('process.update')) 
            ->see( trans('process.success',['model' => trans('models.category.article') , 'process' => trans('process.updated') ]) );

    }

    private function deleteData()
    {
        return $this->delete('/categories/'.$this->lastCategoryId)
	        ->followRedirects() 
            ->see( trans('process.success',['model' => trans('models.category.article') , 'process' => trans('process.deleted') ]) );
    }

    private function getLastUserId()
    {
    	$this->lastCategoryId = DB::table('categories')->select('id')->orderBy('id', 'DESC')->first()->id;
    }
    
    /**
     * rellena formulario
     *
     * @return $this->visit
      **/
    private function setDataForm()
    {
        return $this->type('categoria 1', 'name')
            ->type('Categoria de Prueba', 'description')
            ->type('fa-asterisk', 'logo')
            ->type('A5FF87', 'color')
            ->press(trans('process.create'));
    }

}
