<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ShopsTest extends BrowserKitTestCase
{
    use DatabaseTransactions;
    
    private $lastShopId;

    /**
     * Inicia Prueba unitaria
     */
    public function testShopsCrud()
    {
        Auth::loginUsingId(1, true);

        $this->registerData();

        $this->getLastUserId();
        
        $this->updateData();

        $this->deleteData();
    }

    private function registerData()
    {
        $this->visit('/shops/create')
            ->setDataForm()
            ->seePageIs('/shops')
            ->see( trans('process.success',['model' => trans('models.shop.article') , 'process' => trans('process.created') ]) );
    }

    private function updateData()
    {
        return $this->visit('/shops/'.$this->lastShopId)
            ->type('Adidas', 'name')
            ->forceSelect('2', 'province_id')
            ->forceSelect('1', 'locality_id')
            ->press(trans('process.update'))
            ->seePageIs('/shops/'.$this->lastShopId)
            ->see( trans('process.success',['model' => trans('models.shop.article') , 'process' => trans('process.updated') ]) );

    }

    private function deleteData()
    {
        return $this->delete('/shops/'.$this->lastShopId)
            ->followRedirects()
            ->see( trans('process.success',['model' => trans('models.shop.article') , 'process' => trans('process.deleted') ]) );
    }
    
    private function getLastUserId()
    {
        $this->lastShopId = DB::table('shops')->select('id')->orderBy('id', 'DESC')->first()->id;
    }

    /**
     * rellena formulario
     *
     * @return $this->visit
      **/
    private function setDataForm()
    {
        return $this->type('Nike', 'name')
            ->type('Nike', 'business_name')
            ->type('12345678911', 'cuit')
            ->type('vip', 'vip')
            ->type('admin name','contact_management')
            ->type('45000', 'iibb')
            ->type('Responsable Inscripto', 'iva')
            ->forceSelect('2', 'province_id')
            ->forceSelect('1', 'locality_id')
            ->type('-34.606085', 'lat')
            ->type('-58.486404', 'lng')
            ->type('buenos aires', 'address')
            ->type('nike@nike.com', 'email')
            ->check('active')
            ->type('1166675109', 'phone')
            ->type('9hrs - 18hrs', 'schedule')
            ->press(trans('process.create'));
    }
}