<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\models\User;

class UsersTest extends BrowserKitTestCase
{
    use DatabaseTransactions;

    private $lastUserId;

    /**
     * Inicia Prueba unitaria
     */
    public function testUsersCrud()
    {
        Auth::loginUsingId(1, true);

        $this->registerData();

        $this->getLastUserId();

        $this->updateData();

        $this->deleteData();
    }

    private function registerData()
    {
    	$this->visit('/users/create')
        	->setDataForm()
            ->see( trans('process.success',['model' => trans('models.user.article') , 'process' => trans('process.created') ]) );
    }

    private function updateData()
    {    	
        return $this->visit('/users/'.$this->lastUserId)
            ->type('Adidas', 'first_name')
            ->press(trans('process.update'))
            ->see( trans('process.success',['model' => trans('models.user.article') , 'process' => trans('process.updated') ]) );

    }

    private function deleteData()
    {
        return $this->delete('/users/'.$this->lastUserId)
	        ->followRedirects()
            ->see( trans('process.success',['model' => trans('models.user.article') , 'process' => trans('process.deleted') ]) );
    }

    private function getLastUserId()
    {
    	$this->lastUserId = DB::table('users')->select('id')->orderBy('id', 'DESC')->first()->id;
    }
    
    /**
     * rellena formulario
     *
     * @return $this->visit
      **/
    private function setDataForm()
    {
        return $this->type('Name', 'first_name')
            ->type('LastName', 'last_name')
            ->type('emailbydefault@email.com', 'email')
            ->type('+581234568', 'phone')
            ->type('operator', 'type')
            ->type('1981', 'birthday')
            ->type('abc789', 'password')
            ->type('abc789', 'password_confirmation')
            ->press(trans('process.create'));
    }
}
