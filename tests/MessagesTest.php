<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MessagesTest extends BrowserKitTestCase
{
    use DatabaseTransactions;

    private $lastMessageId;

    /**
     * Inicia Prueba unitaria
     */
    public function testMessagesCrud()
    {
        Auth::loginUsingId(1, true);

        $this->registerData();

        $this->getLastUserId();

        $this->updateData();

        $this->deleteData();
    }

    private function registerData()
    {
    	$this->visit('/messages/create')
        	->setDataForm() 
            ->see( trans('process.success',['model' => trans('models.message.article') , 'process' => trans('process.created') ]) );
    }

    private function updateData()
    {    	
        return $this->visit('/messages/'.$this->lastMessageId)
            ->type('nueva descripcion', 'content')
            ->forceSelect('1', 'product_id')
            ->press(trans('process.update'))
            ->see( trans('process.success',['model' => trans('models.message.article') , 'process' => trans('process.updated') ]) );

    }

    private function deleteData()
    {
        return $this->delete('/messages/'.$this->lastMessageId)
	        ->followRedirects() 
            ->see( trans('process.success',['model' => trans('models.message.article') , 'process' => trans('process.deleted') ]) );
    }

    private function getLastUserId()
    {
    	$this->lastMessageId = DB::table('product_advertising')->select('id')->orderBy('id', 'DESC')->first()->id;
    }
    
    /**
     * rellena formulario
     *
     * @return $this->visit
      **/
    private function setDataForm()
    {
        return $this->forceSelect('1','shop_id')
            ->forceSelect('1', 'product_id')
            ->type('Contenido de prueba', 'content')
            ->check('active')
            ->press(trans('process.create'));;
    }
}
