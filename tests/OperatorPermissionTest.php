<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OperatorPermissionTest extends BrowserKitTestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testOperatorPermission()
    {
        Auth::loginUsingId(2, true);
        $this->assertRedirectToHome('/users');
        $this->visit('/shops/1')->seePageIs('/shops');
        $this->assertRedirectToHome('/categories');
    }

    private function assertRedirectToHome($route)
    {
        $this->visit($route)
            ->seePageIs('/home');
        $this->visit($route.'/1')
            ->seePageIs('/home');
    }
}
