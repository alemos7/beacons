<?php

use Illuminate\Contracts\Console\Kernel;
use Laravel\BrowserKitTesting\TestCase as BaseTestCase;

abstract class BrowserKitTestCase extends BaseTestCase
{
    /**
     * The base URL of the application.
     *
     * @var string
     */
    public $baseUrl = 'http://localhost';

    /**
     * Disabled validation fields array
     *
     * @var array
     */
    protected $disabledValidationFields;

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    /**
     * Select an option from a drop-down.
     *
     * @param  string  $option
     * @param  string  $element
     * @return $this
     */
    protected function forceSelect($option, $element)
    {
        return $this->storeDisabledValidationInput($element, $option);
    }

    /**
     * Store a form input in the local array.
     *
     * @param  string  $element
     * @param  string  $text
     * @return $this
     */
    private function storeDisabledValidationInput($element, $text)
    {
        $this->assertFilterProducesResults($element);

        $element = str_replace('#', '', $element);

        $this->inputs[$element] = $text;

        $this->disabledValidationFields[] = $element;

        return $this;
    }

    /**
     * Fill the form with the given data.
     *
     * @param  string  $buttonText
     * @param  array  $inputs
     * @return \Symfony\Component\DomCrawler\Form
     */
    protected function fillForm($buttonText, $inputs = [])
    {
        if (! is_string($buttonText)) {
            $inputs = $buttonText;

            $buttonText = null;
        }

        $form = $this->getForm($buttonText);

        $this->disableValidationOn($form);

        return $form->setValues($inputs);
    }

    /**
     * Adds disableValidation on selected fields
     *
     * @param $form  \Symfony\Component\DomCrawler\Form
     */
    private function disableValidationOn($form)
    {
        if (!is_array($this->disabledValidationFields)) {
            return;
        }

        foreach($this->disabledValidationFields as $field)
        {
            $form->get($field)->disableValidation();
        }

        $this->disabledValidationFields = [];
    }
}