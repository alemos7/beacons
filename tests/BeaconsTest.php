<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BeaconsTest extends BrowserKitTestCase
{
    use DatabaseTransactions;

    private $lastBeaconId;
    private $lastShopId;

    /**
     * Inicia Prueba unitaria
     */
    public function testUsersCrud()
    {
        Auth::loginUsingId(1, true);

        $this->getLastShopId();

        $this->registerData();

        $this->getLastBeaconId();

        $this->updateData();

        $this->deleteData();
    }

    private function registerData()
    {
    	$this->visit('/beacons/create')
        	->setDataForm() 
            ->see( trans('process.success',['model' => trans('models.beacon.article') , 'process' => trans('process.created') ]) );
    }

    private function updateData()
    {    	
        return $this->visit('/beacons/'.$this->lastBeaconId)
            ->type('direccion cambiada', 'description')
            ->press(trans('process.update')) 
            ->see( trans('process.success',['model' => trans('models.beacon.article') , 'process' => trans('process.updated') ]) );

    }

    private function deleteData()
    {
        return $this->delete('/beacons/'.$this->lastBeaconId)
	        ->followRedirects() 
            ->see( trans('process.success',['model' => trans('models.beacon.article') , 'process' => trans('process.deleted') ]) );
    }

    private function getLastBeaconId()
    {
        $this->lastBeaconId = DB::table('shop_beacons')->select('id')->orderBy('id', 'DESC')->first()->id;
    }

    private function getLastShopId()
    {
    	$this->lastShopId = DB::table('shops')->select('id')->orderBy('id', 'DESC')->first()->id;
    }
    
    /**
     * rellena formulario
     *
     * @return $this->visit
      **/
    private function setDataForm()
    {
        return $this->forceSelect('1','shop_id')
            ->type('asdasd', 'serial')
            ->type('descripcion del beacon', 'description')
            ->type(true, 'active')
            ->press(trans('process.create'));
    }

}
