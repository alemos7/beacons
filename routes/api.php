<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'prefix' => 'v1'], function () {

    /**
     * Resetear contraseña
     */
    Route::post('/resetPassword', 'Api\AuthController@resetPassword');

    /**
     * Autorización / Registro
     */
    Route::group(['prefix' => 'auth'], function () {
        /**
         * Login
         */
        Route::post('/signIn', 'Api\AuthController@signIn');
        Route::post('/signInV2', 'Api\AuthController@signInV2');
        /**
         * Logout
         */
        Route::post('/logout', 'Api\AuthController@logout');
        Route::post('/logoutV2', 'Api\AuthController@logoutV2');

        /**
         * Registro
         */
        Route::post('/signUp', 'Api\AuthController@signUp');
        Route::post('/signUpV2', 'Api\AuthController@signUpV2');
        
        Route::post('/userGuest', 'Api\AuthController@userGuest');
        Route::post('/userGuestV2', 'Api\AuthController@userGuestV2');

        Route::post('/facebook/callback', 'Api\AuthController@facebookCallback');
        Route::post('/google/callback', 'Api\AuthController@googleCallback');
        
        Route::post('/facebook/callbackV2', 'Api\AuthController@facebookCallbackV2');
        Route::post('/google/callbackV2', 'Api\AuthController@googleCallbackV2');
        
        Route::post('/signDown', 'Api\AuthController@signDown');
        Route::post('/update_register_id', 'Api\AuthController@updateRegisterId');
        
        Route::post('/changePassword', 'Api\AuthController@changePassword');
        
    });

    Route::resource('/users', 'Api\UsersController');
    Route::post('/users/editProfileV2', 'Api\UsersController@editProfileV2');

    /**
     * Productos
     */
    Route::get('products/closer', 'Api\ProductController@closer');
    Route::get('products/get-products', 'Api\ProductController@getProducts');
    Route::get('products/get-banners', 'Api\ProductController@getBanners');
    Route::resource('products', 'Api\ProductController');

    /**
     * MP
     */

    Route::post('mp/mailTest', 'Api\MPController@mailTest');

    Route::post('mp', 'Api\MPController@checkout');
    Route::post('mp/checkoutV2', 'Api\MPController@checkoutV2');
    Route::post('mp/saveV2', 'Api\MPController@saveV2');
    Route::get('mp/get', 'Api\MPController@get');
    Route::get('mp/getV2', 'Api\MPController@getV2');

//    Route::post('mp/update_payments_versions', 'Api\MPController@updatePaymentsVersions');

    Route::get('mp/notification', 'Api\MPController@notification');
    Route::post('mp/calificate', 'Api\MPController@calificate');
    Route::post('mp/calificateV2', 'Api\MPController@calificateV2');
    Route::post('messages/send', 'Api\MessagesController@send');
    Route::post('messages/sendV2', 'Api\MessagesController@sendV2');
    Route::post('messages/send/ios', 'Api\MessagesController@sendIOS');
    Route::post('messages/statistics/views', 'Api\MessagesController@statisticsViews');
    
    Route::get('messages/get_notifications', 'Api\MessagesController@getUserNotifications');
    Route::get('messages/get_notificationsV2', 'Api\MessagesController@getUserNotificationsV2');
    Route::post('messages/delete_notifications', 'Api\MessagesController@deleteNotifications');

    /**
     * Tiendas / Shops
     */
    Route::get('shops/getpoints', 'Api\ShopController@getPoints');
    Route::resource('shops', 'Api\ShopController');

    /**
     * Categorías
     */
    Route::get('categories/shops', 'Api\CategoryController@withShops');
    Route::resource('categories', 'Api\CategoryController');

    /**
     * Listado de beacons
     */
    Route::post('beacons', 'Api\BeaconController@index');
    
    Route::get('/content', 'Api\UsersController@content');
    /**
     * Sección protegida por login
     */

    Route::delete('me/favoritesV2', 'Api\MyProfileController@deleteFavoritesV2');
    Route::get('me/favoritesV2', 'Api\MyProfileController@favoritesV2');
    Route::post('me/favoritesV2', 'Api\MyProfileController@putFavoritesV2');

    Route::get('/me/settingsV2', 'Api\MyProfileController@settingsV2');
    Route::post('/me/settingsV2', 'Api\MyProfileController@saveSettingsV2');

    Route::get('/me/interestsV2', 'Api\MyProfileController@getInterestsV2');
    Route::post('/me/interestsV2', 'Api\MyProfileController@saveInterestsV2');

    Route::group(['middleware' => 'auth'], function () {

        Route::get('/users/{id}/points', 'Api\PointsController@index');

        /**
         * Perfiles
         */
        Route::group(['prefix' => 'profile'], function () {
            /**
             * Perfil propio
             */
            Route::get('/me/preferences', 'Api\MyProfileController@preferences');
            Route::delete('/me/favorites', 'Api\MyProfileController@deleteFavorites');
            Route::get('/me/favorites', 'Api\MyProfileController@favorites');
            Route::put('/me/favorites', 'Api\MyProfileController@putFavorites');


            Route::put('/me/settings', 'Api\MyProfileController@putSettings');
            Route::get('/me/settings', 'Api\MyProfileController@settings');
            Route::post('/me/interests', 'Api\MyProfileController@interests');
            Route::get('/me', 'Api\MyProfileController@index');
            Route::put('/me', 'Api\MyProfileController@update');
            Route::delete('/me', 'Api\MyProfileController@delete');
        });
    });
});
