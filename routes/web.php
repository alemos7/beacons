<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::resource('shops', 'ShopController');
Route::post('shops/{id}/set-active', 'ShopController@setActiveStatus');
Route::post('shops/sort-images/', 'ShopController@sortImages');
Route::get('shops/{id}/products', 'ShopController@showProducts');

Route::resource('shop-images', 'ShopImageController');
Route::get('shop-images/shop/{id}', 'ShopImageController@showByShop');
Route::get('shops/{id}/set-image', 'ShopController@setImageAfterCreate');
Route::get('shops/check-images/{id}', 'ShopController@checkIfHaveImages');

Route::resource('words', 'WordController');

Route::resource('users', 'UserController');

Route::post('categories/sort-images/', 'CategoryController@sortImages');
Route::resource('categories', 'CategoryController');

Route::resource('category-images', 'CategoryImageController');
Route::get('category-images/category/{id}', 'CategoryImageController@showByCategory');

Route::resource('products', 'ProductController');
Route::post('products/sort-images', 'ProductController@sortImages');
Route::get('products/{id}/set-image', 'ProductController@setImageAfterCreate');
Route::get('products/check-images/{id}', 'ProductController@checkIfHaveImages');
Route::post('products/{id}/set-active', 'ProductController@setActiveStatus');

Route::resource('product-images', 'ProductImageController');
Route::get('product-images/product/{id}', 'ProductImageController@showByProduct');

Route::post('product-offers/calculate', 'ProductOfferController@calculate');

Route::resource('messages', 'MessageController');
Route::get('send-to-ios', 'MessageController@sendIOS');
Route::post('messages/send/{id}', 'MessageController@send');

Route::resource('beacons', 'ShopBeaconController');
Route::get('beacon/generate', 'ShopBeaconController@generateSerial');

Route::resource('products-beacon', 'ProductBeaconController');

Route::get('province/localities/{id}','ProvinceController@showLocalities');

Route::get('public_content', 'ContentController@public');
Route::get('content', 'ContentController@index');
Route::post('content', 'ContentController@store');


Route::get('help', 'HelpController@index');
Route::post('help', 'HelpController@store');

Route::resource('clients', 'ClientController');

Route::get('banners', 'BannerController@index');
Route::post('banners','BannerController@store');
Route::put('banners', 'BannerController@update');

Route::get('payments', 'PaymentController@index');
Route::get('payments/{id}', 'PaymentController@show');
Route::put('payments/{id}', 'PaymentController@update');

Route::post('mp/notifications', 'MPController@notifications');
//Route::post('mp/notifications', 'MPController@notifications');

Route::get('statistics', 'StatisticsController@index');
Route::get('statistics/{id}', 'StatisticsController@index');
