<?php

use Illuminate\Foundation\Inspiring;
// Models
use App\Models\User;
use App\Models\Shop;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('make:admin', function () {
    $user = new User();
    $user->first_name =  "admin";
    $user->last_name =  "admin";
    $user->email = "admin@email.com";
    $user->phone =  "+58123456789";
    $user->birthday =  "2016";
    $user->type =  "administrator";
    $user->password =  Hash::make("abc789");
    try{
        $user->save(); 
        $this->comment("Administrador registrado correctamente" );
        return true;
    }
    catch(Exception $e){
        $this->comment("No se ha registrado debido a que el usuario admin ya existe" );
        return false;
    }
});

Artisan::command('make:operator', function () {
    $user = new User();
    $user->first_name =  "operator";
    $user->last_name =  "operator";
    $user->email = "operador@email.com";
    $user->phone =  "+58123456789";
    $user->birthday =  "2016";
    $user->type =  "operator";
    $user->password =  Hash::make("abc789");
    try{
        $user->save(); 
        $this->comment("Operador registrado correctamente" );
        return true;
    }
    catch(Exception $e){
        $this->comment("No se ha registrado debido a que el usuario operator ya existe" );
        return false;
    }
});