<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayments extends Migration
{

    private $table_name = "payments";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('shop_payment_id')->unsigned();
            $table->integer('product_id');
            $table->text('response');
            $table->enum('status',['paid', 'processing', 'failed']);
            $table->timestamps();

            /**
             * Indexs and foreign keys
             */

            $table->foreign('user_id')->references("id")->on("users")
                ->onDelete('cascade');
            $table->foreign('shop_payment_id')->references("id")->on("shop_payments")
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['shop_payment_id']);
        });

        Schema::drop($this->table_name);
    }
}
