<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsBeaconTable extends Migration
{
    
    private $table_name = "products_beacon";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('beacon_id')->unsigned();
            $table->integer('product_close_id')->unsigned()->nullable();
            $table->string('product_close_message');
            $table->integer('product_far_id')->unsigned()->nullable();
            $table->string('product_far_message');

            /**
             * Indexs and foreign keys
             */
            $table->foreign('beacon_id')->references("id")->on("shop_beacons")
                ->onDelete('cascade');
            $table->foreign('product_close_id')->references("id")->on("products")
                ->onDelete('cascade');            
            $table->foreign('product_far_id')->references("id")->on("products")
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
           $table->dropForeign(['beacon_id']);
           $table->dropForeign(['product_close_id']);
           $table->dropForeign(['product_far_id']);
        });

        Schema::drop($this->table_name);
    }
}
