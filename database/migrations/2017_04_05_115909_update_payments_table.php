<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePaymentsTable extends Migration
{

    private $table_name = "payments";
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');
        
        DB::statement("ALTER TABLE ".$this->table_name." CHANGE COLUMN status status 
                ENUM('pending',
                    'approved',
                    'in_process',
                    'in_mediation',
                    'rejected',
                    'cancelled',
                    'refunded',
                    'charged_back')");

        Schema::table($this->table_name, function($table) {
            $table->dropForeign($this->table_name.'_shop_payment_id_foreign');
            $table->dropForeign($this->table_name.'_payment_type_id_foreign');
            $table->dropColumn(['shop_payment_id']);
            $table->dropColumn(['payment_type_id']);
            
            $table->integer('mp_payment_id')->after('id');

            $table->integer('product_id')->unsigned()->change();

            $table->foreign('product_id')->references("id")->on("products")
                ->onDelete('cascade');     
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
