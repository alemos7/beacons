<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalificationsTable extends Migration
{

    private $table_name = "califications";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_id')->unsigned();            
            $table->integer('stars');            
            $table->text('comment')->nullable();            
            $table->integer('shop_id')->unsigned();            
            $table->integer('product_id')->unsigned();            
            $table->integer('user_id')->unsigned();                
            $table->timestamps();

            /**
             * Indexs and foreign keys
             */

            $table->foreign('product_id')->references("id")->on("products")
                ->onDelete('cascade'); 
            $table->foreign('payment_id')->references("id")->on("payments")
                ->onDelete('cascade');                
            $table->foreign('shop_id')->references("id")->on("shops")
                ->onDelete('cascade');                
            $table->foreign('user_id')->references("id")->on("users")
                ->onDelete('cascade');        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {        
        Schema::table($this->table_name, function ($table) {
           $table->dropForeign(['payment_id']);
           $table->dropForeign(['shop_id']);
           $table->dropForeign(['user_id']);
        });
        Schema::drop($this->table_name);
    }
}