<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpCategoriesTable extends Migration
{
    private $table_name = "mp_categories";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('description');
        });

        Schema::table('products', function($table) {
            $table->integer('mp_category_id')->unsigned();
           
            $table->foreign('mp_category_id')->references("id")->on($this->table_name);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function($table) {
            $table->dropForeign(['mp_category_id']);
        });
        Schema::dropIfExists($this->table_name);
    }
}
