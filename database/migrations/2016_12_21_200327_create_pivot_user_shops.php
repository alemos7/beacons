<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotUserShops extends Migration
{

    private $table_name = "pivot_user_shops";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('shop_id')->unsigned();

            /**
             * Indexs and foreign keys
             */

            $table->foreign('user_id')->references("id")->on("users")
                ->onDelete('cascade');
            $table->foreign('shop_id')->references("id")->on("shops")
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['shop_id']);
        });

        Schema::drop($this->table_name);
    }
}
