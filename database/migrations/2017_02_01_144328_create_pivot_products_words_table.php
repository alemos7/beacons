<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotProductsWordsTable extends Migration
{
    private $table_name = "pivot_products_words";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function(Blueprint $table){
            $table->increments('id');
            $table->integer('word_id')->unsigned();
            $table->integer('product_id')->unsigned();

            /**
             * Indexs and foreign keys
             */

            $table->foreign('word_id')->references("id")->on("words")
                ->onDelete('cascade');
            $table->foreign('product_id')->references("id")->on("products")
                ->onDelete('cascade');        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
            $table->dropForeign(['word_id']);
            $table->dropForeign(['product_id']);
        });

        Schema::drop($this->table_name);
    }
}
