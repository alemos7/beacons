<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAdvertising extends Migration
{

    private $table_name = "product_advertising";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->text('content');
            $table->date('date_send')->nullable();
            $table->timestamps();
            $table->boolean('active')->default(true);

            /**
             * Indexs and foreign keys
             */

            $table->foreign('product_id')->references("id")->on("products")
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
            $table->dropForeign(['product_id']);
        });

        Schema::drop($this->table_name);
    }
}
