<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialNetworks extends Migration
{

    private $table_name = "social_networks";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->enum('type',['facebook','google']);
            $table->string('token');
            $table->string('email')->unique();
            $table->timestamps();

            /**
             * Indexs and foreign keys
             */

            $table->foreign('user_id')->references("id")->on("users")
                ->onDelete('cascade');        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
            $table->dropForeign(['user_id']);
        });

        Schema::drop($this->table_name);
    }
}
