<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeBeaconShopIdNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE `shop_beacons` CHANGE `shop_id` `shop_id` INT(10) UNSIGNED NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DELETE FROM `shop_beacons` WHERE `shop_id` IS NULL');
        DB::statement('ALTER TABLE `shop_beacons` CHANGE `shop_id` `shop_id` INT(10) UNSIGNED NOT NULL;');
    }
}
