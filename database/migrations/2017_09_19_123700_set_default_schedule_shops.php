<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefaultScheduleShops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `shops` CHANGE `schedule` `schedule` VARCHAR(255) CHARSET utf8 COLLATE utf8_unicode_ci DEFAULT '-' NOT NULL;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `shops` CHANGE `schedule` `schedule` VARCHAR(255) CHARSET utf8 COLLATE utf8_unicode_ci NOT NULL;");
    }
}
