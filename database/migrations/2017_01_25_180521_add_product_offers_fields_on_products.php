<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductOffersFieldsOnProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_offers', function (Blueprint $table) {
            $table->unsignedInteger('product_offer_id')->nullable();
            $table->decimal('before_offer_price', 32, 2)->nullable();

            /**
             * Indexs and foreign keys
             */
            $table->foreign('product_offer_id')->references("id")->on("products")
                ->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_offers', function (Blueprint $table) {
            /**
             * Foreign keys
             */
            $table->dropForeign(['product_offer_id']);

            $table->dropColumn('product_offer_id');
            $table->dropColumn('before_offer_price');
        });
    }
}
