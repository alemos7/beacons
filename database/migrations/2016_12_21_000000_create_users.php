<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration
{

    private $table_name = "users";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['user', 'operator', 'administrator'])
                ->default('user');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('first_name', 100)->nullable();
            $table->string('last_name', 100)->nullable();
            $table->string('birthday', 50)->nullable();
            $table->string('phone')->nullable();
            $table->string('image')->nullable();
            $table->boolean('active')->default(true);
            $table->enum('from', ['Manual', 'Facebook', 'Gmail', 'Email'])
                ->default('Manual');
            $table->text('config')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->table_name);
    }
}
