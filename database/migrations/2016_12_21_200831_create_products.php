<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration
{

    private $table_name = "products";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->string('name',100);
            $table->decimal('price',32,2);
            $table->string('code',50)->nullable();
            $table->text('description',360)->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
            $table->softDeletes();
            
            /**
             * Indexs and foreign keys
             */

            $table->foreign('shop_id')->references("id")->on("shops")
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
            $table->dropForeign(['shop_id']);
        });

        Schema::drop($this->table_name);
    }
}
