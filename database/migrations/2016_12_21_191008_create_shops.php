<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShops extends Migration
{

    private $table_name = "shops";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->string('cuit',11);
            $table->string('name',100);
            $table->mediumText('description');
            $table->boolean('active')->default(true);
            $table->string('business_name',150);
            $table->string('phone');
            $table->float('lat',16,7);
            $table->float('lng',16,7);
            $table->string('address');
            $table->string('email');
            $table->boolean('vip')->default(false);
            $table->enum('iva', [
                                    'Responsable Inscripto', 
                                    'Responsable No Inscripto', 
                                    'Responsable Monotributista', 
                                    'Exento', 
                                    'Consumidor Final', 
                                    'Otros'
                                ]);
            $table->decimal('iibb', 32);
            $table->string('schedule');
            $table->string('observation')->nullable();
            $table->string('contact_management')->nullable();
            $table->string('logo')->nullable()->default('');
            $table->integer('locality_id')->unsigned();
            $table->integer('province_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            /**
             * Indexs and foreign keys
             */

            $table->foreign('locality_id')->references("id")->on("locality")
                ->onDelete('cascade');
            $table->foreign('province_id')->references("id")->on("province")
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
           $table->dropForeign(['locality_id']);
           $table->dropForeign(['province_id']);
        });
        Schema::drop($this->table_name);
    }
}
