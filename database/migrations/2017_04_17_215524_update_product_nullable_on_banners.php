<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductNullableOnBanners extends Migration
{
    private $table_name = "banners";
    private $number_fields = 5;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->table_name, function (Blueprint $table) {
            
            for( $i=1 ; $i<= $this->number_fields ; $i++){
                // Names of fields
                $product_id = 'product_'.$i.'_id';
                $table->integer($product_id)->nullable()->unsigned()->change();
                $table->string('image_'.$i)->nullable()->change();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
