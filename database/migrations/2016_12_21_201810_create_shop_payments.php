<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopPayments extends Migration
{

    private $table_name = "shop_payments";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->integer('payment_type_id')->unsigned();
            $table->timestamps();

            /**
             * Indexs and foreign keys
             */

            $table->foreign('shop_id')->references("id")->on("shops")
                ->onDelete('cascade');
            $table->foreign('payment_type_id')->references("id")->on("payment_types")
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
            $table->dropForeign(['shop_id']);
            $table->dropForeign(['payment_type_id']);
        });

        Schema::drop($this->table_name);
    }
}