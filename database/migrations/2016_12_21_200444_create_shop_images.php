<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopImages extends Migration
{

    private $table_name = "shop_images";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->nullable()->unsigned();
            $table->string('image',100); 
            $table->integer('order')->default(0); 
            $table->boolean('active')->default(true);

            /**
             * Indexs and foreign keys
             */

            $table->foreign('shop_id')->references("id")->on("shops")
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
            $table->dropForeign(['shop_id']);
        });

        Schema::drop($this->table_name);
    }
}
