<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsUsersNotifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    
    private $table_name = "users_notifications";
    
    public function up()
    {
        Schema::table($this->table_name, function (Blueprint $table) {
            $table->string('advertising');
            $table->integer('statistic_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
