<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMessageStatistics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages_statistics', function(Blueprint $table) {

            $table->increments('id');
            $table->string('position');
            $table->unsignedTinyInteger('views');
            $table->unsignedTinyInteger('issues');
            $table->unsignedInteger('products_beacon_id');
            $table->timestamps();

            $table->foreign('products_beacon_id')
                ->references('id')
                ->on('products_beacon')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages_statistics');
    }
}
