<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotProductCategories extends Migration
{

    private $table_name = "pivot_product_categories";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->integer('category_id')->unsigned();

            /**
             * Indexs and foreign keys
             */

            $table->foreign('product_id')->references("id")->on("products")
                ->onDelete('cascade');
            $table->foreign('category_id')->references("id")->on("categories")
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.s
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
            if (Schema::hasTable($this->table_name)) {
                $table->dropForeign(['product_id']);
                $table->dropForeign(['category_id']);
            }
        });

        Schema::dropifExists($this->table_name);
    }
}
