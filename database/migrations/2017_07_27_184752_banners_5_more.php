<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Banners5More extends Migration
{
    private $table_name = "banners";
    private $number_fields = 10;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->table_name, function (Blueprint $table) {
            for( $i=6 ; $i<= $this->number_fields ; $i++){

                // Names of fields
                $product_id = 'product_'.$i.'_id';
                $table->integer($product_id)->unsigned();
                $table->string('image_'.$i);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
