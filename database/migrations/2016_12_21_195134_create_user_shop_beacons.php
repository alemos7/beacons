<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserShopBeacons extends Migration
{

    private $table_name = "user_shop_beacons";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_beacon_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->float('distance');
            $table->dateTime('created_at');

            /**
             * Indexs and foreign keys
             */

            $table->foreign('user_id')->references("id")->on("users")
                    ->onDelete('cascade');
            $table->foreign('shop_beacon_id')->references("id")->on("shop_beacons")
                    ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['shop_beacon_id']);
        });

        Schema::drop($this->table_name);
    }
}
