<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopScores extends Migration
{

    private $table_name = "shop_scores";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('payment_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->smallInteger('score');
            $table->string('comment')->nullable();

            /**
             * Indexs and foreign keys
             */

            $table->foreign('user_id')->references("id")->on("users")
                ->onDelete('cascade');
            $table->foreign('payment_id')->references("id")->on("payments")
                ->onDelete('cascade');
            $table->foreign('product_id')->references("id")->on("products")
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['payment_id']);
            $table->dropForeign(['product_id']);
        });

        Schema::drop($this->table_name);
    }
}
