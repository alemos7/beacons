<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Models\MP_Category;

class ChangeMpCategoryTrans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("UPDATE `mp_categories` SET `trans` = 'Bebés' WHERE `name` = 'baby';");
        DB::statement("UPDATE `mp_categories` SET `trans` = 'Aprendizaje' WHERE `name` = 'learnings';");
        DB::statement("UPDATE `mp_categories` SET `trans` = 'Cámaras' WHERE `name` = 'cameras';");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
