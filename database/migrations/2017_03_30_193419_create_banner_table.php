<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannerTable extends Migration
{

    private $table_name = "banners";
    private $number_fields = 5;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            
            $table->increments('id');
            
            for( $i=1 ; $i<= $this->number_fields ; $i++){
                
                // Names of fields
                $product_id = 'product_'.$i.'_id';
                $table->integer($product_id)->unsigned();
                $table->string('image_'.$i);

                /*
                 * Index and foreign keys
                 */
                $table->foreign($product_id)->references('id')->on('products')
                    ->onDelete('cascade');
            }

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function($table) {
            for( $i=1 ; $i<= $this->number_fields ; $i++){
                $table->dropForeign(['product_'.$i.'_id']);
            }
        });

        Schema::drop($this->table_name);
    }
}
