<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopBeacons extends Migration
{

    private $table_name = "shop_beacons";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->integer('product_id')->nullable();
            $table->string('serial');
            $table->string('distance')->nullable();
            $table->mediumText('description')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();

            /**
             * Indexs and foreign keys
             */

            $table->foreign('shop_id')->references("id")->on("shops")
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
           $table->dropForeign(['shop_id']);
        });

        Schema::drop($this->table_name);
    }
}
