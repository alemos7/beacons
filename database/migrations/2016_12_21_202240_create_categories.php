<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategories extends Migration
{

    private $table_name = "categories";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',100);
            $table->mediumText('description')->nullable();
            $table->string('logo',100);
            $table->string('color',6);
            $table->integer('childof')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            /**
             * Indexs and foreign keys
             */
            
            $table->foreign('childof')->references("id")->on($this->table_name)
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->table_name);
    }
}
