<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductBeaconSetNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('messages_statistics', function(Blueprint $table) {
            $table->unsignedInteger('products_beacon_id')->nullable()->change();

            $table->unsignedInteger('product_advertising_id')->nullable();

            $table->foreign('product_advertising_id')
                ->references('id')
                ->on('product_advertising')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {       
        Schema::table('messages_statistics', function ($table) {
           $table->dropForeign(['product_advertising_id']);
        });
    }
}
