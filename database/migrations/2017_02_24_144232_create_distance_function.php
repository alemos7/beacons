<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDistanceFunction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared(DB::raw('DROP FUNCTION IF EXISTS `distance`'));
        DB::unprepared(DB::raw('CREATE FUNCTION `distance` (
  lat1 DECIMAL (8, 6),
  lng1 DECIMAL (8, 6),
  lat2 DECIMAL (8, 6),
  lng2 DECIMAL (8, 6)
) RETURNS DECIMAL (8, 6) 
BEGIN
  DECLARE R INT ;
  DECLARE distLng1 DECIMAL (30, 15) ;
  DECLARE distLat1 DECIMAL (30, 15) ;
  DECLARE d DECIMAL (30, 15) ;
  SET R = 3959 * 1.5 ;
  SET distLat1 = SIN(RADIANS(lat1)) * SIN(RADIANS(lat2)) ;
  SET distLng1 = COS(RADIANS(lat1)) * COS(RADIANS(lat2)) * COS(RADIANS(lng2) - RADIANS(lng1)) ;
  SET d = R * ACOS(distLng1 + distLat1) ;
  RETURN d ;
END ;
$$

DELIMITER ;'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared(DB::raw('DROP FUNCTION IF EXISTS `distance`'));
    }
}
