<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePivotShopCategories extends Migration
{

    private $table_name = "pivot_shop_categories";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shop_id')->unsigned();
            $table->integer('category_id')->unsigned();

            /**
             * Indexs and foreign keys
             */

            $table->foreign('shop_id')->references("id")->on("shops")
                ->onDelete('cascade');
            $table->foreign('category_id')->references("id")->on("categories")
                ->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.s
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
            $table->dropForeign(['shop_id']);
            $table->dropForeign(['category_id']);
        });

        Schema::drop($this->table_name);
    }
}
