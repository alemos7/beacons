<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocality extends Migration
{

    private $table_name = "locality";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table_name, function (Blueprint $table) {
            $table->increments('id');
            $table->integer('province_id')->unsigned();
            $table->string('name',100);

            /**
             * Indexs and foreign keys
             */

            $table->foreign('province_id')->references("id")->on("province")
                ->onDelete('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table_name, function ($table) {
            $table->dropForeign(['province_id']);
        });

        Schema::drop($this->table_name);
    }
}
