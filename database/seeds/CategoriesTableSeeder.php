<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate the table.
        // DB::table('categories')->truncate();

    	$categories = $this->fillData();

        foreach($categories as $category) {
            Category::create($category);
        }
    }

    private function fillData()
    {
    	$categories = [
	    	['name' => 'Deportes' ,'description' => 'Calzado e indumentaria deportiva' ,'logo' => 'futbol-o', 'color'=> 'A5FF87' ],
	    	['name' => 'Electrodomésticos' ,'description' => '' ,'logo' => 'laptop', 'color'=> 'FF8A8A' ],
	    	['name' => 'Telefonía' ,'description' => '' ,'logo' => 'mobile-phone', 'color'=> '8FD4FF' ],
	    	['name' => 'Ropa y accesorios' ,'description' => '' ,'logo' => 'suitcase', 'color'=> 'A78FFF' ],
    	];
    	return $categories;
    }
}


