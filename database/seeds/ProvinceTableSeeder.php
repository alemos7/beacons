<?php

use Illuminate\Database\Seeder;

class ProvinceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('locality')->delete();
        DB::table('province')->delete();
        
        $provinces = [
            ['id' => 2,  'key' => 'CABA', 'name' => 'CIUDAD AUTÓNOMA DE BUENOS AIRES'],
            ['id' => 6,  'key' => 'BA', 'name' => 'BUENOS AIRES'],
            ['id' => 10, 'key' => 'CT', 'name' => 'CATAMARCA'],
            ['id' => 14, 'key' => 'CBA', 'name' => 'CÓRDOBA'],
            ['id' => 22, 'key' => 'CC', 'name' => 'CHACO'],
            ['id' => 26, 'key' => 'CH', 'name' => 'CHUBUT'],
            ['id' => 18, 'key' => 'CR', 'name' => 'CORRIENTES'],
            ['id' => 30, 'key' => 'ER', 'name' => 'ENTRE RÍOS'],
            ['id' => 34, 'key' => 'FO', 'name' => 'FORMOSA'],
            ['id' => 38, 'key' => 'JY', 'name' => 'JUJUY'],
            ['id' => 42, 'key' => 'LP', 'name' => 'LA PAMPA'],
            ['id' => 46, 'key' => 'LR', 'name' => 'LA RIOJA'],
            ['id' => 50, 'key' => 'MZ', 'name' => 'MENDOZA'],
            ['id' => 54, 'key' => 'MN', 'name' => 'MISIONES'],
            ['id' => 58, 'key' => 'NQ', 'name' => 'NEUQUÉN'],
            ['id' => 62, 'key' => 'RN', 'name' => 'RÍO NEGRO'],
            ['id' => 66, 'key' => 'SA', 'name' => 'SALTA'],
            ['id' => 70, 'key' => 'SJ', 'name' => 'SAN JUAN'],
            ['id' => 74, 'key' => 'SL', 'name' => 'SAN LUIS'],
            ['id' => 78, 'key' => 'SC', 'name' => 'SANTA CRUZ'],
            ['id' => 82, 'key' => 'SF', 'name' => 'SANTA FE'],
            ['id' => 86, 'key' => 'SE', 'name' => 'SANTIAGO DEL ESTERO'],
            ['id' => 90, 'key' => 'TM', 'name' => 'TUCUMÁN'],
            ['id' => 94, 'key' => 'TF', 'name' => 'TIERRA DEL FUEGO'],
        ];
        foreach($provinces as $province) {
            $province['name'] = mb_convert_case($province['name'], MB_CASE_TITLE, "UTF-8");
            $province['name'] = ($province['id'] == 2)? "Ciudad Autónoma de Buenos Aires" : $province['name'];

            App\Models\Province::create($province);
        }
    }
}
