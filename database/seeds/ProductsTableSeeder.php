<?php

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate the table.
        DB::table('products')->truncate();

        $products = $this->fillData();

        foreach($products as $product) {
            Product::create($product);
        }

        $this->extraSeed();
    }

    private function fillData()
    {
        $products = [
            // ['shop_id'=>2, 'category_id'=>'value','name'=>'value', 'price'=>'value', 'code'=>'value', 'description'=>'value', 'active'=>true],
            ['shop_id'=>1, 'category_id'=>1  ,'mp_category_id'=>13 , 'name'=>'Zapatilla Barricade club W', 'price'=> 1200, 'code'=>'00045', 'description'=>'Incluyen la tecnología ADIPRENE®, que aporta mayor estabilidad en la mediasuela y una resistente suela ADIWEAR® 6 para tener impulso a lo largo de los cinco sets.', 'active'=>true],
            ['shop_id'=>1, 'category_id'=>1  ,'mp_category_id'=>13 , 'name'=>'Zapatilla Adizero attack W', 'price'=> 1399, 'code'=>'00074', 'description'=>'Controlá tu servicio y tus devoluciones para determinar el rumbo del partido. Estas ligeras y veloces zapatillas de tenis están diseñadas para que domines la cancha.Incorporan un exterior en material sintético microperforado para mayor transpirabilidad y una resistente suela ADIWEAR™ para soportar el uso en superficies abrasivas y brindar un agarre superior en todas las direcciones.', 'active'=>true],
            ['shop_id'=>1, 'category_id'=>4  ,'mp_category_id'=>13 , 'name'=>'Remera levis standard graphic crew', 'price'=>600, 'code'=>'00045', 'description'=>'', 'active'=>true],
            ['shop_id'=>4, 'category_id'=>4  ,'mp_category_id'=>13 , 'name'=>'Reloj Tommy Hilfiger Liverpool', 'price'=>5000, 'code'=>'00045', 'description'=>'', 'active'=>true],
            ['shop_id'=>3, 'category_id'=>3  ,'mp_category_id'=>17 , 'name'=>'Teléfono Iphone 7', 'price'=>20000, 'code'=>'00078', 'description'=>'', 'active'=>true],
            ['shop_id'=>1, 'category_id'=>2  ,'mp_category_id'=>8  , 'name'=>"TV Led 43' Admiral", 'price'=>6999, 'code'=>'00087', 'description'=>'Accedé a la Televisión Digital Abierta (TDA) Gracias a su sintonizador digital incorporado, vas a acceder a esta plataforma de canales en resolución HD de manera gratuita. Paka Paka, Encuentro, C5N y la TV Pública son algunas de las señales del paquete de distribución nacional que vas a poder disfrutar con este servicio.', 'active'=>true],
        ];
        return $products;
    }

    private function extraSeed()
    {
        for($i=0; $i<50 ; $i++){
            Product::create(
                ['shop_id'=>1, 
                'category_id'=> 1 ,
                'mp_category_id'=> random_int ( 1 , 23 ) ,
                'name'=>'Zapatilla Seeds '. $i, 
                'price'=> random_int ( 1000 , 2000 ), 
                'code'=>'00' . random_int(100, 998), 
                'description'=>'Incluyen la tecnología ADIPRENE®, que aporta mayor estabilidad en la mediasuela y una resistente suela ADIWEAR® 6 para tener impulso a lo largo de los cinco sets.', 
                'active'=>true]
                );
        } 
    }
}
