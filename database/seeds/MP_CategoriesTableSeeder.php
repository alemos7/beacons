<?php

use Illuminate\Database\Seeder;
use App\Models\MP_Category;

class MP_CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate the table.
        // DB::table('mp_categories')->truncate();

        
        $mp_categories = $this->fillData();
        $current_mp_categories = MP_Category::all();

        if( count($current_mp_categories) > 0 ){
          $i=0;
          foreach ($current_mp_categories as $category) {
            $category->trans = $mp_categories[$i]['trans'];
            $category->save();
            $i++;
          }
        }else{
          foreach($mp_categories as $category) {
              MP_Category::create( $category );
          }
        }
    }

    private function fillData()
    {
        $mp_categories = [
			[ "name" => "art",          	'trans' => 'Arte'                     ,"description" => "Collectibles & Art"],
			[ "name" => "baby",           'trans' => 'Bebes'                    ,"description" => "Toys for Baby, Stroller, Stroller Accessories, Car Safety Seats"],
			[ "name" => "coupons",        'trans' => 'Cupones'                  ,"description" => "Coupons"],
			[ "name" => "donations",      'trans' => 'Donaciones'               ,"description" => "Donations"],
			[ "name" => "computing",      'trans' => 'Computadoras'             ,"description" => "Computers & Tablets"],
			[ "name" => "cameras",        'trans' => 'Camaras'                  ,"description" => "Cameras & Photography"],
			[ "name" => "video_games",    'trans' => 'Vídeo Juegos'             ,"description" => "Video Games & Consoles"],
			[ "name" => "television",     'trans' => 'Televisión'               ,"description" => "LCD, LED, Smart TV, Plasmas, TVs"],
			[ "name" => "car_electronics",'trans' => 'Electronica de vehiculos' ,"description" => "Car Audio, Car Alarm Systems & Security, Car DVRs, Car Video Players, Car PC"],
			[ "name" => "electronics",    'trans' => 'Electronica'              ,"description" => "Audio & Surveillance, Video & GPS, Others"],
			[ "name" => "car",            'trans' => 'Automóvil'                ,"description" => "Parts & Accessories"],
			[ "name" => "entertainment",  'trans' => 'Entretenimiento'          ,"description" => "Music, Movies & Series, Books, Magazines & Comics, Board Games & Toys"],
			[ "name" => "fashion",        'trans' => 'Moda'                     ,"description" => "Men's, Women's, Kids & baby, Handbags & Accessories, Health & Beauty, Shoes, Jewelry & Watches"],
			[ "name" => "games",          'trans' => 'Juegos'                   ,"description" => "Online Games & Credits"],
			[ "name" => "home",           'trans' => 'Casa'                     ,"description" => "Home appliances. Home & Garden"],
			[ "name" => "musical",        'trans' => 'Musica'                   ,"description" => "Instruments & Gear"],
			[ "name" => "phones",         'trans' => 'Telefono'                 ,"description" => "Cell Phones & Accessories"],
			[ "name" => "services",       'trans' => 'Servicios'                ,"description" => "General services"],
			[ "name" => "learnings",      'trans' => 'Aprendisaje'              ,"description" => "Trainings, Conferences, Workshops"],
			[ "name" => "tickets",        'trans' => 'Tickets'                  ,"description" => "Tickets for Concerts, Sports, Arts, Theater, Family, Excursions tickets, Events & more"],
			[ "name" => "travels",        'trans' => 'Viajes'                   ,"description" => "Plane tickets, Hotel vouchers, Travel vouchers"],
			[ "name" => "virtual_goods",  'trans' => 'Digital'                         ,"description" => "E-books, Music Files, Software, Digital Images,  PDF Files and any item which can be electronically stored in a file, Mobile Recharge, DTH Recharge and any Online Recharge"],
			[ "name" => "others",         'trans' => 'Otros'                    ,"description" => "Other categories"],
        ];
        return $mp_categories;
    }
}
