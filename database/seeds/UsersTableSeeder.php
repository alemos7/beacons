<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate the table.
        DB::table('products')->truncate();
        
        $this->createAdminUser();
    	$this->createOperatorUser();
	    factory(User::class, 50)->create();
    }

    private function createAdminUser(){
        Artisan::call("make:admin");
    }

    private function createOperatorUser(){
        Artisan::call("make:operator");
    }
}
