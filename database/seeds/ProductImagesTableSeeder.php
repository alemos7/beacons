<?php

use Illuminate\Database\Seeder;
use App\Models\ProductImage;

class ProductImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate the table.
        DB::table('product_images')->truncate();

        $this->resetImageFolder();
        
        $productImages = $this->fillData();

        foreach($productImages as $productImage) {
            ProductImage::create($productImage);
        }

        $this->extraSeed();
    }

    private function fillData()
    {
        $path = 'storage/' . env('STORAGE_PRODUCT_IMAGES'). '/';

        $productImage = [
        	[ 'product_id'=> 1, 'image' => $path . '1_1.jpg', 'order' => 1],
        	[ 'product_id'=> 1, 'image' => $path . '1_2.jpg', 'order' => 2],
        	[ 'product_id'=> 1, 'image' => $path . '1_3.jpg', 'order' => 3],
        	[ 'product_id'=> 1, 'image' => $path . '1_4.jpg', 'order' => 4],

        	[ 'product_id'=> 2, 'image' => $path . '2_5.jpg', 'order' => 1],
        	[ 'product_id'=> 2, 'image' => $path . '2_6.jpg', 'order' => 2],
        	[ 'product_id'=> 2, 'image' => $path . '2_7.jpg', 'order' => 3],

        	[ 'product_id'=> 3, 'image' => $path . '3_8.jpg', 'order' =>  1],
        	[ 'product_id'=> 3, 'image' => $path . '3_9.jpg', 'order' =>  2],
        	[ 'product_id'=> 3, 'image' => $path . '3_10.jpg', 'order' => 3],
        
        	[ 'product_id'=> 4, 'image' => $path . '4_11.jpg', 'order' => 1],

        	[ 'product_id'=> 5, 'image' => $path . '5_12.jpg', 'order' => 3],
        	[ 'product_id'=> 5, 'image' => $path . '5_13.jpg', 'order' => 4],
        	
        	[ 'product_id'=> 6, 'image' => $path . '6_14.jpg', 'order' => 1],
        ];
        return $productImage;
    }

    private function resetImageFolder()
    {
    	$directory = storage_path('app/public/'. env('STORAGE_PRODUCT_IMAGES'));
		File::deleteDirectory($directory);
		File::copyDirectory('database/seeds/imagesSeeder/images-product', $directory);
    }

    private function extraSeed()
    {
        $path = 'storage/' . env('STORAGE_PRODUCT_IMAGES'). '/';

        for($i=7; $i<57 ; $i++){
            ProductImage::create(
                    [ 'product_id'=> $i, 'image' => $path . '6_14.jpg', 'order' => 1]
                );
        } 
    }
}
