<?php

use Illuminate\Database\Seeder;
use App\Models\Shop;

class ShopsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate the table.
        DB::table('shops')->truncate();

    	$shops = $this->fillData();

        foreach($shops as $shop) {
            Shop::create($shop);
        }
    }

    private function fillData()
    {
        $path = 'storage/' .env('STORAGE_SHOP_IMAGES') . '/';
    	$shops = [
    		['cuit' => '30594986161', 'name' => 'Adidas Store','business_name' => 'Adidas Store','iibb'=>10, 'description' => '', 'phone' => '1554747474', 'lat' => '-34.6161593', 'lng' => '-58.465766', 'address' => 'Gavilán 1207', 'email' => 'adidas.admin@test.com', 'iva' => 'Responsable Inscripto', 'schedule' => 'Lunes a Sábados de 10:00 a 20:00hs', 'contact_management' => 'Gonzalo Perez', 'locality_id' => 1, 'province_id' => 2, 'logo'=> $path.'1_logo.jpg'],
    		['cuit' => '30545645646', 'name' => 'Musimundo','business_name' => 'Musimundo','iibb'=>10, 'description' => '', 'phone' => '154789564', 'lat' => '-34.6161593', 'lng' => '-58.465766', 'address' => 'Gavilán 1207', 'email' => 'musimundo.admin@test.com', 'iva' => 'Responsable Inscripto', 'schedule' => 'Lunes a Sábados de 10:00 a 20:00hs', 'contact_management' => 'Jorge estevez', 'locality_id' => 1, 'province_id' => 2, 'logo'=> $path.'2_logo.jpg'],
    		['cuit' => '35465464546', 'name' => 'Apple Store','business_name' => 'Apple','iibb'=>10, 'description' => '', 'phone' => '1556895132', 'lat' => '-34.638261', 'lng' => '-58.504217', 'address' => 'Av. Rivadavia 10000', 'email' => 'apple.admin@test.com', 'iva' => 'Responsable Inscripto', 'schedule' => 'Lunes a Sábados de 10:00 a 20:00hs', 'contact_management' => 'José Rodriguez', 'locality_id' => 1, 'province_id' => 2, 'logo'=> $path.'3_logo.jpg'],
    		['cuit' => '32545689464', 'name' => 'Tommy Hilfiger','business_name' => 'Tommy Hilfiger','iibb'=>10, 'description' => '', 'phone' => '154564846', 'lat' => '-34.6148723', 'lng' => '-58.4617172', 'address' => 'Av. Gaona 2500', 'email' => 'tommy.admin@test.com', 'iva' => 'Responsable Inscripto', 'schedule' => 'Lunes a Sábados de 10:00 a 20:00hs', 'contact_management' => 'Esteban Morientes', 'locality_id' => 1, 'province_id' => 2, 'logo'=> $path.'4_logo.jpg'],
    	];
    	return $shops;
    }
}
