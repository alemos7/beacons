<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        $this->call(ProvinceTableSeeder::class);
        $this->call(LocalityTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ShopsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(MP_CategoriesTableSeeder::class);        
        $this->call(ProductsTableSeeder::class);
        // $this->call(ShopBeaconsTableSeeder::class);
        // $this->call(ProductAdvertisingSeeder::class);
        $this->call(WordTableSeeder::class);        
        $this->call(ProductImagesTableSeeder::class);
        $this->call(ShopImagesTableSeeder::class);
        $this->call(ProductOffersTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}