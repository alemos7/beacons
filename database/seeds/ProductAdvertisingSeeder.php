<?php

use Illuminate\Database\Seeder;
use App\Models\ProductAdvertising;

class ProductAdvertisingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    factory(ProductAdvertising::class, 50)->create();
    }
}
