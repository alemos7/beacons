<?php

use Illuminate\Database\Seeder;
use App\Models\ShopBeacon;

class ShopBeaconsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    factory(ShopBeacon::class, 50)->create();
    }
}
