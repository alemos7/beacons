<?php

use Illuminate\Database\Seeder;
use App\Models\Word;

class WordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(Word::class, 50)->create();
    	for( $i=1 ; $i < 50 ; $i++){
    		Word::find($i)->products()->attach( rand ( 1, 50 ));
    	}
    }
}