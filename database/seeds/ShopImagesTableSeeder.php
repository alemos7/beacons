<?php

use Illuminate\Database\Seeder;
use App\Models\ShopImage;

class ShopImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate the table.
        DB::table('shop_images')->truncate();

        $this->resetImageFolder();
        
        $shopImages = $this->fillData();

        foreach($shopImages as $shopImage) {
            ShopImage::create($shopImage);
        }
    }

    private function fillData()
    {
        $path = 'storage/' . env('STORAGE_SHOP_IMAGES'). '/';
        $shopImage = [
        	[ 'shop_id'=> 1, 'image' => $path . '1_1.jpg', 'order' => 1],
        	[ 'shop_id'=> 2, 'image' => $path . '2_2.jpg', 'order' => 1],
        	[ 'shop_id'=> 3, 'image' => $path . '3_3.jpg', 'order' => 1],
        	[ 'shop_id'=> 4, 'image' => $path . '4_4.jpg', 'order' => 1],
        ];
        return $shopImage;
    }

    private function resetImageFolder()
    {
    	$directory = storage_path('app/public/'. env('STORAGE_SHOP_IMAGES'));
		File::deleteDirectory($directory);
		File::copyDirectory('database/seeds/imagesSeeder/images-shop', $directory);
    }
}
