<?php

use Illuminate\Database\Seeder;
use App\Models\ProductOffer;

class ProductOffersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Truncate the table.
        DB::table('product_offers')->truncate();

        $offers = $this->fillData();

        foreach($offers as $offer) {
	        ProductOffer::create($offer);
        }

        $this->extraSeed();
    }

    private function fillData()
    {
        $offers = [
	        ['algorithm' => 'porcentual' , 'name' => '', 'vars' => '{"descuento":"10"}','product_offer_id'=> 1 ,'before_offer_price'=> 1333.33],
	        ['algorithm' => 'masPorMenos', 'name' => '', 'vars' => '{"lleve":"2","pague":"1"}','product_offer_id'=> 2,'before_offer_price'=> 2798.00],
	        ['algorithm' => 'porcentual' , 'name' => '', 'vars' => '{"descuento":"50"}','product_offer_id'=> 3,'before_offer_price'=> 1200.00],
	        ['algorithm' => 'masPorMenos', 'name' => '', 'vars' => '{"lleve":"3","pague":"2"}','product_offer_id'=> 4,'before_offer_price'=> 7500.00],
	        ['algorithm' => 'porcentual' , 'name' => '', 'vars' => '{"descuento":"80"}','product_offer_id'=> 5,'before_offer_price'=> 100000.00],
	        ['algorithm' => 'porcentual' , 'name' => '', 'vars' => '{"descuento":"20"}','product_offer_id'=> 6,'before_offer_price'=> 8748.75],
        ];
        return $offers;
    }

    private function extraSeed()
    {
        for($i=7; $i<57 ; $i++){
            ProductOffer::create(
                    ['algorithm' => 'porcentual' , 'name' => '', 'vars' => '{"descuento":"20"}','product_offer_id'=> $i,'before_offer_price'=> 8748.75]
                );
        } 
    }

}
