<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    // All password is -> abc789
    static $password = '$10$jmpwHjashjFlWA6UOvGA.er3Cq9hGQti9NFYAQ55eYHvyj.3cFA.2';

    return [
        'type' => $faker->randomElement(['user','operator','administrator']),
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'birthday' => $faker->year(),
        'gender' => $faker->randomElement(['Masculino', 'Femenino','Otro']),
        'from' => $faker->randomElement(['Manual','Facebook','Gmail','Email']),
        'email' => $faker->unique()->safeEmail,
        'password' => $password ,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Shop::class, function (Faker\Generator $faker) {
    return [
        'cuit' => $faker->unique()->bothify("###########"),
        'name' => $faker->unique()->word, 
        'business_name' => $faker->unique()->word, 
        'description' => $faker->sentence,
        'phone' => $faker->phoneNumber,
        'lat' => $faker->latitude(),
        'lng' => $faker->longitude(),
        'address' => $faker->address,
        'email' => $faker->email,
        'vip' => $faker->randomElement([true,false]),
        'iva' => $faker->randomElement([
                                        'Responsable Inscripto', 
                                        'Responsable No Inscripto', 
                                        'Responsable Monotributista', 
                                        'Exento', 
                                        'Consumidor Final', 
                                        'Otros'
                                    ]),
        'iibb' => $faker->randomFloat(2,0,8),
        'schedule' => $faker->sentence,
        'observation' => $faker->sentence,
        'contact_management' => $faker->phoneNumber,
        'locality_id' => '1',
        'province_id' => '2',
    ];
});

$factory->define(App\Models\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word ,
        'description' => $faker->sentence,
        'logo' => 'asterisk',
    ];
});

$factory->define(App\Models\Product::class, function (Faker\Generator $faker) {
    return [
        'shop_id' => $faker->numberBetween(1,50),
        'category_id' => $faker->numberBetween(1,50),
        'name' => $faker->word,
        'price' => $faker->randomFloat(2,1,8),
        'code' => $faker->unique()->bankAccountNumber,
        'color' =>  substr($faker->hexcolor(),1),
        'description' => $faker->paragraph,
        'active' => true,
    ];
});

$factory->define(App\Models\ShopBeacon::class, function (Faker\Generator $faker) {
    return [
        'shop_id' => $faker->numberBetween(1,50),
        'product_id' => $faker->numberBetween(1,50),
        'serial' => $faker->unique()->bankAccountNumber,
        'distance' => '',
        'description' => $faker->paragraph,
        'active' => $faker->randomElement([true,false]),
    ];
});


$factory->define(App\Models\ProductAdvertising::class, function (Faker\Generator $faker) {
    return [
        'product_id' => $faker->numberBetween(1,50),
        'content' => $faker->paragraph,
        'date_send' => $faker->date,
        'active' => $faker->randomElement([true,false]),
    ];
});

$factory->define(App\Models\Word::class, function (Faker\Generator $faker) {
    return [
        'word' => $faker->word,
    ];
});
