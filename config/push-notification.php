<?php

return array(
    'android' => array(
        'environment' => 'development',
        'apiKey'      => env('GCM_API_KEY'),
        'service'     => 'gcm'
    ),
    'ios' => array(
        'environment' => 'production',
        'certificate' => __DIR__ . '/BS.pem',
        'passPhrase'  => 'blueshop2016',
        'service'     => 'apns'
    )
);
