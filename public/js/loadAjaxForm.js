loadAjaxForm = function(id){ 
    var bar = $('.bar');
    var percent = $('.percent');
    var status = $('#status'); 

    $("form#"+id).ajaxForm({
        beforeSend: function() {
            status.empty();
            var percentVal = '0%';
            bar.width(percentVal);
            percent.html(percentVal); 
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal);
            percent.html(percentVal); 
            // input = $("#image");
            // input.replaceWith(input.val('').clone(true));
        },
        complete: function(xhr) {
            status.html(xhr.responseText);
            $('#image').val('');
            bar.fadeOut('slow',function(){
                bar.width(0);
                bar.fadeIn();
                percent.html('0%'); 
                $('.tab-content').first().prepend('<div class="alert alert-success"><span class="icon mdi mdi-check"></span>        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Las imagenes se han agregado correctamente</div>');
            });
            // location.reload();
        },
        error: function(){ 
        }
    });
}