$('#messages-datatable').DataTable({    
    "language": {
        "decimal": ",",
        "thousands": "."
    },
    "lengthChange": false,
    "order": [[ 0, "desc" ]],
});


// Simple Search
$("#filter-search").on('keyup', function(){
    $('#messages-datatable').DataTable().search( $(this).val() ).draw();
});
/////////////////
// Cantidad de Resultados
$("#filter-length").on('change', function(){
    $('#messages-datatable').DataTable().page.len( $(this).val() ).draw();
});
/////////////////

// Filtro por Estado de Envio
$('#filterBySend').on( 'change', function (){
    $('#messages-datatable').DataTable().draw();
});

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var status = $('#filterBySend').val();
        if(status=='Todos'){
            return true;
        }
        return (status == data[3]);
    });
///////////////


$("#messages-datatable_filter").hide();

$("body").on("change", ".set-message-user-filter", function(){
    
    var name = $(this).data("name");
    var value = $(this).val();
            
    $("input[name='" + name + "']").val(value);
    
});

$("body").on("click", ".send-message",function(){
    $(this).closest("form").submit();
});