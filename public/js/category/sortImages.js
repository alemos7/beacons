$(document).ready(function(){
    $(".images-grid").disableSelection();
});

sortImages = function(){
    var CSRF_TOKEN = $('input[name=_token]').val();
    var images = $('.images-id').serializeArray();
    var categoryId = $('#categoryId').val();

    $.ajax({
        type: 'POST',
        data: { 
            images:images,
            _token: CSRF_TOKEN
        },
        url: '../categories/sort-images/',
        success: function (data) {
            $('.tab-content').first().prepend('<div class="alert alert-success"><span class="icon mdi mdi-check"></span>        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Las imagenes se han ordenado correctamente</div>');
        },
        error: function(data){
        }
    }); 
}