/* Code based on Google Map APIv3 Tutorials */


var gmapdata;
var gmapmarker;
var infoWindow;

var def_zoomval = 14;
var def_longval = -58.381691;
var def_latval = -34.604389;
var mapKey = 'AIzaSyBXl5YUF9ix8fRcMrPTQi5bDniKDs9K7d4';

function if_gmap_init()
{ 

	if(document.getElementById("shop_lat").value){
		def_latval = document.getElementById("shop_lat").value;
		def_longval = document.getElementById("shop_long").value;
	}else{
		var is_new = true;
	}
	
	var curpoint = new google.maps.LatLng(def_latval,def_longval);

	gmapdata = new google.maps.Map(document.getElementById("google-map"), {
		center: curpoint,
		zoom: def_zoomval,
		mapTypeId: 'roadmap'
	});

	gmapmarker = new google.maps.Marker({
		map: gmapdata,
		position:(is_new)? new google.maps.LatLng(1,1) : curpoint
	});

	infoWindow = new google.maps.InfoWindow;
	google.maps.event.addListener(gmapdata, 'click', function(event) {
		lat = event.latLng.lat().toFixed(6);
		lng = event.latLng.lng().toFixed(6);

		document.getElementById("shop_lat").value = lat;
		document.getElementById("shop_long").value = lng;
		// document.getElementById("shop_formated").value = getFormatted_address(lat ,lng);

		gmapmarker.setPosition(event.latLng);
		if_gmap_updateInfoWindow();

	});

	google.maps.event.addListener(gmapmarker, 'click', function() {
		if_gmap_updateInfoWindow();
		infoWindow.open(gmapdata, gmapmarker);
	});

	return false;
} // end of if_gmap_init


function if_gmap_loadpicker()
{
	var latval = document.getElementById("shop_lat").value;
	var longval = document.getElementById("shop_long").value;

	if (longval.length > 0) {
		if (isNaN(parseFloat(longval)) == true) {
			longval = def_longval;
		} // end of if
	} else {
		longval = def_longval;
	} // end of if

	if (latval.length > 0) {
		if (isNaN(parseFloat(latval)) == true) {
			latval = def_latval;
		} // end of if
	} else {
		latval = def_latval;
	} // end of if

	var curpoint = new google.maps.LatLng(latval,longval);

	gmapmarker.setPosition(curpoint);
	gmapdata.setCenter(curpoint);
	//gmapdata.setZoom(zoomval);

	// document.getElementById("shop_formated").value = getFormatted_address(latval ,longval);
	if_gmap_updateInfoWindow();
	return false;
} // end of if_gmap_loadpicker



function if_gmap_updateInfoWindow()
{	    
	$('#shop_lat').parsley().validate();
	infoWindow.setContent("Longitude: "+ gmapmarker.getPosition().lng().toFixed(6)+"<br>"+"Latitude: "+ gmapmarker.getPosition().lat().toFixed(6));
} // end of if_gmap_bindInfoWindow

function resetLocation(){
	document.getElementById("shop_lat").value = def_latval;
	document.getElementById("shop_long").value = def_longval; 
	if_gmap_loadpicker();
}

function getLocationByCity() {

	var xhr = new XMLHttpRequest();
	// var busqueda =	document.getElementById("first").options[document.getElementById("first").selectedIndex].text;
	var busqueda = document.getElementById("shop_address").value;
	busqueda = busqueda.split(' ').join('+');
	//console.log(busqueda); 
	xhr.open("GET", "https://maps.googleapis.com/maps/api/geocode/json?address="+ busqueda +"&components=country:AR&key="+mapKey, false);
	xhr.send();
	var res  = JSON.parse(xhr.response);
	res = res.results[0];
	//console.log(res);
	//console.log(res.geometry.location.lat);
	//console.log(res.geometry.location.lng);
	document.getElementById("shop_lat").value = res.geometry.location.lat;
	document.getElementById("shop_long").value = res.geometry.location.lng;
	// document.getElementById("shop_formated").value = res.formatted_address;
	if_gmap_loadpicker();
	//console.log(xhr.response.geometry.location);
}

function getFormatted_address(lat,lng) {
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "https://maps.googleapis.com/maps/api/geocode/json?latlng="+ lat +","+ lng +"&key="+mapKey, false);
	xhr.send();
	var res  = JSON.parse(xhr.response);
	res = res.results[0];
	return res.formatted_address;
}

window.onload = function () {
	if_gmap_init();
	// resetLocation();
}