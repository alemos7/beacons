
$(window).bind("load", function() {
  deleteImagesAjax('image-delete','shop');
  $('#shop_lat').parsley().validate();
  
  $('#form-shop button').click(function() {
    if( $('#shop_lat').val() == '' ){
      $("#full-warning").niftyModal();
      event.preventDefault();
    }
  });

  $("#shop-active, #mp_client_secret, #mp_id").on('change',function(){
      $(this).parsley().validate();
      $("#shop-active").parsley().validate();
  });

    window.Parsley
    .addValidator('status', {
      validateMultiple: function() {
        let secret = $('#mp_client_secret').val();
        let id = $('#mp_id').val();
        let status = $("#shop-active");
        if( (secret == '' || id == '')  && status.is(':checked') ){
          return false;
        }
        return true;
      },
      messages: {
        es: 'Para que su negocio esté activo tiene que validar los dos campos CLIENT_ID y CLIENT_SECRET de Mercado Pago. Por favor, revise el formulario y vuelva a intentarlo.'
      }
    });
  
});


$('#select-provider').multiSelect({
  selectableHeader: "<div class='select-header'>Usuarios</div><input type='text' class='form-control input-xs' autocomplete='off' placeholder='Buscar...'>",
  selectionHeader: "<div class='select-header'>Operadores</div><input type='text' class='form-control input-xs' autocomplete='off'  placeholder='Buscar...'>",
  afterInit: function(ms){
    var that = this,
    $selectableSearch = that.$selectableUl.prev(),
    $selectionSearch = that.$selectionUl.prev(),
    selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
    selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
    .on('keydown', function(e){
      if (e.which === 40){
        that.$selectableUl.focus();
        return false;
      }
    });

    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
    .on('keydown', function(e){
      if (e.which == 40){
        that.$selectionUl.focus();
        return false;
      }
    });
  },
  afterSelect: function(){
    this.qs1.cache();
    this.qs2.cache();
  },
  afterDeselect: function(){
    this.qs1.cache();
    this.qs2.cache();
  }
});


// CUIT Format
$(document).ready(function(){
    $('#cuit').on('change , keydown , keyup , keypress',function(e) {
        $(this).val( this.value.replace(/\D/gi,'') ); //Borramos guiones
    });
  /*
  $('#cuit').on('change , keydown , keyup , keypress',function(e){
    if(e.keyCode == 8 || e.keyCode == 46 || (e.keyCode >= 37 && e.keyCode <= 40) ){
      $('[name="cuit"]').val( this.value.replace(/-/gi,'') );
      return;
    }
    if( !((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) ){
      e.preventDefault();
    }
    this.value = cuitFormat(this.value);
    $('[name="cuit"]').val( this.value.replace(/-/gi,'') );
  });

  $('#cuit').trigger('change');

  function cuitFormat(n){
    let formated = n;

    formated = formated.replace(/\s/g,'');
      
      
    if( formated.substring(2,3) != '-' && formated.length > 1){
      formated = formated.substring(0,2) + '-' + formated.substring(2);
    }     

    if( formated.substring(11,12) != '-' && formated.length > 10){
      formated = formated.substring(0,11) + '-' + formated.substring(11);
    } 

    return formated;
  }
  */

});