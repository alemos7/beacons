$(document).ready(function(){
    $(".images-grid").disableSelection();
});

sortImages = function(){
    var CSRF_TOKEN = $('input[name=_token]').val();
    var images = $('.images-id').serializeArray();
    var shopId = $('#shopId').val();

    $.ajax({
        type: 'POST',
        data: { 
            images:images,
            _token: CSRF_TOKEN
        },
        url: '../shops/sort-images/',
        success: function (data) {
            $('.tab-content').first().prepend('<div class="alert alert-success"><span class="icon mdi mdi-check"></span>        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Las imagenes se han ordenado correctamente</div>');
        },
        error: function(data){
        }
    }); 
}

changeImageSuccess = function () {
    $('.tab-content').first().prepend('<div class="alert alert-success"><span class="icon mdi mdi-check"></span><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Se han guardado los cambios</div>');
    document.body.scrollTop = 0; // For Chrome, Safari and Opera
    document.documentElement.scrollTop = 0; // For IE and Firefox
}