
if($('#use_mp').val() == 0){
    $('#mercado_pago_fields').hide();
}else{
    $('#mercado_pago_fields').show();
}

$('#submit_btn').on('click', function(event){
    event.preventDefault();
    if($('#shop-active').is(':checked')) {
        $.get('/shops/check-images/' + $('#shop-id').val(), function (response) {

            if (response.error) {
                $("#full-danger-shop").niftyModal("show");
                $('.shop-active').removeAttr('checked');
                return false;

            } else if (response.success) {
                $('#form-shop').submit();

            } else {
                alert('ah ocurrido un error inesperado. contacte con el administrador');
                return false;

            }
        }, 'json');
    }else{
        $('#form-shop').submit();
    }
});

$('.hover-help').hover(
    function() {
        $(this).closest('.form-group').find('.place-image').fadeIn(50);
    },function() {
        $(this).closest('.form-group').find('.place-image').fadeOut(150);
    }
);


$('#use_mp').on('change', function(){
    if($(this).val() == 0){
        $('#mercado_pago_fields').hide();
        $('#mp_id').prop('required', false);
        $('#mp_client_secret').prop('required', false);
        $('#mp_key').prop('required', false);
    }else{
        $('#mercado_pago_fields').show();
        $('#mp_id').attr('required', 'true');
        $('#mp_client_secret').attr('required', 'true');
        $('#mp_key').attr('required', 'true');
    }
});