
$(window).bind("load", function() { 
  $('#province_id').trigger('change');
  setTimeout(function(){
    provinceId = $('#selected_locality_id').val();
    $('#locality_id').val( provinceId );
  }, 1000); 
});
$('#province_id').change(function () { 
    var provinceId = $(this).val();
    if(provinceId=='' || provinceId=='Seleccionar' )
      return;
    $.ajax({
        type: 'get',
        url: '../province/localities/' + provinceId,
        success: function (data) {          
          var option = '';
          for (var i=0;i<data.length;i++){
             option += '<option value="'+ data[i]['id'] + '">' + data[i]['name'] + '</option>';
          }
          $('#locality_id').find('option').remove().end();
          $('#locality_id').append(option);
        }
    });

});