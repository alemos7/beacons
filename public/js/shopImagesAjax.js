uploadImagesAjax = function(id,model){ 
    var bar = $('.bar');
    var percent = $('.percent');
    var status = $('#status'); 

    $("form#"+id).ajaxForm({
        beforeSend: function() {
            status.empty();
            var percentVal = '0%';
            bar.width(percentVal);
            percent.html(percentVal); 
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal);
            percent.html(percentVal); 
        },
        complete: function(xhr) {
            // status.html(xhr.responseText);
            $('#image').wrap('<form>').closest('form').get(0).reset();
            $('#image').unwrap();
            $('#image').val('');
            bar.fadeOut('slow',function(){
                bar.width(0);
                bar.fadeIn();
                percent.html('0%'); 
                $('.tab-content').first().prepend('<div class="alert alert-success"><span class="icon mdi mdi-check"></span>        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Las imagenes se han agregado correctamente</div>');
            });
            reloadImageList(model);
        },
        error: function(){ 
        }
    });
}

deleteImagesAjax = function(cssClass,model){
    $("form."+cssClass).ajaxForm({
        complete: function() {
            $('.tab-content').first().prepend('<div class="alert alert-success"><span class="icon mdi mdi-check"></span>        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>La imagen se ha borrado correctamente</div>');
            reloadImageList(model);
        },
        error: function(){ 
        }
    });
}

reloadImageList = function(model){
   $.ajax({
        type: 'GET',
        url: '../' + model + '-images/' + model + '/' + $('#' + model + '-id').val(),
        success: function(imageList) {
            console.log(imageList);
            $('#shopimage-list').html(imageList);
            deleteImagesAjax('image-delete');
        }
    });
}