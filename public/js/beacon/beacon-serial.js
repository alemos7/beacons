$( document ).ready(function() {
    select = $('#shop_id').val();
    window.Parsley.addValidator('beaconSerial', {
        validateNumber: function (value) {
            let mayor = $('#beacon-serial-mayor');
            let minor = $('#beacon-serial-minor');
            let condition = ( parseInt(mayor.val()) > parseInt(minor.val()) );
            return condition;
        },
        messages: {
            es: 'Los cuatro primero digitos deben ser mayor a los otros cuatro digitos',
        }
    });

    $('#beacon-serial-mayor,#beacon-serial-minor').on('keydown keyup keypress change', function () {
        $('#serial').val($('#beacon-serial-mayor').val() + '/' + $('#beacon-serial-minor').val());
    });

    $('#shop_id').on('change', function(){
        let original = $('#original_shop_id');
        if(original.length && $(this).val() !== original.val()){
            $("#full-warning-shop-change").niftyModal("show");
        }
    });


    beaconLoad();

    function beaconLoad() {

        let serial = document.getElementById('serial');
        if (serial.value.split('/').length > 1) {
            document.getElementById('beacon-serial-mayor').value = serial.value.split('/')[0];
            document.getElementById('beacon-serial-minor').value = serial.value.split('/')[1];
        }
    }
});