$('#password, #password2').on('change', function(){
    var pw1 = $("#password");
    var pw2 = $("#password2");
    if( pw1.val() == pw2.val() && pw1.val() == ''){
        pw1.removeAttr('required');
        pw2.removeAttr('required');
    }else{
    	pw1.prop('required',true);
    	pw2.prop('required',true); 
    	$(pw1).parsley().validate();
    	$(pw2).parsley().validate();
    }
});