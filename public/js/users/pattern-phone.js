$(document).ready(function(){

  $('[name=phone]').on('change , keydown , keyup , keypress',function(e){

    if(e.keyCode == 8){
      return;
    }
    if( !((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105)) ){
      e.preventDefault();
    }
    this.value = phoneFormat(this.value);
  });

function phoneFormat(n){
  let formated = n;

  formated = formated.replace(/\s/g,'');
    
  if( formated.substring(0,1) != '(' ){
    formated = '(' + formated;
  } 

    
  if( formated.substring(3,4) != ')' && formated.length > 2){
    formated = formated.substring(0,3) + ')' + formated.substring(3);
  } 
    
  if( formated.substring(4,5) != ' ' && formated.length > 2){
    formated = formated.substring(0,4) + ' ' + formated.substring(4);
  } 

    
  if( formated.substring(9,10) != '-' && formated.length > 8){
    formated = formated.substring(0,9) + '-' + formated.substring(9);
  } 

  return formated;
}

});