deleteImagesAjax = function(cssClass,model){
    $("form."+cssClass).ajaxForm({
        beforeSend: function(xhr, myForm) {
            $('[action="' + myForm.url +'"]').parent().fadeOut();
        },
        success: function() {
            Messager.success('La imagen se ha borrado correctamente');
            reloadImageList(model);
        },
        error: function(res){
            $('[action="' + myForm.url +'"]').parent().fadeIn();
            Messager.error('Error al eliminar la imagen');
        }
    });
};

reloadImageList = function(model){
     var id =  $('#' + model + '-id').val();
     if(id==undefined){
        id = $('input[name="_token"]').val();
    }
    $.ajax({
        type: 'GET',
        url: '/' + model + '-images/' + model + '/' + id,
        success: function(imageList) {
            $('#' + model + 'image-list').html(imageList);
            $('.category-images .image img').attr('src', $('img').attr('src') + '?' + Math.random());
            deleteImagesAjax('image-delete', model);
            btnImageListInit();
        },
        error: function(res){
            Messager.error('Error al mostrar imagenes');
        }
    });
};

btnImageListInit =  function(){
    $(".btn-left").on('click',function(){
        event.preventDefault();
        $(this).parent().after( $(this).parent().prev() );
    }); 
    $(".btn-right").on('click',function(){
        event.preventDefault();
        $(this).parent().before( $(this).parent().next() );
    });
};

btnImageListInit();

Dropzone.options.productimages = {
    url: "/product-images",
    paramName: "images", 
    dictDefaultMessage: "Arraste las imágenes o haga clic aquí para agregarlas al producto",
    sending: function(file, xhr, formData){
        token  = $('input[name="_token"]').val();
        productId  = $('input[name="product_id"]').val();
        formData.append('_token',token);
        formData.append('product_id',productId);
    },
    success: function(file, response){
        this.removeFile(file);
        reloadImageList('product');
    },
    error: function(file, response){
        this.removeFile(file);
        reloadImageList('product');
        Messager.error(response.error);
    },
};

Dropzone.options.categoryimages = {
    url: "/category-images",
    paramName: "images", 
    dictDefaultMessage: "Arraste las imágenes o haga clic aquí para agregarlas a la categoria",
    sending: function(file, xhr, formData){
        token  = $('input[name="_token"]').val();
        categoryId  = $('input[name="category_id"]').val();
        formData.append('_token',token);
        formData.append('category_id',categoryId);
    },
    success: function(file,response){
        this.removeFile(file);
        reloadImageList('category');
    }
};

Dropzone.options.shopimages = {
    url: "/shop-images",
    paramName: "images", 
    dictDefaultMessage: "Arraste las imágenes o haga clic aquí para agregarlas al negocio",
    sending: function(file, xhr, formData){
        token  = $('input[name="_token"]').val();
        shopId  = $('input[name="shop_id"]').val();
        formData.append('_token',token);
        formData.append('shop_id',shopId);
    },
    success: function(file,response){
        this.removeFile(file);
        reloadImageList('shop');
    },
    error: function(file, response){
        this.removeFile(file);
        reloadImageList('shop');
        Messager.error(response.error);
    },
};
