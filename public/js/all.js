$(document).ready(function() {
	if($("input[name='active']").is(":checked")) {
	    $("#active-text").text('Activo');
    }else{
	    $("#active-text").text('Inactivo');
	}
});

$("input[name='active']").change(function(){
	if($(this).is(":checked")) {
    	$("#active-text").text('Activo');
    }else{
    	$("#active-text").text('Inactivo');
	}
});

// Config Default
$.extend( true, $.fn.dataTable.defaults, {
	"bSort" : true,
    "order": [[ 0, "desc" ]],
    "iDisplayLength": 100,
	"language":
	{
		"sProcessing":     "Procesando...",
		"sLengthMenu":     "Mostrar _MENU_",
		"sZeroRecords":    "No se encontraron resultados",
		"sEmptyTable":     "Ningún dato disponible en esta tabla",
		"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
		"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
		"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
		"sInfoPostFix":    "",
		"sSearch":         "Buscar:",
		"sUrl":            "",
		"sInfoThousands":  ",",
		"sLoadingRecords": "Cargando...",
		"oPaginate": {
			"sFirst":    "Primero",
			"sLast":     "Último",
			"sNext":     "Siguiente",
			"sPrevious": "Anterior"
		},
		"oAria": {
			"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
		}
	},
	"columnDefs": [
	{ orderable: false, targets: -1 }
	],
});
  $(function() {
    $('.datetimepicker').datetimepicker({
      language: 'es',
      autoclose: true,
      componentIcon: '.mdi.mdi-calendar',
      navIcons:{
        rightIcon: 'mdi mdi-chevron-right',
        leftIcon: 'mdi mdi-chevron-left'
      }
    });
  });
$(document).ready(function() {

	setClassError();
	
	function setClassError(){
    	if(typeof errorsKey !== 'undefined'){
			i=0;    		
    		errorsKey.forEach(function(item){
	    		selector = '[name='+ item +']'; 
	    		$( selector ).addClass( 'error' );
	    		htmlError = '<ul class="parsley-errors-list filled"><li class="parsley-required">' + errorsMessage[i] + '</li></ul>';
	    		$( selector ).after( htmlError );
	    		i++;
    		});
    	}
	}
});

$(document).ready(function(){
	$(".modal").hide();
});
$(document).ready(function() {
	$('.icp-auto').iconpicker({
		// icons: [ 'fa-car','fa-suitcase','fa-laptop','fa-mobile-phone','fa-futbol-o','fa-phone','fa-gamepad','fa-cutlery'],
		templates: {
			search: '<input type="search" class="form-control iconpicker-search" placeholder="Buscar..." />',
		}
	});
	if($('.icp-auto').val()){
		$('.icp-auto').val( $('.icp-auto').val().substring(3) );
	}
	$('.icp-auto').on('iconpickerUpdated', function() {
		$(this).val( $(this).val().substring(3) );
	});
});
$(document).ready(function(){
	$('.local-dropzone').on('click', function(){
		$(this).next('input[type="file"]').click();
	});
	$('.local-dropzone').next('input[type="file"]').on('change', function(e){
		var filename =  URL.createObjectURL(event.target.files[0]);
		$(this).prev(".local-dropzone").find('#logo-preview').attr('src', filename );
		local_dropzone_checkfile($(this).prev(".local-dropzone"));
	});
  local_dropzone_checkfile = function(dropzone){
    if( dropzone.find("#logo-preview").attr('src') ){
			dropzone.find('#logo-preview').prev('label').css('visibility','hidden');
		}
	}
  $('.local-dropzone').each( function(indx){
    local_dropzone_checkfile( $(this) ); 
  });
});
function set_money_format(n){
  n = n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  return n.replace(/.([^.]*)$/,',$1');
}
$(window).on('load', function() {
	// Animate loader off screen
	$(".se-pre-con").delay(300).fadeOut("slow");
});
$('.left-sidebar-content a').on('click',function(){
	$(".se-pre-con").fadeIn();
});
$(document).ready(function() {
  $.fn.select2.defaults.set( "theme", "bootstrap" );
  $('.select2').select2();
  $('span.select2').removeAttr('style');
  
  // Coloca span.select2-container antes del select
  // Para evitar errores visuales con la libreria de validacion 'parsley'
  $('span.select2-container').each(function(){
    $(this).insertBefore( $(this).prev('select') );
  });

});
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
$(document).ready(function(){
	$('form').parsley({
		excluded: "input[type=button], input[type=submit], input[type=reset], input[type=hidden], input:hidden"
	});
	$('form button').click(function() {
		if(!$(this).parents('form:first').parsley().validate()){
      if(window.location.pathname != '/login'){
			 $("#full-warning").niftyModal();
      }
		}
	});
	
	$('form [required]').on('focusout', function() {
		if($(this).is(':visible')){
			$(this).parsley().validate();
		}
	});
});
//# sourceMappingURL=all.js.map

$(".nav-responsive").click(function() {
  $(".left-sidebar-spacer").toggleClass('open');
});

Messager = {};

Messager.success = function(message)
{
    $('<div class="alert alert-success"><span class="icon mdi mdi-check"></span>        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + message + '</div>')
		.prependTo('.tab-content:first')
		.delay(2000)
		.fadeOut('slow',function() { $(this).remove(); });
};

Messager.error = function(message)
{
    $('<div class="alert alert-danger"><span class="icon mdi mdi-close-circle-o"></span>        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' + message + '</div>')
        .prependTo('.tab-content:first')
        .delay(2000)
        .fadeOut('slow',function() { $(this).remove(); });
};

$(document).ready(function(){
    $('form[data-form=disable-on-submit]').submit(function(){
        $(this).find('button').attr('disabled','disabled');

        setTimeout(function(){
            $('form[data-form=disable-on-submit]').find('button').removeAttr('disabled');
        }, 5000);
    });
});
