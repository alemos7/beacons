$('#submit_btn').on('click', function(event){
    event.preventDefault();

    if($('#swt5').is(':checked')) {
        $.get('/products/check-images/' + $('#product-id').val(), function (response) {

            if (response.error) {
                $("#full-danger-shop").niftyModal("show");
                $('.swt5').removeAttr('checked');
                return false;

            } else if (response.success) {
                $('#form-product').submit();

            } else {
                alert('ah ocurrido un error inesperado. contacte con el administrador');
                return false;

            }
        }, 'json');
        
    }else{
        $('#form-product').submit();
    }
});