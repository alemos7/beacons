$(document).ready(function() {
    character_left = function(){
        $('#character-left').html( $('#description').attr('maxlength') - $('#description').val().length );
    }
    
    $('[name="description"]').on('change, keydown, keyup, keypress',function(){
        character_left();
    });
    $('[name="description"]').keydown(function(){
        character_left();
    });
    character_left();
});