<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/6/17
 * Time: 3:45 PM
 */

namespace App\Formatters\Api;


trait CategoryFormatter
{
    /**
     * @param array $unformattedItems
     * @return array
     */
    public static function apiFormatIndex($unformattedItems)
    {

        return array_map(function($category) {

          $item = [];

          if (!isset($category['images']) || empty($category['images'])) {
            $item['banner'] = null;
          } else {

            $image = array_pop($category['images']);

            if (!empty($image) && isset($image['image']) && !empty($image['image'])) {
              $item['banner'] = config('app.url') . '/' . $image['image'];
            }
          }

          return array_merge($item,
            array_except($category, [
              'childof',
              'created_at',
              'updated_at',
              'deleted_at',
              'images'
          ]));
        }, $unformattedItems);
    }

    /**
     * @param array $unformattedItem
     * @return array
     */
    public static function apiFormatShow($unformattedItem)
    {
        $unformattedItem = array_except($unformattedItem, ['childof', 'created_at', 'updated_at', 'deleted_at']);

        return $unformattedItem;
    }
}
