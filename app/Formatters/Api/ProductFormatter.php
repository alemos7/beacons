<?php

namespace App\Formatters\Api;

use App\Models\Califications;
use App\Models\Category;
use App\Models\Product;
use \App\Models\ProductImage;
use \App\Models\ShopImage;
use \App\Models\ProductOffer;
use Illuminate\Support\Facades\DB;

trait ProductFormatter
{
    /**
     * Formatea la respuesta en la acción index del controlador de productos.
     * @param array $unformattedArray
     * @return array
     */
    public static function apiFormatIndex($unformattedArray)
    {
      return array_map('self::apiFormatIndexItem', $unformattedArray);
    }

    public static function apiFormatIndexItem($productUnformated)
    {
        /**
         * Filtramos los campos de categoria.
         */
        $productUnformated['category'] = Category::apiFormatShow($productUnformated['category']);

        /**
         * Limitamos los campos del shop a mostrar.
         */
        $productUnformated['shop'] = ShopFormatter::apiFormatItem($productUnformated['shop']);
        $productUnformated['shop']['lat'] = floatval($productUnformated['shop']['lat']);
        $productUnformated['shop']['lng'] = floatval($productUnformated['shop']['lng']);
        $productUnformated['calification'] = Product::calculateCalification($productUnformated['califications']);
        $productUnformated['images'] = array_map('self::formatImages', $productUnformated['images']);

        $productUnformated = array_except($productUnformated, ['active', 'califications']);

          if(count($productUnformated['offer'])){

              usort($productUnformated['offer'], function($a, $b) {
                  return strtotime($b['created_at']) - strtotime($a['created_at']);
              });

          }

        return $productUnformated;
    }

    public static function formatImages($image = array())
    {
        if (isset($image['image'])) {
          $image_exploded = explode('.', $image['image']);
          return [
            'thumb' => config('app.url') . '/' . "{$image_exploded[0]}_sm.{$image_exploded[1]}",
            'medium' => config('app.url') . '/' . $image['image']
          ];
        }
    }


    public static function productFormater( array $products ){

        $full_url = env('APP_URL');

        foreach( $products as &$p ){

            if(isset($p->distance)){
                $distance = $p->distance;

                if($distance > 1000){
                    $distance = number_format(($distance/1000), 1);
                    $distance_unit = "km";
                    $distance_unit_full = "kilometros";
                }else{
                    $distance = intval($p->distance);
                    $distance_unit = "m";
                    $distance_unit_full = "metros";
                }

                $p->distance = $distance;
                $p->distance_unit = $distance_unit;
                $p->distance_unit_full = $distance_unit_full;
            }

            $p->offer = ProductOffer::where("product_offer_id", $p->id)->orderBy("id", "DESC")->first()->toArray();
            $p->images = ProductImage::select(DB::raw("CONCAT('{$full_url}/', image) AS url"))->where("product_id", $p->id)->get()->toArray();

            $shop_reviews_comments = Califications::getCalificationByShop($p->shop_id);

            $p->shop = array(
                "id" => $p->shop_id,
                "name" => $p->shop_name,
                "description" => $p->shop_description,
                "calification" => array(
                    "average" => $p->shop_calification_average ? $p->shop_calification_average : 0,
                    "reviews" => $p->shop_reviews,
                    "comments" => array_slice($shop_reviews_comments,0,3),
                    "several_comments" => count($shop_reviews_comments) > 3 ? $shop_reviews_comments : array()
                ),
                "phone" => $p->shop_phone,
                "lat" => $p->lat,
                "lng" => $p->lng,
                "address" => $p->shop_address,
                "schedule" => $p->shop_schelude,
                "mp" => $p->mp ? true : false,
                "logo" => $full_url . "/" . $p->shop_logo,
                "images" => ShopImage::select(DB::raw("CONCAT('{$full_url}/', image) AS url"))->where("shop_id", $p->shop_id)->get()->toArray()
            );

            if(isset($p->distance)){
                $p->shop["distance"] = $distance;
                $p->shop["distance_unit_full"] = $distance_unit_full;
                $p->shop["distance_unit"] = $distance_unit;
            }

            $p->category = array(
                "id" => $p->cat_id,
                "color" => $p->cat_color
            );

            $p = self::_clean($p);

        }

        return $products;

    }

    public static function _clean(&$p){

        unset($p->shop_id);
        unset($p->shop_name);
        unset($p->shop_description);
        unset($p->shop_phone);
        unset($p->lat);
        unset($p->lng);
        unset($p->shop_address);
        unset($p->shop_schelude);
        unset($p->shop_logo);

        unset($p->cat_id);
        unset($p->cat_color);

        return $p;

    }

    /**
     * Formatea la respuesta en la acción closer del controlador de productos.
     * @param array $unformattedArray
     * @param array $distancesByShop
     * @return array
     */
    public static function apiFormatCloser($products = array(), $distances, $prodByShop = null)
    {
//        $shopDistance = [];
//        foreach ($shopsDistances as $shop) {
//
//            $shopDistance[$shop->id] = [
//                'distance' => $shop->distance,
//                'radius' => $shop->radius
//            ];
//        }

        $shopsCount = array();

        $products = array_map(function($product) use($distances, &$shopsCount, $prodByShop) {

            if($prodByShop){
                if(isset($shopsCount[$product['shop']['id']])){

                    if( $shopsCount[$product['shop']['id']] >= $prodByShop){
                        return "delete_this";
                    }else{
                        $shopsCount[$product['shop']['id']]++;
                    }

                }else{
                    $shopsCount[$product['shop']['id']] = 1;
                }
            }

            $unformatedDist = intval($distances[$product['shop']['id']]);

            $formatedDist = $unformatedDist >= 1000 ? round(($unformatedDist / 1000)) . " km" : (round($unformatedDist) == 0 ? 1 : round($unformatedDist)) . " metros";

            $product['distance'] = $formatedDist;
            $product['plane_distance'] = $unformatedDist;
            $product['shop']['lat'] = floatval($product['shop']['lat']);
            $product['shop']['lng'] = floatval($product['shop']['lng']);

            if(count($product['offer'])){

              usort($product['offer'], function($a, $b) {
                  return strtotime($b['created_at']) - strtotime($a['created_at']);
              });

            }

          return $product;

        }, $products);

        $cleanProducts = array_filter($products, function($a){

            return $a != "delete_this";

        });

        usort($cleanProducts, function($a, $b) {
            if ( $a['plane_distance'] == $b['plane_distance'] ) {
                return self::compareOffers($a["offer"][0], $b["offer"][0]);
                //return 0;
            }
            return ($a['plane_distance'] < $b['plane_distance']) ? -1 : 1;
        });

        return array_map('self::apiFormatCloserItem', $cleanProducts);
    }

    public static function compareOffers($offer_a, $offer_b){

        if(!$offer_a)
            return -1;

        if(!$offer_b)
            return 1;

        if( $offer_a["algorithm"] == "ninguna" && $offer_b["algorithm"] != "ninguna" ){
            return 1;
        }elseif( $offer_a["algorithm"] == "ninguna" && $offer_b["algorithm"] == "ninguna" ){
            return 0;
        }elseif( $offer_a["algorithm"] != "ninguna" && $offer_b["algorithm"] == "ninguna" ){
            return -1;
        }elseif( $offer_a["algorithm"] != "ninguna" && $offer_b["algorithm"] != "ninguna" ){

            $offer_val_a = json_decode($offer_a["vars"]);
            $offer_val_b = json_decode($offer_b["vars"]);

            switch( $offer_a["algorithm"] ){
                case "masPorMenos":
                    $priority_points_a = ($offer_val_a->pague * 100) / $offer_val_a->lleve;
                    break;
                case "porcentual":
                    $priority_points_a = $offer_val_a->descuento;
                    break;
            }

            switch( $offer_b["algorithm"] ){
                case "masPorMenos":
                    $priority_points_b = ($offer_val_b->pague * 100) / $offer_val_b->lleve;
                    break;
                case "porcentual":
                    $priority_points_b = $offer_val_b->descuento;
                    break;
            }

            if( $priority_points_a == $priority_points_b ){
                return 0;
            }

            return $priority_points_a > $priority_points_b ? -1 : 1;

        }

        return 0;

    }

    public static function apiFormatCloserItem($data = array())
    {
      $data['images'] = array_map('self::formatImages', $data['images']);
      $data['calification'] = Product::calculateCalification($data['califications']);
      $data['shop'] = ShopFormatter::apiFormatItem($data['shop']);
      $data['category'] = Category::apiFormatShow($data['category']);
      $data = array_except($data, ['active', 'califications']);
      return $data;
    }

    /**
     * Formatea la respuesta en la acción show del controlador de productos.
     * @param $productUnformated
     * @return mixed
     */
    public static function apiFormatShow($productUnformated)
    {
        /**
         * Quitamos ciertos campos
         */
        $productUnformated = array_except($productUnformated, ['active', 'shop_id']);
        /**
         * Filtramos los campos de categoria.
         */
        $productUnformated['category'] = Category::apiFormatShow($productUnformated['category']);
        /**
         * Limitamos los campos del shop a mostrar.
         */
        $productUnformated['shop'] = array_except($productUnformated['shop'],
            [
                'cuit',
                'active',
                'business_name',
                'email',
                'iva',
                'iibb',
                'observation',
                'contact_management',
                'created_at',
                'updated_at',
                'deleted_at'
            ]);

        return $productUnformated;
    }

    /**
     * Formatea la respuesta en la acción show cuando se esta filtrando por beacon y distancia.
     * @param $productUnformated
     * @return mixed
     */
    public static function apiFormatShowOffer($productUnformated, $msg)
    {
        /**
         * Quitamos ciertos campos
         */
        $productUnformated['msg'] = $msg;
        $productUnformated = array_except($productUnformated, ['active', 'shop_id']);
        /**
         * Filtramos los campos de categoria.
         */
        $productUnformated['category'] = Category::apiFormatShow($productUnformated['category']);
        /**
         * Limitamos los campos del shop a mostrar.
         */
        $productUnformated['shop'] = array_except($productUnformated['shop'],
            [
                'cuit',
                'active',
                'business_name',
                'email',
                'iva',
                'iibb',
                'observation',
                'contact_management',
                'created_at',
                'updated_at',
                'deleted_at'
            ]);

        return $productUnformated;
    }
}
