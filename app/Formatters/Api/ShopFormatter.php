<?php

namespace App\Formatters\Api;

use App\Models\Product;

trait ShopFormatter
{
    private static $apiVisibleFields = [
        'id',
        'name',
        'business_name',
        'logo',
        'images',
        'payments',
        'points',
        'phone',
        'description',
        'location',
        'address',
        'lat',
        'lng',
        'mp',
        'dist',
        'distance',
        'distance_unit',
        'distance_unit_full',
        'calification',
        'deleted_at',
        'vip',
        'schedule',
        'locality_id',
        'province_id'
    ];

    /**
     * @param array $unformattedShops
     * @return array
     */
    public static function apiFormatIndex($unformattedShops)
    {
        return array_map('self::apiFormatItem', $unformattedShops);
    }

    public static function apiFormatItem($data = array())
    {
      $data['logo'] = config('app.url') . '/' . $data['logo'];

      if (isset($data['califications'])) {
        $data['calification'] = self::formatCalifications($data['califications']);
      }

        if (isset($data['points'])) {

            $data['points'] = array_reduce(array_map(function($item) {
                return $item['points'];
            }, $data['points']), function ($a, $b) {
                return $a + $b;
            }, 0);
        }

      if (isset($data['payments'])) {
        $data['payments'] = count($data['payments']);
      }

      $data['mp'] = $data['mp_key'] && $data['mp_client_id'] && $data['mp_client_secret'];

      $shop = array_only($data, self::getApiVisibleFields());

      /**
       * Formato imagenes.
       */
      $shop['images'] = array_map(function($image) {

        $image_exploded = explode('.', $image['image']);

        return [
          'thumb' => config('app.url') . '/' . "{$image_exploded[0]}_sm.{$image_exploded[1]}",
          'medium' => config('app.url') . '/' . $image['image']
        ];
      }, $data['images']);

      return $shop;
    }

    public static function formatCalifications($califications)
    {
        //die(var_dump(date("d-m-Y", strtotime($califications[0]["created_at"]))));

        return [
            'average' => Product::calculateCalification($califications),
            'reviews' => count($califications),
            'several_comments' => array_map(function($item){
                return [
                    'text' => $item['comment'],
                    'stars' => $item['stars'],
                    'user' => !$item['user']['first_name'] ? explode("@", $item['user']['email'])[0] : $item['user']['first_name'],
                    'date' => date("d/m/Y", strtotime($item["created_at"]))
                ];
            }, array_slice($califications, 0, 3)),
            'comments' => array_map(function($item) {
                return [
                    'text' => $item['comment'],
                    'stars' => $item['stars'],
                    'user' => !$item['user']['first_name'] ? explode("@", $item['user']['email'])[0] : $item['user']['first_name'],
                    'date' => date("d/m/Y", strtotime($item["created_at"]))
                ];
            }, $califications)
        ];
    }

    public static function getApiVisibleFields()
    {
      return self::$apiVisibleFields;
    }

    public static function apiFormatCloserShops($unformattedShops, $beaconDistances)
    {
        /**
         * Formato tienda
         */
        foreach ($unformattedShops as &$unformattedShop) {
            $fromBeacon = false;

            /**
             * Formato logo
             */
            $unformattedShop['logo'] = config('app.url') . '/' . $unformattedShop['logo'];
            $unformattedShop['mp'] = $unformattedShop['mp_key'] && $unformattedShop['mp_client_id'] && $unformattedShop['mp_client_secret'];
            $unformattedShop['images'] = array_map(function($image) {

              $image_exploded = explode('.', $image['image']);

              return [
                'thumb' => config('app.url') . '/' . "{$image_exploded[0]}_sm.{$image_exploded[1]}",
                'medium' => config('app.url') . '/' . $image['image'],
                'url' => config('app.url') . '/' . $image['image']
              ];
            }, $unformattedShop['images']);

            $unformattedShop["lat"] = floatval($unformattedShop["lat"]);
            $unformattedShop["lng"] = floatval($unformattedShop["lng"]);

            /**
             * Pasamos a metros la distancia.
             */

            $distance = $unformattedShop['dist'];

            if(count($beaconDistances)){
                foreach($beaconDistances as $bd){
                    if( $bd["shop"]["id"] ==  $unformattedShop["id"] ){
                        $fromBeacon = true;
                        $distance = $bd["distance"] < 1 ? 1 : $bd["distance"];
                    }
                }
            }

            $unfformatedDist = intval($distance);

            if($unfformatedDist > 1000){
                $distance = number_format(($unfformatedDist/1000), 1);
                $distance_unit = "km";
                $distance_unit_full = "kilometros";
            }else{
                $distance = $unfformatedDist;
                $distance_unit = "m";
                $distance_unit_full = "metros";
            }

            $unformattedShop['distance'] = $distance;
            $unformattedShop['distance_unit'] = $distance_unit;
            $unformattedShop['distance_unit_full'] = $distance_unit_full;


//            $dist = !$unfformatedDist ? 1 : $unfformatedDist;

//            $unformattedShop['dist'] = $unfformatedDist >= 1000 ? round(($unfformatedDist / 1000)) . " km" : round($unfformatedDist) . " m";

//            if ($distanceFromStore != null) {
//                /**
//                 * Sumamos la distancia entre el usuario y el beacon a todas las tiendas.
//                 */
//                $dist = $dist + $distanceFromStore;
//
//                $unformattedShop['dist'] = $dist >= 1000 ? round(($dist / 1000)) . " km" : round($dist) . " m";
//            }


            if (isset($unformattedShop['califications'])) {
                $unformattedShop['calification'] = self::formatCalifications($unformattedShop['califications']);
            }
            if (isset($unformattedShop['points'])) {
                $unformattedShop['points'] = array_reduce(array_map(function($item) {
                    return $item['point'];
                }, $unformattedShop['points']), function ($a, $b) {
                    return $a + $b;
                }, 0);
            }

            if (isset($unformattedShop['payments'])) {
                $unformattedShop['payments'] = count($unformattedShop['payments']);
            }

            $unformattedShop = array_only($unformattedShop, self::getApiVisibleFields());
            if ($fromBeacon)
              $unformattedShop["from_beacon"] = true;
        }

        return $unformattedShops;
    }

    /**
     * @param array $unformattedShop
     * @return array
     */
    public static function apiFormatShow($unformattedShop)
    {
        $unformattedShop = array_except($unformattedShop, [
            'cuit',
            'active',
            'email',
            'iva',
            'iibb',
            'observation',
            'contact_management'
        ]);
        /**
         * Formato logo
         */
        $unformattedShop['logo'] = config('app.url') . '/' . $unformattedShop['logo'];

        /**
         * Formato imagen
         */
        foreach ($unformattedShop['images'] as &$image) {
            $image['image'] = config('app.url') . '/' . $image['image'];
        }

        return $unformattedShop;
    }
}
