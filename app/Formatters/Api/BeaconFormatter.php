<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/6/17
 * Time: 3:45 PM
 */

namespace App\Formatters\Api;


trait BeaconFormatter
{
    /**
     * @param array $unformattedItems
     * @return array
     */
    public static function apiFormatIndex($unformattedItems)
    {
        $items = [];
        foreach ($unformattedItems as &$unformattedItem) {
            array_push($items, $unformattedItem['serial']);
        }

        return $items;
    }
}