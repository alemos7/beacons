<?php
/**
 * Created by PhpStorm.
 * User: danielmaldonado
 * Date: 4/20/17
 * Time: 00:26
 */

namespace App\Formatters\Api;

class PointsFormatter
{
    private static function formatItem($item = array())
    {
        return $item;
    }

    public static function format($items = array())
    {
        return array_map('self::formatItem', $items);
    }

}