<?php

namespace App\OfferCalculate;

class OfferCalculate {
	private $offer = NULL; 
	
	public function __construct($offer_type) {
		switch ($offer_type) {
			case "porcentual": 
			$this->offer = new OfferPorcentual();
			break;
			case "masPorMenos": 
			$this->offer = new OfferMasPorMenos();
			break;
		}
	}
	public function showCalculation($vars, $price) {
		if(!$vars || !$price) return 0;
		return $this->offer->calculate($vars, $price);
	}
}