<?php

namespace App\OfferCalculate;

interface OfferInterface {
	public function calculate($vars, $price);
}