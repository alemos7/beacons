<?php

namespace App\OfferCalculate;

class OfferMasPorMenos implements OfferInterface {
	public function calculate($vars, $price) {
		return $price*($vars[0]/$vars[1]);
	}
}