<?php

namespace App\OfferCalculate;

class OfferPorcentual implements OfferInterface {
	public function calculate($vars, $price) {
		return 100/(100-$vars[0])*$price;
	}
}