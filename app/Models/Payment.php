<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;
class Payment extends Model {

    protected $fillable = [
        'id',
        'user_id',    
        'shop_id',
        'product_id',
        'mp_payment_id',
        'status',
        'response',
        'delivery',
        'quantity',
        'payment_method',
        'payment_created_date',
        'delivery_date'
    ];
    
    public static function createPaymentV2($user_id, $data){        
        
        date_default_timezone_set("America/Argentina/Buenos_Aires");
        
        return self::create(array(
            "user_id" => $user_id,
            "shop_id" => $data["product"]["shop"]["id"],
            "product_id" => $data["product"]["prod_id"],
            "mp_payment_id" => $data["payment"]["id"],
            "status" => $data["payment"]["status"],
            "response" => json_encode($data),
            "quantity" => $data["quantity"],
            "payment_method" => $data["payment_method"],
            'payment_created_date' => date("Y-m-d H:i:s")
        ));

    }
  
    public static function createPayment($user_id, $data){        
        
        date_default_timezone_set("America/Argentina/Buenos_Aires");
        
        return self::create(array(
            "user_id" => $user_id,
            "shop_id" => $data["product"]["shop"]["id"],
            "product_id" => $data["product"]["id"],
            "mp_payment_id" => $data["payment"]["id"],
            "status" => $data["payment"]["status"],
            "response" => json_encode($data),
            "quantity" => $data["quantity"],
            "payment_method" => $data["payment_method"],
            'payment_created_date' => date("Y-m-d H:i:s")
        ));

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shop()
    {
        return $this->belongsTo('App\Models\Shop');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
    
    /**
     * Get Date Formated
     *
     * @return date
     */
    public function getFullCreatedAttribute()
    {
        return  Carbon::parse($this->created_at)->format('d/m/Y h:i:s A');
    } 

    /**
     * Get Date Without hour
     *
     * @return date
     */
    public function getDateCreatedAttribute()
    {
        return  Carbon::parse($this->created_at)->format('d/m/Y');
    }
    /**
     * Get Only Hour
     *
     * @return date
     */
    public function getHourCreatedAttribute()
    {
        return  Carbon::parse($this->created_at)->format('h:i:s A');
    }

    public function getPedidoAttribute()
    {
        if($this->status != 'approved'){
            return "-";
        }
        $pedido = DB::table('payments')
                        ->where('id','<=',$this->id)
                        ->where('shop_id',$this->shop_id)
                        ->where('status','approved')->count();
        return $pedido;
    }

    public function getTransactionAmountAttribute()
    {
        $transactionAmount = isset($this->response->payment->transactionAmount) ? 
                             $this->response->payment->transactionAmount : 
                             $this->response->payment->transaction_amount;
        
        $price = number_format($transactionAmount ,2,",",".");
        return $price;
    }

    public function getStatusFormattedAttribute()
    {
        $status = '';
        switch ($this->status) {
            case 'pending':
                    $status = 'Pendiente';
                break;
            case 'approved':
                    $status = 'Recibido';
                break;
            case 'in_process':
                    $status = 'En Progreso';
                break;
            case 'in_mediation':
                    $status = 'En Mediación';
                break;
            case 'rejected':
                    $status = 'Rechazado';
                break;
            case 'cancelled':
                    $status = 'Cancelado';
                break;
            case 'refunded':
                    $status = 'Reintegrado';
                break;
            case 'charged_back':
                    $status = 'Devuelto';
                break; 
        }

        return $status;
    }
  
}
