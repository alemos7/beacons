<?php

namespace App\Models;

use App\Notifications\ResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Hash;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\UserSetting;
// 
use App\Models\Product;
use App\Models\Shop;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'email',
        'password',
        'register_id',
        'first_name',
        'last_name',
        'birthday',
        'phone',
        'gender',
        'from',
        'image',
        'active',
        'config',
        'provider_id',
        'provider_access_token',
        'created_by',
        'device_os'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function shops()
    {
        return $this->belongsToMany('App\Models\Shop', 'pivot_user_shops', 'user_id','shop_id');
    }

    public function favorites()
    {
        return $this->hasMany('App\Models\UserFavorite');
    }

    public function settings()
    {
        return $this->hasOne('App\Models\UserSetting');
    }
    
    public function interests()
    {
        return $this->hasOne('App\Models\UserInterests');
    }
    
    public function getSettings()
    {
        $settings = $this->settings()->first();

        if ($settings) {
            return $settings;
        }
        
        $settings = $this->settings()->create([]);

        return $settings;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function products()
    {
        $user = auth()->user();

        switch($this->type)
        {
            case 'administrator':
                return Product::orderBy('name','asc')->get();

            case 'operater':
                $shops = $this->shops()->get();
                $products = [];
                foreach ($shops as $shop) {
                    $products = $shop->products()->get()->merge($products);
                }
                if( $products ){
                    return $products->sortBy('name');
                }
                return $products;

            case 'seller':

                return Product::whereHas('shop', function($subQuery){
                    $subQuery->whereHas('creator',function($subSubQuery){
                        $subSubQuery->where('id', auth()->user()->id);
                    });
                })->orderBy('name','asc')->get();
        }

        $shops = $this->shops()->get();
        $products = [];
        foreach ($shops as $shop) {
            $products = $shop->products()->get()->merge($products);
        }
        if( $products ){
            return $products->sortBy('name');
        }
        return $products;

        /* TOdo: Hacer bien, borrar los foreachs furiosos
         * $user = $this;

        return Product::whereHas('shop.users', function ($subQuery) use ($user){
            $subQuery->where('user.id', $user->id);
        })->orderBy('name','asc')
          ->get();


        );
         */
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function payments()
    {
        $products = $this->products();
        $payments = [];
        foreach ($products as $product) {
            $payments = $product->payments()->get()->merge($payments);
        }
        return $payments;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder $query
     * //Scope para traer facilmente los usuarios que pueden ser asignados a un shop como operadores
     * Si es admin, trae todos los operadores, si es vendedor, los operadores que haya creado el
     */
    public function scopeAsignable($query)
    {
        return $query->accesible()->where('type','operator');
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder $query
     * //Scope para traer facilmente los usuarios que se pueden ver
     */
    public function scopeAccesible($query)
    {
        $user = Auth::user();

        switch($user->type)
        {
            case 'administrator': //El admin ve everything
                return $query;

            case 'seller': //El vendedor solo ve los usuarios que creo el mismo

                return $query->whereHas('creator', function ($subQuery) use ($user){
                        $subQuery->where('id', $user->id);
                    }
                );

            case 'operator': //El operador no puede ver usuarios

                return $query->where('id', null); //Todo: Vacio siempre, ver una manera decente
        }

        throw new \Exception('El usuario no puede ver usuarios');
    }

    /**
    * Get Date Formated
    *
    * @return date
    */
    public function getFullCreatedAttribute()
    {
        return  Carbon::parse($this->created_at)->format('d/m/Y h:i:s A');
    }

    /**
    * Get Date Formated
    *
    * @return date
    */
    public function getCreatedAttribute()
    {
        return  Carbon::parse($this->created_at)->format('d/m/Y');
    }

    public function creator()
    {
        return $this->belongsTo('App\Models\User','created_by');
    }

    public function usersCreated()
    {
        return $this->hasMany('App\Models\User','created_by');
    }

    public function shopsCreated()
    {
        return $this->hasMany('App\Models\Shop','created_by');
    }

    /**
    * Get User Full Name
    *
    * @return string
    */
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public static function createUser(array $data)
    {
        $data['type'] = 'user';
        $data['active'] = 1;
        if( !array_key_exists ( 'gender',  $data ) ){
            $data['gender'] = 'Otro';
        }
        return self::create($data);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }
}
