<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Formatters\Api\ShopFormatter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;
use File;
use Image;
use App\Helpers\ImagesResize;
use Illuminate\Support\Facades\DB;

class Shop extends Model
{
    use ShopFormatter;
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shops';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cuit',
        'name',
        'business_name',
        'description',
        'active',
        'phone',
        'lat',
        'lng',
        'address',
        'mp_key',
        'mp_client_id',
        'mp_client_secret',
        'email',
        'vip',
        'iva',
        'iibb',
        'schedule',
        'observation',
        'contact_management',
        'logo',
        'locality_id',
        'province_id',
        'created_by'
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * Hidden fields
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'pivot_user_shops', 'shop_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Payment');
    }

    public function points()
    {
        return $this->hasMany('App\Models\UserPoint');
    }
    
    public function califications()
    {
        return $this->hasMany('App\Models\Califications');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('App\Models\ShopImage', 'shop_id', 'id')->orderBy('order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function beacons()
    {
        return $this->hasMany('App\Models\ShopBeacon');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('App\Models\User', 'created_by', 'id');
    }

    /**
     * Update Shop in the database.
     *
     * @param  array $attributes
     * @param  array $options
     * @return bool
     */
    public function update(array $attributes = [], array $options = [])
    {
        $attributes['users'] = (array_key_exists('users', $attributes)) ? $attributes['users'] : [];
        $attributes['words'] = (array_key_exists('words', $attributes)) ? $attributes['words'] : [];
        $attributes['active'] = (array_key_exists('active', $attributes)) ? (bool) $attributes['active'] : false;
        $attributes['vip'] = (array_key_exists('vip', $attributes)) ? (bool) $attributes['vip'] : false;

        $this->fill($attributes)->save();

        if(array_key_exists ( 'logo',  $attributes )){
            $this->logo = 'storage/' .env('STORAGE_SHOP_IMAGES') . '/' . $this->id.'_logo.png';
            $this->saveLogo($attributes['logo']);
        }

        if (in_array(Auth::user()->type,['administrator','seller']))
        {
            $this->updateUsers($attributes['users']);
        }

        // Un negocio no debe poder pasarse a estado activo si no completó la Key de Mercado Pago 
        /*if( !$this->mp_client_id || !$this->mp_client_secret){
            $this->active = false;
            $this->save();
        }*/
    }

    /**
     * Update Logo in Shop
     *
     */
    private function saveLogo($image)
    {
        $image_name =  $this->id.'_logo.png';
        Storage::put( '/public/' .env('STORAGE_SHOP_IMAGES') . '/' . $image_name ,File::get($image) );

        $path = public_path().'/..'.'/storage/app/'. '/public/' . env('STORAGE_SHOP_IMAGES') . '/';
        
        ImagesResize::maxSize($path,$image_name,100,100);
    }

    /**
     * Update Users in Shop
     *
     * @param array $users
     */
    private function updateUsers(array $users = [])
    {
        $this->users()->detach();
        $this->users()->attach($users);
        $this->save();
    }    

    /**
     * Sólo tiendas activas.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder $query
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder $query
     */
    //Scope para traer facilmente solo a los que tenemos acceso
    public function scopeAccesible($query)
    {
        $user = Auth::user();

        switch($user->type)
        {
            case 'administrator': //El admin ve everything
                return $query;

            case 'operator': //El operador solo puede ver los shops que puede administrar

                return $query->whereHas('users', function($subQuery) use ($user){
                        $subQuery->where('users.id', $user->id);
                    });

            case 'seller': //El vendedor solo ve los shops que creo el mismo

                return $query->whereHas('creator', function($subQuery) use ($user){
                    $subQuery->where('users.id', $user->id);
                });
        }

        throw new \Exception('El usuario no tiene acceso a shops');
    }

    /**
    * Get CUIT Formated
    *
    * @return string
    */
    public function getCuitFormatedAttribute()
    {
        if($this->cuit == '')
            return '';
        //  Formato ##-########-#
        return  substr($this->cuit, 0,2).'-'.substr($this->cuit, 2,8).'-'.substr($this->cuit, 10,1);
    }

    public function getIibbAttribute($value)
    {
        return ($value > 0) ? $value : '';
    }
    
    public static function getShopsByBeacon( $beacons, array $options ){
        
        $shopBeaconModel = ShopBeacon::with('shop')
                ->has('shop');

        foreach( $beacons as $key => $b ){

            $serial = $b->major . "/" . $b->minor;
            
            if(!$key)
                $shopBeaconModel->where('serial', $serial);
            
            $shopBeaconModel->orWhere('serial', $serial);

        }
        
        if(isset($options["limit"]))
            $shopBeaconModel->limit($options["limit"]);
        
        $shops = $shopBeaconModel->get()->toArray();        
        
        if(!empty($options["add_distances"]))
            $shops = self::addDistances ($shops, $beacons);


        return $shops;
        
    }
    
    public static function addDistances(array $shops, array $beacons){
        
        foreach($shops as &$s){
            
            $shopBeaconSerial = $s["serial"];
            
            foreach($beacons as $b){
                
                $beaconSerial = $b->major . "/" . $b->minor;
                
                if($beaconSerial == $shopBeaconSerial){
                    $s["distance"] = $b->distance;
                }
               
            }          
            
        }
        
        return $shops;
        
    }
    
    public static function getShopsByCoordinates(array $select, $lat, $lng, $maxDistance){
        
        $fields = "";
        
        foreach($select as $key=>$s){
            if($key)
               $fields .= ",";
            
            $fields .= $s;
        }
        
        $shopDistanceQuery = 
        "SELECT {$fields},
            (p.distance_unit
             * DEGREES(ACOS(COS(RADIANS(p.latpoint))
             * COS(RADIANS(shop.lat))
             * COS(RADIANS(p.longpoint - shop.lng))
             + SIN(RADIANS(p.latpoint))
             * SIN(RADIANS(shop.lat))))) * 1000 AS distance
        FROM shops AS shop
        JOIN (
            SELECT  {$lat}  AS latpoint,  {$lng} AS longpoint,
            {$maxDistance} AS radius, 111.045 AS distance_unit
        ) AS p ON 1=1 WHERE shop.lat
        BETWEEN p.latpoint  - (p.radius / p.distance_unit)
            AND p.latpoint  + (p.radius / p.distance_unit)
       AND shop.lng
        BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
            AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
         AND shop.active = 1 ORDER BY distance";
            
        return DB::select( \DB::raw($shopDistanceQuery) );
        
    }

}
