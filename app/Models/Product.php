<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Formatters\Api\ProductFormatter;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use ProductFormatter;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'shop_id',
    'name',
    'price',
    'code',
    'description',
    'active',
    'point',
    'mp_key',
    'category_id',
    'mp_category_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function shop()
    {
        return $this->belongsTo('App\Models\Shop');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('App\Models\ProductImage', 'product_id', 'id')->orderBy('order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mp_category()
    {
        return $this->hasOne('App\Models\MP_Category');
    }

    public function califications()
    {
        return $this->hasMany('App\Models\Califications');
    }

    public function payments()
    {
        return $this->hasMany('App\Models\Payment');
    }

    public static function calculateCalification($califications = array())
    {
        if (count($califications) == 0) {
            return 0;
        }

        return array_reduce(array_map(function($c) {
            return $c['stars'];
        }, $califications), function($a, $b) {

            return intval($a) + intval($b);
        }, 0) / count($califications);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function beacons()
    {
        return $this->hasMany('App\Models\ShopBeacon');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany('App\Models\ProductAdvertising');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function offer()
    {
        return $this->hasMany('App\Models\ProductOffer', 'product_offer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function words()
    {
        return $this->belongsToMany('App\Models\Word', 'pivot_products_words', 'product_id', 'word_id');
    }
    /**
     * Update Products in the database.
     *
     * @param  array $attributes
     * @param  array $options
     * @return bool
     */
    public function update(array $attributes = [], array $options = [])
    {
        $attributes['active'] = (array_key_exists('active', $attributes)) ? true : false;
        $this->fill($attributes)->save();
        $this->updateOffer($attributes);

        $attributes['words'] = (array_key_exists('words', $attributes)) ? $attributes['words'] : [];
        $this->updateWords($attributes['words']);
    }

    /**
     * Update offer in this product.
     *
     * @param  array $attributes
     */
    private function updateOffer(array $attributes = [])
    {
        $offerAttributes['algorithm'] = $attributes['algorithm'];
        $offerAttributes['name'] = '';

        switch ($offerAttributes['algorithm']) {
            case 'porcentual':
                $offerAttributes['vars'] = json_encode(array('descuento'=>$attributes['vars'][0]));
                break;
            case 'masPorMenos':
                $offerAttributes['vars'] = json_encode(array('lleve'=>$attributes['vars'][0], 'pague'=>$attributes['vars'][1]));
                break;
            default:
                $offerAttributes['algorithm'] = 'ninguna';
                $offerAttributes['vars'] = '';
                break;
        }

        $offerAttributes['product_offer_id'] = $this->id;
        $offerAttributes['created_at'] = \Carbon\Carbon::now();
        $offerAttributes['before_offer_price'] = $attributes["before_offer_price"];

        $this->offer()->create($offerAttributes);
    }

    /**
     * Update Words in Shop
     *
     * @param array $words
     */
    private function updateWords(array $words = [])
    {
        $this->words()->detach();
        $i = 0;
        foreach ($words as $word) {
            if (!Word::find($word) ) {
                $nWord = new Word();
                $nWord->word = $word;
                $nWord->save();
                $words[$i] = $nWord->id;
            }
            $i++;
        }
        $this->words()->attach($words);
        $this->save();
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function scopeOnlyOffers($query) {
        return $query->has('offer');
    }

    public function getPriceFormattedAttribute(){
        $price = number_format($this->price ,2,",",".");
        return $price;
    }
    public function getOfferTypeAttribute(){
        $offerLenth = count($this->offer);
        if($offerLenth ){
            switch ($this->offer[ $offerLenth-1 ]->algorithm) {
                case 'masPorMenos':
                    return "Más por menos";
                break;
                case 'porcentual':
                    return "Porcentual";
                break;
                default:
                    return "Sin Oferta";
            }
        }else{
            return "Sin oferta";
        }
    }
    /**
     * Return count of payments with delivery 'Concretada'.
     *
     * @return Integer
     */
    public function sells(){
        return count($this->payments->where('delivery','Concretada'));
    }

    public static function getProductsForApp(array $options){

        $sqlCases = "";
        $beaconCases = "";

        if(isset($options["shops"])){

            foreach($options["shops"] as $key => $shop){

                if($shop["distance"] < 1)
                    $shop["distance"] = 1;

                $sqlCases .= " WHEN {$shop["shop"]["id"]} THEN {$shop["distance"]} ";
                $beaconCases .= "WHEN {$shop["shop"]["id"]} THEN 1";

            }

        }

        $select = "SELECT
            P.id as id,
            P.name as name,
            P.price as price,
            P.description as description,
            P.point,
            S.id as shop_id,
            S.name as shop_name,
            S.description as shop_description,
            S.phone as shop_phone,
            S.lat as lat,
            S.lng as lng,
            S.address as shop_address,
            S.schedule as shop_schelude,
            S.logo as shop_logo,
            (FLOOR((SELECT SUM(CAL.stars) FROM califications CAL WHERE CAL.product_id = P.id)/(SELECT COUNT(*) FROM califications CAL WHERE CAL.product_id = P.id ))) AS calification,
            (FLOOR((SELECT SUM(CAL.stars) FROM califications CAL WHERE CAL.shop_id = S.id)/(SELECT COUNT(*) FROM califications CAL WHERE CAL.shop_id = S.id ))) AS shop_calification_average,
            (SELECT COUNT(*) FROM califications CAL WHERE CAL.shop_id = S.id) AS shop_reviews,
            IF(
            S.mp_key != '' AND S.mp_client_id != '' AND S.mp_client_secret != '',
                1,
                0
            ) AS mp,
            C.id as cat_id,
            C.name as cat_name,
            C.color as cat_color,
            @shop_num := IF(@current_product_shop = S.id, @shop_num + 1, 1) AS shop_num,
            @current_product_shop := S.id";

        $case = ",CASE S.id
            WHEN 0 THEN 1
            {$sqlCases}
            ELSE ((
                p.distance_unit * DEGREES(
                    ACOS(
                      COS(RADIANS(p.latpoint)) * COS(RADIANS(S.lat)) * COS(RADIANS(p.longpoint - S.lng)) + SIN(RADIANS(p.latpoint)) * SIN(RADIANS(S.lat))
                    )
                )
            ) * 1000)
            END AS distance";

        $beaconCase = ", CASE S.id
        WHEN 0 THEN 0
        {$beaconCases}
        ELSE 0 END AS isBeacon";

        $from = " FROM products P
            LEFT JOIN shops S ON S.id = P.shop_id
            LEFT JOIN categories C ON P.category_id = C.id";

        $distanceQuery = " JOIN
                (SELECT
                    {$options["lat"]} AS latpoint,
                    {$options["lng"]} AS longpoint,
                    {$options["maxDistance"]} AS radius,
                    111.045 AS distance_unit) AS p
            ON 1 = 1
            WHERE S.lat BETWEEN p.latpoint - (p.radius / p.distance_unit)
                AND p.latpoint + (p.radius / p.distance_unit)
                AND S.lng BETWEEN p.longpoint - (
                  p.radius / (
                    p.distance_unit * COS(RADIANS(p.latpoint))
                  )
                )
                AND p.longpoint + (
                  p.radius / (
                    p.distance_unit * COS(RADIANS(p.latpoint))
                  )
            )";

            if(!$options["lat"] || !$options["lng"]){
                $query = $select . $from . " WHERE 1 ";
            }else{
                $query = $select . $case . $from . $distanceQuery;
            }

        if(isset($options["search"])){
            $search = addslashes(trim($options["search"]));
            $query .= " AND P.name LIKE '%{$search}%'";
            $query .= " OR P.description LIKE '%{$search}%'";
            $query .= " OR S.name LIKE '%{$search}%'";
        }

        if(isset($options["filters"])){
            foreach( $options["filters"] as $key => $value){

                if(strpos($key, ':')){

                    $keyVals = explode(':', $key);

                    if($keyVals[0] == "not"){

                        $query .= " AND {$keyVals[1]} NOT IN ({$value})";

                    }

                }else{

                    if (strpos($value, ',') == false) {
                        $query .= " AND {$key} = '{$value}'";
                    } else {
                        $query .= " AND {$key} IN ({$value})";
                    }
                }

            }
        }

//        if(empty($options['includeDeleted'])){
        $query .= " AND P.active = 1";
        $query .= " AND S.active = 1";
        $query .= " AND S.deleted_at IS NULL";
        $query .= " AND P.deleted_at IS NULL";
//        }

        $query .= " AND (SELECT COUNT(*) FROM product_images PRI WHERE PRI.product_id = P.id > 0)";

        if($options["lat"] && $options["lng"])
            $query .= " ORDER BY distance";

        $finalQuery = "SELECT * FROM (" . $query . ") a WHERE shop_num <= " . (!empty($options['maxPerShop']) ? $options['maxPerShop'] : 5000);

        if(isset($options["page"]))
            $finalQuery .= " LIMIT {$options["page"]}";

//        die($finalQuery);

        return DB::select ($finalQuery);

    }

    public static function getProductsByShopsId(array $options){

        $query = Product::with(['category', 'images', 'shop', 'shop.images', 'califications', 'offer', 'shop.califications', 'shop.califications.user']);

        if(isset($options['active']))
            $query->active();

        if(!empty($options['shops']) && is_array($options['shops']))
            $query->whereIn('shop_id', $options['shops']);

        if (!empty($options['search'])) {
            $query->where('name', 'like', '%' . $request->input('search') . '%');
            $query->orWhere('code', 'like', '%' . $request->input('search') . '%');
            $query->orWhere('description', 'like', '%' . $request->input('search') . '%');
            $query->orWhereHas('shop', function($q) use ($request){
                $q->where('name', 'LIKE', '%' . $request->input('search') . '%');
            });
        }

        if(isset($options['page'])){
            $query->skip( $options['page']*30 );
            $query->take(30);
        }

//        dd($query->getQuery()->toSql());
        $products = $query->get()->toArray();
        return $products;

    }

}
