<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Storage;
use File;
use Image;
use App\Helpers\ImagesResize;

class Banner extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'banners';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_1_id',
        'image_1',
        'product_2_id',
        'image_2',
        'product_3_id',
        'image_3',
        'product_4_id',
        'image_4',
        'product_5_id',
        'image_5',
        'product_6_id',
        'image_6',
        'product_7_id',
        'image_7',
        'product_8_id',
        'image_8',
        'product_9_id',
        'image_9',
        'product_10_id',
        'image_10',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_1()
    {
        return $this->belongsTo('App\Models\Product','product_1_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_2()
    {
        return $this->belongsTo('App\Models\Product','product_2_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_3()
    {
        return $this->belongsTo('App\Models\Product','product_3_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_4()
    {
        return $this->belongsTo('App\Models\Product','product_4_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_5()
    {
        return $this->belongsTo('App\Models\Product','product_5_id','id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_6()
    {
        return $this->belongsTo('App\Models\Product','product_6_id','id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_7()
    {
        return $this->belongsTo('App\Models\Product','product_7_id','id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_8()
    {
        return $this->belongsTo('App\Models\Product','product_8_id','id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_9()
    {
        return $this->belongsTo('App\Models\Product','product_9_id','id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_10()
    {
        return $this->belongsTo('App\Models\Product','product_10_id','id');
    }

    /**
     * Update Banner in the database.
     *
     * @param  array $attributes
     * @param  array $options
     * @return bool
     */
    public function update(array $attributes = [], array $options = [])
    {
        //Extremadamente villero, mas adelante corregir lo de jose
        $this->id = 1;
        for($i = 1; $i <= 10 ; $i++){
            if( !array_key_exists ( 'product_'. $i . '_id',  $attributes ) || $attributes['product_'. $i . '_id'] == '' ){
                $attributes['product_'. $i . '_id'] = NULL;
            }
        }

        $this->fill($attributes)->save();

        for($i = 1; $i <= 10 ; $i++){
            if(array_key_exists ( 'image_'.$i,  $attributes )){
                $image_name = $this->id.'_'.$i.'.jpg';
                $this['image_'.$i] = 'storage/' .env('STORAGE_BANNER_IMAGES') . '/' . $image_name;
                $this->saveImage($attributes['image_'.$i],$image_name);
            }
        }
        $this->save();
    }


    /**
     * Update Image in banner
     *
     */
    private function saveImage($image,$image_name)
    {
        Storage::put( '/public/' .env('STORAGE_BANNER_IMAGES') . '/' . $image_name ,File::get($image) );

        $path = public_path().'/..'.'/storage/app/'. '/public/' . env('STORAGE_BANNER_IMAGES') . '/';
        ImagesResize::maxSize($path,$image_name,1080,450);
    }
    
    public static function getBanners(){
        
        return self::first();
        
    }
}
