<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'notification_closer', 'notification_massive'
    ];
}
