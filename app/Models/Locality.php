<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locality extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'locality';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];  

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
    public function province(){ 
        return $this->belongsTo('App\Models\Province');
    }
}
