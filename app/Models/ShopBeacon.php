<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Formatters\Api\BeaconFormatter;

class ShopBeacon extends Model
{
    use BeaconFormatter;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shop_beacons';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shop_id',
        'product_id',
        'serial',
        'distance',
        'description',
        'active',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function shop(){
        return $this->belongsTo('App\Models\Shop');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function product(){
        return $this->belongsTo('App\Models\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function products_around(){
        return $this->hasOne('App\Models\ProductsBeacon','beacon_id');
    }

     /**
     * Update ShopImages in the database.
     *
     * @param  array  $attributes
     * @param  array  $options
     * @return bool
     */
    public function updateModified(array $attributes = [], array $options = [])
    {
        if(isset($attributes["serial"])){
            
            $serial = explode("/", $attributes["serial"]);
            
            $attributes["serial"] = hexdec($serial[0]) . "/" . hexdec($serial[1]);
            
        }
        
        $attributes['active'] = (array_key_exists ( 'active',  $attributes ))? true : false;
        return $this->fill($attributes)->save();
    }

    /**
     * Sólo beacons activos.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder $query
     */
    public function scopeActive($query) {
        return $query->where('active', 1);
    }

    public static function makeSerial($major, $minor)
    {
      return "$major/$minor";
    }

    public static function suggestDistance($distance = 0)
    {
        if($distance <= 5){
            return 'close';
        }else{
            return 'far';
        }
    }

    public function scopeAccesible($query)
    {
        //ini_set('xdebug.max_nesting_level', 200);
        //Hola, si te tira error de nesting max, setea esto en php.ini o descomenta esta linea,
        // el framework nestea muchas funciones cuando usas las querys compuestas y si tenes xdebug desactualizado te puede dar error

        $user = \Auth::user();

        switch($user->type)
        {
            case 'administrator': //El admin ve everything
                return $query;

            case 'operator': //El operador ve los beacons de los shops de los que es operador asignado

                return $query->whereHas('shop', function ($subQuery) use ($user){ //shop.users ?
                        $subQuery->whereHas('users', function($subSubQuery) use ($user){
                            $subSubQuery->where('users.id', $user->id);
                        });
                    }
                );

            case 'seller': //El vendedor solo ve los beacons sin asignar a ningun shop o asignados a shops a los que tiene acceso

                return $query->doesntHave('shop')->orWhereHas('shop', function ($subQuery){
                        $subQuery->accesible();
                    }
                );
        }

        throw new \Exception('El usuario no tiene acceso a shops');
    }

    public function getSerialStringAttribute()
    {
        $serial = explode("/", $this->serial,2);
        $minor = str_pad( dechex(@$serial[0]), 4, "0", STR_PAD_LEFT);
        $mayor = str_pad( dechex(@$serial[1]), 4, "0", STR_PAD_LEFT);
        return  $minor . "/" . $mayor;
    }
    
    
    
}