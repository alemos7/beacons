<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Formatters\Api\CategoryFormatter;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use CategoryFormatter;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'logo',
        'color',
        'childof'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany('App\Models\CategoryImage', 'category_id', 'id')->orderBy('order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product', 'category_id', 'id');
    }

    /**
     * Update Products in the database.
     *
     * @param  array $attributes
     * @param  array $options
     * @return bool
     */
    public function update(array $attributes = [], array $options = [])
    {
        $this->fill($attributes)->save();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subcategories()
    {
        return $this->hasMany('App\Models\Category', 'childof');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeOnlyRoots($query)
    {
        return $query->where('childof', null);
    }
}
