<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProductsBeacon extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products_beacon';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'beacon_id',
        'product_close_id',
        'product_close_message',
        'product_far_id',
        'product_far_message'
    ];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function beacon() {
        return $this->belongsTo('App\Models\ShopBeacon', 'beacon_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product_close(){ 
        return $this->belongsTo('App\Models\Product','product_close_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */    
    public function product_far(){ 
		return $this->belongsTo('App\Models\Product','product_far_id');
	}

    /**
     * Update Products in the database.
     *
     * @param  array $attributes
     * @param  array $options
     * @return bool
     */
    public function update(array $attributes = [], array $options = [])
    {
        $attributes['product_close_id'] = ($attributes['product_close_id'] == 0) ? null : $attributes['product_close_id'];
        $attributes['product_far_id'] = ($attributes['product_far_id'] == 0) ? null : $attributes['product_far_id'];
        $this->fill($attributes)->save();  
    }

    public static function getProductsOfBeacons($beacons){


        $sql = "SELECT PB.id,
                PB.product_close_message, 
                PB.product_far_message, 
                PB.product_close_id, 
                SB.serial,
                PB.product_far_id 
                FROM shop_beacons SB LEFT JOIN products_beacon PB ON PB.beacon_id = SB.id WHERE ";

        foreach($beacons as $key => $b){

            $serial = $b->major . "/" . $b->minor;

            if($key)
                $sql .= " OR ";

            $sql .= " SB.serial = '{$serial}'" ;

        }

        return DB::select ($sql);

    }
}
