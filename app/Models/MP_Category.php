<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MP_Category extends Model
{

    protected $table = 'mp_categories';
    
    public $timestamps = false;

    protected $fillable = [
    	'name',
    	'description',
    ];
}
