<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model {

  protected $fillable = [
    'name',
    'key'
  ];

  public static function getByKey($key)
  {
    return self::where(['key' => $key])->get()->first();
  }
}
