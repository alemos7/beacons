<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;
use File;
use App\Helpers\ImagesResize;


class ProductImage extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'image',
        'order',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function products()
    {
        return $this->belongsTo('App\Models\Product');
    }
     /**
     * Update ShopImages in the database.
     *
     * @param  array  $attributes
     * @param  array  $options
     * @return bool
     */
    public function update(array $attributes = [], array $options = [])
    {
        $this->fill($attributes)->save();
        if($this->product_id)
            $this->image = 'storage/'. env('STORAGE_PRODUCT_IMAGES') . '/' . $attributes['product_id'].'_'.$this->id.'.jpg';

        if(array_key_exists ( 'file_image',  $attributes )){
            $file_image = File::get($attributes['file_image']);
            $image =     $attributes['product_id'].'_'.$this->id.'.jpg';
            $image_sm = $attributes['product_id'].'_'.$this->id.'_sm.jpg';

            Storage::put('/public/'. env('STORAGE_PRODUCT_IMAGES') . '/' . $image, $file_image);
            Storage::put('/public/'. env('STORAGE_PRODUCT_IMAGES') . '/' . $image_sm, $file_image);

            $path = public_path().'/..'.'/storage/app/'.'/public/'. env('STORAGE_PRODUCT_IMAGES') . '/';

            ImagesResize::maxSize($path,$image,1080,1920);
            ImagesResize::maxSize($path,$image_sm,480,480);
        }
        return $this->save();
    }

    public function delete()
    {       
        $image_sm = $this->product_id.'_'.$this->id.'_sm.jpg';
        $image = $this->product_id.'_'.$this->id.'.jpg';

        Storage::delete('/public/'. env('STORAGE_PRODUCT_IMAGES') . '/' . $image);
        Storage::delete('/public/'. env('STORAGE_PRODUCT_IMAGES') . '/' . $image_sm);

        if (is_null($this->getKeyName())) {
            throw new Exception('No primary key defined on model.');
        }
        if ($this->exists) {
            if ($this->fireModelEvent('deleting') === false) {
                return false;
            }
            $this->touchOwners();
            $this->performDeleteOnModel();
            $this->exists = false;
            $this->fireModelEvent('deleted', false);
            return true;
        }
    }     

    public function updateFileNames()
    {
        Storage::move('/public/'. env('STORAGE_PRODUCT_IMAGES') . '/_'.$this->id.'.jpg','/public/'. env('STORAGE_PRODUCT_IMAGES') . '/' . $this->product_id.'_'.$this->id.'.jpg');
        Storage::move('/public/'. env('STORAGE_PRODUCT_IMAGES') . '/_'.$this->id.'_sm.jpg','/public/'. env('STORAGE_PRODUCT_IMAGES') . '/' . $this->product_id.'_'.$this->id.'_sm.jpg');
    }

    /**
     * return name of small image
     * @return string 
     */
    public function getImageSmAttribute()
    {
        $image_sm = explode('.', $this->image);
        if(count($image_sm) > 1){
            return $image_sm[0] . '_sm.' . $image_sm[1];
        }
        return 'storage'. env('STORAGE_PRODUCT_IMAGES') . '/' . $this->product_id.'_'.$this->id.'_sm.jpg';
    }


    /**
     * return name of small image
     * @return string 
     */
    public function getImageLgAttribute()
    {
        $image_sm = explode('.', $this->image);
        if(count($image_sm) > 1){
            return $this->image;
        }
        return 'storage/' . env('STORAGE_PRODUCT_IMAGES') . '/' . $this->product_id.'_'.$this->id.'.jpg';
    }
}