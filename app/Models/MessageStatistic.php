<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageStatistic extends Model
{
    protected $table = 'messages_statistics';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $fillable = [
        'products_beacon_id',
        'product_advertising_id',
        'views',
        'issues',
        'position',
        'created_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function products_beacon()
    {
        return $this->belongsTo('App\Models\ProductsBeacon','products_beacon_id');
    }

    public function product_advertising()
    {
        return $this->belongsTo('App\Models\ProductAdvertising','product_advertising_id');
    }

    public function product()
    {

        switch ( $this->position ) {
            case 'masive_notification':
                return $this->product_advertising;
                break;
            case 'far':
                return $this->products_beacon->product_far;
                break;
            case 'close':
                return $this->products_beacon->product_close;
                break;
            default:
                return null;
                break;
        }
    }

    public static function findOrCreate($data = [])
    {
        $statistic = self::where([
            'products_beacon_id' => $data['products_beacon_id'],
            'position' => $data['position']
        ])->first();

        if ($statistic) {
            return $statistic;
        }

        $statistic = new MessageStatistic();
        $statistic->save([
            'products_beacon_id' => $data['products_beacon_id'],
            'position' => $data['position'],
            'issues' => 0,
            'views' => 0,
            'created_at' => date('Y-d-m H:i:s')
        ]);
        return $statistic;
    }

    public function issuedPlus($n = 0)
    {
        $this->issues = $this->issues + $n;
//        return $this;
    }

    public function getTotalIssuesAttribute()
    {
        if($this->position != 'masive_notification'){
            $total_issues = MessageStatistic::where('products_beacon_id', $this->products_beacon_id )->get();
            return $total_issues->sum('issues');
        }else{
            return $this->issues;
        }
    }

    public function getTotalViewsAttribute()
    {
        if($this->position != 'masive_notification'){
            $total_issues = MessageStatistic::where('products_beacon_id', $this->products_beacon_id )->get();
            return $total_issues->sum('views');
        }else{
            return $this->views;
        }
    }

    public function getTotalSellsAttribute()
    {
        if($this->position != 'masive_notification'){
            try {
                return $this->products_beacon->product_close->sells() + $this->products_beacon->product_far->sells();
            } catch (\Exception $e) {
                return 0;
            }
        }else{
            return $this->product_advertising ? $this->product_advertising->sells() : 0;
        }
    }

    public function getPositionFormattedAttribute()
    {
        switch ( $this->position ) {
            case 'far':
                return 'Lejana';
                break;
            case 'close':
                return 'Cercana';
                break;
            default:
                return '-';
                break;
        }
    }
}
