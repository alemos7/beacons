<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPoint extends Model
{
    protected $fillable = [
        'shop_id',
        'user_id',
        'point',
    ];
    
    public static function saveUserPoints($data){
        
        $UPM = self::where([
            ["user_id", "=", $data['user_id']],
            ["shop_id", "=", $data['shop_id']]
        ])->first();
        
        if(!$UPM){
            self::create($data);
        }else{
            $UPM->point = intval($UPM->point) + $data["point"];
            $UPM->save();
        }
        
    }
    
}
