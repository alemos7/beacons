<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ProductAdvertising extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_advertising';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'content',
        'active',
    ];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'date_send',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function product(){ 
        return $this->belongsTo('App\Models\Product');
    }
    
     /**
     * Update ProductAdvertising in the database.
     *
     * @param  array  $attributes
     * @param  array  $options
     * @return bool
     */
    public function update(array $attributes = [], array $options = [])
    {
        $attributes['active'] = (array_key_exists ( 'active',  $attributes ))? true : false;
        return $this->fill($attributes)->save();
    }

     /**
     * Send Message.
     *
     * 
     */ 
    public function send()
    {

    }
    /**
    * Get Date Formated
    *
    * @return date
    */
    public function getDateSendFormatedAttribute(){
        return  Carbon::parse($this->date_send)->format('d-m-Y'); 
    }
}
