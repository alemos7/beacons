<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Califications extends Model
{
    protected $fillable = [
        'shop_id',
        'product_id',
        'user_id',
        'payment_id',
        'stars',
        'comment'
    ];
    
    public static function createCalification($data){
        
        return self::create($data);
        
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public static function getCalificationByShop($shop_id){

        $sql = "
            SELECT 
              C.comment AS text,
              C.stars,
              DATE_FORMAT(C.created_at, '%d/%m/%Y') AS date,
              (
                IF(
                  U.first_name IS NULL,
                  U.email,
                  U.first_name
                )
              ) AS user
            FROM
              califications C 
              LEFT JOIN users U 
                ON C.user_id = U.id
            WHERE C.shop_id = {$shop_id}
        ";

        return DB::select ($sql);

    }
    
    public static function getByPaymentId($id){
        
        $calification = self::where("payment_id", $id)->select("stars", "comment")->first();
        
        if($calification){
            return $calification->toArray();
        }else{
            return null;
        }
        
    }
    
}
