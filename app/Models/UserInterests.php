<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserInterests extends Model
{
    protected $table = 'user_interests';
    
    protected $fillable = [
          'user_id',
          'not_interesed_cats_id',
    ];
    
    public static function getInterestsByUserId($id){
        
        return self::where("user_id", $id)->first();
        
    }
    
    public static function setInterests($id, $interests){
        
        $cats = array();
        
        foreach($interests as $key => $value){
            if(!$value){
                $cats[] = $key;
            }
        }
        
        $catsIds = count($cats) ? implode(',', $cats) : null;
        
        DB::insert("INSERT INTO user_interests (user_id, not_interesed_cats_ids) VALUES({$id}, '{$catsIds}') ON DUPLICATE KEY UPDATE    
                    not_interesed_cats_ids='{$catsIds}'");
        
    }
        
}
