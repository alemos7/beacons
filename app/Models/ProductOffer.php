<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ProductOffer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product_offers';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'algorithm',
        'name',
        'vars',
        'product_offer_id',
        'before_offer_price',
        'created_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongTo
     */
    public function product()
    {
    	return $this->belongTo('App\Models\Product');
    }
    /**
     * @return json_decode
     */
    public function getVariablesAttribute($val)
    {
        return json_decode($this->vars);
    }
    /**
     * @return json_decode
     */
    public function getDescuentoAttribute()
    {
        return $this->getVarsByKey('descuento');
    }
    /**
     * @return json_decode
     */
    public function getLleveAttribute()
    {
        return $this->getVarsByKey('lleve');
    }
    /**
     * @return json_decode
     */
    public function getPagueAttribute()
    {
        return $this->getVarsByKey('pague');
    }

    private function getVarsByKey($key)
    {
        if($this->vars == '') return 0;
        $vars = json_decode($this->vars);
        if(!array_key_exists($key, $vars ) )
            return 0;
        else
            return $vars->$key;
    }

    public function getOfferDateAttribute()
    {
        return  Carbon::parse($this->created_at)->format('d/m/Y') . ' - ' . Carbon::parse($this->updated_at)->format('d/m/Y');
    }
}