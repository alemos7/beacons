<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ImagesClean extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'images:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean all images not relation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        // Negocios
        $images =  \App\Models\ShopImage::where('shop_id',null)->get();
        $contShopImages =  $this->deleteImages($images);
        $this->comment('Imagenes de negocios eliminados: '.$contShopImages);
        
        // Productos
        $images =  \App\Models\ProductImage::where('product_id',null)->get();
        $contProductImages = $this->deleteImages($images);
        $this->comment('Imagenes de productos eliminados: '.$contProductImages);
     
        $this->comment('Total de elementos eliminados: '.($contShopImages + $contProductImages) );
    }

    private function deleteImages($images)
    {
        $i = 0;
        foreach($images as $image){
            $i++;
            $image->delete();
        }
        return $i;
    }
}
