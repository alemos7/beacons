<?php

namespace App\Http\Middleware;

use Closure;

class ApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->hasApiKey($request)) {
          return $next($request);
        }
        return response(null, 401);
    }

    private function hasApiKey(\Illuminate\Http\Request $req)
    {
      $api_key = $req->header('api-key') ?
        $req->header('api-key') :
        $req->query('api-key');

      return env('API_KEY') == $api_key;
    }
}
