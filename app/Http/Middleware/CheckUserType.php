<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth; 

use Closure;

class CheckUserType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  array of arguments (En este caso son strings con nombres de tipos de usuarios que tienen acceso)
     * @return mixed
     */
    public function handle($request, Closure $next, ...$type)
    {
        $user = Auth::user();

        if (!in_array($user->type, (array) $type)) {
            return redirect('/home');
        }

        return $next($request);
    }
}
