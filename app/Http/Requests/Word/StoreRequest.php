<?php

namespace App\Http\Requests\Word;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Response;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the category is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'word'   =>  'required|max:100|unique:categories,id,'.$this->category,
        ];
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->messages();
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'word.required' =>   trans('validation.required',['attribute'=> trans('models.word') ]),
            'word.unique'  =>  trans('validation.unique',['attribute'=> trans('models.word') ]),
            'word.max'  =>  trans('validation.max.string',['attribute'=> trans('models.word') ]),
        ];
    }

}
