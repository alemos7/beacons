<?php

namespace App\Http\Requests\ProductImage;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Response;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'images'  =>  'required | mimes:jpeg,jpg,png',
        ];
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->messages();
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'product_id.required' =>   trans('validation.required',['attribute'=> trans('models.product') ]),

            'image.required' =>   trans('validation.required',['attribute'=> trans('models.image') ]),

            'description.required' =>  trans('validation.required',['attribute'=> trans('data.description') ]),
        ];
    }
}
