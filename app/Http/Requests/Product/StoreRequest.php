<?php

namespace App\Http\Requests\Product;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Response;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shop_id' => 'required',
            'name'  =>  'required|max:100',
            'price' => 'required|numeric',
            'description' => 'required|max:1000',
            'code' => 'max:50',
        ];
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->messages();
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'shop_id.required' =>   trans('validation.required',['attribute'=> trans('models.shop') ]),

            'name.required' =>   trans('validation.required',['attribute'=> trans('data.name') ]),
            'name.max'  =>  trans('validation.max.string',['attribute'=> trans('data.name') ]),

            'price.required' =>  trans('validation.required',['attribute'=> trans('data.price') ]),
            'price.numeric'  => trans('validation.numeric',['attribute'=> trans('data.price') ]),
            
            'description.required' =>  trans('validation.required',['attribute'=> trans('data.description') ]),
            'description.max'  =>  trans('validation.max.string',['attribute'=> trans('data.description') ]),

            //'code.required' => trans('validation.required',['attribute'=> trans('data.code') ]),
            'code.max'  =>  trans('validation.max.string',['attribute'=> trans('data.code') ]),
        ];
    }
}
