<?php

namespace App\Http\Requests\Category;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Response;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the category is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'   =>  'required|unique:categories,id,'.$this->category,
            'logo'   =>  'required',
            'color'   =>  'required',
        ];
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->messages();
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' =>   trans('validation.required',['attribute'=> trans('data.name') ]),
            'name.unique'  =>  trans('validation.unique',['attribute'=> trans('data.name') ]),
          
            'logo.required' =>   trans('validation.required',['attribute'=> trans('data.logo') ]),

            'color.required' =>   trans('validation.required',['attribute'=> trans('data.color') ]),
        ];
    }

}
