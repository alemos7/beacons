<?php

namespace App\Http\Requests\Shop;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Response;
// Models
use App\Models\Shop;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    { 
        return [
            'name'  =>  'required|max:100',
            'business_name' => 'required|max:150',
            'cuit'  =>  'nullable|max:11',
            'vip'   =>  'required',
            'iibb'  =>  'nullable|numeric',
            'iva'   =>  'required',
            'province_id' => 'required',
            'locality_id' => 'required',
            'lat'   =>  'required',
            'lng'   =>  'required',
            'address'   =>  'required',
            'email' =>  'email|max:100',
            'phone' =>  'required',
            'schedule'  =>  'max:100'
        ];
    }

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->messages();
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' =>   trans('validation.required',['attribute'=> trans('data.name') ]),
            'name.max'  =>  trans('validation.max.string',['attribute'=> trans('data.name') ]),

            'business_name.required' =>  trans('validation.required',['attribute'=> trans('data.business_name') ]),
            'business_name.max'  => trans('validation.max.string',['attribute'=> trans('data.business_name') ]),

            'cuit.require' => trans('validation.required',['attribute'=> trans('data.cuit') ]),
            'cuit.max' => trans('validation.max.string',['attribute'=> trans('data.cuit') ]),
            'cuit.unique' => trans('validation.unique',['attribute'=> trans('data.cuit') ]),

            'vip.required' =>  trans('validation.required',['attribute'=> trans('data.vip') ]),

            'iibb.required' =>  trans('validation.required',['attribute'=> trans('data.iibb') ]),
            'iibb.numeric' =>  trans('validation.numeric',['attribute'=> trans('data.iibb') ]),

            'iva.required' =>  trans('validation.required',['attribute'=> trans('data.iva') ]),

            'province_id.required'  => trans('validation.required',['attribute'=> trans('data.provinces') ]),

            'locality_id.required' => trans('validation.required',['attribute'=> trans('data.locality') ]),
            
            'address.required' =>  trans('validation.required',['attribute'=> trans('data.locality') ]),

            'email.required' =>  trans('validation.required',['attribute'=> trans('data.email') ]),
            'email.email' => trans('validation.email',['attribute'=> trans('data.email') ]),
            'email.max' => trans('validation.max.string',['attribute'=> trans('data.email') ]),

            'status.required'  =>  trans('validation.required',['attribute'=> trans('data.status') ]),

            'phone.required' =>  trans('validation.required',['attribute'=> trans('data.phone') ]),

            'schedule.required'  =>  trans('validation.required',['attribute'=> trans('data.schedule') ]),
            'schedule.max'  =>  trans('validation.max.string',['attribute'=> trans('data.schedule') ])
        ];
    }
}
