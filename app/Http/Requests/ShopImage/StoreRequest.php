<?php

namespace App\Http\Requests\ShopImage;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Response;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shop_id' => 'required',
            'images' => 'required | mimes:jpeg,jpg,png',
        ];
    }

    protected function formatErrors(Validator $validator)
    {
        return 'Error fatal';
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'images.required'=>'Requiere una imagen',
            'images.mimes' => 'Debe ser una imagen de formato valido',
        ];
    }
}