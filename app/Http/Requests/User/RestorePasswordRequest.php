<?php

namespace App\Http\Requests\User;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Response;


class RestorePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' =>  'required|email'/*|exists:users'*/,
        ];
    }

/*
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->messages();
    }
*/

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' =>   trans('validation.required',['attribute'=> trans('data.email') ]),
            'email.max'  =>  trans('validation.max.string',['attribute'=> trans('data.email') ])/*,*/
            /*'email.exists'  =>  trans('validation.exists',['attribute'=> trans('data.email') ]),*/
        ];
    }
}
