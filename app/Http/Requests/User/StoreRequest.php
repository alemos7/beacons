<?php

namespace App\Http\Requests\User;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Response;


class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' =>  'required|max:100',
            'last_name'  =>  'required|max:100', 
            'phone'      =>  'required',
            'type'       =>  'required',
            'birthday'   =>  'required',
            'email'      =>  'required|email|max:100|unique:users', 
        ];
    }    

    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->messages();
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

            'name.required' =>   trans('validation.required',['attribute'=> trans('data.name') ]),
            'name.max'  =>  trans('validation.max.string',['attribute'=> trans('data.name') ]),

            'first_name.required' =>   trans('validation.required',['attribute'=> trans('data.first_name') ]),
            'first_name.max'  =>  trans('validation.max.string',['attribute'=> trans('data.first_name') ]),

            'last_name.required' =>   trans('validation.required',['attribute'=> trans('data.last_name') ]),
            'last_name.max'  =>  trans('validation.max.string',['attribute'=> trans('data.last_name') ]),

            'phone.required' =>   trans('validation.required',['attribute'=> trans('data.phone.mobile') ]),

            'type.required' =>   trans('validation.required',['attribute'=> trans('data.type') ]),

            'birthday.required' =>   trans('validation.required',['attribute'=> trans('data.birthday') ]),
            'birthday.date'  =>  trans('validation.date',['attribute'=> trans('data.birthday') ]),

            'email.required' =>   trans('validation.required',['attribute'=> trans('data.email') ]),
            'email.max'  =>  trans('validation.max.string',['attribute'=> trans('data.email') ]),
            'email.date'  =>  trans('validation.email',['attribute'=> trans('data.email') ]),


        ];
    }
}
