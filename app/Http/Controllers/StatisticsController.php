<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Models
// use App\Models\ShopBeacon;
use App\Models\MessageStatistic;

class StatisticsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=0)
    {       
        if( $id != 0 ){
            $statistics = MessageStatistic::where('id',$id)->get()->first();
            if(isset($statistics->position) &&  $statistics->position != 'masive_notification' ){
                $statistics = MessageStatistic::where('products_beacon_id',$statistics->products_beacon_id )->get();
            }
        }else{
            $statistics = MessageStatistic::with(
                ['product_advertising.product.shop','products_beacon.beacon.shop']
                    )->where('position','close')
                    ->orWhere('position','far')
                    ->orWhere('position','masive_notification')
                    ->groupBy('products_beacon_id')
                    ->get();
        }
        return view('statistics.index',compact('statistics','id'));
    }
}