<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Models
use App\Models\Banner;
use App\Models\Product;
use App\Models\Shop;

class BannerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = Banner::find(1);
        if(!count($banner)){
            $banner = new Banner;
        }
        $shops = Shop::with('products')->get();
        return view('banner.show',compact('banner','shops'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $banner = new Banner();
        $banner->update( $request->all() );
        flash( trans('process.success',['model' => trans('models.banner.article') , 'process' => trans('process.created') ]) , 'success');
        return redirect('banners');    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $banner = Banner::find(1);
        $banner->update( $request->all() );
        flash( trans('process.success',['model' => trans('models.banner.article') , 'process' => trans('process.updated') ]) , 'success');
        return redirect('banners');  
    }
}
