<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Word\StoreRequest;
// Models
use App\Models\Word;

class WordController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user.type:administrator');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $words = Word::all();
        return view('word.index',compact('words'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Word $word)
    {
        $title = trans('process.create').' '.trans('models.word');
        return view('word.form', compact('word','title'));
    }

    /**
     * @param Request $request
     * @param Word $word
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreRequest $request)
    {
        $word = new Word(); 
        $word->update($request->all());     
        if($request->ajax()){
            return $word;
        }else{
            flash( ucfirst( trans('process.success',['model' => trans('models.word.article') , 'process' => trans('process.created') ]) ), 'success');
            return redirect('words');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $word = Word::find($id);
        $title = trans('process.update').' '.trans('models.word');
        return view('word.form',compact('word','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
        $word = Word::find($id);
        $word->update($request->all());
        flash( trans('process.success',['model' => trans('models.word.article') , 'process' => trans('process.updated') ]) , 'success');
        return redirect('words');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Word::find($id)->destroy($id);
        flash( trans('process.success',['model' => trans('models.word.article') , 'process' => trans('process.deleted') ]), 'success');
        return redirect('words');
    }
}
