<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
// Models
use App\Models\Content;

class HelpController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->type != 'administrator') {return redirect('/');}
        $content = Content::find(1);

        if(!$content){
            $content = new Content();
        }

        $help = $content->help;

        return view('help.index',compact('help'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->type != 'administrator') {return redirect('/');}

        $content = Content::find(1);

        if(!$content)
        {
            $content = new Content();
        }
        $content->help = $request->help;

        $content->save();

        return $this->index();
    }
}
