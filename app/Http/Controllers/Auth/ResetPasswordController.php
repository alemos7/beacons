<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function resetPassword($user, $password)
    {
        $user->forceFill(['password' => bcrypt($password), 'remember_token' => Str::random(60)])->save();
        //$this->guard()->login($user);
    }

    public function sendResetResponse($response)
    {
        return view('auth.passwords.result',[
            'message' => 'Ya puede iniciar sesion en la aplicacion con la nueva contraseña.'
        ]);
    }


    public function sendResetFailedResponse($request, $response)
    {
        return view('auth.passwords.result',[
            'error' => 'No se pudo reestablecer la contraseña: el email es inválido o el link de recuperación de contraseña expiró.'
        ]);
    }

}
