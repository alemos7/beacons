<?php

namespace App\Http\Controllers;

use App\Http\Requests\Message\StoreRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
// Models
use App\Models\MessageStatistic;
use App\Models\ProductAdvertising;
use App\Models\Product;
use App\Models\Shop;
use App\Models\User;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use App\Models\UsersNotifications;

class MessageController extends Controller
{    
     /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
     	$this->middleware('auth');
         //ini_set('xdebug.max_nesting_level', 200);
         //Hola, si te tira error de nesting max, setea esto en php.ini o descomenta esta linea,
         // el framework nestea muchas funciones cuando usas las querys compuestas y si tenes xdebug desactualizado te puede dar error
     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(Auth::user()->type == 'operator')
            $messages = $this->selectByUser();
        else
            $messages = ProductAdvertising::with('product')->get();
        return view('message.index',compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {       
        if(Auth::user()->type == 'operator')
            $shops = Auth::user()->shops()->orderBy('name','asc')->get();
        else
            $shops = Shop::orderBy('name','asc')->get();
        $message = new ProductAdvertising();
        $post = url('messages');
        return view('message.show',compact('message','shops','post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        if(!$this->getPermission($request->product_id))      
            return redirect('/messages');
        $message = new ProductAdvertising();
        $message->update($request->all());
        flash( trans('process.success',['model' => trans('models.message.article') , 'process' => trans('process.created') ]), 'success');
        return redirect('messages');    
    }
    
    public function send(Request $request, $id){
        
        try{
            
            $options = $request->all();
            
            $message = ProductAdvertising::find($id);

            $productModel = Product::with(['shop', 'shop.images', 'category', 'images', 'offer', 'califications', 'shop.califications', 'shop.califications.user'])->has('images')
                    ->where("id", $message->product_id)
                    ->active();

            $productArray = Product::apiFormatIndex($productModel->get()->toArray());
            $product = $productArray[0];

            $product["shop"]["lat"] = floatval($product["shop"]["lat"]);
            $product["shop"]["lng"] = floatval($product["shop"]["lng"]);

            $statistic = MessageStatistic::where([
                ['product_advertising_id', "=", $id],
                ['position', "=", 'masive_notification']
            ])->first();

            if(!$statistic){
                $statistic = new MessageStatistic([
                    'product_advertising_id' => $id,
                    'position' => 'masive_notification',
                    'issues' => 1
                ]);
                $statistic->save();
            }else{
                $statistic->issuedPlus(1);
                $statistic->save();
            }       

            $data = [
                'title' => $product["shop"]["name"],
                'product' => $product,
                'type' => 'product',
                'statistic_id' => $statistic->id,
                'advertising' => $message->content
            ];

            $usersModel = User::with(["settings", "interests"])
                    ->whereNotNull('register_id');

            if($options["register-method"] != 'todos')
                $usersModel->where('from', '=', $options["register-method"]);
            
            if($options["gender"] != 'todos'){                
                $usersModel->where('gender', '=', $options["gender"]);                          
                $usersModel->orWhere('gender', 'NULL');
                $usersModel->orWhere('gender', '');
            }
            
            $users = $usersModel->get();
            
            $messageDataParsed = $this->parseMessageForPushNotification($data);
            
            $pushNotificationMessageAndroid = PushNotification::Message($message->content, $messageDataParsed);
            $pushNotificationMessageIOS = PushNotification::Message($message->content, [
                'custom' => $this->parseMessageForIosPushNotification($messageDataParsed),
                'badge' => 1
            ]);
                    
            $toAndroid = array();
            $toIos = array();
//            die(var_dump($users->toArray()));
            foreach($users as $u){

                $send = true;
                if($u->interests){

                    $interestsCatIds = explode(",", $u->interests["not_interesed_cats_ids"]);

                    foreach($interestsCatIds as $ici){                   
                        if($product["category_id"] == $ici)
                            $send = false;
                    }               
                }
                
                if($options["age"] != 'todos'){
                    
                    $birthDay = strtotime(str_replace('/', '-', $u->birthday));
                    
                    switch($options["age"]){
                        case 'teen':
                            if( $birthDay < strtotime("-20 years")){
                                $send = false;
                            }
                            break;
                        case 'young':
                            if( $birthDay > strtotime("-20 years") || $birthDay < strtotime("-30 years")){
                                $send = false;
                            }
                            break;
                        case 'adult':
                            if( $birthDay > strtotime("-30 years") || $birthDay > strtotime("-40 years")){
                                $send = false;
                            }
                            break;
                        case 'big-adult':
                            if( $birthDay > strtotime("-40 years")){                                
                                $send = false;
                            }
                            break;
                    }
                }

                if( ($u->settings == NULL || $u->settings->notification_massive) && $send ){
                    
                    $userNotificationsModel = new UsersNotifications([
                        'user_id' => $u->id,
                        'prod_id' => $product["id"],
                        'statistic_id' => $statistic->id,
                        'advertising' => $message->content
                    ]);
                    $userNotificationsModel->save();
                    
                    switch ($u->device_os){
                        case 'ios':
                            $toIos[] = PushNotification::Device($u->register_id);                        
                            break;
                        case 'android':
                            $toAndroid[] = PushNotification::Device($u->register_id);                        
                            break; 
                    }
                }            
            }

            $devicesAndroid = PushNotification::DeviceCollection($toAndroid);
            $devicesIOS = PushNotification::DeviceCollection($toIos);
            
            PushNotification::app('android')
                              ->to($devicesAndroid)
                              ->send($pushNotificationMessageAndroid);   
            
            PushNotification::app('ios')
                              ->to($devicesIOS)
                              ->send($pushNotificationMessageIOS); 

            ProductAdvertising::where("id", $id)->update([
                "date_send" => date("Y-m-d")
            ]);
            
            return redirect('messages');
            
        }catch(\Exception $e){
            return redirect('messages');
        }
        
    }
    
    private function parseMessageForIosPushNotification($data){
        
        $data["product"] = $data["product"]["id"];
        
        return $data;
        
    }
    
    private function parseMessageForPushNotification($data){
//        die(var_dump($data));
        unset($data["product"]["created_at"]);
        unset($data["product"]["updated_at"]);
        unset($data["product"]["deleted_at"]);
        unset($data["product"]["code"]);
        unset($data["product"]["shop"]["created_at"]);
        unset($data["product"]["shop"]["updated_at"]);
        unset($data["product"]["shop"]["deleted_at"]);
        
        $data["product"]["offer"] = [array_shift($data["product"]["offer"])];
        
        return $data;        
        
    }
    
    /**
     * Display the specified resource.z
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {       
        if(Auth::user()->type == 'operator')
            $shops = Auth::user()->shops()->orderBy('name','asc')->get();
        else
            $shops = Shop::orderBy('name','asc')->get();
        $message = ProductAdvertising::find($id);
        $post = url('messages/'.$message->id);
        return view('message.show',compact('message','shops','post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
        if($this->getPermission($request->product_id)){
           $message = ProductAdvertising::find($id);
           $message->update($request->all());
           flash( trans('process.success',['model' => trans('models.message.article') , 'process' => trans('process.updated') ]) , 'success');
       }
       return redirect('messages');   
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->getPermission(0)){
         ProductAdvertising::find($id)->destroy($id);
         flash( trans('process.success',['model' => trans('models.message.article') , 'process' => trans('process.deleted') ]), 'success');
     }
     return redirect('messages');        
 }

    private function selectByUser()
    {
        $userId = Auth::user()->id;
        $messages =  ProductAdvertising::whereHas('product.shop.users', function ($query) use ($userId){
            $query->where('user_id','=', $userId);
        })->get();
        return $messages;
    }

    private function getPermission($id)
    {
        if(Auth::user()->type == 'administrator')
            return true;
        $products = Auth::user()->products();
        foreach ($products as $product) {
            if($product->id == $id)
                return true;
        }
        flash( trans('process.alert.denied') , 'danger');
        return false;
    }
}