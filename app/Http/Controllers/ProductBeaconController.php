<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// Models
use App\Models\ShopBeacon;
use App\Models\ProductsBeacon;
use App\Models\Shop;
use App\Models\Product;

class ProductBeaconController extends Controller
{
    /**
     * Display the specified resource.z
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shopBeacon = ShopBeacon::find($id);
        if (count($shopBeacon->products_around))
            $productsBeacon = $shopBeacon->products_around;
        else
            $productsBeacon = new ProductsBeacon;

        $post = url('products-beacon'.$shopBeacon->id);
        $shopId = $shopBeacon->shop_id;
        $products = Product::where('shop_id',$shopId)->orderBy('name','asc')->get();
        
        return view('shopbeacon.products-beacon',compact('productsBeacon','shopBeacon','post','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $shopBeacon = ShopBeacon::find($id);
        if (count($shopBeacon->products_around))
            $shopBeacon->products_around->update($request->all());
        else{
            $request['product_close_id']  = ($request['product_close_id']  == 0) ? null : $request['product_close_id'] ;
            $request['product_far_id'] = ($request['product_far_id'] == 0) ? null : $request['product_far_id'];
            $shopBeacon->products_around()->create($request->all());

        }
        
        if( !$request['product_close_id'] || 
            !$request['product_far_id'] ){
            // Si las ofertas son `null` se desactiva el beacon
            $shopBeacon->active = false;
        }else{
            $shopBeacon->active = true;
        }
        $shopBeacon->save();
        flash( trans('process.success',['model' => trans('models.beacon.article') , 'process' => trans('process.updated') ]) , 'success');
        return redirect('beacons');
    }
}
