<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\StoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// Models
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductOffer;
use App\Models\Shop;
use App\Models\Category;
use App\Models\MP_Category;
use App\Models\Word;
use App\Models\User;

class ProductController extends Controller
{    
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('user.type:administrator,seller');
    }   

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!$this->getPermission(0)) {
            return redirect('/');
        }

        $data = array();

        if(Auth::user()->type == 'operator' || Auth::user()->type == 'seller') {
            $categories = Category::all();
            $products = Auth::user()->products();

        }
        else {
            $categories = Category::all();
            $products = Product::with(['offer', 'shop', 'category'])->get();

        }
        $data['categories'] = $categories;
        $data['products'] = $products;

        return view('product.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!$this->getPermission(0))
            return redirect('/');


        if(Auth::user()->type == 'operator' || Auth::user()->type == 'seller') {
            $shops = Shop::accesible()->orderBy('name', 'asc')->get();
        }else {
            $shops = Shop::orderBy('name', 'asc')->get();
        }

        $product = new Product();
        $categories = Category::orderBy('name','asc')->get();
        $title = trans('process.create');
        $post = url('products');
        $offer = new ProductOffer;
        $words = Word::all();
        $mp_categories = MP_Category::orderBy('trans','asc')->get();
        return view('product.show',compact('product','shops','categories','title','post','offer','words','mp_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        if(!$this->getPermission(0))
            return redirect('/products');


        $product = new Product;
        $product->update($request->all());
        $cont = 0;
        $images = ProductImage::where('product_id',null)->where('image',$request->_token)->get();
        if( count($images) ){
            $attributes = [];
            $attributes['product_id'] = $product->id;
            foreach ($images as $image) {
                $image->update($attributes);
                $image->updateFileNames();
            }
        }
        flash( trans('process.success',['model' => trans('models.product.article') , 'process' => trans('process.created') ]) , 'success');
        return redirect('products/'.$product->id.'/set-image');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!$this->getPermission($id))      
            return redirect('/products');
        
        
        if(Auth::user()->type == 'operator' || Auth::user()->type == 'seller')
            $shops = Shop::accesible()->orderBy('name', 'asc')->get();
        else
            $shops = Shop::orderBy('name','asc')->get();
        
        $product = Product::find($id);
        $categories = Category::orderBy('name','asc')->get();
        $title = trans('process.update');
        $post = url('products/'.$product->id);
        $offer =  count($product->offer)? $product->offer[count($product->offer)-1] : new ProductOffer;
        $words = Word::all();
        $mp_categories = MP_Category::orderBy('trans','asc')->get();
        return view('product.show',compact('product','shops','categories','title','post','offer','words','mp_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {   
        if($this->getPermission($id)){
            $product = product::find($id);
            $product->update($request->all());
            
            flash( trans('process.success',['model' => trans('models.product.article') , 'process' => trans('process.updated') ]), 'success'); 
        }
        return redirect('products');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sortImages(Request $request)
    {
        if($request->ajax()){
            $order = 0;
            foreach ($request->images as $imageId) {
                $order++;
                $image = ProductImage::find($imageId['value']);
                $image->order = $order;
                $image->save(); 
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->getPermission($id)){
            $product = Product::find($id);
            // Delete all images of product
            // foreach ($product->images as $productImage){
            //         $productImage->delete();
            // }
            $product->delete();
            flash( trans('process.success',['model' => trans('models.product.article') , 'process' => trans('process.deleted') ]), 'success');
        }
        return redirect('products');
    }

    private function getPermission($id)
    {
        if(Auth::user()->type == 'administrator')
            return true;

        if(Auth::user()->type == 'operator' || Auth::user()->type == 'seller')
        {
            if ($id == 0)
            {
                return true;
            }

            $products = Auth::user()->products();
            foreach ($products as $product) {
                if($product->id == $id)
                    return true;
            }
        }

        flash( trans('process.alert.denied') , 'danger');
        return false;
    }

    public function setActiveStatus(Request $request, $id)
    {
        $product = product::find($id);

        $product->active = $request->active;

        $product->save();

        flash( trans('process.success',['model' => trans('models.shop.article') , 'process' => trans('process.updated') ]) , 'success');

        return redirect('products');
    }

    public function setImageAfterCreate(Request $request, $id)
    {
        if(Auth::user()->type == 'operator') {
            $shops = Auth::user()->shops()->orderBy('name', 'asc')->get();
        }
        else {
            $shops = Shop::orderBy('name', 'asc')->get();
        }

        $product = Product::find($id);
        $categories = Category::orderBy('name','asc')->get();
        $title = trans('process.update');
        $post = url('products/'.$product->id);
        $offer =  count($product->offer)? $product->offer[count($product->offer)-1] : new ProductOffer;
        $words = Word::all();
        $mp_categories = MP_Category::orderBy('trans','asc')->get();
        return view('product.imageForm',compact('product','shops','categories','title','post','offer','words','mp_categories'));
    }

    public function checkIfHaveImages($id){
        $product = new ProductImage();
        $imgs = $product->where('product_id', $id)->count();

        if($imgs > 0){
            return response()->json([
                'success' => true
            ]);
        }

        return response()->json([
            'error' => 'El producto debe tener al menos una imagen para estar activo'
        ]);
    }
}
