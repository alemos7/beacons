<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use Hash;
// Models
use App\Models\User;

class UserController extends Controller
{    
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user.type:administrator,seller');
    }
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $users = User::all();
        $users = User::accesible()->where('type','<>','user')->get();
        return view('user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User;
        $user->birthday = date("d/m/Y"); 
        $title = trans('process.create');
        $post = url('users');
        return view('user.form',compact('user','title','post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $request['active'] = ($request->active == 'on')? true : false;
        $user = new User();
        if($request['password']){
            $request['password'] = Hash::make($request['password']);
        }
        $user->fill( $request->all() );

        if (\Auth::user()->type == 'seller')
        {
            if ($request['type'] !== 'operator')
            {
                throw new \Exception('Solo puede crear usuarios operadores');
            }
        }

        $user->creator()->associate(\Auth::user());

        $user->save();

        if($user->type=='user'){
            flash( trans('process.success',['model' => trans('models.client.article') , 'process' => trans('process.created') ]), 'success');
            return redirect('clients');
        }else{
            flash( trans('process.success',['model' => trans('models.user.article') , 'process' => trans('process.created') ]), 'success');
            return redirect('users');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::accesible()->where('id',$id)->firstOrFail();
        $title = trans('process.update');
        $post = url('users/'.$user->id);
        return view('user.form',compact('user','title','post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $request['active'] = ($request->active == 'on')? true : false;
        $user = User::find($id);  
        if($request['password'])
        {
            $request['password'] = Hash::make($request['password']);
        }

        if (\Auth::user()->type == 'seller')
        {
            if ($request['type'] !== 'operator')
            {
                throw new \Exception('Solo puede crear usuarios operadores');
            }
        }

        $user->fill( $request->all() ); 
        $user->save();

        if($user->type=='user'){
            flash( trans('process.success',['model' => trans('models.client.article') , 'process' => trans('process.updated') ]) , 'success');
            return redirect('clients');
        }else{
            flash( trans('process.success',['model' => trans('models.user.article') , 'process' => trans('process.updated') ]) , 'success');
            return redirect('users');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$this->middleware('user.type:administrator'); No funciona fuera del constructor :|

        if (\Auth::user()->type !== 'administrator')
        {
            throw new \Exception('Solo el admin puede eliminar usuarios');
        }

        $user = User::find($id);
        $type = $user->type;
        $user->destroy($id);

        if($type=='user'){
            flash( trans('process.success',['model' => trans('models.client.article') , 'process' => trans('process.deleted') ]) , 'success');
            return redirect('clients'); 
        }else{
            flash( trans('process.success',['model' => trans('models.user.article') , 'process' => trans('process.deleted') ]) , 'success');
            return redirect('users'); 
        }
    }
}