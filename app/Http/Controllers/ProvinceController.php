<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// Models
use App\Models\Province;

class ProvinceController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    
	/**
     * Display localities of specified province.
     *
     * @param  int  $province_id
     * @return \Illuminate\Http\Response
     */
    public function showLocalities($province_id)
    {
    	$province = Province::find($province_id);
    	return response()->json($province->localities()->orderBy('name','asc')->get());
    }
}
