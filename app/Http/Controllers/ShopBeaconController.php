<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShopBeacon\StoreRequest;
use App\Models\MessageStatistic;
use Illuminate\Support\Facades\Auth;
// Models
use App\Models\ShopBeacon;
use App\Models\Shop;
use App\Models\Product;

class ShopBeaconController extends Controller
{    
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shopBeacons = ShopBeacon::with([
            'shop',
            'product.offer',
            'products_around.product_close',
            'products_around.product_far'
        ])->accesible()->get();

        return view('shopbeacon.index', compact('shopBeacons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        if(!$this->getPermission(0))      
            return redirect('/beacons');

        $shops = Shop::accesible()->where('active', 1)->orderBy('name','asc')->get();
        $shopBeacon = new ShopBeacon();
        //$shopBeacon->serial = $this->get_next_serial();
        $title = trans('process.create');
        $post = url('beacons');
        return view('shopbeacon.show',compact('shopBeacon','shops','products','title','post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $shopBeacon = new ShopBeacon();
        $input = $request->input();
        $input['shop_id'] = $request->get('shop_id', null) > 0 ? $request->get('shop_id', null) : null;
        $shopBeacon->updateModified($input);
//        $shopBeacon->active = false;
        $shopBeacon->save();

        flash( trans('process.success',['model' => trans('models.beacon.article') , 'process' => trans('process.created') ]), 'success');
        return redirect('beacons');   
    }

    /**
     * Display the specified resource.z
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        if(!$this->getPermission($id))
            return redirect('/beacons');

        $shopBeacon = ShopBeacon::find($id);

        $shops = Shop::accesible()->where('active', 1)->orderBy('name','asc')->get();

        if(Auth::user()->type == 'operator'){
            //$shops = Auth::user()->shops()->orderBy('name','asc')->get();
            $products = Auth::user()->products();
        }
        else
        {
            //$shops = Shop::orderBy('name','asc')->get();
            $products = Product::orderBy('name','asc')->get();
        }
        $title = trans('process.update');
        $post = url('beacons/'.$shopBeacon->id);
        
        return view('shopbeacon.show',compact('shopBeacon','title','post','shops','products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
        if($this->getPermission($id))
        {
            $new_shop_id = $request->get('shop_id', 0) == 0 ? null : $request->get('shop_id');

            $shopBeacon = ShopBeacon::accesible()->findOrFail($id);

            /* inicio Quitamos las ofertas del beacon */
            if (count($shopBeacon->products_around) && ($new_shop_id != $shopBeacon->shop_id))
            { //Hay que ver como hacemos con el tema estadisticas, pero por ahora es mejor que no queden datos incoherentes si cambias el shop
                $shopBeacon->products_around->update([
                    'product_close_id' => null,
                    'product_far_id' => null,
                    'product_close_message' => '',
                    'product_far_message' => '',
                ]);

//                $shopBeacon->active = false;
            }
            /* fin Quitamos las ofertas del beacon */

            /* Reseteamos las estadisticas del beacon por cambio de shop */
            $statics = new MessageStatistic;
            $existing = $statics->where('products_beacon_id', $shopBeacon->id)->get();
            if($new_shop_id != $shopBeacon->shop_id && count($existing)){
                $statics->where('products_beacon_id', $shopBeacon->id)->update(['views' => 0, 'issues' => 0]);
            }

            $shopBeacon->fill($request->except(['shop_id']));
            
            if($request->get('serial')){
            
                $serial = explode("/", $request->get('serial'));
                
                $shopBeacon->serial = hexdec($serial[0]) . "/" . hexdec($serial[1]);

            }
            
            $shopBeacon->active = (array_key_exists ( 'active',  $request->all() )) ? true : false;
            
            $shopBeacon->shop_id = $new_shop_id;
            $shopBeacon->save();

            flash(trans('process.success', ['model' => trans('models.beacon.article'), 'process' => trans('process.updated')]), 'success');
            return redirect('beacons');
        }

        return redirect('beacons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->getPermission(0)){
            ShopBeacon::find($id)->destroy($id);
            flash( trans('process.success',['model' => trans('models.beacon.article') , 'process' => trans('process.deleted') ]), 'success');
        }
        return redirect('beacons');        
    }


    private function selectByUser()
    {
        $shops = Auth::user()->shops()->get();
        $beacons = [];
        foreach ($shops as $shop) {
            $beacons = $shop->beacons()->get()->merge($beacons);
        }
        return $beacons;
    }

    private function getPermission($id)
    {
        $user = \Auth::user();

        if($id == 0)
        {
            if(in_array($user->type,['administrator','seller'])){
                return true;
            }
        }

        if (ShopBeacon::accesible()->where('id', $id)->firstOrFail())
        {
            return true;
        }

        flash()->overlay( trans('process.alert.denied') , trans('process.denied') );
        return false;
    }

    public function generateSerial()
    {
        do {
            $serial = $this->beaconSerial();
            $count =  count(ShopBeacon::where('serial',$serial)->get());
        } while ($count > 0);
        return $serial;
    }

    private function beaconSerial()
    {   
        // Los cuatro digitos de la izquierda deben ser mayor 
        // a los cuatro digitos de la derecha.
        $x = 3;
        $min = pow(10,$x);
        $max = pow(10,$x+1)-1;
        $mayor = rand($min, $max);
        $minor = rand($min, $mayor);
        $serial = $mayor.'/'.$minor;
        return $serial;
    }

    private function get_next_serial(){
        $serial = $this->get_last_serial();
        $serial = explode('/', $serial);
        if( count($serial) > 1 ){
            $serial[0] = $serial[0] + 1;
            $serial[1] = $serial[1] + 1;
            $serial[0] = str_pad($serial[0], 4, '0', STR_PAD_LEFT);
            $serial[1] = str_pad($serial[1], 4, '0', STR_PAD_LEFT);
            return $serial[0] . '/' . $serial[1];
        }
    }
    private function get_last_serial(){                
        return \DB::table('shop_beacons')->select(['id','serial'])->orderBy('id', 'DESC')->first()->serial;
    }

}
