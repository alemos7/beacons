<?php

namespace App\Http\Controllers;

use App\Http\Requests\Category\StoreRequest; 
use Illuminate\Http\Request;
// Models
use App\Models\Category;
use App\Models\CategoryImage;

class CategoryController extends Controller
{    
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user.type:administrator');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with('products')->get();
        return view('category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        $category = new Category();
        $category->logo = 'car';
        $title = trans('process.create').' '.trans('models.category');
        $post = url('category');
        return view('category.show',compact('category','title','post'));
    }

    /**
     * @param Request $request
     * @param Category $category
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreRequest $request)
    {
        $category = new Category(); 
        $category->update($request->all());
        $cont = 0;
        $images = CategoryImage::where('category_id',null)->get();
        if( count($images) ){
            $attributes = [];
            $attributes['category_id'] = $category->id;
            foreach ($images as $image) {
                $image->update($attributes);
                $image->updateFileNames();
            }
        }
        flash( ucfirst( trans('process.success',['model' => trans('models.category.article') , 'process' => trans('process.created') ]) ), 'success');
        return redirect('categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);
        $title = trans('process.update').' '.trans('models.category');
        $post = url('category/'.$category->id);
        return view('category.show',compact('category','title','post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreRequest $request, $id)
    {
        $category = Category::find($id);
        $category->update($request->all());
        flash( trans('process.success',['model' => trans('models.category.article') , 'process' => trans('process.updated') ]) , 'success');
        return redirect('categories');
    }

     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sortImages(Request $request)
    {
        if($request->ajax()){
            $order = 0;
            foreach ($request->images as $imageId) {
                $order++;
                $image = CategoryImage::find($imageId['value']);
                $image->order = $order;
                $image->save(); 
            }            
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        // Delete all images of category
        // foreach ($category->images as $categoryImage){
        //         $categoryImage->delete();
        // }
        $category->delete();
        flash( trans('process.success',['model' => trans('models.category.article') , 'process' => trans('process.deleted') ]), 'success');
        return redirect('categories');
    }
}