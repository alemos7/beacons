<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductImage\StoreRequest; 
use App\Http\Requests\ProductImage\UpdateRequest; 
use Illuminate\Support\Facades\Request;
use Storage;
use File;
use Response;
// Models
use App\Models\Product;
use App\Models\ProductImage;

class ProductImageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user.type:administrator,seller');
    }

    /**
     * Display all images from shop
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showByProduct($id)
    {
        if(! is_numeric($id) ){
            $product = new Product();
            $product->images = ProductImage::where('product_id',null)->where('image',$id)->get();
        }else{
            $product = Product::find($id);
        }
        return view('productimage.list',compact('product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        if($request->ajax())
        {
            $request['product_id'] = ($request['product_id']) ? $request['product_id'] : null;

            if (ProductImage::where('product_id', $request['product_id'])->count() >= intval(ENV('STORAGE_PRODUCT_MAX_IMAGES',1))) {
                //Si estas leyendo esto, gracias Juan!

                return Response::json([
                    'error' => 'Cantidad maxima de imagenes (' . intval(ENV('STORAGE_PRODUCT_MAX_IMAGES',1)) . ') superada.'
                ], 500);
            }

            $productimage = new ProductImage();
            $request['file_image'] = $request->images;
            $request['image'] = $request->_token;
            $productimage->update($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $path = storage_path() . '/app/' . env('STORAGE_PRODUCT_IMAGES') . '/' . $id;

        if(!File::exists($path)) abort(404);

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productimage = ProductImage::find($id);
        $productimage->delete();
        flash( trans('process.success',['model' => trans('models.image.article') , 'process' => trans('process.deleted') ]) , 'success');
        return redirect()->back();    
    }
}