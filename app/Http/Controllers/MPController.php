<?php

namespace App\Http\Controllers;

use App\Models\UserPoint;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use SantiGraviano\LaravelMercadoPago\MP;
use App\Models\Payment;
use App\Mail\PaymentMail;
use App\Mail\PaymentMailSeller;
use Davibennun\LaravelPushNotification\Facades\PushNotification;

class MPController extends Controller
{
    
    private $_PS = array(
        'pending' => "Su pago está siendo procesado.",
        'approved' => "Su pago fue aceptado.",
        'in_process' => "Su pago se está procesando.",
        'in_mediation' => "Su pago está en proceso de mediación.",
        'rejected' => "Su pago fue rechazado.",
        'cancelled' => "Su pago fue cancelado.",
        'refunded' => "Su pago fue devuelto.",
        'charged_back' => "Su pago fue devuelto."
    );
    
    public function notifications(Request $request)
    {
      try {

        \Log::info('MP Method');
        
        if (!$request->get('id')) {
          return \Response::json(null, 200);
        }
        
        $mp_payment_id = $request->get('id');
        
        $paymentModel = Payment::where("payments.mp_payment_id", $mp_payment_id)
                ->select(
                    "shops.mp_client_id",
                    "shops.id AS shop_id",
                    "shops.mp_client_secret",
                    "payments.response",
                    "payments.id",
                    "products.point",
                    "users.register_id",
                    "users.id AS user_id")
                ->join('products', 'payments.product_id', '=', 'products.id')
                ->join('users', 'payments.user_id', '=', 'users.id')
                ->join('shops', 'payments.shop_id', '=', 'shops.id')
                ->first();
                
        if($paymentModel){                                  
            
            $paymentData = $paymentModel->toArray();
            
            $MP_MODEL = new MP($paymentData["mp_client_id"], $paymentData["mp_client_secret"]);
            
            $payment_info = $MP_MODEL->get_payment($mp_payment_id);
            
            $payment_status = $payment_info["response"]["collection"]["status"];

            $response = json_decode($paymentData["response"]);
            $response->payment->status = $payment_status;
            
            Payment::where("id", $paymentData["id"])
                    ->update([
                        "response" => json_encode($response),
                        "status" => $payment_status
                    ]);
            
            $data = [
                'title' => $response->product->shop->name,
                'paymentData' => json_encode($response),
                'type' => 'payment'
            ];
            
            $pushNotificationMessage = PushNotification::Message($this->_PS[$payment_status], $data);
            
            PushNotification::app('android')
                          ->to($paymentData["register_id"])
                          ->send($pushNotificationMessage);
            
            if($payment_status == "approved"){
                $this->send_approved_email($paymentData['id']);

                if ($paymentData['point'] > 0) {
                    UserPoint::saveUserPoints([
                        'user_id' => $paymentData['user_id'],
                        'shop_id' => $paymentData['shop_id'],
                        'point' => $paymentData['point'],
                    ]);
                }
            }
        }
        
        return \Response::json("OK", $paymentData);
        
      } catch (Exception $e) {
        \Log::info(json_encode([
          'MP_error' => $e->getMessage()
        ]));
        return \Response::json([
          'error' => $e->getMessage()
        ], 500);
      }

    }

    /**
     * Send email when mp status is approved
     */
    public function send_approved_email($payment_id)
    {
        $payment = Payment::findOrFail($payment_id);
        $payment->msj = ['method'=>'Tu pago a través de Mercado Pago fue aceptado. Contacta con el negocio para coordinar la entrega de tu producto.',
            'clasification'=> 'Recuerda que una vez que el vendedor te entregue el producto y confirme la venta, podrás calificarlo desde la app de BlueShop.',
            'title'=>'Detalle de tu compra en BlueShop',
            'details'=>'Detalles de la compra'];
        $payment->type = 'comprador';

        Mail::to($payment->user->email)->send( new PaymentMail($payment) );

        unset($payment->msj);
        unset($payment->type);

        Mail::to($payment->shop->email)->send( new PaymentMailSeller($payment) );
    }
}
