<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use Hash;
// Models
use App\Models\User;

class ClientController extends Controller
{    
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('user.type:administrator');
    }
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where([
            ['type','user']
        ])->get();
        return view('client.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User;
        $user->birthday = date("d/m/Y"); 
        $title = trans('process.create');
        $post = url('users');
        return view('client.form',compact('user','title','post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $user = new User();  
        if($request['password']){
            $request['password'] = Hash::make($request['password']);
        }
        $user->update($request->all()); 
        flash( trans('process.success',['model' => trans('models.client.article') , 'process' => trans('process.created') ]), 'success');
        return redirect('users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id); 
        $title = trans('process.update');
        $post = url('users/'.$user->id);
        return view('client.form',compact('user','title','post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $user = User::find($id); 
        if($request['password']){
            $request['password'] = Hash::make($request['password']);
        }
        $user->update($request->all());
        flash( trans('process.success',['model' => trans('models.client.article') , 'process' => trans('process.updated') ]) , 'success');
        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->destroy($id);
        flash( trans('process.success',['model' => trans('models.client.article') , 'process' => trans('process.deleted') ]) , 'success');
        return redirect('users'); 
    }
}
