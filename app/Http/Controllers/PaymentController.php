<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
// Models
use App\Models\Payment;
use App\Models\Product;
use App\Models\Shop;
use App\Models\UserPoint;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $payments =  Auth::user()->payments();
      $shops = Shop::orderBy('name','desc')->get();  
      return view('payment.index',compact('payments','shops'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $payment = Payment::with(['user', 'product'])->find($id); 
        $payment->response = json_decode($payment->response);
//        die(var_dump($payment->toArray()));
        $title = trans('process.update');
        $post = url('payments/'.$payment->id);
        
        return view('payment.form',compact('payment','title','post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $payment = Payment::find($id);          
        $payment->status = $request->status;
        $payment->delivery = $request->delivery;
        $payment->payment_method = $request->payment_method;
        $payment->payment_comment = $request->payment_comment;
        $payment->payment_observations = $request->payment_observation;
                
        $response = json_decode($payment->response);
        $response->payment->status = $request->status;
        
        $payment_created_date = explode("/", $request->payment_created_date);
        $delivery_date = explode("/", $request->delivery_date);
        
//        die(date($payment_created_date[2]."/".$payment_created_date[1]."/".$payment_created_date[0]));
        $payment->payment_created_date = date($payment_created_date[2]."/".$payment_created_date[1]."/".$payment_created_date[0]);
        $payment->delivery_date = date($delivery_date[2]."/".$delivery_date[1]."/".$delivery_date[0]);
        $payment->response = json_encode($response);
        $payment->save();
        
        if( $request->status == "approved"){
            
            $product = Product::find($payment->product_id);
            
            if( $product->point > 0){
                
                UserPoint::saveUserPoints([
                    'user_id' => $payment->user_id,
                    'shop_id' => $payment->shop_id,
                    'point' => $product->point,
                ]);
                
            }
            
        }
        
        return redirect('payments');
    }
}
