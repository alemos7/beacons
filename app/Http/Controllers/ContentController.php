<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use File;
use Auth;
// Models
use App\Models\Content;

class ContentController extends Controller
{

      /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\Http\Response
       */
      public function index()
      {
          if(!$this->permission()) return redirect('/');
          $content = Content::find(1);

          if(!$content){
              $content = new Content();
          }

          $terms = $content->terms;
          $privacyPolicies = $content->privacy_policies ;

          return view('content.index',compact('terms','privacyPolicies'));
      }
      /**
       * Display a listing of the resource.
       *
       * @return \Illuminate\Http\Response
       */
      public function public()
      {
          $content = Content::find(1);

          if(!$content){
              $content = new Content();
          }

          $terms = $content->terms;
          $privacyPolicies = $content->privacy_policies ;

          return response($terms . $privacyPolicies, '200');
      }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$this->permission()) return redirect('/');

        $content = Content::find(1);

        if(!$content){
            $content = new Content();
        }

        switch ($request->key) {
            case 'terms':
                $content->terms = $request->terms;
                break;
            case 'privacyPolicies':
                $content->privacy_policies = $request->privacyPolicies;
                break;
        }

        $content->save();

        return $this->index();
    }

    private function permission(){
        if(Auth::user()->type != 'administrator') {
            return false;
        }
        return true;
    }
}
