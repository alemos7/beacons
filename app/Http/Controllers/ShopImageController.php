<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use File;
use Response;
// Models
use App\Models\Shop;
use App\Http\Requests\ShopImage\StoreRequest;
use App\Models\ShopImage;

class ShopImageController extends Controller
{

    /**
     * Display all images from shop
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showByShop($id)
    {
        if(! is_numeric($id) )
        {
            $shop = new Shop();
            $shop->images = ShopImage::where('shop_id', null)->where('image', $id)->get();
        }
        else
        {
            $shop = Shop::find($id);
        }
        return view('shopimage.list',compact('shop'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        if($request->ajax())
        {
            $request['shop_id'] = ($request['shop_id']) ? $request['shop_id'] : null;

            if (ShopImage::where('shop_id', $request['shop_id'])->count() >= intval(ENV('STORAGE_SHOP_MAX_IMAGES', 1)))
            {
                return Response::json([
                    'error' => 'Cantidad maxima de imagenes (' . intval(ENV('STORAGE_SHOP_MAX_IMAGES', 1)) . ') superada.'
                ], 500);
            }

            $shopImage = new ShopImage();
            $request['file_image'] = $request->images;
            $request['image'] = $request->_token;
            $shopImage->update($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $path = storage_path() . '/public/' . env('STORAGE_SHOP_IMAGES') . '/' . $id;

        if(!File::exists($path)) abort(404);

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $shopimage = ShopImage::find($id);
        $shopimage->delete();
        flash( trans('process.success',['model' => trans('models.image.article') , 'process' => trans('process.deleted') ]) , 'success');
        return redirect()->back();
    }
}
