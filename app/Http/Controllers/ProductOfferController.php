<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\OfferCalculate\OfferCalculate;
use Barryvdh\Debugbar\Facade as Debugbar;

class ProductOfferController extends Controller
{
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function calculate(Request $request)
	{
		if($request->ajax()){
			$result = 0;
			$offerCalculate = new OfferCalculate($request->type);
			$result = $offerCalculate->showCalculation($request->vars, $request->price);
			return $result;
		}
	}
}
