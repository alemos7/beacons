<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Models\UserFavorite;
use App\Models\UserSetting;
use App\Models\UserInterests;
use App\Models\User;
use App\Models\Category;

class MyProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return response()->json($request->user());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'string|max:100',
            'last_name'  => 'string|max:100',
            'phone'      => 'string|max:100',
            'birthday'   => 'date_format:Y|before:today',
            'email'      => 'email|max:100',
            'config'     => 'string'
        ]);
        if ($validator->fails()) {
            $message = $validator->errors();
            return response()->json(['status' => 'failed', 'errors' => $message], 400);
        }

        $data = $request->all();
        $data = array_only($data, ['first_name', 'last_name', 'birthday', 'phone', 'config']);
        $request->user()->fill($data)->save();

        return response()->json(['status' => 'success']);
    }

    /**
     * Desactivate user account.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $user = $request->user();
        Auth::logout();
        $user->delete();
        return response()->json(null, 204);
    }
    
    public function deleteFavorites(Request $request){
        
        $data = $request->all();
        
        if (!isset($data['product_id'])) {
            return response()->json('Provide product id', 400);
        }
        
        $product_id = $data['product_id'];
        
        $user = \Auth::user();
        
        $exists = UserFavorite::where([
            [
            'user_id', '=', $user->id
            ],
            [
            'product_id', '=', $product_id
            ]
        ])->first();
        
        if ($exists) {
            UserFavorite::where([
                [
                'user_id', '=', $user->id
                ],
                [
                'product_id', '=', $product_id
                ]
            ])->delete();
            
            return response()->json([
                'success' => true
            ], 201);
        }
        
        return response()->json(["success"=>true], 200);
        
    }
    
    public function putFavorites(Request $request)
    {
        $data = $request->all();

        if (!isset($data['product_id'])) {
            return response()->json('Provide product id', 400);
        }
        $product_id = $data['product_id'];
        
        $user = \Auth::user();
        
        $exists = UserFavorite::where([
            [
            'user_id', '=', $user->id
            ],
            [
            'product_id', '=', $product_id
            ]
        ])->first();
        
        if (!$exists) {
            $favorite = \Auth::user()->favorites()->create([
                'product_id' => $data['product_id']
            ]);
            return response()->json([
                'data' => $favorite
            ], 201);
        }
        
        return response()->json(["success"=>true], 200);
    }

    public function favorites()
    {
        $favorites = \Auth::user()->favorites()->with('product')->get();

        return response()->json([
            'favorites' => $favorites
        ]);
    }

    public function settings()
    {
        $settings = \Auth::user()->getSettings();

        return response()->json([
            'settings' => $settings
        ]);
    }

    public function putSettings(Request $request)
    {
        $data = $request->all();

        if (!isset($data['notification_massive']) && !isset($data['notification_closer'])) {
            return response()->json('Wrong parameters', 400);
        }
        $settings = \Auth::user()->getSettings();
        
        $data = array_only($data, [
            'notification_massive', 'notification_closer'
        ]);

        $settings->update($data);

        return response()->json([
            'settings' => $settings
        ]);
    }
    
    public function preferences(){
        
        $user = \Auth::user();
        
        if(!$user){
            return response()->json(['success' => false, 'error' => 'invalid user']);
        }
        
        $favoritesModel = UserFavorite::where("user_id", "=", $user->id)->select("product_id")->get();
        
        $favorites = array();
        
        foreach($favoritesModel as $f){
            $favorites[] = $f["product_id"];
        }
        
        $response["settings"] = UserSetting::where("user_id", "=", $user->id)->select("notification_closer", "notification_massive")->first();
        $response["favorites"] = $favorites;
        
        $interestsModel = UserInterests::getInterestsByUserId($user->id);
        
        $interests = array();
        
        if($interestsModel){
            
            $categories = Category::select("id")->get()->toArray();
            $interestsList = explode(",", $interestsModel["not_interesed_cats_ids"]);             
            foreach($categories as $cat){                                                                                
                
                foreach($interestsList as $il){
                    if($il == $cat["id"])
                        $interests[$cat["id"]] = false;
                }

            }
        }
        
        $response["interests"] = $interests;
        $response["success"] = true;
        
        return response()->json($response);
        
    }        
    
    public function interests(Request $request){
        
        $user = \Auth::user();
        
        if(!$user){
            return response()->json(['success' => false, 'error' => 'invalid user']);
        }
        
        try{
            
            $interests = $request->input('interests');
            
            if(!$interests)
                throwException ("Datos Invalidos");
            
            UserInterests::setInterests($user->id, $interests);
            
            return response()->json([
                'success' => true
            ]);
            
        }catch(Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        
    }

    //V2

    public function deleteFavoritesV2(Request $request){

        $data = $request->all();

        if (!isset($data['product_id']) || !isset($data['user_id'])) {
            return response()->json('Provide product id', 400);
        }

        $product_id = $data['product_id'];
        $user = User::find($data['user_id']);

        $exists = UserFavorite::where([
            [
                'user_id', '=', $user->id
            ],
            [
                'product_id', '=', $product_id
            ]
        ])->first();

        if ($exists) {
            UserFavorite::where([
                [
                    'user_id', '=', $user->id
                ],
                [
                    'product_id', '=', $product_id
                ]
            ])->delete();

            return response()->json([
                'success' => true
            ], 201);
        }

        //return response()->json(["success"=>true], 200);

    }

    public function putFavoritesV2(Request $request)
    {
        $data = $request->all();

        if (!isset($data['product_id']) || !isset($data['user_id'])) {
            return response()->json('Provide product id', 400);
        }

        $product_id = $data['product_id'];
        $user = User::find($data['user_id']);

        $exists = UserFavorite::where([
            [
                'user_id', '=', $user->id
            ],
            [
                'product_id', '=', $product_id
            ]
        ])->first();

        if (!$exists) {
            $favorite = $user->favorites()->create([
                'product_id' => $data['product_id']
            ]);
            return response()->json([
                'success' => true
            ], 201);
        }

        //return response()->json(["success"=>true], 200);
    }

    public function favoritesV2(Request $request)
    {

        if(!$request->has("user_id"))
            return response()->json('Not', 400);

        $user = User::find($request->get("user_id"));
        $favorites = $user->favorites()->get();

        $favorites_ids = array();

        foreach($favorites as $fav){
            $favorites_ids[] = $fav["product_id"];
        }

        return response()->json([
            'success' => true,
            'data' => $favorites_ids
        ]);
    }

    public function settingsV2(Request $request)
    {

        if(!$request->has("user_id"))
            return response()->json('Not', 400);

        $user = User::find($request->get("user_id"));
        $settings = $user->getSettings();
        $settings = $user->getSettings();

        $data = array_only($settings->toArray(), [
            "notification_closer",
            "notification_massive"
        ]);

        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }

    public function saveSettingsV2(Request $request)
    {

        if(!$request->has("user_id"))
            return response()->json('Not', 400);

        if (!$request->has("notification_massive") && !$request->has("notification_closer")) {
            return response()->json('Wrong parameters', 400);
        }

        $settings = User::find($request->get("user_id"))->getSettings();

        $data = array(
            "notification_massive" => $request->get("notification_massive"),
            "notification_closer" => $request->get("notification_closer")
        );

        $settings->update($data);

        return response()->json([
            'success' => true,
            'data' => $settings
        ]);
    }

    public function getInterestsV2(Request $request){

        //$user = \Auth::user();
        try{

            if(!$request->has("user_id"))
                return response()->json('Not', 400);

            $user = User::find($request->get("user_id"));

            if(!$user){
                return response()->json(['success' => false, 'error' => 'invalid user']);
            }

            $interestsModel = UserInterests::getInterestsByUserId($user->id);

            $interests = array();

            if($interestsModel){

                $categories = Category::select("id")->get()->toArray();
                $interestsList = explode(",", $interestsModel["not_interesed_cats_ids"]);
                foreach($categories as $cat){

                    foreach($interestsList as $il){
                        if($il == $cat["id"])
                            $interests[$cat["id"]] = false;
                    }

                }
            }

            return response()->json([
                'success' => true,
                'data' => $interests
            ]);

        }catch(Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

    }

    public function saveInterestsV2(Request $request){

        if(!$request->has("user_id"))
            return response()->json('Not', 400);

        $user = User::find($request->get("user_id"));

        if(!$user){
            return response()->json(['success' => false, 'error' => 'invalid user']);
        }

        try{

            $interests = $request->input('interests');

            if(!$interests)
                throwException ("Datos Invalidos");

            UserInterests::setInterests($user->id, $interests);

            return response()->json([
                'success' => true
            ]);

        }catch(Exception $e){
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

    }
    
}
