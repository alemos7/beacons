<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\UserPoint as Point;

class PointsController extends Controller
{
    public function index($id)
    {
        try {

            $user = \Auth::user();

            if ($user->id != $id) {
                return response()->json(null, 401);
            }

            $query = Point::where([
                'user_id' => $id
            ]);

            $points = $query->get();

            return response()->json([
                'data' => $points
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ]);
        }
    }
}
