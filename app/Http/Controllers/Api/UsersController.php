<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Content;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
          $user = User::findOrFail($id);

          return \Response::json([
            'user' => $user
          ], 200);

        } catch (\Exception $e) {
          return \Response::json([
            'error' => $e->getMessage()
          ], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function editProfileV2(Request $request){
        
        try{
            
            $input = $request->all();
            
            $user = User::find($input["id"]);
            
            $user->update([
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'phone' => $input['phone'],
                'birthday' => $input['birthday']
            ]);
            
            $user = User::find($input["id"]);
            
            return \Response::json([
                'success' => true,
                'user' => $user
            ], 200);
                
        }catch(Exception $e){
            return \Response::json([
                'success' => false,
                'error' => $e->getMessage()
            ], 500);
        }
        
    }
    
    public function update(Request $request, $id)
    {
        try {
          $data = $request->input();
          $user = \Auth::user();

          if (!$user ||
               $user->id != $id ) {

            return \Response::json([
              'error' => 'UNAUTHORIZED'
            ], 401);
          }

          if (isset($data['password']) &&
              isset($data['new_password'])) {

              if (!\Hash::check($data['password'], $user->password)) {
                return \Response::json([
                  'error' => 'Password is wrong'
                ], 400);
              }

              $data['password'] = \Hash::make($data['new_password']);
          }

          $user = User::findOrFail($id);
          $user->update($data);

          return \Response::json([
            'user' => $user
          ], 200);

        } catch (\Exception $e) {
          return \Response::json([
            'error' => $e->getMessage()
          ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function content(Request $request){
        
        try{
            
            $data = $request->get("content");
            $response = Content::select($data)->first();
            
            return response()->json([
                'success' => true,
                'data' => $response
            ]);
            
        }catch(Exception $e){
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
        
    }
}
