<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Payment;
use App\Mail\PaymentMail;
use App\Mail\PaymentMailSeller;
use App\Formatters\Api\ProductFormatter;
use App\Models\Califications;
use Mockery\Exception;
use SantiGraviano\LaravelMercadoPago\MP;
use App\Models\UserPoint;
use App\Models\User;

class MPController extends Controller
{

    public function checkoutV2(Request $request){

        try{

            $input = $request->all();
            $user = User::where(["email" => $input["email"]])->first();

            if(!$user){
                return \Response::json([
                    'success' => false
                ], 500);
            }

            $product = Product::with([
                'shop',
                'shop.images',
                'category',
                'images',
                'califications',
                'offer'
            ])->find($input["product_id"]);

            $productArray = $product->toArray();
            $response["client_key"] = $productArray["shop"]["mp_key"];

            if (!$product || !$response["client_key"]) {
              return \Response::json([
                'error' => 'Product not found'
              ], 500);
            }

            $data = ProductFormatter::apiFormatIndexItem($productArray);
            $unit_price = $data['price'];
            $units = intval($input['quantity']);
            $offerVars = json_decode($data["offer"][0]["vars"]);

            if( $data["offer"][0]["algorithm"] === "masPorMenos" && !($units % $offerVars->lleve) ){
    //            $unit_price = $data["offer"][0]["before_offer_price"];
                $unit_price = ($data['price'] / $offerVars->lleve);
            }

            $preference_data = [
              'items' => [
                            [
                                'id' => $data['id'],
                                'category_id' => $data['category_id'],
                                'title' => $data['name'],
                                'description' => $data['description'],
                                'picture_url' => $data['images'][0]['medium'],
                                'quantity' => $units,
                                'currency_id' => 'ARS',
                                'unit_price' => (float) $unit_price
                            ]
                    ],
              'payer' => [
                'name' => $user->first_name,
                'surname' => $user->last_name,
                'email' => $user->email
              ],
              'notification_url' => config('app.url') + "/mp/notifications"
            ];

            $MP_MODEL = new MP($productArray["shop"]["mp_client_id"],  $productArray["shop"]["mp_client_secret"]);

            $response["preference"] = $MP_MODEL->create_preference($preference_data);

            return \Response::json([
                'success' => true,
                'data' => $response
            ], 200);

        }catch(Exception $e){
            return \Response::json([
                'success' => false,
                'error' => 'Product not found'
            ], 500);
        }

    }

    public function checkout(Request $request)
    {
      $user = \Auth::user();
      if (!$user) {
        return \Response::json(null, 401);
      }

      if (!$request->has('product_id')) {
        return \Response::json(null, 500);
      }

      $response = array();

      try {
        $product = Product::with([
            'shop',
            'shop.images',
            'category',
            'images',
            'califications',
            'offer'
        ])->find($request->input('product_id'));

        $productArray = $product->toArray();
        $response["client_key"] = $productArray["shop"]["mp_key"];

        if (!$product || !$response["client_key"]) {
          return \Response::json([
            'error' => 'Product not found'
          ], 500);
        }

        $data = ProductFormatter::apiFormatIndexItem($productArray);
        $unit_price = $data['price'];
        $units = intval($request->input('quantity'));
        $offerVars = json_decode($data["offer"][0]["vars"]);

        if( $data["offer"][0]["algorithm"] === "masPorMenos" && !($units % $offerVars->lleve) ){
//            $unit_price = $data["offer"][0]["before_offer_price"];
            $unit_price = ($data['price'] / $offerVars->lleve);
        }

        $preference_data = [
          'items' => [
      			[
      				'id' => $data['id'],
      				'category_id' => $data['category_id'],
      				'title' => $data['name'],
      				'description' => $data['description'],
      				'picture_url' => $data['images'][0]['medium'],
      				'quantity' => $units,
      				'currency_id' => 'ARS',
      				'unit_price' => (float) $unit_price
      			]
      		],
          'payer' => [
            'name' => $user->first_name,
            'surname' => $user->last_name,
            'email' => $user->email
          ],
          'notification_url' => config('app.url') + "/mp/notifications"
        ];

        $MP_MODEL = new MP($productArray["shop"]["mp_client_id"],  $productArray["shop"]["mp_client_secret"]);

        $response["preference"] = $MP_MODEL->create_preference($preference_data);

        // Response
        return \Response::json($response);

      } catch (\Exception $e) {
          return \Response::json([
            'error' => $e->getMessage()
          ], 500);
      }
    }

    public function saveV2(Request $request){

        try{

            $input = $request->all();
            $user = User::where("email", $input["email"])->first();

            if(!$user){
                return \Response::json([
                    'error' => 'UNAUTHORIZED'
                ], 401);
            }


            if($input["payment_method"] == "cash"){

                $units = intval($input["quantity"]);
                $input["payment"]["transactionAmount"] = $input["product"]['price'] * $units;
                $offerVars = json_decode($input["product"]["offer"]["vars"]);

                if( $input["product"]["offer"]["algorithm"] === "masPorMenos" && !($units % $offerVars->lleve) ){
                    $input["payment"]["transactionAmount"] =
                    ($units - ($units - ( $offerVars->pague * ( $units / $offerVars->lleve )))) * $input["product"]['prod_price'];
                }
            }

            $result = Payment::createPaymentV2($user->id, $input);

            if($input["payment"]["status"] == "approved" && $input["product"]["point"] > 0){
                UserPoint::saveUserPoints([
                    'user_id' => $user->id,
                    'shop_id' => $input["product"]["shop"]['id'],
                    'point' => $input["product"]["point"],
                ]);
            }

            if($input['payment_method'] == "cash"){
                $this->send_approved_email($result->id);
            }

            return \Response::json([
                "success" => true,
                "result" => json_decode($result["response"])
            ]);


        }catch(Exception $e){
            return \Response::json([
                'success' => false,
                'error' => $e->getMessage()
            ], 500);
        }

    }

    public function save(Request $request){

        try{

            $data = $request->all();
            $user = \Auth::user();

            if (!$user ) {
              return \Response::json([
                'error' => 'UNAUTHORIZED'
              ], 401);
            }

            if($data["payment_method"] == "cash"){

                $units = intval($data["quantity"]);
                $data["payment"]["transactionAmount"] = $data["product"]['price'] * $units;
                $offerVars = json_decode($data["product"]["offer"][0]["vars"]);

                if( $data["product"]["offer"][0]["algorithm"] === "masPorMenos" && !($units % $offerVars->lleve) ){
                    $data["payment"]["transactionAmount"] =
                    ($units - ($units - ( $offerVars->pague * ( $units / $offerVars->lleve )))) * $data["product"]['price'];
                }
            }

            $result = Payment::createPayment($user->id, $data);

            if($data["payment"]["status"] == "approved" && $data["product"]["point"] > 0){
                UserPoint::saveUserPoints([
                    'user_id' => $user->id,
                    'shop_id' => $data["product"]["shop"]['id'],
                    'point' => $data["product"]["point"],
                ]);
            }

            if($data['payment_method'] == "cash"){
                //By. Agus
                $this->send_approved_email($result->id);
            }

            return \Response::json([
                "success" => true,
                "result" => json_decode($result["response"])
            ]);

        }catch(\Exception $e){
            return \Response::json([
                'error' => $e->getMessage()
            ], 500);
        }

    }

    public function calificateV2(Request $request){

        try{

            $input = $request->all();
            $user = User::find($input["user_id"]);

            if (!$user ) {
                return \Response::json([
                    'error' => 'UNAUTHORIZED'
                ], 401);
            }

            try{

                $data = $request->all();

                $paymentModel = Payment::where("id", $data["id"])->first()->toArray();

                if($user->id != $paymentModel["user_id"]){
                    return \Response::json([
                        'error' => 'Invalid user'
                    ], 401);
                }

                Califications::createCalification(array(
                    "shop_id" => $paymentModel["shop_id"],
                    "user_id" => $paymentModel["user_id"],
                    "product_id" => $paymentModel["product_id"],
                    "payment_id" => $paymentModel["id"],
                    "stars" => $data["stars"],
                    "comment" => $data["comment"]
                ));

                return \Response::json([
                    'success' => true
                ], 200);

            }catch(\Exception $e){
                return \Response::json([
                    'error' => $e->getMessage()
                ], 500);
            }

        }catch(Exception $e){
            return \Response::json([
                'error' => $e->getMessage()
            ], 500);
        }

    }

    public function calificate(Request $request){

        $user = \Auth::user();

        if (!$user ) {
          return \Response::json([
            'error' => 'UNAUTHORIZED'
          ], 401);
        }

        try{

            $data = $request->all();

            $paymentModel = Payment::where("id", $data["id"])->first()->toArray();

            if($user->id != $paymentModel["user_id"]){
                return \Response::json([
                    'error' => 'Invalid user'
                ], 401);
            }

            Califications::createCalification(array(
                "shop_id" => $paymentModel["shop_id"],
                "user_id" => $paymentModel["user_id"],
                "product_id" => $paymentModel["product_id"],
                "payment_id" => $paymentModel["id"],
                "stars" => $data["stars"],
                "comment" => $data["comment"]
            ));

            return \Response::json([
                'success' => true
            ], 200);

        }catch(\Exception $e){
            return \Response::json([
                'error' => $e->getMessage()
            ], 500);
        }

    }

//    public function updatePaymentsVersions(){
//
//        try{
//
//            $payments = Payment::get();
//
//            foreach($payments as $p){
//
//                $response = json_decode($p->response, true);
//
//                if(empty($response['product']['id']))
//                    continue;
//
//                $prod_id = $response['product']['id'];
//
//                $options = array(
//                    'lat' => null,
//                    'lng' => null,
//                    'includeDeleted' => true,
//                    'maxDistance' => 5000,
//                    'maxPerShop' => 5000,
//                    'filters' => array(
//                        'P.id' => $prod_id
//                    )
//                );
//
//                $products = Product::getProductsForApp($options);
//                $products = Product::productFormater($products);
//
//                $response['product'] = $products[0];
//                $p->response = json_encode($response);
//                $p->save();
//
//            }
//
//            die('listo');
//
//        }catch(Exception $e){
//            die(var_dump($e));
//        }
//
//    }

    public function getV2(Request $request){

        try{

            $input = $request->all();
            $user = User::find($input["user_id"]);

            if (!$user ) {
                return \Response::json([
                  'error' => 'UNAUTHORIZED'
                ], 401);
            }

            $purchases = Payment::where("user_id", $user->id)->select('id', 'response', 'quantity', 'delivery', 'status', 'payment_created_date')->orderBy('created_at', 'desc')->get()->toArray();

            $response = array(
                "success" => true,
                "data" => array()
            );

            foreach($purchases as $p){

                $calification = Califications::getByPaymentId($p["id"]);

                $paymentResponse = json_decode($p["response"]);
                $paymentResponse->calification = $calification;
                $paymentResponse->delivery = $p['delivery'];
                $paymentResponse->status = $p['status'];
                $paymentResponse->payment_created_date = date("d/m/Y", strtotime($p['payment_created_date']));

                if($calification){
                    $paymentResponse->calification["stars"] = intval($paymentResponse->calification["stars"]);
                }

                $paymentResponse->quantity = intval($p["quantity"]);
                $paymentResponse->id = $p['id'];
                $paymentResponse->product->shop->dist = 0;
                $paymentResponse->payment->dateCreated = date("d/m/Y", strtotime($paymentResponse->payment->dateCreated));

                $response["data"][] = $paymentResponse;
            }

            return \Response::json($response);


        }catch(Exception $e){
            return \Response::json([
                'success' => false,
                'error' => $e->getMessage()
            ], 500);
        }

    }

    public function get(Request $request){

        $user = \Auth::user();
        $user_id = $user ? $user->id : $request->get("user_id");

        if (!$user_id ) {
          return \Response::json([
            'error' => 'UNAUTHORIZED'
          ], 401);
        }

        try{

            $purchases = Payment::where("user_id", $user_id)->select('id', 'response', 'quantity', 'delivery', 'status', 'payment_created_date')->orderBy('created_at', 'desc')->get()->toArray();

            $response = array(
                "success" => true,
                "data" => array()
            );

            foreach($purchases as $p){

                $calification = Califications::getByPaymentId($p["id"]);

                $paymentResponse = json_decode($p["response"]);
                $paymentResponse->calification = $calification;
                $paymentResponse->delivery = $p['delivery'];
                $paymentResponse->status = $p['status'];
                $paymentResponse->payment_created_date = date("d/m/Y", strtotime($p['payment_created_date']));

                if($calification){
                    $paymentResponse->calification["stars"] = intval($paymentResponse->calification["stars"]);
                }

                $paymentResponse->quantity = intval($p["quantity"]);
                $paymentResponse->id = $p['id'];
                $paymentResponse->product->shop->dist = 0;
                $paymentResponse->payment->dateCreated = date("d/m/Y", strtotime($paymentResponse->payment->dateCreated));

                $response["data"][] = $paymentResponse;
            }

            return \Response::json($response);

        }catch(\Exception $e){
            return \Response::json([
                'error' => $e->getMessage()
            ], 500);
        }

    }

    public function mailTest(){
        $this->send_approved_email(91);
    }

    public function send_approved_email($payment_id)
    {
        $payment = Payment::with(['shop', 'user', 'product', 'product.images', 'product.shop'])->findOrFail($payment_id);

        if(!$payment)
            return;

        $payment->msj = [
            'method'=>'Has indicado que realizarás el pago en efectivo. Contáctate con él negocio para coordinar el pago y entrega.',
            'clasification'=> 'Recuerda que una vez que el vendedor te entregue el producto y confirme la venta, podrás calificarlo desde la app de BlueShop.',
            'title'=>'Detalle de tu compra en BlueShop',
            'details'=>'Detalles de la compra'
        ];

        $payment->type = 'comprador';

        Mail::to($payment->user->email)->send( new PaymentMail($payment) );

        unset($payment->msj);
        unset($payment->type);
        $payment->msj2 = ['method'=>'El comprador ha decido realizar el pago en efectivo. Contáctate con él para coordinar el pago y entrega.'];
        //'El comprador ha decidido realizar el pago en efectivo. Contacte con el mismo para coordinar el pago y la entrega.'

        if(!empty($payment->product->shop->email))
            Mail::to($payment->product->shop->email)->send( new PaymentMailSeller($payment) );

    }

}
