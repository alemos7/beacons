<?php

namespace App\Http\Controllers\Api;

use App\Models\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductsBeacon;
use App\Models\Banner;
use App\Models\ShopBeacon;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    private static $allowed_filters = [
      'category_id'
    ];
    /**
     * Display a listing of the resource.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            /**
             * Listar productos que tienen imagenes.
             */
            $unformattedResponse = Product::with(['shop', 'shop.images', 'category', 'images', 'califications', 'offer', 'shop.califications', 'shop.califications.user'])
                ->has('images')
                ->active();

            /*BANNERS*/

            $banners = false;

            if($request->input('banners')){

                $bannerModel = Banner::getBanners();

                if($bannerModel){

                    $bannerModel = $bannerModel->toArray();
                    $banners = array();


                    for($i=1;$i<10;$i++){

                        $productsQueryObject = clone $unformattedResponse;
                        $bannerProd = $productsQueryObject->where("id", $bannerModel["product_".$i."_id"])->get()->toArray();

                        if($bannerModel["image_".$i] && count($bannerProd) && $bannerProd[0]["shop"]["active"]){

                            $bannerProduct = Product::apiFormatIndex($bannerProd)[0];
                            $bannerProduct["banner_image"] = config('app.url') . "/" . $bannerModel["image_".$i];
                            $banners[] = $bannerProduct;

                        }

                    }

                }

            }

            /**
             * Limitar a sólo productos con ofertas.
             */
            if ($request->input('offers') === 'only') {
                $unformattedResponse->onlyOffers();
            }

        /**
        * Aplicamos los filtros.
        */
            if ($request->has('filters')) {
                $filters = $request->input('filters');
                foreach ($filters as $field => $value) {
                    /*if (!in_array($field, self::$allowed_filters)) {
                      throw new \Exception("Filter {$field} not allowed");
                    }*/

                    if (strpos($field, ':') !== false) {
                      $composition = explode(':', $field);

                      if (count($composition) == 2 &&
                          $composition[0] == 'not') {

                        $unformattedResponse->where($composition[1], '!=', $value);
                      }
                    } else if (strpos($value, ',') === false) {
                        $unformattedResponse->where($field, $value);
                    } else {
                        $multipleValues = explode(',', $value);
                        $unformattedResponse->whereIn($field, $multipleValues);
                    }
                }

            }
          /**
           * Filtro para poder excluir un producto por ID.
           */
          $unformattedResponse->where('id', '<>', $request->input('except'));

          /**
           * Realizamos búsqueda.
           */

            if ($request->has('search')) {
                $unformattedResponse->where('name', 'like', '%' . $request->input('search') . '%');
                $unformattedResponse->orWhere('code', 'like', '%' . $request->input('search') . '%');
                $unformattedResponse->orWhere('description', 'like', '%' . $request->input('search') . '%');
            }

            $unformattedProducts = $unformattedResponse->get()->toArray();
            $products = array();

            foreach($unformattedProducts as $prod){
                if($prod["shop"]["active"]){
                    $products[] = $prod;
                }else{
                    continue;
                }
            }

            return response()
            ->json(array(
                "banners" => $banners,
                "products" => Product::apiFormatIndex($products)
            ));

        } catch (Exception $e) {
            return response()
              ->json([
              'error' => $e->getMessage()
            ], 500);
        }

    }

    /**
     * Show closest products to a location or a beacon.
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function getProducts(Request $request){

        try{

            $lat = $request->input('lat', null);
            $lng = $request->input('lng', null);
            $beacon = $request->input('beacon', null);
            $maxDistance = $request->input('radius', 500000);
            $maxPerShop = $request->input('maxPerShop', 5000);
            $page = $request->input('page', 0);

            if( $beacon ){
                $beacon = json_decode ($beacon);

                $shops = Shop::getShopsByBeacon($beacon, [
                    'one' => true,
                    'add_distances' => true
                ]);

                if(count($shops)){
                    $lat = $shops[0]["shop"]["lat"];
                    $lng = $shops[0]["shop"]["lng"];
                }
            }

            $options = array(
                'lat' => $lat,
                'lng' => $lng,
                'maxDistance' => $maxDistance,
                'maxPerShop' => $maxPerShop
            );

            if($page != "all")
                $options["page"] = $page * 30;

            if($request->has("search"))
                $options["search"] = $request->get ("search");

            if($request->has("filters"))
                $options["filters"] = $request->get ("filters");

            if(!empty($shops))
                $options["shops"] = $shops;

            $products = Product::getProductsForApp($options);
            $products = Product::productFormater($products);

            return response()->json(array(
                "products" => $products
            ));

        }catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }

    }

    public function getBanners(Request $request){
        try{

            $lat = $request->input('lat', null);
            $lng = $request->input('lng', null);
            $maxDistance = $request->input('radius', 500000);
            $banners = array();

            $bannerModel = Banner::getBanners();

            if($bannerModel){

                $bannerModel = $bannerModel->toArray();

                $options = array(
                    'lat' => $lat,
                    'lng' => $lng,
                    'maxDistance' => $maxDistance,
                    'maxPerShop' => 5000
                );

                $products_ids = "";
                $banner_image = array();

                for($i=1;$i<10;$i++){

                    $product_id = $bannerModel["product_{$i}_id"];

                    if(empty($product_id))
                        continue;

                    $banner_image[$product_id] = $bannerModel["image_{$i}"];

                    if(empty($product_id) || empty($banner_image))
                        continue;

                    if($i > 1)
                        $products_ids .= ",";

                    $products_ids .= $product_id;

                }

                $options["filters"] = array(
                    'P.id' => $products_ids
                );

                $products = Product::getProductsForApp($options);
                $products = Product::productFormater($products);

                foreach($products as $p){

                    $product_id = $p->id;
                    $p->banner_image = config('app.url') . "/" . $banner_image[$product_id];

                    $banners[] = $p;

                }

                return response()->json([
                    'banners' => $banners
                ], 200);

            }else{
                return response()->json([
                    'banners' => $banners
                ], 200);
            }

        }catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage()
            ], 500);
        }
    }

    public function closer(Request $request)
    {

        try {

            $lat = $request->input('lat', null);
            $lng = $request->input('lng', null);
//            $distance = 0;
  //          $beacon = null;
            $beaconsShops = [];
            $limitByShop = 2;
          /**
           * @var Max products distance in km.
           */
          $maxDistance = $request->input('radius', 10);
          /**
           * Si envian beacon.
           */
            if ($request->has('beacon')) {
                $beacons = json_decode($request->input('beacon'), true);
                //\Log::info($beacons);
                if(count($beacons)){

                    $shopBeaconModel = ShopBeacon::with('shop')
                        ->has('shop')
                        ->active();

                    foreach( $beacons as $key => $b ){

                        $serial = $b["minor"] . "/" . $b["major"];

                        if(!$key){
                            $shopBeaconModel->where('serial', $serial);
                        }else{
                            $shopBeaconModel->orWhere('serial', $serial);
                        }

                    }

                    $beaconsShops = $shopBeaconModel->get()->toArray();

                    if(count($beaconsShops)){

//                        $distance = $beacons[0]["distance"];
                        $lat = $beaconsShops[0]["shop"]["lat"];
                        $lng = $beaconsShops[0]["shop"]["lng"];

                    }

                }

                if (!count($beaconsShops)) {
                    return response()->json($this->getProductsWithoutBeacons($request) );
                }
            }
            /**
             * Elegimos las tiendas que están dentro del rango.
             */
             //remove filter by radius
            $filterByRadius = false;

            if ($request->has('filters')) {
              $filters = $request->get('filters');

              if (isset($filters['id']) && !empty($filters['id'])) {
                $filterByRadius = false;
              }
            }

            $radiusWhere = "WHERE shop.lat
                               BETWEEN p.latpoint  - (p.radius / p.distance_unit)
                                   AND p.latpoint  + (p.radius / p.distance_unit)
                              AND shop.lng
                               BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
                                   AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
                                AND shop.active = 1";

            $distanceQuery = "SELECT shop.id,
                                shop.lat, shop.lng,
                                p.radius,
                                (p.distance_unit
                                 * DEGREES(ACOS(COS(RADIANS(p.latpoint))
                                 * COS(RADIANS(shop.lat))
                                 * COS(RADIANS(p.longpoint - shop.lng))
                                 + SIN(RADIANS(p.latpoint))
                                 * SIN(RADIANS(shop.lat))))) * 1000 AS distance
                            FROM shops AS shop
                            JOIN (
                                SELECT  {$lat}  AS latpoint,  {$lng} AS longpoint,
                                    $maxDistance AS radius, 111.045 AS distance_unit
                                ) AS p ON 1=1 ";

            if ($filterByRadius) {
                $distanceQuery .= $radiusWhere;
            }else{
                $distanceQuery .= "WHERE shop.active = 1";
            }

            $shops = DB::select( \DB::raw($distanceQuery) );

            /**
             * Sólo conservamos el campo id.
             */

            $shops_ids = [];
            $distances = [];

            foreach($shops as $s){

                $shops_ids[] = $s->id;
                $distances[$s->id] = $s->distance;

                if( count($beaconsShops) ){
                    foreach($beaconsShops as $bs){
                        if($bs["shop"]["id"] == $s->id){
                            foreach($beacons as $b){

                                $serial = $b["major"] . "/" . $b["minor"];
                                if( $serial == $bs["serial"] )
                                    $distances[$s->id] = $b["distance"] == -1 ? 1 : $b["distance"];

                            }
                        }
                    }
                }
            }

            $query = Product::with(['category', 'images', 'shop', 'shop.images', 'califications', 'offer', 'shop.califications', 'shop.califications.user'])
                ->active()
                ->whereIn('shop_id', $shops_ids);

            $banners = false;

            if($request->input('banners')){

                $bannerModel = Banner::getBanners();

                if($bannerModel){

                    $bannerModel = $bannerModel->toArray();
                    $banners = array();

                    for($i=1;$i<10;$i++){

                        $productsQueryObject = clone $query;
                        $product = Product::apiFormatCloser($productsQueryObject->where("id", $bannerModel["product_".$i."_id"])->get()->toArray(), $distances);

                        if($bannerModel["image_".$i] && count($product)){

                            $bannerProduct = $product[0];
                            $bannerProduct["banner_image"] = config('app.url') . "/" . $bannerModel["image_".$i];
                            $banners[] = $bannerProduct;

                        }

                    }

                }

            }

          /**
           * Realizamos búsqueda.
           */

            if ($request->has('search')) {
                $query->where('name', 'like', '%' . $request->input('search') . '%');
                $query->orWhere('code', 'like', '%' . $request->input('search') . '%');
                $query->orWhere('description', 'like', '%' . $request->input('search') . '%');
                $query->orWhereHas('shop', function($q) use ($request){
                    $q->where('name', 'LIKE', '%' . $request->input('search') . '%');
                });

                $limitByShop = null;

            }

            if ($request->has('filters')) {
                $filters = $request->input('filters');
                foreach ($filters as $field => $value) {
                    /*if (!in_array($field, self::$allowed_filters)) {
                      throw new \Exception("Filter {$field} not allowed");
                    }*/
                    if (strpos($value, ',') === false) {
                        $query->where($field, $value);
                    } else {
                        $multipleValues = explode(',', $value);
                        $query->whereIn($field, $multipleValues);
                    }
                }
                $limitByShop = null;
            }

            $products = $query->get()->toArray();

            return response()->json(array(
                "banners" => $banners,
                "products" => Product::apiFormatCloser($products, $distances, $limitByShop)
            ));

        } catch (Exception $e) {

            return response()->json([
              'error' => $e->getMessage()
            ], 500);
        }

    }

    public function getProductsWithoutBeacons($params)
    {
        try {
          /**
           * Listar productos que tienen imagenes.
           */
          $unformattedResponse = Product::with(['shop', 'shop.images', 'category', 'images', 'califications', 'offer', 'shop.califications', 'shop.califications.user'])->has('images')
              ->active();

          /**
           * Aplicamos los filtros.
           */
          if ($params['filters']) {
              $filters = $params['filters'];
              foreach ($filters as $field => $value) {
                  /*if (!in_array($field, self::$allowed_filters)) {
                    throw new \Exception("Filter {$field} not allowed");
                  }*/

                  if (strpos($field, ':') !== false) {
                    $composition = explode(':', $field);

                    if (count($composition) == 2 &&
                        $composition[0] == 'not') {

                      $unformattedResponse->where($composition[1], '!=', $value);
                    }
                  } else if (strpos($value, ',') === false) {
                      $unformattedResponse->where($field, $value);
                  } else {
                      $multipleValues = explode(',', $value);
                      $unformattedResponse->whereIn($field, $multipleValues);
                  }
              }

          }

        /**
           * Filtro para poder excluir un producto por ID.
           */
          //$unformattedResponse->where('id', '<>', $request->input('except'));

          /**
           * Realizamos búsqueda.
           */

          if ($params["search"]) {
              $unformattedResponse->where('name', 'like', '%' . $params["search"] . '%');
              $unformattedResponse->orWhere('code', 'like', '%' . $params["search"] . '%');
              $unformattedResponse->orWhere('description', 'like', '%' . $params["search"] . '%');
          }

            $banners = false;

            if($params['banners']){

                $bannerModel = Banner::getBanners();

                if($bannerModel){

                    $bannerModel = $bannerModel->toArray();
                    $banners = array();

                    for($i=1;$i<10;$i++){

                        $productsQueryObject = clone $unformattedResponse;

                        if($bannerModel["image_".$i]){

                            $bannerProduct = Product::apiFormatIndex($productsQueryObject->where("id", $bannerModel["product_".$i."_id"])->get()->toArray());
                            $bannerProduct["banner_image"] = config('app.url') . "/" . $bannerModel["image_".$i];
                            $banners[] = $bannerProduct;

                        }

                    }

                }

            }

            return array(
                "banners" => $banners,
                "products" => Product::apiFormatIndex($unformattedResponse->get()->toArray())
            );

        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */

    public function show($id, Request $request)
    {
        /**
         * Ubicar producto en base a un serial de beacon y su distancia.
         */
        if ($request->has(['beacon', 'distance'])) {

            $beaconSerial = $request->input('beacon');
            $distance = intval($request->input('distance'));

            /**
             * Filtramos para obtener el beacon.
             */
            $offers = ProductsBeacon::with(
                [
                    'beacon' => function ($query) use ($beaconSerial) {
                        return $query->where('serial', $beaconSerial);
                    },
                    'product_far.images',
                    'product_far.category',
                    'product_far.shop',
                    'product_close.images',
                    'product_close.category',
                    'product_close.shop',
                ])
                ->has('product_far')
                ->orHas('product_close')
                ->first();

            /**
             * Si no hay oferta, devolvemos 404.
             */
            if ($offers === null) {
                return response()->json(null, 404);
            }

            /**
             * Seleccionamos la oferta en relación a la distancia.
             */
            if ($distance > 5) {
                $offer = $offers->product_far;
                $msg = $offers->product_far_message;
            } else {
                $offer = $offers->product_close;
                $msg = $offers->product_close_message;
            }

            return response()->json(Product::apiFormatShowOffer($offer->toArray(), $msg));

        }

        /**
         * Ubicar beacon por ID.
         */
        $product = Product::with(['images', 'category', 'shop', 'offer'])->active()->findOrFail($id)->toArray();
        return response()->json(Product::apiFormatShow($product));


    }
}
