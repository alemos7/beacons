<?php

namespace App\Http\Controllers\Api;

use App\Models\MessageStatistic;
use App\Models\ProductsBeacon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShopBeacon as Beacon;
use App\Models\User;
use App\Models\Shop;
use App\Models\UsersNotifications;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use App\Models\Product;

use App\Formatters\Api\ShopFormatter;
use Mockery\Exception;

class MessagesController extends Controller
{

    public function sendV2(Request $request){

        try{

            $data = $request->all();
            $user_id = $data["user_id"];
            $beacons = json_decode($data["beacons"]);

            $user = User::find($user_id);

            if(!$user)
                return \Response::json([
                    'error' => 'Bad request'
                ], 400);

            $productOfBeacons = ProductsBeacon::getProductsOfBeacons($beacons);
            $productsIds = array();
            $productsAdvertisement = array();

            foreach($productOfBeacons as $pob){

                foreach($beacons as $b){

                    $serial = $b->major . "/" . $b->minor;
                    $prod_id = null;

                    if($pob->serial == $serial){

                        if(floatval($b->distance) <= 5 && $pob->product_close_id != null){

                            $prod_id = $pob->product_close_id;
                            $position = "close";

                            $productsAdvertisement[$pob->product_close_id] = $pob->product_close_message;

                        }else if(floatval($b->distance) > 5 && $pob->product_far_id != null){

                            $prod_id = $pob->product_far_id;
                            $position = "far";

                            $productsAdvertisement[$pob->product_far_id] = $pob->product_far_message;

                        }

                        if($prod_id){

                            $statistic = MessageStatistic::where([
                                ['products_beacon_id', "=", $pob->id],
                                ['position', "=", $position]
                            ])->first();

                            if(!$statistic){
                                $statistic = new MessageStatistic([
                                    'products_beacon_id' => $pob->id,
                                    'position' => $position,
                                    'issues' => 1
                                ]);
                                $statistic->save();
                            }else{
                                $statistic->issuedPlus(1);
                                $statistic->save();
                            }

                            $userNotifications = UsersNotifications::withTrashed()
                                ->where([
                                    'user_id' => $user_id,
                                    'prod_id' => $prod_id,
                                    'advertising' => $productsAdvertisement[$prod_id]
                                ])->orderBy("created_at", "DESC")->first();

                            if( !$userNotifications || ( $userNotifications && (strtotime($userNotifications->created_at) + config('app.notifications_time_window') * 60) < strtotime(date("d-m-Y H:i:s"))) ){

                                $productsIds[] = $prod_id;

                                $userNotificationsModel = new UsersNotifications([
                                    'user_id' => $user_id,
                                    'prod_id' => $prod_id,
                                    'statistic_id' => $statistic->id,
                                    'advertising' => $productsAdvertisement[$prod_id]
                                ]);
                                $userNotificationsModel->save();

                            }

                        }

                    }

                }

            }

            if(!empty($productsIds)){

                $options = array(
                    "lat" => null,
                    "lng" => null,
                    "maxDistance" => null,
                    "filters" => array(
                        "P.id" => join(",", $productsIds)
                    )
                );

                $products = Product::getProductsForApp($options);
                $products = Product::productFormater($products);

                if($user->interests){

                    $interests = $user->interests->toArray();
                    $notInterestedCategories = explode(",", $interests["not_interesed_cats_ids"]);

                    if(count($notInterestedCategories)){

                        foreach( $products as $key => $p ){

                            foreach($notInterestedCategories as $ui){
                                if($ui == $p->category["id"]){
                                    array_splice($products, $key, 1);
                                }
                            }

                        }

                    }

                }

                if(!empty($products)){

                    foreach($products as $p){

                        $productForNotification = $p;

                        $notification = [
                            'title' => $productForNotification->shop["name"],
                            'product' => $productForNotification,
                            'body' => $productsAdvertisement[$productForNotification->id],
                            'icon' => 'ic_notification',
                            'sound' => 'default',
                            'type' => 'product',
                            'statistic_id' => $statistic->id,
                            'advertising' => $productsAdvertisement[$productForNotification->id]
                        ];

                        $device_os = $user->device_os;

                        if($device_os == "android"){
                            $data = $notification;
                        }else{

                            $notification["product"] = $notification["product"]->prod_id;

                            $data = [
                                'custom' => $notification,
                                'badge' => UsersNotifications::where(["readed" => 0, "user_id" => $user_id])->count()
                            ];
                        }

                        $message = PushNotification::Message($productsAdvertisement[$productForNotification->id], $data);

                        $response = PushNotification::app($device_os)
                            ->to($user->register_id)
                            ->send($message);


                    }


                }


            }


            return \Response::json([
                'success' => true
            ]);

        }catch(Exception $e){
            dd($e);
        }

    }

    public function send(Request $request)
    {
        try {
            \Log::info("Data from request:");
          $data = $request->header('beacons') ? json_decode(stripslashes($request->header('beacons')), true) : [$request->get('0')];
          $user_id = $request->header('user-id');
          $silent = $request->header('silent');
//          die(var_dump($data));
          //$data = json_decode(stripslashes($data), true);
            //\Log::info("Data from requestttttttttttttttt:", $data);
            \Log::info(json_encode($data));
            \Log::info("User id from request: {$user_id}");
            \Log::info("Silent: {$silent}");

          $distance = $data[0]['distance'];

          $beacon = Beacon::with(['shop'=>function($query){
              $query->where("active", "=", 1);
          }])->where([
            'serial' => Beacon::makeSerial($data[0]['major'], $data[0]['minor'])
          ])->first();

          if (!$beacon || !$beacon->shop) {
            \Log::info("Beacon not found at " . Beacon::makeSerial($data[0]['major'], $data[0]['minor']));
            return \Response::json([
              'error' => 'Bad request'
            ], 400);
          }

          $suggestDistance = Beacon::suggestDistance($distance);

          $user = User::with(["settings"])->findOrFail($user_id);
          $register_id = $user->register_id;
          $device = $user->device_os;
          $userInterestsModel = $user->interests;

          $products = $beacon->products_around()->with([
            "product_{$suggestDistance}" => function($q) {
              $q->with(['category', 'images', 'shop', 'shop.images', 'offer', 'shop.califications', 'shop.califications.user', 'messages']);
            }
            ])->first()
              ->toArray();

          \Log::info(json_encode($products));

          if (!$products || !count($products)) {
            \Log::info("Products not found");
            return \Response::json([
              'error' => 'Bad request'
            ], 400);
          }

          $beacon = $beacon->toArray();

          // Save message statistics

            $statistic = MessageStatistic::where([
                ['products_beacon_id', "=", $products['id']],
                ['position', "=", $suggestDistance]
            ])->first();

            if(!$statistic){
                $statistic = new MessageStatistic([
                    'products_beacon_id' => $products['id'],
                    'position' => $suggestDistance,
                    'issues' => 1
                ]);
                $statistic->save();
            }else{
                $statistic->issuedPlus(1);
                $statistic->save();
            }

            //$products["product_$suggestDistance"] = Product::apiFormatCloser($products["product_$suggestDistance"]);

            $products["product_$suggestDistance"]['images'] = array_map(
                function($image = array()){
                    if (isset($image['image'])) {
                        $image_exploded = explode('.', $image['image']);
                        return [
                          'thumb' => config('app.url') . '/' . "{$image_exploded[0]}_sm.{$image_exploded[1]}",
                          'medium' => config('app.url') . '/' . $image['image']
                        ];
                    }
                }, $products["product_$suggestDistance"]['images']);

            $products["product_$suggestDistance"]["distance"] = intval($distance)+1;

            $products["product_$suggestDistance"]["shop"] = ShopFormatter::apiFormatItem($products["product_$suggestDistance"]["shop"]);

            $messageDataParsed = $this->parseMessageForPushNotification([
                'title' => $beacon['shop']['name'],
                'product' => $products["product_$suggestDistance"],
                'type' => 'product',
                'statistic_id' => $statistic->id,
                'advertising' => $products["product_{$suggestDistance}_message"],
                'silent' => $silent
            ]);

            $userNotifications = UsersNotifications::withTrashed()
            ->where([
                'user_id' => $user_id,
                'prod_id' => $products["product_$suggestDistance"]["id"],
                'advertising' => $products["product_{$suggestDistance}_message"]
            ])->orderBy("created_at", "DESC")->first();

            if( $userNotifications && ((strtotime($userNotifications->created_at) + config('app.notifications_time_window') * 60) > strtotime(date("d-m-Y H:i:s"))) ){
                return \Response::json([
                    'error' => 'The user cant receive notifications yet.'
                ], 400);
            }else{
                $userNotificationsModel = new UsersNotifications([
                    'user_id' => $user_id,
                    'prod_id' => $products["product_$suggestDistance"]["id"],
                    'statistic_id' => $statistic->id,
                    'advertising' => $products["product_{$suggestDistance}_message"]
                ]);
                $userNotificationsModel->save();
            }


            if($userInterestsModel){

                $userInterests = $userInterestsModel->toArray();

                foreach(explode(",", $userInterests["not_interesed_cats_ids"]) as $ui){

                    if($ui == $products["product_{$suggestDistance}"]["category_id"]){
                         \Log::info("A este usuario no le interesa este producto: {$user_id}, {$products["product_$suggestDistance"]["category_id"]}");
                        return \Response::json([
                            'error' => 'A este usuario no le interesa este producto'
                        ], 200);
                    }

                }
            }

            if($device == "android"){
                $data = $messageDataParsed;
            }else{
                $data = [
                    'custom' => $this->parseMessageForIosPushNotification($messageDataParsed),
                    'badge' => UsersNotifications::where(["readed" => 0, "user_id" => $user_id])->count()
                ];
            }

            if($device == "ios" || $silent == "false" && ($user->settings == NULL || intval($user->settings->notification_closer))){
                $message = $products["product_{$suggestDistance}_message"];
            }else{
                $message = null;
            }

          \Log::info("Trying to send: {$device}, {$register_id}", $data, $message);
          \Log::info("Trying to send: {$message}");

            $message = PushNotification::Message($message, $data);

            $response = PushNotification::app($device)
                            ->to($register_id)
                            ->send($message);

            \Log::info("Response: " . json_encode($response));

            return \Response::json([
              'message' => 'OK'
            ]);

        } catch (\Exception $e) {
          \Log::info("Response: ", [$e->getMessage()]);
          return \Response::json([
            'message' => $e->getMessage()
          ], 500);
        }
    }

    public function deleteNotifications( Request $request ){

        try{

            $user_notification_id = $request->get("user_notification_id");
            $user = \Auth::user();
            $user_id = $user ? $user->id : $request->get("user_id");

            if( $user_notification_id == -1 ){
                UsersNotifications::where(['user_id' => $user_id])->delete();
            }else{
                UsersNotifications::where(['id' => $user_notification_id])->delete();
            }

            return \Response::json([
                'success' => true
            ], 200);

        }catch(Exception $e){
            return \Response::json([
                'message' => $e->getMessage()
            ], 500);
        }

    }

    public function getUserNotificationsV2(Request $request){

        try{

            if(!$request->has("user_id"))
                return \Response::json([
                    'success' => false
                ], 404);

            $user_id = $request->get("user_id");
            $user = User::find($user_id);

            if(!$user)
                return \Response::json([
                    'success' => false
                ], 404);


            $lat = $request->input('lat', null);
            $lng = $request->input('lng', null);
            $beacon = $request->input('beacon', null);
            $maxDistance = $request->input('radius', 500000);

            if( $beacon ){
                $beacon = json_decode ($beacon);

                $shops = Shop::getShopsByBeacon($beacon, [
                    'one' => true,
                    'add_distances' => true
                ]);

                if(count($shops)){
                    $lat = $shops[0]["shop"]["lat"];
                    $lng = $shops[0]["shop"]["lng"];
                }
            }

            $options = array(
                'lat' => $lat,
                'lng' => $lng,
                'maxDistance' => $maxDistance
            );

            $usersNotificationsModel = UsersNotifications::where([
                'user_id' => $user_id,
            ])
            //->groupBy("advertising")
            ->whereNull("deleted_at")
            ->get()
            ->toArray();

            $userNotifications = array();

            foreach( $usersNotificationsModel as $uN ){

                $options["filters"] = array(
                    'P.id' => $uN["prod_id"]
                );

                $products = Product::getProductsForApp($options);

                if(empty($products))
                    continue;

                $products = Product::productFormater($products);

                $userNotifications[] = array(
                    'id' => $uN["id"],
                    'product' => $products,
                    'date' => strtotime($uN["created_at"]) * 1000,
                    'read' => !$uN["readed"] ? 'bold' : 'normal',
                    'statistic_id' => $uN["statistic_id"],
                    'advertising' => $uN["advertising"]
                );

            }

            return \Response::json([
                'success' => true,
                'notifications' => $userNotifications
            ], 200);

        }catch(Exception $e){
            return \Response::json([
                'success' => false
            ], 500);
        }

    }

    public function getUserNotifications(Request $request){

        try{

            $user = \Auth::user();
            $user_id = $user ? $user->id : $request->get("user_id");

            $usersNotificationsModel = UsersNotifications::with("products")->where([
                'user_id' => $user_id,
            ])
            //->groupBy("advertising")
            ->whereNull("deleted_at")
            ->get()
            ->toArray();

            $userNotifications = [];
            $productsIds = [];

            foreach( $usersNotificationsModel as $uN ){

                if(!$uN["products"]["active"])
                    continue;

                $productsIds[] = $uN["prod_id"];
                $userNotifications[] = array(
                    'id' => $uN["id"],
                    'product_id' => $uN["prod_id"],
                    'date' => strtotime($uN["created_at"]) * 1000,
                    'read' => !$uN["readed"] ? 'bold' : 'normal',
                    'statistic_id' => $uN["statistic_id"],
                    'advertising' => $uN["advertising"]
                );

            }

            return \Response::json([
                'success'=> true,
                'notifications' => $userNotifications,
                'products_ids' =>  $productsIds
            ], 200);

        }catch(Exception $e){
            return \Response::json([
              'message' => $e->getMessage()
            ], 500);
        }

    }

    private function parseMessageForIosPushNotification($data){

        $data["product"] = $data["product"]["id"];

        return $data;

    }

    private function parseMessageForPushNotification($data){

        unset($data["product"]["created_at"]);
        unset($data["product"]["updated_at"]);
        unset($data["product"]["deleted_at"]);
        unset($data["product"]["code"]);
        unset($data["product"]["shop"]["created_at"]);
        unset($data["product"]["shop"]["updated_at"]);
        unset($data["product"]["shop"]["deleted_at"]);
        unset($data["product"]["category"]["created_at"]);
        unset($data["product"]["category"]["updated_at"]);
        unset($data["product"]["category"]["deleted_at"]);

        $data["product"]["offer"] = [array_shift($data["product"]["offer"])];

        if($data["product"]["messages"] && count($data["product"]["messages"]) > 1){
            $data["product"]["messages"] = [array_shift($data["product"]["messages"])];
            unset($data["product"]["messages"][0]["created_at"]);
            unset($data["product"]["messages"][0]["updated_at"]);
        }

        if($data["product"]["offer"] && count($data["product"]["offer"]) >= 1){
            unset($data["product"]["offer"][0]["created_at"]);
            unset($data["product"]["offer"][0]["updated_at"]);
        }

        return $data;

    }

    /*public function sendIOS(Request $request){
        $data = $request->header('beacons');
        $user = $request->header('user-id');
        $silent = $request->header('silent');
        \Log::info("REQUEST: {$data}");
        \Log::info("REQUEST: ", $request->all());

    }*/

    public static function statisticsViews(Request $request)
    {
        try {

            $statistic_id = $request->get('statistic_id');
            $statistic = MessageStatistic::find($statistic_id);
            $un_id = $request->get('un_id');

            if ($statistic) {
                $statistic->views = $statistic->views + 1;
                $statistic->save();
            }

            UsersNotifications::where([
                "id" => $un_id
            ])->update([
                'readed'=>1
            ]);
            return \Response::json([
                'message' => 'OK'
            ]);
        } catch (\Exception $e) {
            return \Response::json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

}
