<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\User\RestorePasswordRequest;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use AuthenticatesUsers;
    use SendsPasswordResetEmails;

    /**
     * Respuesta enviada si el usuario se logueo correctamente.
     * @return \Illuminate\Http\JsonResponse
     */
    protected function authenticated()
    {
        return response()->json(['status' => 'success', 'user' => Auth::user()]);
    }

    public function config(Request $request)
    {
      try {

        $data = $request->all();

        $user = \Auth::user();

        if (!$user) {
          return \Response::json(null, 401);
        }

        \Log::info('Request data: ', $data);

        $user->config = json_encode($data);
        $user->save();

        return \Response::json([
          'user' => $user
        ]);

      } catch (\Exception $e) {
        return $this->responseError($e->getMessage());
      }

    }

    /**
     * Respuesta enviada si el usuario no se logueo correctamente.
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendFailedLoginResponse()
    {
        return response()->json(['status' => 'fail']);
    }

    /**
     * Login
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function signIn(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {           
            
            User::where("register_id", $request->register_id)->update(["register_id" => null]);
            
            $user = \Auth::user();
            $user->active = 1;
            $user->register_id = $request->register_id;
            $user->save();

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        //$this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
    
    public function signInV2(Request $request){
        
        try{
            
            $input = $request->all();
            
            $user = User::where([
                "email" => $input["email"]
            ])->first();
            
            if($user && \Illuminate\Support\Facades\Hash::check($input["password"], $user->password)){
                
                User::where('id', $user->id)->update([
                    'register_id' => $input['register_id'] ? $input['register_id'] : null,
                    'device_os' => $input['device_os']
                ]);
                
                return $this->responseSuccess([
                    'success' => true,
                    'user' => $user
                ], 200);
                
            }else{
                return $this->responseSuccess([
                    'success' => false
                ], 200);
            }
            
        }catch(Exception $e){
            return $this->responseError([
                'success' => false,
                'error' => $e
            ], 500);
        }
        
    }

    protected function validateSignUp(Request $request)
    {
        return Validator::make($request->all(),
            [
                'email'    => 'required|email|max:255|unique:users',
                'password' => !$request->has('user_id') ? 'required|min:6|max:255' : '',
                'first_name' => 'max:255',
                'last_name' => 'max:255'
            ], [],
            [
                'email'      => trans('data.email'),
                'password'   => trans('data.password'),
                'first_name' => trans('data.first_name'),
                'last_name'  => trans('data.last_name')
            ]);
    }

    /**
     * Registro guest user
     * @param $request
     * @return Response
     */
    public function userGuest(Request $request)
    {
      try {
        if (!$request->has('register_id')) {
          return $this->responseError('Register ID can not be empty');
        } else {
          $register_id = $request->input('register_id');
          $device_os = $request->input('device_os');
          $data = [
            'register_id' => $register_id,
            'device_os' => $device_os
          ];
          $user = User::createUser($data);
          Auth::login($user);
          return $this->responseSuccess([
            'user' => $user
          ], 201);
        }
      } catch (\Exception $e) {
        \Log::info("Error on register guest: " . $e->getMessage());
        return $this->responseError($e->getMessage());
      }
    }
    
    public function userGuestV2(Request $request){
        
        try{
            
            $input = $request->all();
            
            $data = [
                'register_id' => $input["register_id"],
                'device_os' => $input["device_os"]
            ];
            
            $user = User::createUser($data);
            
            return $this->responseSuccess([
                'success' => true,
                'user' => $user
            ], 200);
            
        }catch(Exception $e){
            return $this->responseError([
                'success' => false
            ], 500);
        }
        
    }

    private function responseSuccess($res = [], $code = 200)
    {
      return response()->json($res, $code);
    }

    private function responseError($msg = "")
    {
      return response()->json([
        'error' => $msg
      ], 500);
    }

    /**
     * Registro
     * @param $request
     */
    
    public function signUpV2(Request $request){
        try{
            
            $input = $request->all();
            
            $userExist = User::where("email", $input['email'])->first();
            
            if($userExist){
                return response()->json([
                    'success' => false
                ], 200);
            }
                
            
            if(!empty($input["id"])){
                
                User::where("id", $input["id"])
                ->update([
                    'email'      => $input['email'],
                    'password'   => bcrypt($input["password"]),
                    'from'       => 'Email',
                    'birthday'  => $input['birthday'],
                    'gender'    => $input['gender'],
                    'active'    => 1
                ]);
                
                $user = User::where("id", $input["id"])->first();
                
            }else{
                
                $user = User::createUser([
                    'email'      => $input['email'],
                    'password'   => bcrypt($input["password"]),
                    'from'       => 'Email',
                    'gender'    => $input['gender'],
                    'birthday'  => $input['birthday'],
                    'register_id' => !empty($input["token"]) ? $input["token"] : null,
                    'active'    => 1
                ]);
                
            }
            
            return response()->json([
                'success' => true,
                'user' => $user
            ], 200);
            
        }catch(Exception $e){
            return response()->json([
                'success' => false,
                'error' => $e
            ], 500);
        }
    }
    
    public function signUp(Request $request)
    {
        $validation = $this->validateSignUp($request);
        if($validation->fails()) {
            return response()->json(['status' => 'fail', 'errors' => $validation->errors()]);
        }
        $res = [];
        $res['status'] = 'success';

        $data = $request->all();
        $this->checkRegistrationId($data["register_id"]);
        $data['password'] = bcrypt($request->password);

        if ($request->has('user_id')) {

          $user_id = $request->input('user_id');
          
          $user = User::find($user_id);
          if (!$user) {
            return $this->responseError('Unmatched user.');
          }
          
          $data['active'] = 1;
          $user->update($data);

          $res['user'] = $user;
          Auth::login($user);
          
        } else {
          $newUser = User::createUser($data);
          Auth::login($newUser);
          $res['user'] = $newUser;
        }
        return response()->json($res);
    }

    public function logout(Request $request)
    {
        try {
        
            $data = $request->all();
            
            $user = \Auth::user();

            User::where([
               "register_id" =>  $data["register_id"]
            ])->update([
                "register_id" => null
            ]);          

            Auth::logout();

            return response()->json([
              'success' => true
            ]);
          
        } catch (\Exception $e) {
          return response()->json([
            'error' => $e->getMessage()
          ], 500);
        }
    }
    
    public function logoutV2(Request $request){
        
        try{
            
            $input = $request->all();
            
            User::where('id', $input['id'])->update(['register_id' => null]);
            
            return response()->json([
                'success' => true
            ], 200);
            
        }catch(Exception $e){
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], 500);
        }
        
    }
    
    public function facebookCallbackV2(Request $request){
        
        try{
            
            $input = $request->all();
            $facebookData = $input["facebookData"];
            
            $user = User::where("email", $facebookData['email'])->first();
            
            if($user){
                
                $user->active = 1;
                $user->register_id = !empty($input["token"]) ? $input["token"] : null;
                $user->device_os = $input['device_os'];
                
                $user->save();
                
            }else if(!empty($input["id"])){
                
                User::where("id", $input["id"])
                ->update([
                    'email'      => $facebookData['email'],
                    'password'   => bcrypt($facebookData["id"]),
                    'first_name' => $facebookData['first_name'],
                    'last_name' => $facebookData['last_name'],
                    'from'       => 'Facebook',
                    'provider_id' => $facebookData['id'],
                    'active'    => 1
                ]);
                
                $user = User::where("id", $input["id"])->first();
                
            }else{
                
                $user = User::createUser([
                    'email'      => $facebookData['email'],
                    'password'   => bcrypt($facebookData["id"]),
                    'first_name' => $facebookData['first_name'],
                    'last_name' => $facebookData['last_name'],
                    'from'       => 'Facebook',
                    'register_id' => !empty($input["token"]) ? $input["token"] : null,
                    'provider_id' => $facebookData['id'],
                    'active'    => 1
                ]);
                
            }
            
            return response()->json([
                'success' => true,
                'user' => $user
            ], 200);
            
        }catch(Exception $e){
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], 500);
        }
        
    }
    
    public function googleCallbackV2(Request $request)
    {
        try {

            $input = $request->all();
            $googleData = $input["googleData"];
            
            $user = User::where("email", $googleData['email'])->first();
//            die(json_encode($user));
            if($user){
                
                $user->active = 1;
                $user->register_id = !empty($input["token"]) ? $input["token"] : null;
                $user->device_os = $input['device_os'];
                
                $user->save();
                
            }else if(!empty($input["id"])){
                
                User::where("id", $input["id"])
                ->update([
                    'email'      => $googleData['email'],
                    'password'   => bcrypt($googleData["userID"]),
                    'first_name' => $googleData['givenName'],
                    'last_name' => $googleData['familyName'],
                    'from'       => 'Gmail',
                    'provider_id' => $googleData['userID'],
                    'active'    => 1
                ]);
                
                $user = User::where("id", $input["id"])->first();
                
            }else{
                
                $user = User::createUser([
                    'email'      => $googleData['email'],
                    'password'   => bcrypt($googleData["userID"]),
                    'first_name' => $googleData['givenName'],
                    'last_name' => $googleData['familyName'],
                    'from'       => 'Gmail',
                    'register_id' => !empty($input["token"]) ? $input["token"] : null,
                    'provider_id' => $googleData['userID'],
                    'active'    => 1
                ]);
                
            }            

            return response()->json([
                'success' => true,
                'user' => $user
            ], 200);

        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], 500);

        }
    }

    public function googleCallback(Request $request)
    {
      try {

        $data = $request->all();                
        
        if (!isset($data['email'])) {
          return response()->json([
            'error' => 'Email not provided'
          ], 500);
        }

        $this->checkRegistrationId($data["register_id"]);
        
        $user = User::where(['email' => $data['email']])
            ->first();
        
        if ($user) {
            
            if (isset($data['register_id'])) {
              $user->register_id = $data['register_id'];
              $user->device_os = $data['device_os'];
              $user->active = 1;
              $user->save();
            }
          
            Auth::login($user);

            return response()->json([
              'user' => $user
            ], 200);
          
        }else if(!$user && $request->has('user_id')){
            
//            $user = \Auth::user();
            $data['active'] = 1;
            
            User::where("id", $data["user_id"])
            ->update([
                'email'      => $data['email'],
                'password'   => bcrypt('password'),
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'from'       => 'Gmail',
                'device_os' => $data['device_os'],
                'register_id' => $data['register_id']
            ]);
            
            $user = User::where("id", $data["user_id"])->first();
            
            Auth::login($user);
            
            return response()->json([
                'user' => $user
            ], 201);
            
        }else {
          $data['password'] = bcrypt('password');
          $user = User::createUser($data);
          Auth::login($user);

          return response()->json([
            'user' => $user
          ], 201);
        }

      } catch (\Exception $e) {
        return response()->json([
          'error' => $e->getMessage()
        ], 500);
      }
    }

    public function facebookCallback(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb, Request $request)
    {
        $data = $request->all();

        
        $access_token = $data['access_token'];

        $fb->setDefaultAccessToken($access_token);

        try {
            $response = $fb->get('/me?fields=id,name,email,first_name,last_name');

            $facebook_profile = $response->getGraphUser();

            $user = User::where(['provider_id' => $facebook_profile->getId()])
              ->first();

            $this->checkRegistrationId($data["register_id"]);
            
            if ($user) {

                if (isset($data['register_id'])) {
                    $user->register_id = $data['register_id'];
                    $user->device_os = $data['device_os'];
                    $user->active = 1;
                    $user->save();
                }
              
                Auth::login($user);

                return response()->json([
                  'user' => $user
                ], 200);

            } else if(!$user && $request->has('user_id')){
            
                $data['active'] = 1;
                $userUpdate = User::where("id", $data['user_id'])
                ->update([
                    'email'      => $facebook_profile->getEmail(),
                    'password'   => bcrypt('password'),
                    'first_name' => $facebook_profile->getFirstName(),
                    'last_name' => $facebook_profile->getLastName(),
                    'from'       => 'Facebook',
                    'provider_id' => $facebook_profile->getId(),
                    'provider_access_token' => $access_token,
                    'device_os' => $data['device_os'],
                    'register_id' => $data['register_id']
                ]);
                
                $user = User::where("id", $data["user_id"])->first();
                
                Auth::login($user);

                return response()->json([
                    'user' => $user
                ], 201);
            
            }else {
              $data = [
                'email'      => $facebook_profile->getEmail(),
                'password'   => bcrypt('password'),
                'first_name' => $facebook_profile->getFirstName(),
                'last_name' => $facebook_profile->getLastName(),
                'from'       => 'Facebook',
                'provider_id' => $facebook_profile->getId(),
                'provider_access_token' => $access_token,
                'device_os' => $data['device_os'],
                'register_id' => $data['register_id']
              ];
              $user = User::createUser($data);
              Auth::login($user);

              return response()->json([
                'user' => $user
              ], 201);
            }
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            return response()->json([
              'error' => $e->getMessage()
            ], 500);
        }
    }
    
    public function updateRegisterId(Request $request){
        
        try{
            
            $user_id = $request->get("user_id");
            $register_id = $request->get("register_id");
            $device_os = $request->get("device_os");
            
            User::where([
                'id' => $user_id
            ])->update([
                'register_id' => $register_id,
                'device_os' => $device_os
            ]);
            
            return response()->json([
                'success' => true
            ], 200);
            
        }catch(Exception $e){
            return response()->json([
                'error' => $e->getMessage(),
                'success' => false
            ], 500);
        }
        
    }
    
    public function signDown(){
        
        try{
            
            $user = \Auth::user();

            if($user){
                
                $user["active"] = 0;
                $user->save();
                
                return response()->json([
                    'success' => true
                ], 201);
                
            }else{
                throw new Exception("Usted no esta logeado");
            }
            
        }catch(Exception $e){
            return response()->json([
              'error' => $e->getMessage()
            ], 500);
        }
        
    }
    
    private function checkRegistrationId($register_id){
        
        if($register_id)
            User::where("register_id", $register_id)->update(["register_id" => null]);
        
    }


    /**
     * Resetear contraseña.
     * @param $request
     */
    public function resetPassword(Request $request)
    {
        try
        {
            /* ¿Que es active? ¿Se usa? Por las dudas buscamos solo los activos */
            if($user = User::where('type','user')->where('active', 1)->where('email', $request->email)->first())
            {
                //Si existe el usuario [Enviamos email]
                $this->sendResetLinkEmail($request);
                return response()->json(['message' => 'El usuario existe, se envia token']);
            }
            else
            {
                //Si no existe [Desestimamos]
                return response()->json(['message' => 'El usuario no existe']);
                //Ver si logueamos algo o no, no se
            }
        }
        catch(\Exception $e)
        {
            //return response()->json(['test' => 'excepcion en resetPassword','exception' => $e->getMessage(), 'trace' => $e->getTraceAsString()],500);
            return response()->json(['error' => 'Error inesperado'],500);
        }
    }
    
    public function changePassword(Request $request){
        
        try{
            
            $input = $request->all();
            $user = User::find($input["id"]);
            
            if (!\Hash::check($input['password'], $user->password)) {
                return \Response::json([
                    'success' => false,
                    'error' => 'Password is wrong'
                ], 200);
            }
            
            $user->update([
                'password' => \Hash::make($input['new_password'])
            ]);
            
            return \Response::json([
                'success' => true
            ], 200);
            
        }catch(Exception $e){
            return \Response::json([
                'success' => false,
                'error' => $e->getMessage()
            ], 500);
        }
        
    }

    /*
    *
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     *
    public function sendPasswordResetNotification($token)
    {


    }
    */
    protected function sendResetLinkResponse($response)
    {
        return response()->json([]);
    }

    /**
     * Get the response for a failed password reset link.
     *
     * @param  \Illuminate\Http\Request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return response()->json(['error' => 'Error'], 500);
    }
}
