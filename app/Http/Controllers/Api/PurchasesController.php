<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Payment;
use App\Http\Controllers\Controller;

class PurchasesController extends Controller
{
  public function index()
  {
    try {
      $user = \Auth::user();
      $payments = Payment::where([
        'user_id' => $user->id
      ])
      ->get()
      ->toArray();

      return \Response::json([
        'payments' => $payments
      ]);
    } catch (\Exception $e) {
      return \Response::json([
        'error' => $e->getMessage()
      ], 500);
    }
  }

  public function store(Request $request)
  {
    try {

      $data = $request->all();
      $user = \Auth::user();

      // Data
      $data['user_id'] = $user->id;
      $data['response'] = json_encode($data);
      $data['payment_type_key'] = $data['status'];
      $paymentType = PaymentType::getByKey($data['payment_type_key']);

      // Save to payments
      $payment = new Payment([
        'user_id' => $data['user_id'],
        'payment_type_id' => $paymentType->id,
        'product_id' => $data['id'],
        'response' => $data['response']
      ]);
      
      $payment->save();              

      return \Response::json([
        'payments' => $payment
      ]);

    } catch (\Exception $e) {
      return \Response::json([
        'error' => $e->getMessage()
      ], 500);
    }

  }
}
