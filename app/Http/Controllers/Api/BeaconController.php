<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ShopBeacon as Beacon;

class BeaconController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('timestamp')) {
            $lastUpdateApp = Carbon::createFromTimestamp($request->input('timestamp'));
            $lastUpdateTimeDbRecord = Beacon::select('updated_at')->orderBy('updated_at', 'desc')->limit(1)->first();
            if(!$lastUpdateTimeDbRecord->updated_at->gt($lastUpdateApp)) {
                return response()->json('', 204);
            }
        }
        $categories = Beacon::active()->get()->toArray();
        return response()->json(Beacon::apiFormatIndex($categories));
    }
}
