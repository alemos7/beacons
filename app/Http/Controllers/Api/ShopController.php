<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Shop;
use App\Models\ShopBeacon;
use App\Models\Payment;
use Illuminate\Support\Facades\DB;

class ShopController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $include = $request->has('include') ? $request->get('include') : [];
        $shopBeacon = [];
        $withBeacons = false;
        $retrievePointsAndPayments = (in_array('points', $include) && in_array('payments', $include)) || in_array('user_points', $include);

        if ($request->has('beacon') OR $request->has(['lat', 'lng'])) {
            if ($request->has('beacon')) {

//                $distance = null;
                $beacon = null;
                /**
                 * Busco el shop al que pertenece el beacon
                 */

                $beacons = json_decode($request->input('beacon'));

                if(count($beacons)){

                    $shopBeacon = Shop::getShopsByBeacon($beacons, [
                        'add_distances' => true
                    ]);

                    if(count($shopBeacon)){
                        $lat = floatval($shopBeacon[0]["shop"]["lat"]);
                        $lng = floatval($shopBeacon[0]["shop"]["lng"]);
                        $withBeacons = true;
                    }

                }

                if ($beacons == null || !$withBeacons) {
                    return response()->json(null, 404);
                }

            } else {
                $lat = floatval($request->input('lat'));
                $lng = floatval($request->input('lng'));
            }

//            $lat = floatval($lat);
//            $lng = floatval($lng);

            if (!is_float($lat) || !is_float($lng)) {
                return \Response::json("Lat or Lng coordinate is wrong", 400);
            }

            $distanceSelect = "shops.*,
                                (p.distance_unit
                                 * DEGREES(ACOS(COS(RADIANS(p.latpoint))
                                 * COS(RADIANS(shops.lat))
                                 * COS(RADIANS(p.longpoint - shops.lng))
                                 + SIN(RADIANS(p.latpoint))
                                 * SIN(RADIANS(shops.lat))))) * 1000 AS dist";

            if($request->has('user_id')){
                $user = User::find($request->input('user_id'));
            }else{
                $user = \Auth::user();
            }

            $paymentPointsRelationships = [
                'payments' => function($q) use($user) {
                    return $q->where('user_id', $user->id);
                },
                'points' => function($q) use($user) {
                    return $q->where('user_id', $user->id);
                }
            ];

            $relationships = [
                'images',
                'califications',
                'califications.user'
            ];

            if ($retrievePointsAndPayments && $user) {
                $relationships = array_merge($relationships, $paymentPointsRelationships);
            }

            $shopsModel = Shop::with($relationships);

            if ($retrievePointsAndPayments && $user) {
                $shopsModel->has('points');
                $shopsModel->has('payments');
            }

            $shops =
                $shopsModel
                ->has('images')
                ->active()
                ->where('logo', '<>', '')
                ->selectRaw($distanceSelect)
                ->leftJoin(
                    \DB::raw("(SELECT
                            {$lat} AS latpoint, {$lng} AS longpoint,
                                111.045 AS distance_unit) AS p"),
                    'shops.id', '=', 'shops.id'
                )
                ->orderBy('dist')
                ->get()
                ->toArray();

            if($retrievePointsAndPayments && $user){
                foreach($shops as $key => $s){
                    if(!count($s["points"])){
                        unset($shops[$key]);
                    }
                }
            }

            $formattedShop = Shop::apiFormatCloserShops($shops, $shopBeacon);

            if ($withBeacons){
              $ret = [];
              foreach($formattedShop as $s){
                if (array_key_exists('from_beacon', $s)){
                  $ret[] = $s;
                }
              }
              return response()->json($ret);
            }else
              return response()->json($formattedShop);
        } else {
            $shops = Shop::with('images', 'califications', 'califications.user')
                ->has('images')
                ->active()
                ->where('logo', '<>', '')
                ->get()
                ->toArray();
            return response()->json(Shop::apiFormatIndex($shops));
        }
    }

    public function getPoints(Request $request){

        try{

            $input = $request->all();
            $shopBeacon = [];
            $user = User::find($input["user_id"]);

            if(!$user){
                return response()->json([
                    'error' => "UNAHUTORIZED"
                ], 500);
            }

            if ($request->has('beacon') OR $request->has(['lat', 'lng'])) {

                if ($request->has('beacon')) {

                    $beacon = null;

                    $beacons = json_decode($request->input('beacon'));

                    if(count($beacons)){

                        $shopBeacon = Shop::getShopsByBeacon($beacons, [
                            'add_distances' => true
                        ]);

                        if(count($shopBeacon)){
                            $lat = floatval($shopBeacon[0]["shop"]["lat"]);
                            $lng = floatval($shopBeacon[0]["shop"]["lng"]);
                        }

                    }

                    if ($beacons == null) {
                        return response()->json(null, 404);
                    }

                } else {
                    $lat = floatval($request->input('lat'));
                    $lng = floatval($request->input('lng'));
                }

                $distanceSelect = "shops.*,
                                (p.distance_unit
                                 * DEGREES(ACOS(COS(RADIANS(p.latpoint))
                                 * COS(RADIANS(shops.lat))
                                 * COS(RADIANS(p.longpoint - shops.lng))
                                 + SIN(RADIANS(p.latpoint))
                                 * SIN(RADIANS(shops.lat))))) * 1000 AS dist";

                $shops =
                    Shop::with(
                    [
                        'images',
                        'califications',
                        'califications.user',
                        'payments' => function($q) use($user) {
                            return $q->where('user_id', $user->id);
                        },
                        'points' => function($q) use($user) {
                            return $q->where('user_id', $user->id);
                        }
                    ])
                    ->has('images')
                    ->active()
                    ->where('logo', '<>', '')
                    ->selectRaw($distanceSelect)
                    ->leftJoin(
                        \DB::raw("(SELECT
                        {$lat} AS latpoint, {$lng} AS longpoint,
                            111.045 AS distance_unit) AS p"),
                        'shops.id', '=', 'shops.id'
                    )
                    ->orderBy('dist')
                    ->get()
                    ->toArray();

                foreach($shops as $key => $s){
                    if(!count($s["points"])){
                        unset($shops[$key]);
                    }
                }

                return response()->json(Shop::apiFormatCloserShops($shops, $shopBeacon));

            }else{

                $shops = Shop::with([
                    'images',
                    'califications',
                    'califications.user',
                    'payments' => function($q) use($user) {
                        return $q->where('user_id', $user->id);
                    },
                    'points' => function($q) use($user) {
                        return $q->where('user_id', $user->id);
                    }
                ])
                ->has('images', 'payments')
                ->active()
                ->where('logo', '<>', '')
                ->get()
                ->toArray();

                foreach($shops as $key => $s){
                    if(!count($s["points"])){
                        unset($shops[$key]);
                    }
                }

                return response()->json(Shop::apiFormatIndex($shops));

            }

        }catch(Exception $e){

            return response()->json([
                'error' => $e->getMessage()
            ], 500);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shop = Shop::with(['images'])->where('logo', '<>', '')->active()->findOrFail($id)->toArray();
        return response()->json(Shop::apiFormatShow($shop));
    }

}
