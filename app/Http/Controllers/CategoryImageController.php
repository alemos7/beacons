<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use File;
use Response;

// Models
use App\Models\Category;
use App\Models\CategoryImage;

class CategoryImageController extends Controller
{
    /**
     * Display all images from shop
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showByCategory($id)
    {
        if(! is_numeric($id) ){
            $category = new Category();
            $category->images = CategoryImage::where('category_id',null)->where('image',$id)->get();
        }else{
            $category = Category::find($id);
        }
        return view('categoryimage.list',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->ajax()){
            $request['category_id'] = ($request['category_id'])? $request['category_id'] : null;

            if( $request['category_id'] && count( Category::find($request['category_id'])->images()->get() )  ){
            // Si la categoria tiene imagenes, selecciona la primera
                $categoryimage = Category::find($request['category_id'])->images()->get()[0];
            }else{                
                $token  = $request['_token'];
                $images = CategoryImage::where('category_id',null)->where('image',$token)->get(); 
                if( count($images)  ){
                    $categoryimage = $images[0];
                }else{
                    $categoryimage = new CategoryImage();
                }
            }
            $request['file_image'] = $request->images;
            $request['image'] = $request->_token;
            $categoryimage->update($request->all());  
        }
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $path = storage_path() . '/app/' . env('STORAGE_CATEGORY_IMAGES') . '/' . $id;

        if(!File::exists($path)) abort(404);

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoryimage = CategoryImage::find($id);
        $categoryimage->delete();
        flash( trans('process.success',['model' => trans('models.image.article') , 'process' => trans('process.deleted') ]) , 'success');
        return redirect()->back();    
    }
}