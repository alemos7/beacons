<?php

namespace App\Http\Controllers;

use App\Http\Requests\Shop\StoreRequest;
use App\Http\Requests\Shop\UpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// Models
use App\Models\Shop;
use App\Models\ShopImage;
use App\Models\User;
use App\Models\Product;
use App\Models\ProductOffer;
use App\Models\ShopBeacon;
use App\Models\Province;
use App\Models\Category;
use App\Models\MP_Category;
use App\Models\Word;

class ShopController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shops = Shop::accesible()->orderBy('name', 'asc')->get();

        return view('shop.index', compact('shops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->handlePermission(['seller','operator','administrator']);

        $shop = new Shop();
        $users = User::asignable()->orderBy('first_name','asc')->get();
        $provinces = Province::orderBy('name','asc')->get();
        $title = trans('process.create');
        $post = url('shops');
        $words = Word::orderBy('word','asc')->get();
        return view('shop.show',compact('shop','provinces','users','title','post','words'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        set_time_limit(60*1);
        $shop = new Shop();
        $data = $request->all();
        if(empty(trim($data['schedule']))){
            $data['schedule'] = '-';
        }
        $data['active'] = false;

        if(!$this->validateKey($request)){
            flash( 'La PUBLIC_KEY producción de mercado pago enviada es de pruebas, porfavor ingrese una PUBLIC_KEY producción de mercado pago valida', 'danger');
            return redirect()->back();
        }

        $shop->update($data); //Se usa update en vez de save por que se agregaron ciertas magias (Ej guardar los usuarios asignados)
        $shop->creator()->associate(\Auth::user());
        $shop->save();

        $cont = 0;
        $images = ShopImage::where('shop_id',null)->where('image',$request->_token)->get();
        if( count($images) ){
            $attributes = [];
            $attributes['shop_id'] = $shop->id;
            foreach ($images as $image) {
                $image->update($attributes);
                $image->updateFileNames();
            }
        }
        flash( trans('process.success',['model' => trans('models.shop.article') , 'process' => trans('process.created') ]) , 'success');

        return redirect('/shops/'.$shop->id.'/set-image');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Se quita el getPermission ya que solo se busca dentro del scope accesible()
        try
        {
            $shop = Shop::accesible()->findOrFail($id);
            $categories = Category::orderBy('name', 'asc')->get();
            $mp_categories = MP_Category::orderBy('name', 'asc')->get();
            $provinces = Province::orderBy('name', 'asc')->get();
            $users = User::asignable()->orderBy('first_name', 'asc')->get();
            $product = new Product();
            $shopBeacon = new ShopBeacon();
            $words = Word::orderBy('word', 'asc')->get();
            $title = trans('process.update');
            $post = url('shops/' . $shop->id);
            $offer = count($product->offer) ? $product->offer : new ProductOffer;
            return view('shop.show', compact('shop', 'provinces', 'categories', 'mp_categories', 'users', 'product', 'shopBeacon', 'words', 'title', 'post', 'offer'));
        }
        catch(\Exception $e)
        {
            return redirect('shops');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $shop = Shop::accesible()->findOrFail($id);

        if(!$this->validateKey($request)){
            flash( 'La PUBLIC_KEY producción de mercado pago enviada es de pruebas, porfavor ingrese una PUBLIC_KEY producción de mercado pago valida', 'danger');
            return redirect()->back();
        }

        $data = $request->all();

        if(empty(trim($data['schedule']))){
            $data['schedule'] = '-';
        }

        $shop->update($data);

        if(!$request->input('active')){
            $shopBeacon = new ShopBeacon();
            $shopBeacon->where('shop_id', $id)->update(['active' => 0]);
        }


        flash( trans('process.success',['model' => trans('models.shop.article') , 'process' => trans('process.updated') ]) , 'success');

        return redirect('shops');
    }

    public function setActiveStatus(Request $request, $id)
    {
        $shop = Shop::accesible()->findOrFail($id);

        $shop->active = $request->active;

        $shop->save();

        flash( trans('process.success',['model' => trans('models.shop.article') , 'process' => trans('process.updated') ]) , 'success');

        return redirect('shops');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sortImages(Request $request)
    {
        if($request->ajax()){
            $order = 0;
            foreach ($request->images as $imageId) {
                $order++;
                $image = ShopImage::find($imageId['value']);
                $image->order = $order;
                $image->save();
            }
        }
    }
    /**
     * Display products of specified shop.
     * Auth: Only administrator
     *
     * @param  int  $shop_id
     * @return \Illuminate\Http\Response
     */
    public function showProducts($shop_id)
    {
        $shop = Shop::accesible()->findOrFail($shop_id);
        return response()->json($shop->products()->get());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (\Auth::user()->type !== 'administrator')
        {
            throw new \Exception('Solo el admin puede eliminar shops');
        }

        $shop = Shop::accesible()->findOrFail($id);
        // Delete all images of shop
        // foreach ($shop->images as $shopImage){
        //         $shopImage->delete();
        // }
        Product::where("shop_id", "=", $id)->delete();
        $shop->delete();
        flash( trans('process.success',['model' => trans('models.shop.article') , 'process' => trans('process.deleted') ]) , 'success');

        return redirect('shops');
    }

    private function handlePermission($roles_allowed)
    {
        $user = Auth::user();

        if($user->type == 'administrator')
        {
            return true;
        }

        if(in_array(Auth::user()->type, (array) $roles_allowed))
        {
            return true;
        }

        flash()->overlay( trans('process.alert.denied') , trans('process.denied') );
        return false;
    }

    public function setImageAfterCreate($id){
        try
        {
            $shop = Shop::accesible()->findOrFail($id);
            $categories = Category::orderBy('name', 'asc')->get();
            $mp_categories = MP_Category::orderBy('name', 'asc')->get();
            $provinces = Province::orderBy('name', 'asc')->get();
            $users = User::asignable()->orderBy('first_name', 'asc')->get();
            $product = new Product();
            $shopBeacon = new ShopBeacon();
            $words = Word::orderBy('word', 'asc')->get();
            $title = trans('process.update');
            $post = url('shops/' . $shop->id);
            $offer = count($product->offer) ? $product->offer : new ProductOffer;
            return view('shop.imageForm', compact('shop', 'provinces', 'categories', 'mp_categories', 'users', 'product', 'shopBeacon', 'words', 'title', 'post', 'offer'));
        }
        catch(\Exception $e)
        {
            return redirect('shops');
        }
    }

    public function checkIfHaveImages($id){
        $shops = new ShopImage();
        $imgs = $shops->where('shop_id', $id)->count();

        if($imgs > 0){
            return response()->json([
                'success' => true
            ]);
        }

        return response()->json([
            'error' => 'El shop debe tener al menos una imagen para estar activo'
        ]);
    }

    private function validateKey($request){
        if ( empty($request->mp_key) )
        {
            return true;
        }
        if(preg_match('/^TEST-/', $request->mp_key) && env('VALIDATE_MP', 1))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}