<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdvertisingController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->type == 'operator')
            $shops = Auth::user()->shops()->get();
        else
            $shops = Shop::all();
        return view('shop.index',compact('shops'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        if(!$this->getPermission(0))
            return redirect('shops');
        $shop = new Shop(); 
        $users = User::all();
        $provinces = Province::all();
        $title = trans('process.create');
        $post = url('shops');
        return view('shop.show',compact('shop','provinces','users','title','post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $shop = new Shop();
        $shop->update($request->all());
        flash( trans('process.success',['model' => trans('models.shop.article') , 'process' => trans('process.created') ]) , 'success');
        return $this->create();     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!$this->getPermission($id))
            return redirect('shops');
        $shop = Shop::find($id); 
        $provinces = Province::all();
        $users = User::all();
        $product = new Product();
        $shopBeacon = new ShopBeacon();
        $title = trans('process.update');
        $post = url('shops/'.$shop->id);
        return view('shop.show',compact('shop','provinces','users','product','shopBeacon','title','post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        if($this->getPermission($id)){
            $shop = Shop::find($id);
            $shop->update($request->all());
            flash( trans('process.success',['model' => trans('models.shop.article') , 'process' => trans('process.updated') ]) , 'success');
        }
        return $this->show($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->getPermission(0)){
            Shop::find($id)->destroy($id);
            flash( trans('process.success',['model' => trans('models.shop.article') , 'process' => trans('process.deleted') ]) , 'success');
        }
        return redirect('shops');        
    }
}
