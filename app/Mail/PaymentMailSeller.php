<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// Models
use App\Models\Payment;

class PaymentMailSeller extends Mailable
{
    use Queueable, SerializesModels;

    public $payment;

    public $subject = 'Vendiste un producto en BlueShop';
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return  $this->view('email.payment')
            ->with('payment', $this->payment);
    }
}