<?php

namespace App\Helpers;

use Image;

class ImagesResize 
{
	public static function resize($path,$name,$w,$h)
    {   
        $img = Image::make($path.$name);
        if($img->width()>$img->height()){
            $img->resize($w, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }else{
            $img->resize(null, $h, function ($constraint) {
                $constraint->aspectRatio();
            });
        }       
        $img->save();
    }	

    public static function maxSize($path,$name,$w,$h)
    {   
        $img = Image::make($path.$name);
        if($img->width() > $w){
            $img->resize($w, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        $img->save();
        if( $img->height() > $h ){
            $img->resize(null, $h, function ($constraint) {
                $constraint->aspectRatio();
            });
        }       
        $img->save();
    }
}