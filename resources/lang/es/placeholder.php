<?php

return [
	
	'business_name' => 'Indique la razón social',
	'category' => 'Seleccione una categoría',
	'code' => 'Indique el código',
	'content' => 'Indique el contenido',
	'color' => 'Indique el color',
	'contact_management' => 'Indique el nombre contacto',
	'cuit' => 'Indique su clave única de identificación tributaria',
	'description' => 'Indique la descripción',
	'distance' => 'Indique la distancia',
	'email' => 'Indique su correo electrónico',
	'first_name' => 'Indique su nombre',
	'iibb' => 'Indique los ingresos brutos',
	'product' => 'Seleccione un producto',
	'point' => 'Indique la cantidad de puntos',
	'phone' => 'Indique su numero de teléfono',
	'name' => 'Indique el nombre',
	'mp_key' => 'Indique la key de Mercado Pago',
	'last_name' => 'Indique su apellido',
	'offer' => 'Indique un tipo de oferta',
	'password' => 'Indique la contraseña',
	'password2' => 'Repita su contraseña',
	'price' => 'Indique el precio',
	'serial' => 'Mayor/Minor ejemplo 2222/1111',
	'shop' => 'Seleccione su negocio',
	'words' => 'Indique las palabras claves de búsqueda del productos',


];
