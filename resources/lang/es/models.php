<?php

return [

    'generic' => 'Elemento',

    'category' => 'Categoría',
    'category.article' => 'La categoría',
    'categories' => 'Categorías',

    'product' => 'Producto',
    'product.article' => 'El producto',
    'products' => 'Productos',

    'message' => 'Mensaje',
    'message.article' => 'El mensaje',
    'message.massive' => 'Mensajes masivos',
    'messages' => 'Mensajes',

    'shop' => 'Negocio',
    'shop.article' => 'El negocio',
    'shops' => 'Negocios',

    'image' => 'Imagen',
    'image.article' => 'La imagen',
    'images' => 'Imágenes',
    'images.article' => 'Las imágenes',

    'word' => 'Palabra',
    'word.article' => 'La palabra',
    'words' => 'Palabras',
    'words.article' => 'Las palabras',

    'user.generic' => 'Usuario',
    'user' => 'Usuario admin',
    'user.article' => 'El usuario admin',
    'users' => 'Usuarios admin',

    'client' => 'Usuario app',
    'client.article' => 'El usuario app',
    'clients' => 'Usuarios app',

    'beacon' => 'Beacon',
    'beacon.article' => 'El beacon',
    'beacons' => 'Beacons',

    'list' => 'Listado de :model',

    'banner' => 'Banner',
    'banner.article' => 'El Banner',
    'banners' => 'Banner',

    'payment' => 'Venta',
    'payment.article' => 'La venta',
    'payments' => 'Ventas',
];
