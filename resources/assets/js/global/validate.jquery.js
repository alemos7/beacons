$(document).ready(function(){
	$('form').parsley({
		excluded: "input[type=button], input[type=submit], input[type=reset], input[type=hidden], input:hidden"
	});
	$('form button').click(function() {
		if(!$(this).parents('form:first').parsley().validate()){
      if(window.location.pathname != '/login'){
			 $("#full-warning").niftyModal();
      }
		}
	});
	
	$('form [required]').on('focusout', function() {
		if($(this).is(':visible')){
			$(this).parsley().validate();
		}
	});
});