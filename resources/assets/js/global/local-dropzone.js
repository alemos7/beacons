$(document).ready(function(){
	$('.local-dropzone').on('click', function(){
		$(this).next('input[type="file"]').click();
	});
	$('.local-dropzone').next('input[type="file"]').on('change', function(e){
		var filename =  URL.createObjectURL(event.target.files[0]);
		$(this).prev(".local-dropzone").find('#logo-preview').attr('src', filename );
		local_dropzone_checkfile($(this).prev(".local-dropzone"));
	});
  local_dropzone_checkfile = function(dropzone){
    if( dropzone.find("#logo-preview").attr('src') ){
			dropzone.find('#logo-preview').prev('label').css('visibility','hidden');
		}
	}
  $('.local-dropzone').each( function(indx){
    local_dropzone_checkfile( $(this) ); 
  });
});