$(document).ready(function() {
	$('.icp-auto').iconpicker({
		// icons: [ 'fa-car','fa-suitcase','fa-laptop','fa-mobile-phone','fa-futbol-o','fa-phone','fa-gamepad','fa-cutlery'],
		templates: {
			search: '<input type="search" class="form-control iconpicker-search" placeholder="Buscar..." />',
		}
	});
	if($('.icp-auto').val()){
		$('.icp-auto').val( $('.icp-auto').val().substring(3) );
	}
	$('.icp-auto').on('iconpickerUpdated', function() {
		$(this).val( $(this).val().substring(3) );
	});
});