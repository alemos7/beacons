$(document).ready(function() {
  $.fn.select2.defaults.set( "theme", "bootstrap" );
  $('.select2').select2();
  $('span.select2').removeAttr('style');
  
  // Coloca span.select2-container antes del select
  // Para evitar errores visuales con la libreria de validacion 'parsley'
  $('span.select2-container').each(function(){
    $(this).insertBefore( $(this).prev('select') );
  });

});