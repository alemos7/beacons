function set_money_format(n){
  n = n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  return n.replace(/.([^.]*)$/,',$1');
}