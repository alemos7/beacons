$(document).ready(function() {
	if($("input[name='active']").is(":checked")) {
	    $("#active-text").text('Activo');
    }else{
	    $("#active-text").text('Inactivo');
	}
});

$("input[name='active']").change(function(){
	if($(this).is(":checked")) {
    	$("#active-text").text('Activo');
    }else{
    	$("#active-text").text('Inactivo');
	}
});
