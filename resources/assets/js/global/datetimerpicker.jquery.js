  $(function() {
    $('.datetimepicker').datetimepicker({
      language: 'es',
      autoclose: true,
      componentIcon: '.mdi.mdi-calendar',
      navIcons:{
        rightIcon: 'mdi mdi-chevron-right',
        leftIcon: 'mdi mdi-chevron-left'
      }
    });
  });