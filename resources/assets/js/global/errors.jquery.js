$(document).ready(function() {

	setClassError();
	
	function setClassError(){
    	if(typeof errorsKey !== 'undefined'){
			i=0;    		
    		errorsKey.forEach(function(item){
	    		selector = '[name='+ item +']'; 
	    		$( selector ).addClass( 'error' );
	    		htmlError = '<ul class="parsley-errors-list filled"><li class="parsley-required">' + errorsMessage[i] + '</li></ul>';
	    		$( selector ).after( htmlError );
	    		i++;
    		});
    	}
	}
});
