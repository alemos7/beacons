$('.banner-shop').on('change',function(){
  let banner = $(this).data('banner');
  let selected = $(this).find('option:selected');
  let products = selected.data('products');  
  let selectProduct = $('#product-'+banner);

  selectProduct.find("option:gt(0)").remove();
  if(this.value){
    products.forEach(function(elem,pos){
      selectProduct.append('<option value="' + elem.id  + '">'+ elem.name +'</option>');
    });
    selectProduct.removeAttr('disabled');
  }else{
    selectProduct.attr('disabled','');
  }
});


function selectProduct(selectShop){
  selectShop.trigger('change');
  let banner = selectShop.data('banner');
  let selected = selectShop.data('product');
  let selectProduct = $('#product-'+banner);
  selectProduct.val(selected);
}
// 
$('.banner-shop').on('load',function(){ selectProduct($(this)); });
$('.banner-shop').trigger('load');