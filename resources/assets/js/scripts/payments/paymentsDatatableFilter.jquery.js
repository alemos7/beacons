// Cantidad de Resultados
$("#filter-length").on('change', function(){
    $('#payment-datatable').DataTable().page.len( $(this).val() ).draw();
});
/////////////////

// Simple Search
$("#filter-search").on('keyup', function(){
    $('#payment-datatable').DataTable().search( $(this).val() ).draw();
});
/////////////////

// Filtro por Estado de Pago
$('#filter-pay-status').on( 'change', function (){
    $('#payment-datatable').DataTable().draw();
});

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var status = $('#filter-pay-status').val();
        if(status=='Todos'){
            return true;
        }
        return (status == data[7]);
    });
/////////////////

// Filtro por Entrega
$('#filter-delivery').on( 'change', function (){
    $('#payment-datatable').DataTable().draw();
});

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var delivery = $('#filter-delivery').val();
        if(delivery=='Todos'){
            return true;
        }
        return (delivery == data[8]);
    });
/////////////////

// Filtro por Negocio
if( $('#filter-shop').length  ){
    $('#filter-shop').on( 'change', function (){
        $('#payment-datatable').DataTable().draw();
    });

    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {
            var shop = $('#filter-shop').val();
            if(shop=='Todos'){
                return true;
            }
            return (shop == data[10]);
        });
}
/////////////////

// Filtro por Fecha
datePicker = $('#date-created');

datePicker.daterangepicker({    
    locale: {
      format: 'DD/MM/YYYY'
    },
    "startDate": new Date().getDate(),
    "endDate": new Date().getDate(),
    "opens": "center",
    "buttonClasses": "btn btn-sm",
     ranges: {
           'Hoy': [moment(), moment()],
           'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Últimos 7 días': [moment().subtract(6, 'days'), moment()],
           'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
           'Este mes': [moment().startOf('month'), moment().endOf('month')],
           'Mes anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
}, function(start, end, label) {
    $('#payment-datatable').DataTable().draw();
});

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var fmin = $('[name=daterangepicker_start]').val().split('/');
        var fmax = $('[name=daterangepicker_end]').val().split('/');
        var date = data[6].split('/');
        date[2] = date[2].split(' ')[0];

        var check = date[2]  + date[1] + date[0];
        var from = fmin[2] + fmin[1] + fmin[0];
        var to = fmax[2] + fmax[1] + fmax[0];
        if(fmin[2]==undefined)return true;
        return (check >= from && check <= to);
    });
///////////////////
$('#payment-datatable').DataTable({
    "lengthChange": false,
    "columnDefs": [
        {
            "targets": 2,
            "render": function ( data, type, row ) {
                return set_money_format(data);
            }
        },
        ]
});

$("#payment-datatable_filter").hide();