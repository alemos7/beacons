$('#products-datatable').DataTable({    
    "language": {
        "decimal": ",",
        "thousands": "."
    },
    "lengthChange": false,
    "order": [[ 0, "desc" ]],
    initComplete: function () {
        this.api().column(4).every( function () {
            var column = this;
            var select = $('#filterByOffer')
            .on( 'change', function () {
                var val = $.fn.dataTable.util.escapeRegex($(this).val());
                column
                .search( val, true, false )
                .draw();
            } );    
        });
    }
});


// Simple Search
$("#filter-search").on('keyup', function(){
    $('#products-datatable').DataTable().search( $(this).val() ).draw();
});
/////////////////
// Cantidad de Resultados
$("#filter-length").on('change', function(){
    $('#products-datatable').DataTable().page.len( $(this).val() ).draw();
});
/////////////////

$("#products-datatable_filter").hide();