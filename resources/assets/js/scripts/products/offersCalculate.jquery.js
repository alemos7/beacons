document.addEventListener('DOMContentLoaded', function() { 

    offerSelectChange = function(){
        $(".offerSelect").hide();
        var target = $('#offerSelect').find(':selected').data('target');
        $( target ).fadeIn();
        $(".offer-calculate:hidden").val('');
        calculate();
    }

    $('.offer-calculate,[name="price"]').on('change',function(){
        calculate();
    });

    $('.price-format,.before-offer-price').on('change',function(){
        $(this).val( setFormat($(this).val()) );
    });

    $('#offerSelect').change(function(){
        offerSelectChange();
    });
    
    $('#form-product').on('submit', function(){
        $('[name="vars[]"]:hidden').removeAttr('name');
        $('[name=price]').val( removeFormat($('[name=price]').val()) );
        $('[name=before_offer_price]').val( removeFormat($('[name=before_offer_price]').val()) );
    });

    $('.price-format,.before-offer-price').trigger('change');
    offerSelectChange();
    calculate();
}, false);

calculate = function(){
    var CSRF_TOKEN = $('input[name=_token]').val();
    var type = $('#offerSelect').val();
    var vars = $(".offer-calculate:visible").map(function(){
        return $(this).val();
        ;}).get();
    var price = removeFormat($('#price').val());

    if(price==0) return;

    $.ajax({
        type: 'POST',
        data: { 
            type:type,
            vars:vars,
            price:price,
            _token: CSRF_TOKEN
        },
        url: '../product-offers/calculate',
        success: function (data) {
            $(".before-offer-price").val( setFormat(data) );
        },
        error: function(data){
            $(".before-offer-price").val(price);
        }
    }); 
}

setFormat = function(val) {
    val = parseFloat( removeFormat(val) );
    if(isNaN(val)){
        return 0;
    }
    var re = '\\d(?=(\\d{' + (3 || 3) + '})+' + (2 > 0 ? '\\D' : '$') + ')',
        num = val.toFixed(Math.max(0, ~~2));
    return (',' ? num.replace('.', ',') : num).replace(new RegExp(re, 'g'), '$&' + ('.' || ','));
}
removeFormat = function(val) {
    if(!isNaN(val)){
        return val;
    }
    val = val.split('.').join('');
    return  val.split(',').join('.') ;
};