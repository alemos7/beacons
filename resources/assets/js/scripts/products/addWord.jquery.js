$(document).ready(function(){
  $("#words").select2({
    tags: true,
    language: "es",
    isNew: true,
    createTag: function (tag) {
        return {
            id: tag.term,
            text: tag.term,
            isNew : true
        };
    }
  }).on("select2:select", function(e) {
    if(e.params.data.isNew){
      word = e.params.data.text;
      let CSRF_TOKEN = $('input[name=_token]').val();
      $.ajax({
        type: 'POST',
        data: { 
         word:word,
         _token: CSRF_TOKEN
        },
        url: '../words',
        success: function (data) {
        // console.log(data);
        // console.log("success");
        },
        error: function(data){
        // console.log(data);         
        // console.log("error");
      }
    }); 
    }
  });
});