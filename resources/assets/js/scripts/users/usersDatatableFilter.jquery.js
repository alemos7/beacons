// Cantidad de Resultados
$("#filter-length").on('change', function(){
    $('#users-datatable').DataTable().page.len( $(this).val() ).draw();
});
/////////////////

// Simple Search
$("#filter-search").on('keyup', function(){
    $('#users-datatable').DataTable().search( $(this).val() ).draw();
});
/////////////////

// Filtro por Estado
$('#filter-status').on( 'change', function (){
    $('#users-datatable').DataTable().draw();
});

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var status = $('#filter-status').val();
        if(status=='Todos'){
            return true;
        }
        return (status == data[1]);
    });
/////////////////

// Filtro por Perfil
$('#filter-profile').on( 'change', function (){
    $('#users-datatable').DataTable().draw();
});

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var type = $('#filter-profile').val();
        if(type=='Todos'){
            return true;
        } 
        return (type == data[7]);
    });
/////////////////


// Filtro por fecha de Alta

$('#date-alta').daterangepicker({    
    locale: {
      format: 'DD-MM-YYYY'
    },
    "startDate": '01-01-2017',
    "endDate": new Date().getDate(),
    "opens": "center",
    "buttonClasses": "btn btn-sm",
     ranges: {
           'Hoy': [moment(), moment()],
           'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Últimos 7 días': [moment().subtract(6, 'days'), moment()],
           'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
           'Este mes': [moment().startOf('month'), moment().endOf('month')],
           'Mes anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
}, function(start, end, label) {
    $('#users-datatable').DataTable().draw();
});
$('#date-alta').on( 'change', function (){
    $('#clients-datatable').DataTable().draw();
});

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        let value = $('#date-alta').val();
        if( value.split(' - ').length <= 1 ){ return true; }

        let fmin = value.split(' - ')[0].split('-');
        let fmax = value.split(' - ')[1].split('-');
        var date = data[6].split('/');

        var check = date[2]  + date[1] + date[0];
        var from = fmin[2] + fmin[1] + fmin[0];
        var to = fmax[2] + fmax[1] + fmax[0];
        if(fmin[2]==undefined)return true;
        // console.log('check: ' + check + ' from: ' + from + ' to: ' + to );
        // console.log(check >= from && check <= to);
        return (check >= from && check <= to);
    });
///////////////////

$('#users-datatable').DataTable({
    "lengthChange": false,
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excelHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4, 5, 6, 7 ]
            },
            title: 'Listado de Usuarios Admin ' + moment().format('DD-MM-YYYY'),
        }
    ],     
    "columnDefs": [
        {
            "targets": [ 2, 4, 5 ],
            "visible": false,
            "searchable": false
        }
    ]
});
$("#users-datatable_filter").hide();

$("#excel-button").on('click',function(){
    $(".buttons-excel").trigger('click');
});