// Cantidad de Resultados
$("#filter-length").on('change', function(){
    $('#clients-datatable').DataTable().page.len( $(this).val() ).draw();
});
/////////////////

// Simple Search
$("#filter-search").on('keyup', function(){
    $('#clients-datatable').DataTable().search( $(this).val() ).draw();
});
/////////////////

// Filtro por Estado
$('#filter-status').on( 'change', function (){
    $('#clients-datatable').DataTable().draw();
});

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        let status = $('#filter-status').val();
        if(status=='Todos'){
            return true;
        }
        return (status == data[1]);
    });
/////////////////


// Filtro por fecha de Alta
$('#date-alta').daterangepicker({    
    locale: {
      format: 'DD-MM-YYYY'
    },
    "startDate": '01-01-2017',
    "endDate": new Date().getDate(),
    "opens": "center",
    "buttonClasses": "btn btn-sm",
     ranges: {
           'Hoy': [moment(), moment()],
           'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Últimos 7 días': [moment().subtract(6, 'days'), moment()],
           'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
           'Este mes': [moment().startOf('month'), moment().endOf('month')],
           'Mes anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
}, function(start, end, label) {
    $('#clients-datatable').DataTable().draw();
});

$('#date-alta').on( 'change', function (){
    $('#clients-datatable').DataTable().draw();
});
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        let value = $('#date-alta').val();
        if( value.split(' - ').length <= 1 ){ return true; }

        let fmin = value.split(' - ')[0].split('-');
        let fmax = value.split(' - ')[1].split('-');
        let date = data[7].split('/');

        let check = date[2]  + date[1] + date[0];
        let from = fmin[2] + fmin[1] + fmin[0];
        let to = fmax[2] + fmax[1] + fmax[0];
        if(fmin[2]==undefined)return true;
        // console.log('check: ' + check + ' from: ' + from + ' to: ' + to );
        // console.log(check >= from && check <= to);
        return (check >= from && check <= to);
    });
///////////////////

// Filtro por Genero
$('#filter-gender').on( 'change', function (){
    $('#clients-datatable').DataTable().draw();
});

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        let gender = $('#filter-gender').val();
        if(gender=='Todos'){
            return true;
        }
        col = 4;
        data[col]= data[col].replace(/\s/g, '');
        return (gender == data[col]);
    });
/////////////////

// Filtro por fecha de nacimiento
$('#date-birthday').daterangepicker({    
    locale: {
      format: 'DD-MM-YYYY'
    },
    "startDate": '01-01-1900',
    "endDate": new Date().getDate(),
    "opens": "left",
    "buttonClasses": "btn btn-sm",
     ranges: {
           'Hoy': [moment(), moment()],
           'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Últimos 7 días': [moment().subtract(6, 'days'), moment()],
           'Últimos 30 días': [moment().subtract(29, 'days'), moment()],
           'Este mes': [moment().startOf('month'), moment().endOf('month')],
           'Mes anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
}, function(start, end, label) {
    $('#clients-datatable').DataTable().draw();
});

$('#date-birthday').on( 'change', function (){
    $('#clients-datatable').DataTable().draw();
});
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        let value = $('#date-birthday').val();
        if( value.split(' - ').length <= 1 ){ return true; }

        let fmin = value.split(' - ')[0].split('-');
        let fmax = value.split(' - ')[1].split('-');
        let date = data[6].split('/');

        let check = date[2]  + date[1] + date[0];
        let from = fmin[2] + fmin[1] + fmin[0];
        let to = fmax[2] + fmax[1] + fmax[0];
        if(fmin[2]==undefined)return true;
        // console.log('check: ' + check + ' from: ' + from + ' to: ' + to );
        // console.log(check >= from && check <= to);
        return (check >= from && check <= to);
    });
///////////////////

// Filtro por Tipo
$('#filter-from').on( 'change', function (){
    $('#clients-datatable').DataTable().draw();
});

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        let from = $('#filter-from').val();
        if(from=='Todos'){
            return true;
        }
        col = 8;
        data[col]= data[col].replace(/\s/g, '');
        return (from == data[col]);
    });
/////////////////

$('#clients-datatable').DataTable({
    "lengthChange": false,
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'excelHtml5',
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8]
            },
            title: 'Listado de Usuarios App ' + moment().format('DD-MM-YYYY'),
        }
    ],     
    "columnDefs": [
        {
            "targets": [ 3, 5 ],
            "visible": false,
            "searchable": false
        }
    ]
});
$("#clients-datatable_filter").hide();

$("#excel-button").on('click',function(){
    $(".buttons-excel").trigger('click');
});