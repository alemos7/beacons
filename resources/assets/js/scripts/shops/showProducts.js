$(window).bind("load", function() {
  $('#shop_id').trigger('change');

  setTimeout(function(){
    var productId = $('#selected_product_id').val();
    $('#product_id').val( productId );
  }, 1000); 
});
$('#shop_id').change(function () {
    var shopId  = $(this).val();
    $('#product_id').find('option').remove().end();
    if(shopId =='' || shopId =='Seleccione su negocio' ){
      $('#product_id').attr('disabled','');     
      return;
    }
    
    
    $.ajax({
        type: 'get',
        url: '../shops/'+ shopId +'/products/',
        beforeSend: function(){
          $('#product_loading').fadeIn();
        },
        success: function (data) {    
          var option = '<option value="">Ninguno</option>';
          for (var i=0;i<data.length;i++){
             option += '<option value="'+ data[i]['id'] + '">' + data[i]['name'] + '</option>';
          }
          $('#product_id').removeAttr('disabled');
          $('#product_id').append(option);
          $('#product_loading').delay(300).fadeOut();    
        }
    });
});