@section('containt-title') Usuarios @endsection

@section('menu-usuarios') class="active" @endsection

@section('breadcrumbs') 
    <li><a href="{{ url('users') }}" >{{ trans('models.list',['model'=>trans('models.users')] ) }}</a></li>
    @yield('user-breadcrumbs')
@endsection