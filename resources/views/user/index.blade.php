@extends('layouts.panel')

@section('head')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('lib/bootstrap-daterangepicker/daterangepicker.css') }}"/>
@endsection

@section('content-panel')
@include('user.header')

<a href="{{ url('users/create') }}"  class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}</a>
<a class="btn btn-success pull-right" id="excel-button" data-toggle="tooltip" title="Descargar Excel" data-placement="bottom"><span class="glyphicon glyphicon-download"></span> Excel</a>
<hr>
<div class="form-group text-center user-wrapper">
    <div class="col-sm-2 user-filter">
        <label>Mostrar</label>
        <select id="filter-length" class="form-control input-sm" >
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100" selected>100</option>
        </select>
    </div>
    <div class="col-sm-2 user-filter">
        <label>{{ trans('data.created') }}</label>
        <input type="text" id="date-alta" class="form-control input-sm text-center">
    </div>    
    <div class="col-sm-2 user-filter">
        <label>{{ trans('data.status') }}</label>
        <select id="filter-status" class="form-control input-sm" >
            <option value="Todos">{{ trans('data.all') }}</option>
            <option value="Activo">{{ trans('data.enable') }}</option>
            <option value="Inactivo">{{ trans('data.disable') }}</option>
        </select>
    </div>
    <div class="col-sm-2 user-filter">
        <label>{{ trans('data.profile') }}</label>
        <select id="filter-profile" class="form-control input-sm" >
            <option value="Todos">{{ trans('data.all') }}</option>
            <option value="{{ trans('data.profile.administrator') }}">{{ trans('data.profile.administrator') }}</option>
            <option value="{{ trans('data.profile.operator') }}">{{ trans('data.profile.operator') }}</option>
            {{-- <option value="{{ trans('data.profile.user') }}">{{ trans('data.profile.user') }}</option> --}}
        </select>
    </div>
    <div class="col-sm-2 user-filter">
        <label>Buscar</label>
        <input type="text" id="filter-search" class="form-control input-sm text-center">
    </div>
</div> 
<div class="col-sm-12 table-users">
	<table class="table table-striped table-condensed" id="users-datatable">
	    <thead>
	        <tr>
	            <th class="col col-xs-1 text-center">{{ trans('data.id') }}</th> 
	            <th class="col col-xs-1">{{ trans('data.status') }}</th>
	            <th class="col col-xs-2">{{ trans('data.name') }}</th>
	            <th class="col col-xs-2">{{ trans('data.email') }}</th>
	            <th class="col col-xs-2">{{ trans('data.phone.mobile') }}</th>
	            <th class="col col-xs-2">{{ trans('data.birthday') }}</th>
	            <th class="col col-xs-2">{{ trans('data.created') }}</th>
	            <th class="col col-xs-3">{{ trans('data.profile') }}</th>
	            <th class="col col-xs-2">{{ trans('data.action') }}</th>
	        </tr>
	    </thead>
	    <tbody>
	    @foreach ($users as $user)
	        <tr @if(!$user->active) class="desactive" @endif>
	            <td class="text-center">{{ $user->id }}</td>
	            <td class="text-center"> 
	                @if($user->active) Activo  @else  Inactivo</td>
	                @endif
	            </td>
	            <td><span>{{ $user->full_name }}</span></td> 
	            <td>
	                <span data-toggle="tooltip" title="{{ $user->email }}">{{ str_limit($user->email,30) }}</span>
	            </td>
	            <td><span>{{ $user->phone }}</span></td>
	            <td><span>{{ $user->birthday }}</span></td>
	            <td>
	                {{ $user->created }}
	            </td>
	            <td>
	                @if($user->type == 'administrator')
	                	<span class="label label-md label-danger">{{ trans('data.profile.administrator') }}</span>
	                @elseif($user->type == 'operator')
	                	<span class="label label-md  label-primary">{{ trans('data.profile.operator') }}</span>
	                @elseif($user->type == 'user')
	                	<span class="label label-md bg-gray">{{ trans('data.profile.user') }}</span>
					@elseif($user->type =='seller')
						<span class="label label-md label-warning">{{ trans('data.profile.seller') }}</span>
					@else
						<span class="label label-md bg-gray">{{$user->type}}</span>
	                @endif
	            </td>            
	            <td>
	                <form action='{{ url('users/'.$user->id) }}' method='POST'  id="user-delete-{{ $user->id }}">
	                    <input type="hidden" name="_method" value="DELETE">
	                    {{ csrf_field()}} 
	                    <a class="icon mdi mdi-edit btn btn-primary" href="{{ url('users/'.$user->id) }}" data-toggle="tooltip" title="{{ trans("process.update") }}"></a>
						@if(\Auth::user()->type == 'administrator')
							<a data-modal="full-danger"  class="btn btn-danger md-trigger " onclick="formSelected='user-delete-{{ $user->id }}'" data-toggle="tooltip" title="{{ trans("process.delete") }}" >
								<span class="icon mdi mdi-delete"></span>
							</a>
						@endif
	                </form>
	            </td>
	        </tr>
	        @endforeach
	    </tbody>
	</table>
</div>
@endsection

@section('footer')
    <script src="{{ URL::asset('js/users/usersDatatableFilter.jquery.js') }}" type="text/javascript"></script>
@endsection