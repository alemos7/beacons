@extends('layouts.panel')

@section('content-panel') 
    @include('shop.header')
    @if(in_array(Auth::user()->type,['administrator','seller']))
        <a href="{{ url('shops/create') }}"  class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}</a><br/>
        <hr/>
    @endif
	@include('shop.datatable')
@endsection