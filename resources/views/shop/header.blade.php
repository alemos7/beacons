@section('containt-title') {{ trans('models.shops') }} @endsection

@section('menu-marcas') class="active" @endsection

@section('breadcrumbs') 
    <li><a href="{{ url('shops') }}" >{{ trans('data.list',['model'=>trans('models.shops')] ) }}</a></li>
    @yield('shop-breadcrumbs')
@endsection