@extends('layouts.panel')

@section('content-panel')
    @include('shop.header')
    <div style="display:none;" class="tab-pane fade in active" id="shop-form">@include('shop.form')</div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            <div role="alert" class="alert alert-primary alert-icon alert-icon-colored">
                <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                <div class="message">
                    <strong>Seleccione las imágenes a mostrar de la portada de su negocio.</strong>
                    <ul>
                        <li>Formato: JPG o PNG</li>
                        <li>Tamaño mínimo: 800 x 400 px</li>
                        <li>Tamaño máximo: 1280 x 720 px</li>
                    </ul>
                </div>
            </div>
        </div>
        @include('shopimage.form')
        <div id="shopimage-list" class="col-xs-12">@include('shopimage.list_set')</div>
    </div>
    <br>
    <div class="row">
        <form action="{{ url('shops/'.$shop->id.'/set-active') }}" id="form_status" method="post">
            <div class="form-group col-md-2">
                <label for="status">Estado</label>
                <select class="form-control" id="status" name="active">
                    <option value="1">Activo</option>
                    <option value="0" selected>Inactivo</option>
                </select>
            </div>
            {{ csrf_field()}}
            <div class="col-md-2 col-md-offset-10">
                <button type="submit" class="btn btn-info">Finalizar</button>
            </div>
        </form>
    </div>

@endsection
@section('footer')
    @php /*<script src="{{ URL::asset('js/users/pattern-phone.js') }}" type="text/javascript"></script> */@endphp
    <script src="{{ URL::asset('js/imagesAjax.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/shops/show.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/shops/gmap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/shops/showLocalities.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/shops/sortImages.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/shops/setting_shop.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/products/selectWords.jquery.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/products/addWord.jquery.js') }}" type="text/javascript"></script>
@endsection