<table class="table table-striped table-condensed" id="shops-datatable">
    <thead>
        <tr>
            <th class="text-center">{{ trans('data.id') }}</th>
            <th>{{ trans('data.name') }}</th>
            <th>{{ trans('data.cuit') }}</th>
            <th>{{ trans('data.phone') }}</th>
            <th>{{ trans('data.email') }}</th>
            <th>{{ trans('data.action') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($shops as $shop)
            <tr @if(!$shop->active) class="desactive" @endif>
                <td class="text-center">{{ str_limit($shop->id,30) }}</td>
                <td><a href="{{ url('shops/'.$shop->id) }}">{{ str_limit($shop->name,30) }}</a></td>
                <td>{{ $shop->cuit_formated }}</td>
                <td>{{ str_limit($shop->phone,30) }}</td>
                <td>{{ str_limit($shop->email,30) }}</td>
                <td>
                    <form action='{{ url('shops/'.$shop->id) }}' method='POST' id="shop-delete-{{ $shop->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field()}}
                        <a class="icon mdi mdi-edit btn btn-primary" href="{{ url('shops/'.$shop->id) }}" data-toggle="tooltip" title="{{ trans("process.update") }}"></a>
                        @if(\Auth::user()->type == 'administrator')
                            <a data-modal="full-danger"  class="btn btn-danger md-trigger " onclick="formSelected='shop-delete-{{ $shop->id }}'" data-toggle="tooltip" title="{{ trans("process.delete") }}">
                                <span class="icon mdi mdi-delete"></span>
                            </a>
                        @endif
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@section('footer')
    <script>
        $('#shops-datatable').DataTable({
            "pageLength": 100
        });
    </script>
@endsection