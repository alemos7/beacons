<form action="{{ $post }}" data-form="disable-on-submit" method="POST" id="form-shop" role="form" class="form-horizontal group-border-dashed" enctype="multipart/form-data">
    @if($shop->id != '')
        <input type='hidden' name='_method' value='PUT'>
        <input type='hidden' id='shop-id' value='{{ $shop->id }}'>
    @endif
    {{ csrf_field() }}
    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.name') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <input type="text" class="form-control input-sm"  name="name" placeholder="{{ trans('placeholder.name') }}" value="{{ old('name',$shop->name) }}" maxlength="100" data-parsley-maxlength="100" autofocus required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.business_name') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <input type="text" class="form-control input-sm"  name="business_name" placeholder="{{ trans('placeholder.business_name') }}" value="{{ old('business_name',$shop->business_name) }}" maxlength="150" data-parsley-maxlength="100" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.type') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <select name="vip" class="form-control input-sm" required="required">
                <option value="0" @if($shop->vip == '0') selected="selected" @endif>Normal</option>
                <option value="1" @if($shop->vip == '1') selected="selected" @endif>VIP</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.contact_management') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <input type="text" class="form-control input-sm"  name="contact_management" placeholder="{{ trans('placeholder.contact_management') }}" value="{{ old('contact_management',$shop->contact_management) }}">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.cuit') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <input type="text" name="cuit" class="form-control input-sm" id="cuit" placeholder="{{ trans('placeholder.cuit') }}" value="{{ old('cuit',$shop->cuit) }}" maxlength="11" data-parsley-maxlength="11" data-parsley-minlength="11">
            <!--<input type="hidden" name="cuit">-->
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.iibb') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <input type="text" data-parsley-type="number" name="iibb" class="form-control input-sm" placeholder="{{ trans('placeholder.iibb') }}" title="" value="{{ old('iibb',$shop->iibb) }}">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.iva') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <select name="iva" class="form-control input-sm" required>
                <option value="Responsable Inscripto"@if($shop->iva == "Responsable Inscripto" || old('iva') == 'Responsable Inscripto' ) selected="" @endif>Responsable Inscripto </option>
                <option value="Responsable No Inscripto" @if($shop->iva == "Responsable No Inscripto" || old('iva') == 'Responsable No Inscripto' ) selected="" @endif>Responsable No Inscripto</option>
                <option value="Responsable Monotributista"@if($shop->iva == "Responsable Monotributista" || old('iva') == 'Responsable Monotributista' ) selected="" @endif>Responsable Monotributista</option>
                <option value="Exento"@if($shop->iva == "Exento" || old('iva') == 'Exento') selected="" @endif>Exento</option>
                <option value="Consumidor Final"@if($shop->iva == "Consumidor Final" || old('iva') == 'Consumidor Final') selected="" @endif>Consumidor Final</option>
                <option value="Otros"@if($shop->iva == "Otros" || old('iva') == 'Otros') selected="" @endif>Otros</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">¿Aceptar pagos por Mercado Pago?</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
        <select class="orm-control input-sm" name="use_mp" id="use_mp">
            <option value="1">Sí</option>
            <option value="0" @if(isset($shop->mp_key) && empty($shop->mp_key)) selected @endif>No</option>
        </select>
        </div>
    </div>

    <div id="mercado_pago_fields" style="display:@if(isset($shop->mp_key) && !empty($shop->mp_key)) block @else none @endif;">
        <div class="form-group">
            <label class="col-sm-3 col-md-4 control-label">{{ trans('data.mp_id') }}</label>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="input-group">
                    <input type="text" name="mp_client_id"
                           id="mp_id"
                           minlength="10"
                           placeholder="Ejemplo: 1865246309935780"
                           class="form-control input-sm" title="" value="{{ old('mp_client_id',$shop->mp_client_id) }}">
                    <span class="input-group-addon input-sm">
                        <i class="glyphicon glyphicon-exclamation-sign hover-help"></i>
                    </span>
                    <div class="place-image"  id="perro"><img src="{{ url('/img/client_id.png') }}"></div>
                </div>
                <span class="text-muted">Para obtener la información ingrese
                    <a href="https://www.mercadopago.com/mla/account/credentials?type=basic" target="_blank">aqui</a>
                </span>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 col-md-4 control-label">{{ trans('data.mp_client_secret') }}</label>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="input-group">
                    <input type="text" name="mp_client_secret"
                           id="mp_client_secret"
                           minlength="10"
                           maxlength="50"
                           placeholder=" Ejemplo: vH3yGdicOSpEtnyRQ9RYa2DFVKfwjNrx"
                           class="form-control input-sm" title="" value="{{ old('mp_client_secret',$shop->mp_client_secret) }}">
                    <span class="input-group-addon input-sm">
                        <i class="glyphicon glyphicon-exclamation-sign hover-help"></i>
                    </span>
                    <div class="place-image"><img src="{{ url('/img/client_secret.png') }}"></div>
                </div>
                <span class="text-muted">Para obtener la información ingrese
                    <a href="https://www.mercadopago.com/mla/account/credentials?type=basic" target="_blank">aqui</a>
                </span>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 col-md-4 control-label">{{ trans('data.mp_key') }}</label>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="input-group">
                    <input type="text" name="mp_key"
                           id="mp_key"
                           minlength="20"
                           placeholder="Ejemplo: APP_USR-gc44e4a7-d2344-485e-2950-77cc1ef99851"
                           class="form-control input-sm" title="" value="{{ old('mp_key',$shop->mp_key) }}">
                    <span class="input-group-addon input-sm">
                        <i class="glyphicon glyphicon-exclamation-sign hover-help"></i>
                    </span>
                    <div class="place-image" style="width: 500px; height: 300px; z-index: 10000;"><img style="width: 500px; height: 300px; " src="{{ url('/img/publik_key.png') }}"></div>
                </div>
                <span class="text-muted">Para obtener la información ingrese
                    <a href="https://www.mercadopago.com/mla/account/credentials" target="_blank">aqui</a>
                </span>
            </div>
        </div>
    </div>





    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.provinces') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <select name="province_id" id="province_id" class="form-control input-sm select2" required>
                <option>  {{ trans('process.select') }} </option>
                @foreach($provinces as $province)
                    <option value="{{ $province->id }}" @if($province->id == old('province_id',$shop->province_id)) selected="selected" @endif>{{ $province->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <input type="hidden" id="selected_locality_id" value="{{ old('locality_id',$shop->locality_id) }}">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.locality') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <select name="locality_id" id="locality_id" class="form-control input-sm select2" required>
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="map-center">
            <input type="hidden" class="form-control"  name="location" placeholder="Location" value="{{ old('location',$shop->location) }}">
            <div class="input-group">
                <input type="text"  class="form-control input-xs"  placeholder="..." id="shop_address" value="" />
                <a class="input-group-addon btn btn-primary btn-xs"  onclick="getLocationByCity();">{{ trans('process.search_locality') }}</a>
                <a class="input-group-addon btn btn-success" onclick="resetLocation();">{{ trans('process.restore') }}</a>
            </div>
            <br/>
            <div id="google-map"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">Dirección que se muestra en la app</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <textarea class="form-control input-sm" name="address"  rows="3" required>{{ old('address',$shop->address) }}</textarea>
            {{-- <label>Latitud</label> --}}
            <input type="hidden"  name="lat" id="shop_lat" value="{{ old('shop_lat',$shop->lat) }}"  readonly  required />
            {{-- <label>Longitud</label> --}}
            <input type="hidden"  name="lng" id="shop_long" value="{{ old('shop_long',$shop->lng) }}" readonly required />
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.email') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <input type="email" name="email"  class="form-control input-sm" placeholder="{{ trans('placeholder.email') }}" value="{{ old('email',$shop->email) }}" maxlength="100" data-parsley-maxlength="100">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.description') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <textarea class="form-control input-sm"  name="description" placeholder="{{ trans('placeholder.description') }}" rows="5">{{ old('description',$shop->description) }}</textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.phone') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <input  type="text" placeholder="{{ trans('placeholder.phone' ) }}" name="phone" autocomplete="off" class="form-control input-sm" value="{{ old('phone',$shop->phone) }}" required="" maxlength="14">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.schedule') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <textarea class="form-control input-sm" name="schedule" maxlength="100" data-parsley-maxlength="100">{{ old('schedule',$shop->schedule) }}</textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.observation') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <textarea class="form-control input-sm" name="observation" >{{ old('observation',$shop->observation) }}</textarea>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-9 col-md-8 col-lg-7 col-md-offset-2">
            <div role="alert" class="alert alert-primary alert-icon alert-icon-colored">
                <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                <div class="message">
                    <strong>Seleccione la imagen del logo de su negocio.</strong>
                    <ul>
                        <li>Formato: PNG (Con fondo transparente)</li>
                        <li>Tamaño mínimo: 200 x 200 px</li>
                        <li>Tamaño máximo: 600 x 600 px</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.logo') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="local-dropzone">
                <label>Arrastre su logo aquí o pulse click para agregarlo a su negocio</label>
                @if( $shop->logo )
                    <img id="logo-preview" src="{{ url('storage/'.env('STORAGE_SHOP_IMAGES').'/'.$shop->id.'_logo.png' ) }}">
                @else
                    <img id="logo-preview" src="">
                @endif
            </div>
            <input type="file" name="logo">
        </div>
    </div>

    @if(in_array(Auth::user()->type, ['administrator', 'seller']))
        <label class="col-sm-5 col-sm-offset-2 control-label">Seleccione los usuarios operadores para gestionar el negocio.</label>
        <br/>
        <br/>
        <div class="form-group">
            <div class="col-sm-6 multiselect-center">
                <select multiple="multiple" id="select-provider" name="users[]">
                    @foreach($users as $user)
                        @if($user->first_name != '')
                            <option value='{{ $user->id }}'
                                    @foreach ($shop->users as $shopUser)
                                    @if($user->id == $shopUser->id)
                                    selected
                                    @endif
                                    @endforeach
                            >{{ $user->first_name.' '.$user->last_name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
    @endif
    @if(!empty($shop->id))
        <div class="form-group">
            <label class="col-sm-3 col-md-4 control-label">{{ trans('data.status') }}</label>
            <div class="col-sm-6 col-md-4 col-lg-3 xs-pt-5">
                <div class="switch-button switch-button-success">
                    <input type="checkbox" @if($shop->active) checked @endif name="active" value="1" id="shop-active"><span>
                <label for="shop-active"></label></span>
                </div>
            </div>
        </div>
    @endif

    <div class="form-group">
        <div class="col-xs-12">
            <button type="submit" id="submit_btn" class="btn btn-primary"> @if($shop->id)<span class="icon mdi mdi-save"></span>  {{ trans('process.save') }} @else <span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}  @endif</button>
            <a href="{{ url('/shops') }}" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span>   {{ trans('process.back') }}</a>
        </div>
    </div>
</form>
@if(in_array(\Auth::user()->type, ['administrator']))
    @if($shop->id != '')
       <hr/>
       <h3 class="panel-title">{{ trans('process.delete').' '.trans('models.shop') }}</h3>
       <form action='{{ url('shops/'.$shop->id) }}' method='POST' id="shop-delete-{{ $shop->id }}">
           <input type="hidden" name="_method" value="DELETE">
           {{ csrf_field()}} {{ trans('process.alert.delete') }}
           <a data-modal="full-danger"  class="btn btn-danger pull-right md-trigger" onclick="formSelected='shop-delete-{{ $shop->id }}'" >
               <span class="mdi mdi-delete"></span>  {{ trans('process.delete') }}
           </a>
       </form>
    @endif
@endif