@extends('layouts.panel')

@section('shop-breadcrumbs')  
    @if($shop->id)
        <li><a href="{{ url('shops/'.$shop->id) }}">{{ trans('process.update') }}</a></li>
    @else
        <li><a href="{{ url('shops/create') }}">{{ trans('process.create') }}</a></li>
    @endif
@endsection

@section('content-panel') 
    @include('shop.header')

    @if($shop->id != '') 
        <ul class="nav nav-tabs">
            @if(in_array(\Auth::user()->type, ['administrator', 'operator']))
                <li class="active"><a href="#shop-form" data-toggle="tab">{{ trans('data.data') }}</a></li>
                <li><a href="#shopimage" data-toggle="tab">{{ trans('models.images') }}</a></li>
                <li><a href="#product-list" data-toggle="tab">{{ trans('models.products') }}</a></li>
                <li><a href="#shopbeacon-list" data-toggle="tab">{{ trans('models.beacons') }}</a></li>
            @endif

            @if(in_array(\Auth::user()->type, ['seller']))
                <li class="active"><a href="#shop-form" data-toggle="tab">{{ trans('data.data') }}</a></li>
                <li><a href="#shopimage" data-toggle="tab">{{ trans('models.images') }}</a></li>
            @endif
        </ul>
    @else
        <ul class="nav nav-tabs">
            <li class="active"><a href="#shop-form" data-toggle="tab">{{ trans('data.data') }}</a></li>
            @php /*<li><a href="#shopimage" data-toggle="tab">{{ trans('models.images') }}</a></li>*/@endphp
        </ul>
    @endif
 
    <div class="tab-content">
        <div class="tab-pane fade in active" id="shop-form">@include('shop.form')</div>

        @if(in_array(\Auth::user()->type, ['administrator', 'operator']))
            @if($shop->id != '')
                <div class="tab-pane fade" id="product-list">@include('product.list')</div>
                <div class="tab-pane fade" id="product-form">@include('product.form')</div>
            @endif
        @endif

        <div class="tab-pane fade" id="shopimage">
            <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <div role="alert" class="alert alert-primary alert-icon alert-icon-colored">
                    <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                    <div class="message">
                        <strong>Seleccione las imágenes a mostrar de la portada de su negocio.</strong>
                        <ul>
                            <li>Formato: JPG o PNG</li>
                            <li>Tamaño mínimo: 800 x 400 px</li>
                            <li>Tamaño máximo: 1280 x 720 px</li>
                        </ul>
                    </div>
                </div>
            </div>
            @include('shopimage.form')
            <div id="shopimage-list" class="col-xs-12">@include('shopimage.list')</div>
        </div>

        @if(in_array(\Auth::user()->type, ['administrator', 'operator']))
            @if($shop->id != '')
                <div class="tab-pane fade" id="shopbeacon-list">@include('shopbeacon.list')</div>
                <div class="tab-pane fade" id="shopbeacon-form">@include('shopbeacon.form')</div>
            @endif
        @endif
    </div>
@endsection

@section('footer')
    @php /*<script src="{{ URL::asset('js/users/pattern-phone.js') }}" type="text/javascript"></script> */@endphp
    <script src="{{ URL::asset('js/imagesAjax.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/shops/show.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/shops/gmap.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/shops/showLocalities.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/shops/sortImages.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/shops/newForm.js') }}" type="text/javascript"></script>
    @if($shop->id)
        <script src="{{ URL::asset('js/products/offersCalculate.jquery.js') }}" type="text/javascript"></script>
    @endif
    <script src="{{ URL::asset('js/products/selectWords.jquery.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/products/addWord.jquery.js') }}" type="text/javascript"></script>
@endsection