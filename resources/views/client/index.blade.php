@extends('layouts.panel')

@section('head')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('lib/bootstrap-daterangepicker/daterangepicker.css') }}"/>
@endsection

@section('content-panel')
@include('client.header')

<a href="{{ url('clients/create') }}"  class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}</a>
<a class="btn btn-success pull-right" id="excel-button" data-toggle="tooltip" title="Descargar Excel" data-placement="bottom"><span class="glyphicon glyphicon-download"></span> Excel</a>
<hr>
<div class="form-group text-center user-wrapper">
    <div class="col-sm-2 user-filter">
        <label>Mostrar</label>
        <select id="filter-length" class="form-control input-sm" >
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100" selected>100</option>
        </select>
    </div>
    <div class="col-sm-2 user-filter">
        <label>{{ trans('data.created') }}</label>
        <input type="text" id="date-alta" class="form-control input-sm text-center">
    </div>    
    <div class="col-sm-2 user-filter">
        <label>{{ trans('data.status') }}</label>
        <select id="filter-status" class="form-control input-sm" >
            <option value="Todos">{{ trans('data.all') }}</option>
            <option value="Activo">{{ trans('data.enable') }}</option>
            <option value="Inactivo">{{ trans('data.disable') }}</option>
        </select>
    </div>
    <div class="col-sm-2 user-filter">
        <label>{{ trans('data.gender') }}</label>
        <select id="filter-gender" class="form-control input-sm" >
            <option value="Todos">{{ trans('data.all') }}</option>
            <option value="Femenino">{{ trans('data.gender.female') }}</option>
            <option value="Masculino">{{ trans('data.gender.male') }}</option>
            <option value="Otro">{{ trans('data.gender.other') }}</option>
        </select>
    </div>
   <div class="col-sm-2 user-filter">
        <label>{{ trans('data.birthday') }}</label>
        <input type="text" id="date-birthday" class="form-control input-sm text-center">
    </div>   
    <input type="hidden" id="filter-profile" value="{{ trans('data.all') }}">
 
    <div class="col-sm-2 user-filter">
        <label>{{ trans('data.type') }}</label>
        <select id="filter-from"  class="form-control input-sm" >
            <option value="Todos">{{ trans('data.all') }}</option>
            {{-- <option value="{{ trans('data.email') }}">{{ trans('data.email') }}</option> --}}
            <option value="{{ trans('data.facebook') }}">{{ trans('data.facebook') }}</option>
            <option value="{{ trans('data.gmail') }}">{{ trans('data.gmail') }}</option>
            <option value="{{ trans('data.manual') }}">{{ trans('data.manual') }}</option>
        </select>
    </div>

    <div class="col-sm-2 user-filter">
        <label>Buscar</label>
        <input type="text" id="filter-search" class="form-control input-sm text-center">
    </div>
</div> 
<div class="col-sm-12 table-users">
	<table class="table table-striped table-condensed" id="clients-datatable">
	    <thead>
	        <tr>
	            @php /*<th class="col col-xs-1 text-center">{{ trans('data.id') }}</th>*/ @endphp
	            <th class="col col-xs-1">{{ trans('data.status') }}</th>
	            <th class="col col-xs-1">{{ trans('data.name') }}</th>
	            <th class="col col-xs-4">{{ trans('data.email') }}</th>
	            <th class="col col-xs-1">{{ trans('data.gender') }}</th>
	            <th class="col col-xs-4">{{ trans('data.phone.mobile') }}</th>
	            <th class="col col-xs-4">{{ trans('data.birthday') }}</th>
	            <th class="col col-xs-4">{{ trans('data.created') }}</th>
	            <th class="col col-xs-4">{{ trans('data.user.list.from') }}</th>
	            <th class="col col-xs-2">{{ trans('data.action') }}</th>
	        </tr>
	    </thead>
	    <tbody>
	    @foreach ($users as $user)
	        <tr @if(!$user->active) class="desactive" @endif>
	            @php /*<td class="text-center">{{ $user->id }}</td>*/ @endphp
	            <td class="text-center"> 
	                @if($user->active)
                        Activo
                    @else
                        Inactivo
	                @endif
	            </td>
	            <td>
                    {{ $user->full_name }}
                </td>
	            <td>
	                <span data-toggle="tooltip" title="{{ $user->email }}">{{ str_limit($user->email, 30) }}</span>
	            </td>
	            <td>
                    <span>{{ $user->gender }}</span>
                </td>
	            <td>
                    <span>{{ $user->phone }}</span>
                </td>
	            <td>
                    <span>{{ $user->birthday }}</span>
                </td>
	            <td>
	                {{ $user->created }}
	            </td>
	            <td>
	                @if($user->from=='Facebook')
	                    <span class="label label-md bg-blue-facebook"><span class="mdi mdi-facebook-box"></span> {{ $user->from }} </span>
	                @elseif($user->from=='Gmail')  
	                    <span class="label label-md bg-red"><span class="mdi mdi-google-glass"></span> {{ $user->from }}</span>
	                @elseif($user->from=='Email')  
	                    <span class="label label-md label-success"><span class="mdi mdi-email"></span> {{ $user->from }}</span>
	                @else   
	                    <span class="label label-md bg-blue-black"><span class="mdi mdi-desktop-windows"></span> {{ $user->from }}</span>
	                @endif
	                
	            </td>           
	            <td>
	                <form action='{{ url('users/'.$user->id) }}' method='POST'  id="user-delete-{{ $user->id }}">
	                    <input type="hidden" name="_method" value="DELETE">
	                    {{ csrf_field()}} 
	                    <a class="icon mdi mdi-edit btn btn-primary" href="{{ url('clients/'.$user->id) }}" data-toggle="tooltip" title="{{ trans("process.update") }}"></a>
	                    <a data-modal="full-danger"  class="btn btn-danger md-trigger " onclick="formSelected='user-delete-{{ $user->id }}'" data-toggle="tooltip" title="{{ trans("process.delete") }}" >
	                        <span class="icon mdi mdi-delete"></span>          
	                    </a>
	                </form>
	            </td>
	        </tr>
	        @endforeach
	    </tbody>
	</table>
</div>
@endsection

@section('footer')
    <script src="{{ URL::asset('js/users/clientsDatatableFilter.jquery.js') }}" type="text/javascript"></script>
@endsection