@extends('layouts.panel')

@section('client-breadcrumbs')  
@if($user->id)
<li><a href="{{ url('users/'.$user->id) }}">{{ trans('process.update') }}</a></li>
@else
<li><a href="{{ url('users/create') }}">{{ trans('process.create') }}</a></li>
@endif
@endsection

@section('content-panel')
@include('client.header')
<br/>  
<form role="form" method="POST" data-form="disable-on-submit" action="{{ $post }}" class="form-horizontal group-border-dashed">
    <input type="hidden" name="type" value="user">

    @if($user->id)
    <input type='hidden' name='_method' value='PUT'>
    @endif
    {{ csrf_field() }}

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.first_name' ) }}</label>
        <div class="col-sm-6 col-lg-4">
            <input id="first_name" type="text" placeholder="{{ trans('placeholder.first_name' ) }}" name="first_name" autocomplete="off" class="form-control input-sm" value="{{ old('first_name',$user->first_name) }}" data-parsley-maxlength="100" required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.last_name' ) }}</label>
        <div class="col-sm-6 col-lg-4">
            <input id="last_name" type="text" placeholder="{{ trans('placeholder.last_name' ) }}" name="last_name" autocomplete="off" class="form-control input-sm" value="{{ old('last_name',$user->last_name) }}" data-parsley-maxlength="100" required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.email' ) }}</label>
        <div class="col-sm-6 col-lg-4">
            <input id="email" type="email" placeholder="{{ trans('placeholder.email' ) }}" name="email" autocomplete="off" class="form-control input-sm" value="{{ old('email',$user->email) }}" data-parsley-maxlength="100" required>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.phone.mobile' ) }}</label>
        <div class="col-sm-6 col-lg-4">
           <input  type="text" placeholder="{{ trans('placeholder.phone' ) }}" name="phone" autocomplete="off" class="form-control input-sm" value="{{ old('phone',$user->phone) }}" required="" maxlength="14">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.birthday' ) }}</label>
        <div class="col-sm-6 col-lg-4">
            <div data-min-view="2" data-date-format="dd/mm/yyyy" class="input-group date datetimepicker">
                <input type="text" size="16" name="birthday" value="{{ old('birthday',$user->birthday) }}" class="form-control input-sm" />
                <span class="input-group-addon btn btn-primary"><i class="icon mdi mdi-calendar"></i></span>
            </div>
        </div>
    </div>
    @if(isset($user->from) && ($user->from !== 'Facebook' || $user->from !== 'Gmail'))
    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.password' ) }}</label>
        <div class="col-sm-6 col-lg-4">
            <input id="password" type="password" placeholder="{{ trans('placeholder.password' ) }}" name="password" class="form-control input-sm" value="" @if(!$user->id) required @endif parsley-trigger="focousout" data-parsley-minlength="6" >
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.password2' ) }}</label>
        <div class="col-sm-6 col-lg-4">
            <input id="password2" type="password" placeholder="{{ trans('placeholder.password2' ) }}" name="password_confirmation" class="form-control input-sm" value="" @if(!$user->id) required @endif data-parsley-equalto="#password"  parsley-trigger="focousout">
        </div>
    </div>
    @endif
    
    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.status') }}</label>
        <div class="col-sm-6 col-lg-4 xs-pt-5">
            <div class="switch-button switch-button-success">
                <input type="checkbox" @if($user->active || !$user->id) checked @endif name="active" id="swt5"><span>
                <label for="swt5"></label></span>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label xs-pt-20">{{ trans('data.gender') }}</label>
        <div class="col-sm-6 col-lg-4">
            <div class="be-radio-icon inline">
                <input type="radio"  
                @if( $user->gender == 'Femenino' || old('gender') == 'Femenino') checked @endif 
                 name="gender" value="Femenino" id="gfemale">
                <label for="gfemale" class="female" title="Femenino"><span class="mdi mdi-female"></span></label>
            </div>
            <div class="be-radio-icon inline">
                <input type="radio" 
                @if( $user->gender == 'Masculino' || old('gender') == 'Masculino') checked @endif 
                name="gender" value="Masculino" id="gmale">
                <label for="gmale" title="Masculino"><span class="mdi mdi-male-alt"></span></label>
            </div>
        </div>
    </div>

    @if($user->id)
    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.date.created') }}</label>
        <div class="col-sm-6 col-lg-4">
            <input  type="text" class="form-control input-sm" value="{{ $user->full_created }}" readonly>
        </div>
    </div>
    @endif

    @if($user->id)
    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.user.from') }}</label>
        <div class="col-sm-6 col-lg-4">
            <input  type="text"  class="form-control input-sm" value="{{ $user->from }}" readonly>
        </div>
    </div>
    @endif

    
    <div class="form-group">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary"> @if($user->id)<span class="icon mdi mdi-edit"></span>  {{ trans('process.save') }} @else <span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}  @endif</button>
            <a href="{{ url('/clients') }}" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span>   {{ trans('process.back') }}</a> 
        </div>
    </div>



</form>
@endsection


@section('footer')
@php /*<script src="{{ URL::asset('js/users/pattern-phone.js') }}" type="text/javascript"></script>*/@endphp
<script src="{{ URL::asset('js/users/passwordChange.jquery.js') }}" type="text/javascript"></script>
@endsection
