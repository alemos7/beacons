@section('containt-title') {{ trans('models.clients') }} @endsection

@section('menu-clientes') class="active" @endsection

@section('breadcrumbs') 
    <li><a href="{{ url('clients') }}" >{{ trans('models.list',['model'=>trans('models.clients')] ) }}</a></li>
    @yield('clients-breadcrumbs')
@endsection