<div class="form-group text-center user-wrapper">
    <div class="col-sm-2 user-filter hidden-xs">
        <label>Mostrar</label>
        <select id="filter-length" class="form-control input-sm" >
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100" selected>100</option>
        </select>
    </div>



    <div class="col-sm-2 user-filter">
        <label class="filter-pay-label">{{ trans('data.pay.status') }}</label>
        <select id="filter-pay-status" class="form-control input-sm">
            <option>Todos</option>
            <option>Pendiente</option>
            <option>Recibido</option>
            <option>Devuelto</option>
        </select>
    </div>

    <div class="col-sm-2 user-filter">
        <label>{{ trans('data.created') }}</label>
        <input type="text" id="date-created" class="form-control input-sm text-center">
    </div>    

    <div class="col-sm-2 user-filter">
        <label>{{ trans('data.delivery') }}</label>
        <select id="filter-delivery" class="form-control input-sm">
                <option>Todos</option>
                <option>Concretada</option>
                <option>Pendiente</option>
                <option>Cancelada</option>
        </select>
    </div>
    @if( Auth::user()->type == 'administrator')
    <div class="col-sm-2 user-filter">
        <label>{{ trans('models.shop') }}</label>
        <select id="filter-shop" class="form-control input-sm select2">
                    <option>Todos</option>
                @foreach($shops as $shop)
                    <option>{{ $shop->name }}</option>
                @endforeach
        </select>
    </div>
    @endif
    <div class="col-sm-2 user-filter">
        <label>Buscar</label>
        <input type="text" id="filter-search" class="form-control input-sm text-center">
    </div>
</div> 