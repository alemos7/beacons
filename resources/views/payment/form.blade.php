@extends('layouts.panel')

@section('payment-breadcrumbs')  
<li><a href="{{ url('payments/'.$payment->id) }}">{{ trans('process.update') }}</a></li>
@endsection

@section('content-panel')
@include('payment.header')
<br/>  
<form role="form" method="POST" action="{{ url('payments/'.$payment->id) }}" class="form-horizontal group-border-dashed"> 
    <input type='hidden' name='_method' value='PUT'>
    {{ csrf_field() }}

    <div class="clearfix"></div>
        <h3 class="page-head-title" style="font-size: 1.7em;">Pago</h3>
    <div class="clearfix"></div>
    <div class="col-lg-8">
        <div class="col-lg-6">
            <p><span style="font-size: 1.1em;font-weight: bolder;color: #333">Producto:</span> <span>{{ $payment->product->name }}</span></p>
        </div>
        <div class="col-lg-6">
            <p><span style="font-size: 1.1em;font-weight: bolder;color: #333">Precio: </span><span>$ {{ $payment->transaction_amount }}</span></p>
        </div>
    </div>
<div class="clearfix"></div>
    <div class="col-lg-8">
        <div class="col-lg-6">
            <p><span style="font-size: 1.1em;font-weight: bolder;color: #333">Comprador: </span><span><?= empty($payment->user->first_name) || empty($payment->user->last_name) ? $payment->user->email : $payment->user->first_name . " " . $payment->user->last_name; ?></span></p>
        </div>
        <div class="col-lg-6">
            <p><span style="font-size: 1.1em;font-weight: bolder;color: #333">Puntos:</span> <span>{{ $payment->response->product->point }}</span></p>
        </div>
    </div>
<div class="clearfix"></div>
    <div class="col-lg-8">
        <div class="col-lg-6">
        </div>
        <div class="col-lg-6">
            <p><span style="font-size: 1.1em;font-weight: bolder;color: #333">Cantidad: </span><span>{{ $payment->response->quantity }}</span></p>
        </div>
    </div>
<div class="clearfix"></div>
<div class="col-lg-8">
    
    <div class="form-group col-lg-5" style="display: flex;">
        <label style="padding: 0;line-height: 35px;margin-left: 5px"  class="col-lg-5 control-label">{{ trans('data.pay.status') }}</label>
        <div class="col-lg-7">
            <select name="status" class="form-control input-sm" required>
                    <option value="pending"      @if($payment->status == 'pending') selected @endif >Pendiente</option>
                    <option value="approved"     @if($payment->status == 'approved') selected @endif >Aprobado</option>
                   {{--  <option value="in_process"   @if($payment->status == 'in_process') selected @endif >En Progreso</option> --}}
                   {{--  <option value="in_mediation" @if($payment->status == 'in_mediation') selected @endif >En Mediación</option> --}}
                   {{--  <option value="rejected"     @if($payment->status == 'rejected') selected @endif >Rechazado</option> --}}
                   {{--  <option value="cancelled"    @if($payment->status == 'cancelled') selected @endif >Cancelado</option> --}}
                   {{--  <option value="refunded"     @if($payment->status == 'refunded') selected @endif >Reintegrado</option> --}}
                    <option value="charged_back" @if($payment->status == 'charged_back') selected @endif >Devuelto</option>
            </select>
        </div>
    </div>
    
    <div class="form-group col-lg-5 col-lg-push-1" style="display: flex;">
        <label style="padding: 0;line-height: 35px;margin-left: 5px" class="col-lg-4 control-label">El día</label>
        <div class="col-lg-8">
            <div style="padding-top: 0;" data-min-view="2" data-date-format="dd/mm/yyyy" class="input-group date datetimepicker">
                <input value="<?= date("d/m/Y", strtotime($payment->payment_created_date)) ?>" autocomplete="off" style="padding: 0;height: 32px" type="text" size="16" name="payment_created_date" class="form-control input-sm" />
                <span class="input-group-addon btn btn-primary"><i class="icon mdi mdi-calendar"></i></span>
            </div>
        </div>
    </div>
    
</div>

<div class="col-lg-8">
    <div class="form-group col-lg-5" style="display: flex;">
        <label style="padding: 0;line-height: 35px;margin-left: 5px"  class="col-lg-5 control-label">Método de pago</label>
        <div class="col-lg-7">
            <select autocomplete="off" name="payment_method" class="form-control input-sm" required>
                <option value="mp" class="mp" <?= $payment->payment_method == 'mp' ? "selected=true": "" ?>>Mercado Pago</option>
                <option value="cash" class="cash" <?= $payment->payment_method == 'cash' ? "selected=true": "" ?>>Efectivo</option>
            </select>
        </div>
    </div>
    
    <div id="payment_comment" class="form-group col-lg-5 col-lg-push-1" style="<?= $payment->payment_method == 'mp' ? "display:none" : "" ?>">
        <div class="col-xs-10 col-lg-push-2">
            <textarea placeholder="Detalle" name="payment_comment" style="max-width: 100%;min-width: 100%;max-height: 75px;min-height: 75px"><?= $payment->payment_comment ?></textarea>
        </div>
    </div>
    
</div>
<div class="clearfix"></div>
<h3 class="page-head-title" style="font-size: 1.7em;">Entrega</h3>
<div class="clearfix"></div>
<div class="col-lg-8">
    
    <div class="form-group col-lg-5">
        <label  class="col-lg-5 control-label">{{ trans('data.delivery') }}</label>
        <div class="col-sm-7 ">
            <select name="delivery" class="form-control input-sm" required>
                    <option value="Concretada"      @if($payment->delivery == 'Concretada') selected @endif >Concretada</option>
                    <option value="Pendiente"     @if($payment->delivery == 'Pendiente') selected @endif >Pendiente</option>
                    <option value="Cancelada"   @if($payment->delivery == 'Cancelada') selected @endif >Cancelada</option>
            </select>
        </div>
    </div> 
    
    <div class="form-group col-lg-5 col-lg-push-1" style="display: flex;">
        <label style="padding: 0;line-height: 35px;margin-left: 5px" class="col-lg-4 control-label">Fecha</label>
        <div class="col-lg-8">
            <div style="padding-top: 0;" data-min-view="2" data-date-format="dd/mm/yyyy" class="input-group date datetimepicker">
                <input style="padding: 0;height: 32px" value="<?= $payment->delivery_date == "0000-00-00" || !$payment->delivery_date ? date("d/m/Y") : date("d/m/Y", strtotime($payment->delivery_date)); ?>" type="text" size="16" name="delivery_date" class="form-control input-sm" />
                <span class="input-group-addon btn btn-primary"><i class="icon mdi mdi-calendar"></i></span>
            </div>
        </div>
    </div>
</div>    
    
    <div class="form-group col-lg-12">
        <div class="col-xs-7">
        <label style="padding: 0;line-height: 35px;margin-left: 5px"  class="control-label">Observaciones</label>
            <textarea name="payment_observation" style="max-width: 100%;min-width: 100%;max-height: 100px;min-height: 100px"><?= $payment->payment_observations ?></textarea>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary"> @if($payment->id)<span class="icon mdi mdi-edit"></span>  {{ trans('process.save') }} @else <span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}  @endif</button>
            <a href="{{ url('/payments') }}" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span>   {{ trans('process.back') }}</a> 
        </div>
    </div>
</form>
@endsection

@section('footer')
    
    <script type="text/javascript">
               
        $(document).ready(function(){

            $("body").on("change", "select[name='payment_method']", function(){

                if($(this).val() === "cash"){
                    $("#payment_comment").show();
                }else{
                    $("#payment_comment").hide();
                }

            });

        });
        
    </script>

@endsection