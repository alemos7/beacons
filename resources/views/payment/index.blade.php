@extends('layouts.panel')

@section('head')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('lib/bootstrap-daterangepicker/daterangepicker.css') }}"/>
@endsection

@section('content-panel')
@include('payment.header')
 <br/>
@include('payment.filter')
<table class="table table-striped table-condensed" id="payment-datatable">
    <thead>
        <tr>
            <th class="col col-xs-1 text-center">{{ trans('data.id') }}</th> 
            <th class="col col-xs-1">{{ trans('models.product') }}</th> 
            <th class="col col-xs-1"> Importe </th> 
            <th class="col col-xs-2">{{ trans('models.user.generic') }}</th>  
            <th class="col col-xs-1">{{ trans('data.phone') }}</th> 
            <th class="col col-xs-1">{{ trans('data.email') }}</th> 
            <th class="col col-xs-2">{{ trans('data.date.full') }}</th>  
            <th class="col col-xs-2">{{ trans('data.pay.status') }}</th>  
            <th class="col col-xs-1">{{ trans('data.delivery') }}</th>  
            <th class="col col-xs-1">Pedido</th>  
            @if(Auth::user()->type == 'administrator')
                <th class="col col-xs-1">{{ trans('models.shop') }}</th>  
            @endif
            <th class="col col-xs-1">{{ trans('data.action') }}</th>
        </tr>
    </thead>
    <tbody>
        @if(!$payments->isEmpty())
            @foreach($payments as $payment)
                <tr >
                    <td class="text-center">{{ $payment->id }}</td>
                    <td class="text-center">{{ $payment->product->name }}</td>
                    <td class="text-center">{{ $payment->product->price }}</td>
                    @if($payment->user)
                        <td class="text-center">{{ $payment->user->full_name }}</td>
                        <td class="text-center">{{ $payment->user->phone }}</td>
                        <td class="text-center">{{ $payment->user->email }}</td>
                    @else
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                        <td class="text-center">-</td>
                    @endif
                    <td class="text-center">{{ $payment->full_created }}</td>
                    <td class="text-center">{{ $payment->status_formatted }}</td>
                    <td class="text-center">{{ $payment->delivery }}</td>
                    <td class="text-center">{{ $payment->pedido }}</td>
            @if(Auth::user()->type == 'administrator' && isset($payment->shop))
                    <td class="text-center">{{ $payment->shop->name }}</td>
            @else
                <td class="text-center">-</td>
            @endif
                <td class="text-center">
                    <a class="icon mdi mdi-edit btn btn-primary" href="{{ url('payments/'.$payment->id) }}" data-toggle="tooltip" title="{{ trans("process.update") }}"></a>
                </td>
            </tr>
            @endforeach
        @else
            <tr>
                <td class="text-center" colspan="12">No hay data</td>
            </tr>
        @endif
    </tbody>
</table>
@endsection

@section('footer')
    <script src="{{ URL::asset('js/payments/paymentsDatatableFilter.jquery.js') }}" type="text/javascript"></script>

@endsection