@section('containt-title') {{ trans('models.payments') }} @endsection

@section('menu-ventas') class="active" @endsection

@section('breadcrumbs') 
    <li><a href="{{ url('payments') }}" >{{ trans('models.list',['model'=>trans('models.payments')] ) }}</a></li>
    @yield('payment-breadcrumbs')
@endsection