@section('containt-title') {{ trans('data.legal-content') }} @endsection

@section('menu-content') class="active" @endsection

@section('breadcrumbs') 
    <li><a href="{{ url('shops') }}" >{{ trans('data.legal-content') }}</a></li>
    @yield('content-breadcrumbs')
@endsection