<form action="{{ url('content') }}" method="POST">
	{{ csrf_field() }}
	<input type="hidden" name="key" value="terms">
	<div class="form-group">
		<div class="col-xs-12">
			<textarea class="trumbowyg-editor" name="terms">{{ $terms }}</textarea>
		</div>
	</div>

	<div class="form-group">
		<div class="col-xs-12">
			<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>  {{ trans('process.update') }} </button> 
		</div>
	</div>
</form>