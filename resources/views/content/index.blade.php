@extends('layouts.panel')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('lib/trumbowyg/ui/trumbowyg.css') }}"/>
@endsection

@section('content-panel') 
	@include('content.header')
	<ul class="nav nav-tabs">
		<li class="active"><a href="#privacy-policies" data-toggle="tab">{{ trans('data.privacy-policies') . ' y '. trans('data.terms') }}</a></li>
		{{--<li><a href="#terms" data-toggle="tab">{{ trans('data.terms') }}</a></li>--}}
	</ul>

	<div class="tab-content">
		<div class="tab-pane fade in active" id="privacy-policies">@include('content.privacy-policies')</div>
		{{--<div class="tab-pane fade" id="terms">@include('content.terms')</div>--}}
	</div>
@endsection

@section('footer') 
    <script src="{{ URL::asset('lib/trumbowyg/trumbowyg.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('lib/trumbowyg/langs/es_ar.min.js') }}" type="text/javascript"></script>
@endsection