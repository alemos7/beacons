@extends('layouts.panel')

@section('product-breadcrumbs')
    @if($message->id)
        <li><a href="{{ url('messages/'.$message->id) }}">{{ trans('process.update') }}</a></li>
    @else
        <li><a href="{{ url('messages/create') }}">{{ trans('process.create') }}</a></li>
    @endif
@endsection

@section('content-panel') 

    @include('message.header')


    <div class="tab-content">
        <div class="tab-pane fade in active" id="message-form">@include('message.form')</div>
    </div>
@endsection