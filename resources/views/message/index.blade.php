@extends('layouts.panel')

@section('content-panel') 

@include('message.header')
<a href="{{ url('messages/create') }}"  class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}</a>
@include('message.list')

@endsection
@section('footer')
	<script src="{{ URL::asset('js/messages/messagesDatatableFilter.jquery.js') }}" type="text/javascript"></script>
@endsection