<form action="{{ $post }}" method="POST" role="form" class="form-horizontal group-border-dashed">

    {{ csrf_field() }}
    @if($message->id != '')
    <input type='hidden' name='_method' value='PUT'>
    @endif
    
    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('models.shop') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <select name="shop_id" id="shop_id" class="form-control input-sm select2" required>
                <option>  {{ trans('placeholder.shop') }} </option>
                @foreach($shops as $shop)
                @php $shop_id = isset($message->product) ? $message->product->shop_id : 0; @endphp
                <option value="{{ $shop->id }}" @if($shop->id == old('shop_id',$shop_id) ) selected="selected" @endif>{{ $shop->name }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <input type="hidden" id="selected_product_id" value="{{ old('product_id',$message->product_id) }}">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('models.product') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <select name="product_id" id="product_id" class="form-control input-sm" required>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.content') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <textarea name="content" placeholder="{{ trans('placeholder.content') }}" class="form-control input-sm" required="">{{ old('content',$message->content) }}</textarea>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.status') }}</label>
        <div class="col-sm-6 col-md-4 col-lg-3 xs-pt-5">
            <div class="switch-button switch-button-success">
                <input type="checkbox" @if($message->active || !$message->id) checked @endif name="active" id="swt5"><span>
                <label for="swt5"></label></span>
            </div>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary"> @if($message->id)<span class="icon mdi mdi-edit"></span>  {{ trans('process.update') }} @else <span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}  @endif</button> 
            <a href="{{ url('/messages') }}" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span>  {{ trans('process.back') }}</a> 
        </div>
    </div>
</form>

@section('footer')
<script src="{{ URL::asset('js/shops/showProducts.js') }}" type="text/javascript"></script>
@endsection