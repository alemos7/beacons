@section('containt-title'){{ trans('models.messages') }} @endsection

@section('menu-messages') class="active" @endsection

@section('breadcrumbs') 
    <li><a href="{{ url('messages') }}" >{{ trans('models.list',['model'=>trans('models.messages')] ) }}</a></li>
    @yield('messages-breadcrumbs')
@endsection