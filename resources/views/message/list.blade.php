<hr/>
<div class="form-group text-center row product-wrapper">
    <div class="col-sm-3 user-filter hidden-xs">
        <label>Mostrar</label>
        <select id="filter-length" class="form-control input-sm" >
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100" selected>100</option>
        </select>
    </div>
    <div class="col-sm-3 user-filter">
        <label>Buscar</label>
        <input type="text" id="filter-search" class="form-control input-sm text-center">
    </div>
    <div class="col-sm-3 user-filter">
        <label class="sendLabel">Enviado</label>
        <select id="filterBySend" class="form-control input-sm" required>
            <option value="Todos">Todos</option>
            <option value="{{ trans('process.sended') }}">      {{ trans('process.sended') }}</option>
            <option value="{{ trans('process.not.sended') }}">  {{ trans('process.not.sended') }}</option>
        </select>
    </div>
    <div class="clearfix"></div>
    <hr/>
    <p style="color: #666;font-weight: bolder;text-align: left">A que tipo de usuarios desea que le llegue la notificación.</p>
    <div class="col-sm-3 user-filter hidden-xs">
        <label>Genero</label>
        <select autocomplete="off" class="form-control input-sm set-message-user-filter" data-name="gender" >
            <option selected value="todos">Todos</option>
            <option value="Masculino">Hombres</option>
            <option value="Femenino">Mujeres</option>
        </select>
    </div>
    
    <div class="col-sm-3 user-filter hidden-xs">
        <label>Edad</label>
        <select autocomplete="off" class="form-control input-sm set-message-user-filter" data-name="age" >
            <option selected value="todos">Todos</option>
            <option value="teen">18 - 20</option>
            <option value="young">20 - 30</option>
            <option value="adult">30 - 40</option>
            <option value="big-adult">40+</option>
        </select>
    </div>
    
    <div class="col-sm-3 user-filter hidden-xs">
        <label>Registrado con</label>
        <select autocomplete="off" class="form-control input-sm set-message-user-filter" data-name="register-method">
            <option selected value="todos">Todos</option>
            <option value="Manual">Email</option>
            <option value="Gmail">Gmail</option>
            <option value="Facebook">Facebook</option>
        </select>
    </div>

</div>
<table class="table table-striped table-condensed" id="messages-datatable">
    <thead>
        <tr>
            <th class="col-sm-2 text-center">{{ trans('data.id')}}</th>
            @if(!isset($products))
            <th class="col-sm-2">{{ trans('models.product') }}</th>
            @endif
            <th class="col-sm-2">{{ trans('data.content') }}</th>
            <th class="col-sm-2">{{ trans('process.sended') }}</th>
            <th class="col-sm-2">{{ trans('data.action') }}</th>
        </tr>
    </thead>
    <tbody> 

        @foreach ($messages  as $message)
        <tr @if(!$message->active) class="desactive" @endif>
            <td class="text-center">{{ str_limit($message->id,30) }}</td>
            <td>{{ str_limit($message->product->name,30) }}</td>
            <td>{{ str_limit($message->content,30) }}</td>
            <td>
                @if($message->date_send)
                    {{ trans('process.sended') . ' ' . $message->date_send_formated }}
                @else
                    {{ trans('process.not.sended') }}
                @endif
            </td>
            <td>
                <form action='{{ url('messages/send/'.$message->id) }}' method='POST' id="message-delete-{{ $message->id }}">                    
                    <input autocomplete="off" type="hidden" name="gender" value="todos"/>
                    <input autocomplete="off" type="hidden" name="register-method" value="todos"/>
                    <input autocomplete="off" type="hidden" name="age" value="todos"/>
                    {{ csrf_field()}} 
                    @if(Auth::user()->type=="administrator" )
                    <a class="icon mdi mdi-mail-send btn btn-warning send-message" data-toggle="tooltip" title="{{ trans("process.send.mail") }}"></a>
                    @endif
                    <a class="icon mdi mdi-edit btn btn-primary" href="{{ url('messages/'.$message->id) }}" data-toggle="tooltip" title="{{ trans("process.update") }}"></a>
                    @if(Auth::user()->type=="administrator")
                    <a data-modal="full-danger"  class="btn btn-danger md-trigger " onclick="formSelected='message-delete-{{ $message->id }}'" data-toggle="tooltip" title="{{ trans("process.delete") }}">
                        <span class="icon mdi mdi-delete"></span>          
                    </a>
                    @endif
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>