
@if(count($shop->images))
<legend>{{ trans('data.list',['model' => trans('models.images') ]) }}</legend>
<div class="images-grid">
    @foreach($shop->images as $image) 
    <div class="image">
        <a href="#" class="btn-left fa fa-chevron-left"></a>
        <a href="#" class="btn-right fa fa-chevron-right"></a>
        <input type="hidden" name="images[]" class="images-id" value="{{ $image->id }}">
        <input type="hidden" id="shopId" value="{{ $shop->id }}">
        <a href="{{ url($image->image_lg) }}" data-lightbox="shop-images" data-title="<a href='{{ url($image->image_lg) }}' class='mdi mdi-zoom-in'></a>"><img src="{{ url($image->image_sm) }}"/></a>
        <form action='{{ url('shop-images/'.$image->id ) }}' method='POST' class="image-delete">
            <input type="hidden" name="_method" value="DELETE">
            {{ csrf_field()}} 
            <button class="btn btn-danger btn-delete">X</button>
        </form>
    </div>
    @endforeach
</div>
<button class="btn btn-primary" onclick="sortImages()">Actualizar Orden</button>
@endif
