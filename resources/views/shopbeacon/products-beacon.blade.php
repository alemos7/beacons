@extends('layouts.panel')

@section('product-breadcrumbs')
        <li><a href="{{ url('products-beacon/'.$shopBeacon->id) }}">{{ trans('process.update').' '.trans('data.offer') }}</a></li>
@endsection


@section('content-panel') 
    @include('shopbeacon.header')

    <form method="POST" role="form" class="form-horizontal group-border-dashed">
        <input type='hidden' name='_method' value='PUT'>
    {{ csrf_field() }}
        
    <legend>{{ trans('data.offer-close') }}</legend>
    <p>Será recibida por usuarios que se encuentren entre 0 y 5 de distancia del beacon</p>
    <input type='hidden' name='beacon_id' value='{{ $shopBeacon->id }}'>
    {{-- {{ $productsBeacon }} --}}
    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('models.product') }}</label>
        <div class="col-sm-6 col-lg-4">        
            <select name="product_close_id"  class="form-control input-sm select2" required id="product_close">
                <option value="0" selected>{{ trans('data.none') }}</option>
                @foreach($products as $product)
                    <option value="{{ $product->id }}" @if($product->id ==  $productsBeacon->product_close_id) selected="selected" @endif>{{ $product->name }}</option>
                @endforeach
            </select>
            <span class="text-muted">{{ trans('data.offer-select') }}</span>
        </div>
    </div>
    <div class="form-group">
    <label class="col-sm-3 col-md-4 control-label">{{ trans('data.offer-message') }}</label>
        <div class="col-sm-6 col-lg-4">
            <input type="text" class="form-control input-sm"  name="product_close_message" placeholder="{{ trans('data.offer-add-message') }}" value="{{ old('product_close_message', $productsBeacon->product_close_message) }}" >
        </div>
    </div>
    <br/><br/>
    <legend>{{ trans('data.offer-far') }}</legend>
    <p>Será recibida por usuarios que se encuentren entre 6 y 30/40 de distancia del beacon</p>
    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('models.product') }}</label>
        <div class="col-sm-6 col-lg-4">
            <select name="product_far_id" class="form-control input-sm select2" id="product_far" required data-parsley-offer>
                <option value="0" selected>{{ trans('data.none') }}</option>
                @foreach($products as $product)
                    <option value="{{ $product->id }}" @if($product->id ==  $productsBeacon->product_far_id) selected="selected" @endif>{{ $product->name }}</option>
                @endforeach
            </select>
            <span class="text-muted">{{ trans('data.offer-select') }}</span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">{{ trans('data.offer-message') }}</label>
        <div class="col-sm-6 col-lg-4">
            <input type="text" class="form-control input-sm"  name="product_far_message" placeholder="{{ trans('data.offer-add-message') }}" value="{{ old('product_close_message', $productsBeacon->product_far_message) }}" >
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary"> @if($shopBeacon->id)<span class="icon mdi mdi-save"></span>  {{ trans('process.save') }} @else <span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}  @endif</button>
            <a href="{{ url('/beacons') }}" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span>  {{ trans('process.back') }}</a> 
        </div>
    </div>
</form>

@endsection

@section('footer')
    <script src="{{ URL::asset('js/products/productsbeacon.js') }}" type="text/javascript"></script>
@endsection