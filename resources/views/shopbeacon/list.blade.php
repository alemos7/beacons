@if(!isset($shopBeacons) && in_array(Auth::user()->type,['administrator','seller']))
    <a href="#shopbeacon-form" data-toggle="tab" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }} {{ trans('models.beacon') }}</a>
@endif
<hr/>
<table class="table table-striped table-condensed" id="shopBeacons-datatable">
    <thead>
        <tr>
            <th class="text-center">{{ trans('data.id')}}</th>
            @if(!isset($shop))
                <th>{{ trans('models.shop') }}</th>
            @endif
            <th>{{ trans('data.serial') }}</th>
            <th>{{ trans('data.description') }}</th>
            <th>{{ trans('data.offer-close') }}</th>
            <th>{{ trans('data.offer-far') }}</th>
            <th>{{ trans('data.action') }}</th>
        </tr>
    </thead>
    <tbody>
    @php $shopBeacons = isset($shopBeacons) ? $shopBeacons : $shop->beacons; @endphp

    @foreach ($shopBeacons  as $beacon)
        <tr @if(!$beacon->active) class="desactive" @endif>
            <td class="text-center">{{ str_limit($beacon->id,30) }}</td>
            @if(!isset($shop))
                <td>
                    @if ($beacon->shop)
                        {{ str_limit($beacon->shop->name,30) }}
                    @else
                        Ninguno
                    @endif
                </td>
            @endif
            <td>{{ $beacon->serial_string }}</td>
            <td>
                <span
                    @if(strlen($beacon->description) > 30)
                        data-toggle="tooltip" title="{{$beacon->description}}"
                    @endif
                >
                    {{ str_limit($beacon->description,30) }}
                </span>
            </td>
            <td>
                @if($beacon->products_around && $beacon->products_around->product_close)
                    <span
                        @if(strlen($beacon->products_around->product_close_message) > 35)
                            data-toggle="tooltip" title="{{$beacon->products_around->product_close_message}}"
                        @endif
                    >
                        {{ str_limit($beacon->products_around->product_close_message, 35)}}
                    </span>
                    <span class="label label-default"
                        @if(strlen($beacon->products_around->product_close->name) > 25)
                            data-toggle="tooltip" title="{{$beacon->products_around->product_close->name}}"
                        @endif
                    >
                        {{ str_limit($beacon->products_around->product_close->name, 25) }}
                    </span>
                @else
                    {{ trans('data.none') }}
                @endif
            </td>
            <td>
                @if($beacon->products_around && $beacon->products_around->product_far)
                    <span
                        @if(strlen($beacon->products_around->product_far_message) > 35)
                            data-toggle="tooltip" title="{{$beacon->products_around->product_far_message}}"
                        @endif
                    >
                        {{ str_limit($beacon->products_around->product_far_message, 35)}}
                    </span>
                    <span class="label label-default"
                        @if(strlen($beacon->products_around->product_far->name) > 25)
                            data-toggle="tooltip" title="{{$beacon->products_around->product_far->name}}"
                        @endif
                    >
                        {{ str_limit($beacon->products_around->product_far->name, 25) }}
                    </span>
                @else
                    {{ trans('data.none') }}
                @endif
            </td>
            <td>
                <form action='{{ url('beacons/'.$beacon->id) }}' method='POST' id="beacon-delete-{{ $beacon->id }}">
                    <input type="hidden" name="_method" value="DELETE">
                    {{ csrf_field()}}
                    @if(in_array(Auth::user()->type,['operator','administrator']))
                        @if ($beacon->shop)
                            <a class="icon mdi mdi-case btn btn-success" href="{{ url('products-beacon/'.$beacon->id) }}" data-toggle="tooltip" title="{{ trans("process.update").' '.trans("data.offer") }}"></a>
                        @endif
                    @endif
                    @if(in_array(Auth::user()->type,['seller']))
                        <a class="icon mdi mdi-edit btn btn-primary" href="{{ url('beacons/'.$beacon->id) }}" data-toggle="tooltip" title="{{ trans("process.update") }}"></a>
                    @endif
                    @if(in_array(Auth::user()->type,['administrator']))
                        <a class="icon mdi mdi-edit btn btn-primary" href="{{ url('beacons/'.$beacon->id) }}" data-toggle="tooltip" title="{{ trans("process.update") }}"></a>
                        <a data-modal="full-danger"  class="btn btn-danger md-trigger " onclick="formSelected='beacon-delete-{{ $beacon->id }}'" data-toggle="tooltip" title="{{ trans("process.delete") }}">
                            <span class="icon mdi mdi-delete"></span>
                        </a>
                    @endif
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>