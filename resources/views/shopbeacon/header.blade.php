@section('containt-title'){{ trans('models.beacons') }} @endsection

@section('menu-beacons') class="active" @endsection

@section('breadcrumbs') 
    <li><a href="{{ url('beacons') }}" >{{ trans('models.list',['model'=>trans('models.beacons')] ) }}</a></li>
    @yield('product-breadcrumbs')
@endsection