@if(isset($shop))
    <a href="#shopbeacon-list" data-toggle="tab" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-th-list"></span>   {{ trans('models.list',['model'=> trans('models.beacons')]) }}</a>
    <hr/>
    <form action="{{ url('beacons') }}" data-form="disable-on-submit" method="POST" role="form" id="save_changes" class="form-horizontal group-border-dashed">
@else
    <form action="{{ $post }}" data-form="disable-on-submit" method="POST" role="form" id="save_changes" class="form-horizontal group-border-dashed">
@endif 
    

    {{ csrf_field() }}

    <div class="form-group">
        <label class="col-sm-3 col-md-4 control-label">UUID</label>
        <div class="col-sm-6 col-lg-4">
            <input type="text" class="form-control input-sm text-center" value="1c0a1dfeffcbac416dd375e95c23ce6b" readonly="" />
        </div>
    </div>

    @if($shopBeacon->id != '')
        <input type='hidden' name='_method' value='PUT'>
        <input type="hidden" id="original_shop_id" value="{{ $shopBeacon->shop_id }}">
    @endif
    @if(isset($shop))
        <input type="hidden" name="shop_id"  value="{{  $shop->id }}">
    @else
        <div class="form-group">
            <label class="col-sm-3 col-md-4 control-label">{{ trans('models.shop') }}</label>
            <div class="col-sm-6 col-lg-4">
                <select name="shop_id" id="shop_id" class="form-control input-sm select2">
                    <option value="">Ninguno</option>
                        @foreach($shops as $shop)
                            <option value="{{ $shop->id }}" @if($shop->id ==  $shopBeacon->shop_id) selected="selected" @endif>{{ $shop->name }}</option>
                        @endforeach
                </select>
            </div>
        </div>
    @endif
    <input type="hidden" name="serial" id="serial" value="{{ old('serial',$shopBeacon->serial_string) }}">
    @if(\Auth::user()->type != 'seller')
        <div class="form-group">
            <label class="col-sm-3 col-md-4 control-label">Major</label>
            <div class="col-sm-6 col-lg-4">
                <input type="text" class="form-control input-sm text-center"  placeholder="Mayor"  required
                id="beacon-serial-mayor"
                data-parsley-pattern="[a-fA-F\d]{4}"
                data-parsley-minlength="4"
                data-parsley-maxlength="4"
                minlength="4"
                maxlength="4">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 col-md-4 control-label">Minor</label>
            <div class="col-sm-6 col-lg-4">
                <input type="text" class="form-control input-sm text-center"  placeholder="Minor" required
                id="beacon-serial-minor"
                data-parsley-pattern="[a-fA-F\d]{4}"
                data-parsley-minlength="4"
                data-parsley-maxlength="4"
                minlength="4"
                maxlength="4">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 col-md-4 control-label">{{ trans('data.description') }}</label>
            <div class="col-sm-6 col-lg-4">
                <textarea name="description" placeholder="{{ trans('placeholder.description') }}" class="form-control input-sm">{{ old('description',$shopBeacon->description) }}</textarea>
            </div>
        </div>


        <div class="form-group">
            <label class="col-sm-3 col-md-4 control-label">{{ trans('data.status') }}</label>
            <div class="col-sm-6 col-lg-4 xs-pt-5">
                <div class="switch-button switch-button-success" data-toggle="tooltip" title="{{$shopBeacon->id ? '' : 'No se puede activar antes de tener ofertas'}}">
                    <input type="checkbox" @if($shopBeacon->active) checked @endif name="active" {{$shopBeacon->id ? '' : 'disabled'}} id="swt5"><span>
                    <label for="swt5"></label></span>
                </div>
            </div>
        </div>
    @endif

    <div class="form-group">
        <div class="col-xs-12">
            <button type="submit" id="submit_button" class="btn btn-primary">
                @if($shopBeacon->id)
                    <span class="icon mdi mdi-edit"></span>
                    {{ trans('process.save') }}
                @else
                    <span class="glyphicon glyphicon-plus"></span>
                    {{ trans('process.create') }}
                @endif
            </button>
            <a href="{{ url('/beacons') }}" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span>  {{ trans('process.back') }}</a>
        </div>
    </div>
</form>