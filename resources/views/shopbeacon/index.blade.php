@extends('layouts.panel')

@section('content-panel') 

    @include('shopbeacon.header')
    @if(Auth::user()->type=="administrator")
    	<a href="{{ url('beacons/create') }}"  class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}</a>
	@endif
	@include('shopbeacon.list')
    
@endsection
@section('footer')
    <script>$('#shopBeacons-datatable').DataTable({
            "pageLength": 100
        });</script>
@endsection