@extends('layouts.panel')

@section('product-breadcrumbs')
    @if($shopBeacon->id)
        <li><a href="{{ url('beacons/'.$shopBeacon->id) }}">{{ trans('process.update') }}</a></li>
    @else
        <li><a href="{{ url('beacons/create') }}">{{ trans('process.create') }}</a></li>
    @endif
@endsection

@section('content-panel') 

    @include('shopbeacon.header')


    <div class="tab-content">
        <div class="tab-pane fade in active" id="shopBeacon-form">@include('shopbeacon.form')</div>
    </div>
@endsection

@section('footer')
    <script src="{{ URL::asset('js/shops/showProducts.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/loadAjaxForm.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/beacon/beacon-serial.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(window).bind("load", function() { 
            loadAjaxForm('form-productimage'); 
        });
    </script>
@endsection