<nav class="navbar navbar-default navbar-fixed-top be-top-header">
  <div class="container-fluid">
    <div class="navbar-header">
    	<span class="navbar-brand"></span>
    	<a href="#" class="nav-responsive"><i class="fa fa-bars"></i></a>
    </div>
    <ul class="nav navbar-nav navbar-right be-user-nav">
      <li class="nav-item">
          <a class="nav-link">{{Auth()->user()->first_name}} {{Auth()->user()->last_name}}</a>
      </li>
      <li class="dropdown">
          <a href="#" data-toggle="dropdown" id="avatar" role="button" aria-expanded="false" class="dropdown-toggle">
              <img src="/img/default.jpg" alt="Avatar">
          </a>
          <ul role="menu" class="dropdown-menu">
              <li>
                  <div class="user-info">
                      <div class="user-name">{{Auth()->user()->username}}</div>
                      <div class="user-position online">{{Auth()->user()->type}}</div>
                  </div>
              </li>
              @if(strtolower(Auth()->user()->type) == 'administrator')
                <li><a href="/users/{{Auth()->user()->id}}"><span class="icon mdi mdi-settings"></span> Ajustes</a></li>
              @endif
              <li><a href="/logout"><span class="icon mdi mdi-power"></span> Cerrar Sesión</a></li>
          </ul>
      </li>
    </ul>
    <div class="be-right-navbar">
      <div class="page-title"><span>{{-- Titulo Header --}}</span></div>
      {{-- Botones Extra para el Header --}}
      {{-- @include('layouts.panel.header-toolbar') --}}
    </div>
  </div>
</nav>