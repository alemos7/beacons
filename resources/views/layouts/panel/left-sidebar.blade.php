<div class="be-left-sidebar">
  <div class="left-sidebar-wrapper">
    <div class="left-sidebar-spacer">
      <div class="left-sidebar-scroll">
        <div class="left-sidebar-content">
          <ul class="sidebar-elements">

            @if( \Auth::user()->type == 'administrator')
              <li class="divider">Menu</li>

              <li @yield("menu-marcas") ><a href="{{url('/shops')}}"><i class="icon mdi mdi-store"></i>{{ trans('models.shops') }}</a></li>

              <li @yield("menu-productos") ><a href="{{url('/products')}}"><i class="icon mdi mdi-shopping-cart"></i>{{ trans('models.products') }}</a></li>

              <li @yield("menu-ventas") ><a href="{{ url('/payments') }}"><i class="icon mdi mdi-money"></i>{{ trans('models.payments') }}</a></li>

              <li @yield("menu-statistics") ><a href="{{url('/statistics')}}"><i class="icon mdi mdi-trending-up"></i>Estadísticas</a></li>

              <li @yield("menu-messages") ><a href="{{url('/messages')}}"><i class="icon mdi mdi-email"></i>{{ trans('models.message.massive') }}</a></li>

              <li class="divider">Administración</li>

              <li @yield("menu-usuarios")><a href="{{url('/users')}}"><i class="icon mdi mdi-account"></i>{{ trans('models.users') }}</a></li>

              <li @yield("menu-clientes")><a href="{{url('/clients')}}"><i class="icon mdi mdi-accounts-list-alt"></i>{{ trans('models.clients') }}</a></li>

              <li @yield("menu-categorias") ><a href="{{url('/categories')}}"><i class="icon mdi mdi-format-list-bulleted"></i>{{ trans('models.categories') }}</a></li>

              <li @yield("menu-banner")><a href="{{url('/banners')}}"><i class="icon mdi mdi-collection-image"></i>{{ trans('models.banners') }}</a></li>

              <li @yield("menu-beacons") ><a href="{{ url('/beacons') }}"><i class="icon mdi mdi-format-bold"></i>{{ trans('models.beacons') }}</a></li>

              <li @yield("menu-content") ><a href="{{ url('/content') }}"><i class="icon mdi mdi-format-subject"></i>{{ trans('data.legal-content') }}</a></li>

              <!--<li @yield("menu-help") ><a href="{{ url('/help') }}"><i class="icon mdi mdi-help"></i>{{ trans('data.help') }}</a></li>-->

            @elseif(\Auth::user()->type == 'operator')
              <li class="divider">Menu</li>

              <li @yield("menu-marcas") ><a href="{{url('/shops')}}"><i class="icon mdi mdi-store"></i>{{ trans('models.shops') }}</a></li>

              <li @yield("menu-productos") ><a href="{{url('/products')}}"><i class="icon mdi mdi-shopping-cart"></i>{{ trans('models.products') }}</a></li>

              <li @yield("menu-messages") ><a href="{{url('/messages')}}"><i class="icon mdi mdi-email"></i>{{ trans('models.message.massive') }}</a></li>

              <!--<li class="divider">Administración</li>-->

              <li @yield("menu-beacons") ><a href="{{ url('/beacons') }}"><i class="icon mdi mdi-format-bold"></i>{{ trans('models.beacons') }}</a></li>

              <li @yield("menu-ventas") ><a href="{{ url('/payments') }}"><i class="icon mdi mdi-money"></i>{{ trans('models.payments') }}</a></li>

            @elseif(\Auth::user()->type == 'seller')
              <li class="divider">Menu</li>

              <li @yield("menu-marcas") ><a href="{{url('/shops')}}"><i class="icon mdi mdi-store"></i>{{ trans('models.shops') }}</a></li>

              <li @yield("menu-productos") ><a href="{{url('/products')}}"><i class="icon mdi mdi-shopping-cart"></i>{{ trans('models.products') }}</a></li>

              <!--<li class="divider">Administración</li>-->

              <li @yield("menu-usuarios")><a href="{{url('/users')}}"><i class="icon mdi mdi-account"></i>{{ trans('models.users') }}</a></li>

              <li @yield("menu-beacons") ><a href="{{ url('/beacons') }}"><i class="icon mdi mdi-format-bold"></i>{{ trans('models.beacons') }}</a></li>

            @endif
            <li><a href="{{ url('/logout') }}"><span class="icon mdi mdi-power"></span>{{ trans('process.logout') }}</a></li>
          </ul>
        </div>
      </div>
    </div>

    {{--   Widget Que muestra Barra de Porcentaje     --}}
    {{--
     <div class="progress-widget">
       <div class="progress-data"><span class="progress-value">60%</span><span class="name">Current Project</span></div>
       <div class="progress">
         <div style="width: 60%;" class="progress-bar progress-bar-primary"></div>
       </div>
     </div>
     --}}
  </div>
</div>