<ul class="nav navbar-nav navbar-right be-icons-nav">
  <li class="dropdown"><a href="#" role="button" aria-expanded="false" class="be-toggle-right-sidebar"><span class="icon mdi mdi-settings"></span></a></li>
  <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon mdi mdi-notifications"></span><span class="indicator"></span></a>
  </li>
  <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon mdi mdi-apps"></span></a>
    <ul class="dropdown-menu be-connections">
      <li>
        <div class="list">
          <div class="content">
            <div class="row">
              <div class="col-xs-4"><a href="#" class="connection-item"><img src="img/github.png" alt="Github"><span>GitHub</span></a></div>
              <div class="col-xs-4"><a href="#" class="connection-item"><img src="img/bitbucket.png" alt="Bitbucket"><span>Bitbucket</span></a></div>
              <div class="col-xs-4"><a href="#" class="connection-item"><img src="img/slack.png" alt="Slack"><span>Slack</span></a></div>
            </div>
            <div class="row">
              <div class="col-xs-4"><a href="#" class="connection-item"><img src="img/dribbble.png" alt="Dribbble"><span>Dribbble</span></a></div>
              <div class="col-xs-4"><a href="#" class="connection-item"><img src="img/mail_chimp.png" alt="Mail Chimp"><span>Mail Chimp</span></a></div>
              <div class="col-xs-4"><a href="#" class="connection-item"><img src="img/dropbox.png" alt="Dropbox"><span>Dropbox</span></a></div>
            </div>
          </div>
        </div>
        <div class="footer"> <a href="#">More</a></div>
      </li>
    </ul>
  </li>
</ul>