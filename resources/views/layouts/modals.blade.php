<!-- Nifty Modal-->
<div id="full-danger" class="modal-container modal-full-color modal-full-color-danger modal-effect-8">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
    </div>
    <div class="modal-body">
      <div class="text-center"><span class="modal-main-icon mdi mdi-close-circle-o"></span>
        <h3>{{ trans('process.delete') }}</h3>
        <p>{{ trans('process.alert.delete') }}<br/>{{ trans('process.alert.confirm') }} </p>
        <div class="xs-mt-50">
          <a data-dismiss="modal" class="btn btn-default btn-space modal-close" >{{ trans('process.cancel') }}</a>
          <a data-dismiss="modal" class="btn btn-primary btn-space modal-close" onclick="sendForm()" name="proceed">{{ trans('process.proceed') }}</a>
        </div>
      </div>
    </div>
    <div class="modal-footer"></div>
  </div>
</div>
<!-- Nifty Modal-->
<div id="full-danger-shop" class="modal-container modal-full-color modal-full-color-danger modal-effect-8">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
    </div>
    <div class="modal-body">
      <div class="text-center"><span class="modal-main-icon mdi mdi-close-circle-o"></span>
        <h3>Error al guardar cambios</h3>
        <p>Debe seleccionar como minimo una imagen para estar activo</p>
        <div class="xs-mt-50">
          <a data-dismiss="modal" class="btn btn-default btn-space modal-close" >Aceptar</a>
        </div>
      </div>
    </div>
    <div class="modal-footer"></div>
  </div>
</div>
<!-- Nifty Modal-->
<div id="full-warning" class="modal-container modal-full-color modal-full-color-danger modal-effect-8">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
    </div>
    <div class="modal-body">
      <div class="text-center"><span class="modal-main-icon mdi mdi-alert-triangle"></span>
        <h3>{{ trans('process.warning') }}</h3>
        <p>{!! trans('process.alert.warning-next') !!} </p>
        <div class="xs-mt-50">
          <a data-dismiss="modal" class="btn btn-primary btn-space modal-close" name="proceed">{{ trans('process.ok') }}</a>
        </div>
      </div>
    </div>
    <div class="modal-footer"></div>
  </div>
</div>
<!-- Modal Category Update-->
<div id="full-update-category" class="modal-container modal-full-color modal-full-color-danger modal-effect-8">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
    </div>
    <div class="modal-body">
      <div class="text-center"><span class="modal-main-icon mdi mdi-alert-triangle"></span>
        <h3>{{ trans('process.warning') }}</h3>
        <p>{{ trans('process.alert.update.category') }}</p>
        <div class="xs-mt-50">
          <a data-dismiss="modal" class="btn btn-default btn-space modal-close" >{{ trans('process.cancel') }}</a>
          <a data-dismiss="modal" class="btn btn-primary btn-space modal-close" onclick="sendForm()" name="proceed">{{ trans('process.proceed') }}</a>
        </div>
      </div>
    </div>
    <div class="modal-footer"></div>
  </div>
</div>
<!-- Warning Category-->
<div id="full-warning-category" class="modal-container modal-full-color modal-full-color-warning modal-effect-8">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
    </div>
    <div class="modal-body">
      <div class="text-center"><span class="modal-main-icon mdi mdi-alert-triangle"></span>
        <h3>{{ trans('process.warning') }}</h3>
        <p>{!! trans('process.alert.warning.category') !!} </p>
        <div class="xs-mt-50">
          <a data-dismiss="modal" class="btn btn-primary btn-space modal-close" name="proceed">{{ trans('process.ok') }}</a>
        </div>
      </div>
    </div>
    <div class="modal-footer"></div>
  </div>
</div>
<!-- Warning shop change-->
<div id="full-warning-shop-change" class="modal-container modal-full-color modal-full-color-warning modal-effect-8">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="mdi mdi-close"></span></button>
    </div>
    <div class="modal-body">
      <div class="text-center"><span class="modal-main-icon mdi mdi-alert-triangle"></span>
        <h3>Intenta cambiar el negocio asociado.</h3>
        Al cambiar el negocio asociado al beacon, se reiniciarán las estadísticas del mismo.
        <div class="xs-mt-50">
          <a data-dismiss="modal" class="btn btn-default btn-space modal-close" >{{ trans('process.cancel') }}</a>
          <a data-dismiss="modal" class="btn btn-primary btn-space modal-close" onclick="sendForm()" name="proceed">{{ trans('process.proceed') }}</a>
        </div>
      </div>
    </div>
    <div class="modal-footer"></div>
  </div>
</div>

<div class="modal-overlay"></div>
<script type="text/javascript">
  formSelected = 'null';
  function sendForm(){
      if(formSelected=='null') return;
      form = document.getElementById(formSelected);
      form.submit();
  }
</script>