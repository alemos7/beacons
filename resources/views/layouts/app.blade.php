<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{ URL::asset('favicon.ico') }}">
    <title>Blue Shop</title>
    
    @include('layouts.head')
    
    @yield('head')
  </head>
  
  <body @yield('body-tag')>
    
    @yield('content')

    @include('layouts.footer')
    @yield('footer')
  </body>
</html>