<link rel="stylesheet" type="text/css" href="{{ URL::asset('lib/perfect-scrollbar/css/perfect-scrollbar.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ URL::asset('lib/material-design-icons/css/material-design-iconic-font.min.css') }}"/>
<!--[if lt IE 9]>
<script src="{{ URL::asset('https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js') }}"></script>
<script src="{{ URL::asset('https://oss.maxcdn.com/respond/1.4.2/respond.min.js') }}"></script>
<![endif]--> 

<link rel="stylesheet" type="text/css" href="{{ URL::asset('lib/select2/css/select2.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ URL::asset('lib/fontawesome-iconpicker/css/fontawesome-iconpicker.min.css') }}"/>

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="{{ URL::asset('lib/lightbox/css/lightbox.min.css') }}"/>

{{-- DropZone --}}
<link rel="stylesheet" type="text/css" href="{{ URL::asset('lib/dropzone/dist/min/dropzone.min.css') }}"/>
<link href="https://file.myfontastic.com/WPmLdfktKUUvgQWoPHwFGT/icons.css" rel="stylesheet">

{{-- Jasny --}}
<link rel="stylesheet" type="text/css" href="{{ URL::asset('lib/jasny-bootstrap/css/jasny-bootstrap.min.css') }}"/>

{{-- DateTime --}}
<link rel="stylesheet" type="text/css" href="{{ URL::asset('lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}"/>


{{-- all css --}}
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/all.css') }}"/>