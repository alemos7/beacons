<script src="{{ URL::asset('lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/jquery-ui/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/jquery-ui/jquery-migrate.3.0.0.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/datatables/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/jquery/jquery.quicksearch.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/jquery/jquery.multi-select.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/select2/js/select2.min.js') }}" type="text/javascript"></script>


<script src="{{ URL::asset('lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/all.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/jquery.niftymodals/dist/jquery.niftymodals.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/parsley/parsley.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/parsley/i18n/es.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/bootstrap-daterangepicker/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/moment.js/locale/es.js') }}" type="text/javascript"></script>
<script type="text/javascript">moment.locale('es');</script>
<script src="{{ URL::asset('lib/bootstrap-daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/datetimepicker/js/locales/bootstrap-datetimepicker.es.js') }}" type="text/javascript"></script>


<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXl5YUF9ix8fRcMrPTQi5bDniKDs9K7d4" type="text/javascript"></script>

<script src="{{ URL::asset('lib/datatables/plugins/buttons/js/dataTables.buttons.min.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('lib/datatables/plugins/buttons/js/buttons.flash.min.js') }}"      type="text/javascript"></script>
<script src="{{ URL::asset('lib/datatables/plugins/buttons/js/jszip.min.js') }}"       type="text/javascript"></script>
<script src="{{ URL::asset('lib/datatables/plugins/buttons/js/pdfmake.min.js') }}"   type="text/javascript"></script>
<script src="{{ URL::asset('lib/datatables/plugins/buttons/js/vfs_fonts.js') }}"     type="text/javascript"></script> 
<script src="{{ URL::asset('lib/datatables/plugins/buttons/js/buttons.html5.min.js') }}"      type="text/javascript"></script> <script src="{{ URL::asset('lib/datatables/plugins/buttons/js/buttons.print.min.js') }}"      type="text/javascript"></script>
<script src="{{ URL::asset('lib/jasny-bootstrap/js/jasny-bootstrap.min.js') }}"      type="text/javascript"></script> <script src="{{ URL::asset('lib/datatables/plugins/buttons/js/buttons.print.min.js') }}"      type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.js"      type="text/javascript"></script>

<script src="{{ URL::asset('lib/fontawesome-iconpicker/js/fontawesome-iconpicker.min.js') }}"      type="text/javascript"></script>

<script src="{{ URL::asset('lib/lightbox/js/lightbox.min.js') }}"  type="text/javascript"></script>
{{-- DropZone --}}
<script src="{{ URL::asset('lib/dropzone/dist/min/dropzone.min.js') }}"></script>

<script>$('#flash-overlay-modal').modal();</script>
<script type="text/javascript">
  $.fn.niftyModal('setDefaults',{
    overlaySelector: '.modal-overlay',
    closeSelector: '.modal-close',
    classAddAfterOpen: 'modal-show',
  });
</script>


@if (Session::has('flash_notification'))
  {{ Session::forget('flash_notification') }}
@endif
@include('layouts.modals') 
