@if (session()->has('flash_notification.message'))
    <div class="alert alert-{{ session('flash_notification.level') }}">
    @if(session('flash_notification.level') == "success")
        <span class="icon mdi mdi-check"></span>
    @endif
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    {!! session('flash_notification.message') !!}
    </div>
@endif
@if (count($errors) > 0)
	<script type="text/javascript">  
	    var errorsMessage = {!! json_encode($errors->all()) !!};
        var errorsKey = {!! json_encode($errors->keys()) !!};
	</script>
@endif