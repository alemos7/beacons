@extends('layouts.app')

@section('content')
<div class="be-wrapper">
      
      @include('layouts.panel.header')
      @include('layouts.panel.left-sidebar')
      <div class="be-content">
        <div class="page-head">
          <h2 class="page-head-title">@yield('containt-title')</h2>
          {{-- Breadcrumb  --}}
          <ol class="breadcrumb page-head-nav">
            @yield('breadcrumbs')
          </ol> 
        </div> 
        <div class="col col-md-12">
          <div class="panel panel-default panel-border-color panel-border-color-primary">
            <div class="tab-content">
              <div class="se-pre-con"></div>
              @include('layouts.errors')
              @yield('content-panel')
            </div>
          </div> 
        </div>
      </div>
      {{-- @include('layouts.panel.right-sidebar') --}}
    </div>
@endsection
