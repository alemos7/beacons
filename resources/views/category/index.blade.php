@extends('layouts.panel')

@section('content-panel')
    @include('category.header')
    <a href="{{ url('categories/create') }}"  class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}</a>
    <hr/>
    <table class="table table-striped table-condensed" id="categories-datatable">
        <thead>
            <tr>
                <th class="col col-xs-2 text-center" ></th>
                <th class="col col-xs-3" >{{ trans('data.name') }}</th>
                <th class="col col-xs-5" >{{ trans('data.description') }}</th>
                <th class="col col-xs-2">{{ trans('data.action') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($categories as $category)
                <tr>
                    <td class="text-center color-white">
                        <div style="background-color:#{{$category->color}};" class="tag-color">
                            <span class="fa fa-{{ $category->logo }}"></span>
                        </div>
                    </td>
                    <td>{{ str_limit($category->name,30) }}</td>
                    <td>
                        <span data-toggle="tooltip">{{ str_limit($category->description,30) }}</span>
                    </td>
                    <td>
                        <form action='{{ url('categories/'.$category->id) }}' method='POST' id="category-delete-{{ $category->id }}">
                            <input type="hidden" name="_method" value="DELETE">
                            {{ csrf_field()}} 
                            <a class="icon mdi mdi-edit btn btn-primary" href="{{ url('categories/'.$category->id) }}" data-toggle="tooltip" title="{{ trans("process.update") }}"></a>
                            @if(!count($category->products))
                            <a data-modal="full-danger"  class="btn btn-danger md-trigger " onclick="formSelected='category-delete-{{ $category->id }}'" data-toggle="tooltip" title="{{ trans("process.delete") }}">
                                <span class="icon mdi mdi-delete"></span>          
                            </a>
                            @else
                            <a data-modal="full-warning-category"  class="btn btn-danger md-trigger " >
                                <span class="icon mdi mdi-delete"></span>          
                            </a>
                            @endif
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@section('footer')
    <script>$('#categories-datatable').DataTable();</script>
@endsection