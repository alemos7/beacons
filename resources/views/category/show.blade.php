@extends('layouts.panel')

@section('category-breadcrumbs')
    @if($category->id)
        <li><a href="{{ url('categories/'.$category->id) }}">{{ trans('process.update') }}</a></li>
    @else
        <li><a href="{{ url('categories/create') }}">{{ trans('process.create') }}</a></li>
    @endif
@endsection

@section('head')
@endsection

@section('content-panel')
    @include('category.header')

    <ul class="nav nav-tabs">
        <li class="active"><a href="#category-form" data-toggle="tab" >{{ trans('data.data') }}</a></li>
        <li><a href="#category-images" data-toggle="tab" >{{ trans_choice('models.images',2) }}</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane fade in active" id="category-form"> @include('category.form') </div>
        <div class="tab-pane fade" id="category-images">
            <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                <div role="alert" class="alert alert-primary alert-icon alert-icon-colored">
                    <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                    <div class="message">
                        <strong>Seleccione la imagen del banner de la categoría.</strong>
                        <ul>
                            <li>Formato: JPG o PNG (Sin fondo transparente)</li>
                            <li>Tamaño: 1080 x 332 px</li>
                        </ul>
                    </div>
                </div>
            </div>
            @include('categoryimage.form')
            <div id="categoryimage-list" class="col-sm-12">@include('categoryimage.list')</div>
        </div>
    </div>
@endsection


@section('footer')
    <script src="{{ URL::asset('js/imagesAjax.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/category/sortImages.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('lib/jscolor/jscolor.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(window).bind("load", function() {
            deleteImagesAjax('image-delete','category');
        });
    </script>
@endsection