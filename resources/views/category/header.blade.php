@section('containt-title') {{ trans_choice('models.category', 2) }} @endsection

@section('menu-categorias') class="active" @endsection

@section('breadcrumbs') 
    <li><a href="{{ url('categories') }}" >{{ trans('models.list',['model'=>trans('models.categories')] ) }}</a></li>
    @yield('category-breadcrumbs')
@endsection