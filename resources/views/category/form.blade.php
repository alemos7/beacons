<form role="form" method="POST" action="{{ url('categories/'.$category->id) }}" class="form-horizontal group-border-dashed" enctype="multipart/form-data" id="category-update">

    @if($category->id != '')
        <input type='hidden' name='_method' value='PUT'>
        <input type='hidden' id='category-id' value='{{ $category->id }}'>
    @endif

    {{ csrf_field() }}

    <div class="form-group">
        <label class="col-sm-3 control-label">{{ trans('data.name') }}</label>
        <div class="col-sm-6">
            <input type="text" placeholder="{{ trans('placeholder.name') }}" name="name" autocomplete="off"  class="form-control input-sm" maxlength="100" value="{{ old('name',$category->name) }}" data-parsley-maxlength="100" required autofocus>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">{{ trans('data.color') }}</label>
        <div class="col-sm-6">
            <input class="jscolor form-control" name="color" placeholder="{{ trans('placeholder.color') }}" value="{{ old('color',$category->color) }}"  required maxlength="6" data-parsley-minlength="6" data-parsley-maxlength="6" >
            <span class="text-muted">Seleccione el color para identificar la categoría</span>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">{{ trans('data.description') }}</label>
        <div class="col-sm-6">
            <textarea id="description" placeholder="{{ trans('placeholder.description') }}" name="description" autocomplete="off"  class="form-control input-sm">{{ old('description',$category->description) }}</textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">{{ trans('data.logo') }}</label>
        <div class="col-sm-6">
            <div class="input-group">
                <input data-placement="bottomRight" class="form-control icp icp-auto" id="logo" name="logo" value="{{ old('logo','fa-'.$category->logo) }}" type="text" required data-parsley-maxlength="100" readonly />
                <span class="input-group-addon"></span>
            </div>
            <span class="text-muted">Seleccione el logo para identificar la categoría</span>
        </div>
    </div>

    <div class="form-group">
        <div class="col-xs-12">
            
            @if($category->id)
                <a data-modal="full-update-category"  class="btn btn-primary md-trigger " onclick="formSelected='category-update'">
                    <span class="icon mdi mdi-edit"></span>{{ trans('process.save') }}
                </a>
            @else
            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}</button> 
            @endif
            <a href="{{ url('/categories') }}" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span>   {{ trans('process.back') }}</a> 
        </div>
    </div>
</form>