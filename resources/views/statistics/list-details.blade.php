<hr/>
<table class="table table-striped table-condensed" id="statistics-datatable">
    <thead>
        <tr>
            <th>{{ trans('data.offer-now') }}</th>
            <th>{{ trans('data.position') }}</th>
            <th>Fecha</th>
            <th>{{ trans('data.sends') }}</th>
            <th>{{ trans('data.views') }}</th>
        </tr>
    </thead>
    <tbody>

        @foreach( $statistics  as $statistic ) <!-- Ver esto -->
            <tr>
                <td>{{ (!empty($statistic->product())) ? $statistic->product()->name : '- Nombre Indefinido' }}</td>
                <td>{{ $statistic->position_formatted }}</td>
                <td>{{ $statistic->created_at->format('d M Y') }} -> {{ $statistic->updated_at->format('d M Y') }}</td>
                <td>{{ $statistic->issues }}</td>
                <td>{{ $statistic->views }}</td>
            </tr>
        @endforeach
    </tbody>
</table>