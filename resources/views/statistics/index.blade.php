@extends('layouts.panel')

@section('content-panel') 

  @include('statistics.header')

  @if($id != 0)
    @include('statistics.list-details')
  @else
    @include('statistics.list-statistics')
  @endif
@endsection

@section('footer')
    <script>$('#statistics-datatable').DataTable({"pageLength": 100});</script>
@endsection