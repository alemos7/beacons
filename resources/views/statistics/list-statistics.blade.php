<hr/>
<table class="table table-striped table-condensed" id="statistics-datatable">
    <thead>
    <tr>
        {{-- <th>{{ trans('models.beacon') }}</th> --}}
        <th>Tipo de notificación</th>
        <th>{{ trans('models.shop') }}</th>
        <th>Descripción</th>
        <th>{{ trans('data.sends')  }}</th>
        <th>{{ trans('data.views')  }}</th>
        <th>{{ trans('data.action') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($statistics as $statistic)
        <tr>
            {{-- <td>{{ $statistic->position == 'masive_notification' ? "Notificación masiva" : "Beacon cercano" }}</td> --}}
            <td>
            @if(isset($statistic->position) && $statistic->position == 'masive_notification')
                Notificación masiva
                {{-- {{ $statistic->product_advertising->name }} --}}
            @else
                Beacon
            @endif
            </td>
            <td>
            @if (isset($statistic->position) && $statistic->position == 'masive_notification')
                -
            @else
                @if(isset($statistic->products_beacon->beacon->shop->name) && !empty($statistic->products_beacon->beacon->shop))
                    {{ $statistic->products_beacon->beacon->shop->name}}
                @else
                    -No se encontro el shop del beacon
                @endif
            @endif
            </td>
            <td>
                @if(!empty($statistic->products_beacon->beacon->description))
                    {{ $statistic->products_beacon->beacon->description}}
                @else
                    -Sin descripcion
                @endif
            </td>
            <td>{{ $statistic->total_issues }}</td>
            <td>{{ $statistic->total_views }}</td>
            <td>
                @if(isset($statistic->position) && $statistic->position !== 'masive_notification')
                    <a class="icon mdi mdi-eye btn btn-warning" href="{{ url('statistics/'.$statistic->id) }}" data-toggle="tooltip" title="{{ trans("process.details") }}"></a>
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
