@section('containt-title')
  @if($id != 0)
    @if(isset($statistics[0]->position) && $statistics[0]->position == 'masive_notification')
      Notificación masiva 
    @else
      @if(isset($statistics[0]->products_beacon->beacon))
        {{ $statistics[0]->products_beacon->beacon->description }}
      @endif
    @endif
  @else
      Estadísticas
  @endif

@endsection

@section('menu-statistics') class="active" @endsection

@section('breadcrumbs') 

  @if($id != 0)
    <li><a href="{{ url('statistics/'.$id) }}" > Detalle de estadísticas </a></li>
  @else
    <li><a href="{{ url('statistics') }}" > Estadísticas generales </a></li>
  @endif
@endsection