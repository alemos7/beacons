@extends('layouts.panel')

@section('product-breadcrumbs')
    @if($product->id)
        <li><a href="{{ url('products/'.$product->id) }}">{{ trans('process.update') }}</a></li>
    @else
        <li><a href="{{ url('products/create') }}">{{ trans('process.create') }}</a></li>
    @endif
@endsection

@section('content-panel') 

    @include('product.header')

    <ul class="nav nav-tabs">
        <li class="active"><a href="#product-form" data-toggle="tab">{{ trans('data.data') }}</a></li>
        @if($product->id)
            <li><a href="#productimage" data-toggle="tab">{{ trans_choice('models.images',2) }}</a></li>
        @endif
    </ul>

    <div class="tab-content">
        <div class="tab-pane fade in active" id="product-form">@include('product.form')</div>
        <div class="tab-pane fade" id="productimage">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <div role="alert" class="alert alert-primary alert-icon alert-icon-colored">
                        <div class="icon"><span class="mdi mdi-info-outline"></span></div>
                        <div class="message">
                            <strong>Seleccione las imágenes a mostrar de su producto.</strong>
                            <ul>
                                <li>Formato: JPG o PNG</li>
                                <li>Tamaño mínimo: 500 x 500 px</li>
                                <li>Tamaño máximo: 2000 x 2000 px</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    @include('productimage.form')
                </div>
                <div id="productimage-list" class="col-xs-12">@include('productimage.list')</div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    <script src="{{ URL::asset('js/imagesAjax.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/products/offersCalculate.jquery.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/products/selectWords.jquery.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/products/addWord.jquery.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/products/sortImages.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/products/character-left.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/products/form.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(window).bind("load", function() { 
            deleteImagesAjax('image-delete','product');
        });
    </script>
@endsection