@extends('layouts.panel')

@section('content-panel') 

    @include('product.header')

    <a href="{{ url('products/create') }}"  class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}</a>
	@include('product.list')
    
@endsection
@section('footer')
	<script src="{{ URL::asset('js/products/productsDatatableFilter.jquery.js') }}" type="text/javascript"></script>
@endsection