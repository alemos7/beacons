@section('containt-title') {{ trans('models.products') }} @endsection

@section('menu-productos') class="active" @endsection

@section('breadcrumbs') 
    <li><a href="{{ url('products') }}" >{{ trans('models.list',['model'=>trans('models.products')] ) }}</a></li>
    @yield('product-breadcrumbs')
@endsection