<div class="form-group">
    <label class="col-sm-3 col-md-4 control-label">{{ trans('data.offer-type') }}</label>
    <div class="col-sm-6 col-md-4 col-lg-3">
        <select name="algorithm" id="offerSelect" class="form-control input-sm">
            <option value="ninguna" @if($offer->algorithm == 'ninguna') selected @endif>{{ trans('data.offer-none') }}</option>
            <option value="porcentual" data-target="#porcentual" @if($offer->algorithm == 'porcentual') selected @endif>{{ trans('data.offer-percentage') }}</option>
            <option value="masPorMenos" data-target="#masPorMenos"@if($offer->algorithm == 'masPorMenos') selected @endif>{{ trans('data.offer-more-for-less') }}</option>
        </select>
    </div>
</div>

<div class="form-group offerSelect" id="porcentual">
    <div class="col-sm-3 col-md-4"></div>
    <div class="col-sm-3 col-md-2">
        <label class="control-label">{{ trans('data.offer-discount') }}</label>
        <input type="number" class="form-control input-sm offer-calculate" max="99"  step="1" name="vars[]" placeholder="" value="{{ $offer->descuento }}" data-parsley-maxlength="50" data-parsley-type="integer" required>
    </div>
    <div class="col-sm-3 col-md-2">
        <label class="control-label">{{ trans('data.offer-price-before') }}</label>
        <input type="text" class="form-control input-sm before-offer-price" step="1"  name="before_offer_price" placeholder="" value="{{ $offer->before_offer_price }}" data-parsley-maxlength="50" required>
    </div>
</div>

<div class="form-group offerSelect center" id="masPorMenos"> 
    <div class="col-sm-2 col-lg-3"></div>
    <div class="col-sm-3 col-lg-2">
        <label class="control-label">{{ trans('data.offer-get') }}</label>
        <input type="number" class="form-control input-sm offer-calculate"  step="1" name="vars[]" placeholder="" value="{{ $offer->lleve }}" data-parsley-maxlength="50" data-parsley-type="integer" required>
    </div>
    <div class="col-sm-3 col-lg-2">
        <label class="control-label">{{ trans('data.offer-pay') }}</label>
        <input type="number" class="form-control input-sm offer-calculate"  step="1" name="vars[]" placeholder="" @if(! empty( 'hola' )) value="{{ $offer->pague }}" @endif data-parsley-maxlength="50" data-parsley-type="integer" required>
    </div>
    <div class="col-sm-3 col-lg-2">
        <label class="control-label">{{ trans('data.offer-price-before') }}</label>
        <input type="text" class="form-control input-sm before-offer-price" step="0.01"  name="before_offer_price" placeholder="" value="{{ $offer->before_offer_price }}" data-parsley-maxlength="50" required readonly>
    </div>
</div>