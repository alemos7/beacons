@if(isset($shop))
    <a href="#product-list" data-toggle="tab" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-th-list"></span>   {{ trans('models.list',['model' => trans('models.products')]) }}</a>
    <hr/>
    <form id="form-product" data-form="disable-on-submit" action="{{ url('products') }}" method="POST" role="form" class="form-horizontal group-border-dashed">
@else
    <form id="form-product" data-form="disable-on-submit" action="{{ $post }}" method="POST" role="form" class="form-horizontal group-border-dashed" enctype="multipart/form-data">
@endif

    {{ csrf_field() }}
    @if($product->id != '')
        <input type='hidden' name='_method' value='PUT'>
        <input type='hidden' id='product-id' value='{{ $product->id }}'>
    @endif
    @if(isset($shop))
        <input type="hidden" name="shop_id" placeholder="Nombre" value="{{  $shop->id }}">
    @else
        <div class="form-group">
            <label  class="col-sm-3 col-md-4 control-label">{{ trans('models.shop') }}</label>
            <div class="col-sm-6  col-md-4 col-lg-3">
                <select name="shop_id" id="inputShop_id" class="form-control input-sm select2" required>
                    <option value="">{{ trans('placeholder.shop') }}</option>
                    @foreach($shops as $shop)
                        <option value="{{ $shop->id }}" @if($shop->id == $product->shop_id) selected @endif>{{ $shop->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    @endif

    <div class="form-group">
        <label  class="col-sm-3 col-md-4 control-label">{{ trans('models.category') }}</label>
        <div class="col-sm-6  col-md-4 col-lg-3">
            <select name="category_id" class="form-control input-sm" required>
                    <option value=""  selected>{{ trans('placeholder.category') }}</option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}" @if($category->id == $product->category_id) selected @endif>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label  class="col-sm-3 col-md-4 control-label">{{ trans('models.category') }} de MercadoPago</label>
        <div class="col-sm-6  col-md-4 col-lg-3">
            <select name="mp_category_id" class="form-control input-sm select2" required>
                <option value=""  selected>{{ trans('placeholder.category') }}</option>
            @foreach($mp_categories as $mp_category)
                    @if($mp_category->trans == 'Otros')
                        <option value="{{ $mp_category->id }}"selected>{{ $mp_category->trans }}</option>
                    @else
                        <option value="{{ $mp_category->id }}" @if($mp_category->id == $product->mp_category_id) selected @endif>{{ $mp_category->trans }}</option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group">
        <label  class="col-sm-3 col-md-4 control-label">{{ trans('data.name') }}</label>
        <div class="col-sm-6  col-md-4 col-lg-3">
            <input type="text" class="form-control input-sm"  name="name" placeholder="{{ trans('placeholder.name') }}" value="{{  old('name',$product->name) }}" autofocus data-parsley-maxlength="100" required>
        </div>
    </div>

    <div class="form-group">
        <label  class="col-sm-3 col-md-4 control-label">{{ trans('data.code') }}</label>
        <div class="row col-sm-6  col-md-6 col-lg-5">
            <div class="col-sm-6 col-md-6 col-lg-3">
                <input type="text" class="form-control input-sm"  name="code" placeholder="{{ trans('placeholder.code') }}" value="{{ old('code',$product->code) }}" data-parsley-maxlength="50">
            </div>
        </div>
    </div>

    <div class="form-group">
        <label  class="col-sm-3 col-md-4 control-label">{{ trans('data.price') }}</label>
        <div class="row col-sm-6  col-md-6 col-lg-5">
            <div class="col-sm-6 col-md-6 col-lg-3">
                <input type="text" class="form-control input-sm price-format"  name="price" id="price" step="0.01" placeholder="{{ trans('placeholder.price') }}"  value="{{  old('price',$product->price) }}" required>
            </div>
        </div>
    </div>


    @include('product.offers')

    <div class="form-group">
        <label  class="col-sm-3 col-md-4 control-label">{{ trans('data.point') }}</label>
        <div class="row col-sm-6  col-md-6 col-lg-5">
            <div class="col-sm-6 col-md-6 col-lg-3">
                <input type="number" class="form-control input-sm"  name="point" step="1" placeholder="{{ trans('placeholder.point') }}"  value="{{  old('point',$product->point) }}" required>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label  class="col-sm-3 col-md-4 control-label">{{ trans('data.description') }}</label>
        <div class="col-sm-9  col-md-6 col-lg-6">
            <textarea name="description" id="description" rows="10" placeholder="{{ trans('placeholder.description') }}" class="form-control input-sm" required maxlength="1000">{{ old('description',$product->description) }}</textarea>
            <span class="text-muted">Caracteres restantes: <span id="character-left">1000</span></span>
        </div>
    </div>
    @if(isset($product->id))
        <div class="form-group">
            <label class="col-sm-3 col-md-4 control-label">{{ trans('data.status') }}</label>
            <div class="col-sm-6  col-md-4 col-lg-3 xs-pt-5">
                <div class="switch-button switch-button-success">
                    <input type="checkbox" @if($product->active || !$product->id) checked @endif name="active" id="swt5"><span>
                    <label for="swt5"></label></span>
                </div>
            </div>
        </div>
    @endif
        @php
            /*
                <div class="form-group">
                  <label class="col-sm-3 col-md-4 control-label">{{ trans('models.words') }}</label>
                  <div class="col-sm-6  col-md-4 col-lg-3">
                      <select multiple="multiple" class="tags col-sm-12" id="words" name="words[]">
                        @foreach($words as $word)
                        <option value='{{ $word->id }}'
                            @foreach ($product->words as $productWord)
                                    @if($word->id == $productWord->id)
                                    selected
                                    @endif
                                @endforeach>{{ $word->word }}</option>
                            @endforeach
                        </select>
                        <span class="text-muted">{{ trans('placeholder.words') }}</span>
                    </div>
                </div>
            */
     @endphp
    <div class="form-group">
        <div class="col-xs-12">
            <button type="submit" id="submit_btn" class="btn btn-primary"> @if($product->id)<span class="icon mdi mdi-save"></span>  {{ trans('process.save') }} @else <span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}  @endif</button>
            <a href="{{ url('/products') }}" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span>   {{ trans('process.back') }}</a>
        </div>
    </div>
</form>