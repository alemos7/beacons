@if(!isset($data['products']) && !isset($products))
<a href="#product-form" data-toggle="tab" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }} {{ trans('models.product') }}</a>
@endif
@if(isset($data))
    @php $products = $data['products']; @endphp
@endif
<hr/>
<div class="form-group text-center row product-wrapper">
    <div class="col-sm-3 user-filter hidden-xs">
        <label>Mostrar</label>
        <select id="filter-length" class="form-control input-sm" >
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100" selected>100</option>
        </select>
    </div>
    <div class="col-sm-3 user-filter">
        <label class="offerLabel">Tipo de Oferta</label>
        <select name="offert" id="filterByOffer" class="form-control input-sm" required="required">
            <option value="">Todos</option>
            <option value="{{ trans('data.offer-none') }}">          {{ trans('data.offer-none') }}</option>
            <option value="{{ trans('data.offer-more-for-less') }}"> {{ trans('data.offer-more-for-less') }}</option>
            <option value="{{ trans('data.offer-percentage') }}">    {{ trans('data.offer-percentage') }}</option>
        </select>
    </div>
    @if(isset($data))
    <div class="col-sm-3 user-filter">
        <label class="offerLabel">Categoria</label>
        <select name="offert" id="filterByCategory" class="form-control input-sm" required="required">
            <option value="">Todos</option>
            @foreach($data['categories'] as $category)
                <option value="{{$category->name}}"> {{ $category->name }}</option>
            @endforeach
        </select>
    </div>
    @endif
    <div class="col-sm-3 user-filter">
        <label>Buscar</label>
        <input type="text" id="filter-search" class="form-control input-sm text-center">
    </div>


</div>
<table class="table table-striped table-condensed" id="products-datatable">
    <thead>
        <tr>
            <th class="text-center">{{ trans('data.id') }}</th>
            <th>{{ trans('data.buisness') }}</th>
            <th>{{ trans('data.name') }}</th>
            <th id="category_thead">{{ trans('data.category') }}</th>
            <th>{{ trans('data.price') }}</th>
            <th>{{ trans('data.code') }}</th>
            <th id="offer_thead">{{ trans('data.offer') }}</th>
            <th>{{ trans('data.action') }}</th>
        </tr>
    </thead>
    <tbody>
    @php $products = isset($products) ? $products : $shop->products; @endphp

    @foreach ($products  as $product)
        <tr @if(!$product->active) class="desactive" @endif>
            <td class="text-center">{{ str_limit($product->id,30) }}</td>
            <td>{{ str_limit($product->shop->name,30) }}</td>
            <td>{{ str_limit($product->name,30) }}</td>
            <td>{{ str_limit($product->category->name,30) }}</td>
            <td>{{ $product->price_formatted }}</td>
            <td>{{ str_limit($product->code,30) }}</td>
            <td>
                @if($product->offer_type === 'Porcentual')
                <span class="label label-md  label-warning">{{ str_limit($product->offer_type,30) }}</span>
                @elseif($product->offer_type === 'Más por menos')
                    <span class="label label-md  label-primary">{{ str_limit($product->offer_type,30) }}</span>
                @else
                    <span>{{ str_limit($product->offer_type,30) }}</span>
                @endif
            </td>
            <td>
                <form action='{{ url('products/'.$product->id) }}' method='POST' id="product-delete-{{ $product->id }}">
                    <input type="hidden" name="_method" value="DELETE">
                    {{ csrf_field()}}
                    <a class="icon mdi mdi-edit btn btn-primary" href="{{ url('products/'.$product->id) }}" data-toggle="tooltip" title="{{ trans("process.update") }}" ></a>
                    <a data-modal="full-danger"  class="btn btn-danger md-trigger " onclick="formSelected='product-delete-{{ $product->id }}'"  data-toggle="tooltip" title="{{ trans("process.delete") }}">
                        <span class="icon mdi mdi-delete"></span>
                    </a>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>