@extends('layouts.app')

@section('body-tag')class="be-splash-screen"@endsection

@section('content')
  <div class="be-wrapper be-login">
      <div class="be-content">
        <div class="main-content container-fluid">
          <div class="splash-container">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
              <div class="panel-heading"><img src="img/logo-login-azul.png" alt="logo" class="logo-img"><span class="splash-description">Por favor ingresa tu informacion para ingresar.</span></div>
              <div class="panel-body">
                <form action="{{ url('/login') }}" method="post">
                    {{ csrf_field() }}
                  <div class="form-group">
                    <input id="username" type="email" placeholder="Correo Electronico" name="email" autocomplete="on" class="form-control" autofocus value="{{ old('email') }}" required>
                  </div>
                  <div class="form-group">
                    <input id="password" type="password" placeholder="Contraseña" name="password" class="form-control" required>
                  </div>
                  @include('layouts.errors')
                  <div class="form-group row login-tools">
                    <div class="col-xs-6 login-remember">
                      <div class="be-checkbox">
                        <input type="checkbox" id="remember" name="remember">
                        <label for="remember">Recordar</label>
                      </div>
                    </div>
                    {{-- <div class="col-xs-6 login-forgot-password"><a href="{{ url('password/reset') }}">¿Olvido su contraseña?</a></div> --}}
                  </div>
                  <div class="form-group login-submit">
                    <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">Entrar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
