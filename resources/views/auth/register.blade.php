@extends('layouts.app')

@section('body-tag')class="be-splash-screen"@endsection

@section('content')
  <div class="be-wrapper be-login">
      <div class="be-content">
        <div class="main-content container-fluid">
          <div class="splash-container">
            <div class="panel panel-default panel-border-color panel-border-color-primary">
              <div class="panel-heading"><img src="img/logo-xx.png" alt="logo" width="102" height="27" class="logo-img"><span class="splash-description">Por favor ingresa tu informacion para registrarte.</span></div>
              <div class="panel-body">
                <form role="form" method="POST" action="{{ url('/register') }}">
                    {{ csrf_field() }}
                  <div class="form-group">
                    <input id="username" type="text" placeholder="Nombre de Usuario" name="username" autocomplete="off" class="form-control">
                  </div>
                  <div class="form-group">
                    <input id="name" type="text" placeholder="Nombre" name="name" autocomplete="off" class="form-control">
                  </div>
                  <div class="form-group">
                    <input id="email" type="email" placeholder="Correo Electronico" name="email" autocomplete="off" class="form-control">
                  </div>
                  <div class="form-group">
                    <span>Fecha de Nacimiento</span>
                    <input id="birthday" type="date" name="birthday" class="form-control" value="" required="required">
                  </div>
                  <div class="form-group">
                    <input id="password" type="password" placeholder="Contraseña" name="password" class="form-control">
                  </div>
                  <div class="form-group">
                    <input id="password2" type="password" placeholder="Confirmar contraseña" name="password_confirmation" class="form-control">
                  </div>
                  @include('layouts.errors')
                  <div class="form-group login-submit">
                    <button data-dismiss="modal" type="submit" class="btn btn-primary btn-xl">Registrar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection