@php
    /* Esto va en la App
        @extends('layouts.app')

        @section('body-tag')class="be-splash-screen"@endsection

        <!-- Main Content -->
        @section('content')
        <div class="be-wrapper be-login">
            <div class="be-content">
                <div class="main-content container-fluid">
                    <div class="splash-container">
                        <div class="panel panel-default">
                            <div class="panel-heading">Recuperar Contraseña</div>
                            <div class="panel-body">
                                @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                                @endif

                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="control-label">{{ trans('data.email') }}</label>
                                        <div class="">
                                            <input id="email" type="email" class="form-control input-sm" name="email" value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class=" text-center">
                                            <button type="submit" class="btn btn-primary">
                                                Enviar correo de recuperación
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
    */
@endphp