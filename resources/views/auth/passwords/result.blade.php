
@extends('layouts.app')

@section('body-tag')class="be-splash-screen"@endsection

<!-- Main Content -->
@section('content')
    <div class="be-wrapper be-login">
        <div class="be-content">
            <div class="main-content container-fluid">
                <div class="splash-container">
                    <div class="panel panel-default">
                        <div class="panel-heading">Recuperacion de contraseña</div>
                        <div class="panel-body">
                            @if (isset($message))
                                <div class="alert alert-success">
                                    {{$message}}
                                </div>
                            @endif
                            @if (isset($error))
                                <div class="alert alert-danger">
                                    {{$error}}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection