@extends('layouts.panel')

@section('content-panel') 
    @include('banner.header')
        <a href="{{ url('banners/create') }}"  class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}</a><br/>
        <hr/>
	 @include('banner.datatable')
@endsection