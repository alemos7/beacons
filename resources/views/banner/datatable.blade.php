<table class="table table-striped table-condensed" id="shops-datatable">
    <thead>
        <tr>
            <th class="text-center">{{ trans('data.id') }}</th>
            @for($i=1;$i<=5;$i++)
            <th>{{ trans('models.shop').' '.$i }}</th>
            <th>{{ trans('models.product').' '.$i }}</th>
            @endfor
            <th>{{ trans('data.action') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($banners as $banner)
            <tr>
                <td class="text-center">{{ str_limit($banner->id,30) }}</td>
                  @for($i=1;$i<=5;$i++)
                  <td>{{ str_limit($banner['product_'.$i]->shop->name, 30) }}</td>
                  <td>{{ str_limit($banner['product_'.$i]->name, 30) }}</td>
                  @endfor
                <td>
                    <form action='{{ url('banners/'.$banner->id) }}' method='POST' id="banner-delete-{{ $banner->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field()}} 
                        <a class="icon mdi mdi-edit btn btn-primary" href="{{ url('banners/'.$banner->id) }}" data-toggle="tooltip" title="{{ trans("process.update") }}"></a>
                        <a data-modal="full-danger"  class="btn btn-danger md-trigger " onclick="formSelected='banner-delete-{{ $banner->id }}'" data-toggle="tooltip" title="{{ trans("process.delete") }}">
                            <span class="icon mdi mdi-delete"></span>          
                        </a>
                    </form>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@section('footer')
    <script>
        $('#shops-datatable').DataTable();
    </script>
@endsection