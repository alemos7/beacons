<form role="form" method="POST" action="{{ url('banners') }}" class="form-horizontal group-border-dashed" enctype="multipart/form-data">

  @if($banner->id != '')
      <input type='hidden' name='_method' value='PUT'> 
  @endif

  {{ csrf_field() }}
  <div class="banner-container">
    <div role="alert" class="alert alert-primary alert-icon alert-icon-colored">
      <div class="icon"><span class="mdi mdi-info-outline"></span></div>
      <div class="message">
        <strong>Seleccione la imagen del banner de publicidad.</strong>
        <ul>
          <li>Formato: JPG o PNG (Sin fondo transparente)</li>
          <li>Tamaño: 1050 x 426 px</li>
        </ul>
      </div>
    </div>
    @for( $i = 1 ; $i <= 10 ; $i++)

    <div class="section-banner">
      <i class="mdi mdi-collection-item-{{$i}}"></i>
      <div class="form-group">
        <label  class="col-md-3 control-label">{{ trans('models.shop') }}</label>
        <div class="col-md-8">
          <select  class="form-control input-sm banner-shop select2" data-banner="{{$i}}"
            @if( $banner['product_'.$i.'_id'] ) data-product="{{ $banner['product_'.$i.'_id'] }}" @endif>
            <option value="">{{ trans('placeholder.shop') }}</option>
            @foreach($shops as $shop)
            <option value="{{ $shop->id }}" data-products="{{ $shop->products }}"
              @if(  $banner->id && 
                    $banner['product_'.$i] && 
                    $banner['product_'.$i]->shop->id == $shop->id) selected="" @endif>
              {{ $shop->name }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="form-group">
        <label  class="col-md-3 control-label">{{ trans('models.product') }}</label>
        <div class="col-md-8">
          <select name="product_{{$i}}_id" class="form-control input-sm select2" id="product-{{$i}}" disabled="">
            <option value="">{{ trans('placeholder.product') }}</option>
          </select>
        </div>
      </div>


      <div class="form-group">
        <label class="col-md-3 control-label">{{ trans('models.image') }}</label>
        <div class="col-md-8">
          <div class="local-dropzone">
            <label>Arrastre su imagen aquí o haga clic para agregarla.</label>
            
            <img id="logo-preview" 
              @if( $banner['image_'.$i] ) src="{{ url($banner['image_'.$i]) }}" @endif>

          </div>
          <input type="file" name="image_{{$i}}">
        </div>
      </div>

    </div>
    @endfor

  </div>
  <div class="form-group">
    <div class="col-xs-12">
      <button type="submit" class="btn btn-primary"><span class="icon mdi mdi-floppy"></span>  {{ trans('process.save') }}</button>
    </div>
  </div>
</form>