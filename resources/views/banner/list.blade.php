 <div class="[ col-xs-12 col-sm-offset-2 col-sm-8 ]">
     <ul class="shops-list">
        @foreach ($shops as $shop)
        <a  href="{{ url('shops/'.$shop->id) }}">
            <li>
                <img src="http://placehold.it/120x120" />
                <div class="info">
                    <h2 class="title">{{ $shop->name }}</h2> 
                    <p class="desc">{{ $shop->business_name }}</p>
                    <label class="desc">{{ $shop->description }}</label>
                </div>
            </li>
        </a>
        @endforeach
    </ul>
</div>