@extends('layouts.panel')

@section('content-panel') 
    @include('banner.header')

    @include('banner.form')
@endsection

@section('footer')
    <script src="{{ URL::asset('js/banner/banner-select.js') }}" type="text/javascript"></script>
@endsection