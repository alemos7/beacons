<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="format-detection" content="telephone=no"/>
    <title>Blueshop</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700,300|Roboto:400,300,700&subset=latin,cyrillic,greek" rel="stylesheet" type="text/css">
    <style type="text/css">

        /* Resets: see reset.css for details */
        .ReadMsgBody { width: 100%; background-color: #ffffff;}
        .ExternalClass {width: 100%; background-color: #ffffff;}
        .ExternalClass, .ExternalClass p, .ExternalClass span,
        .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
        #outlook a{ padding:0;}
        body{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0;}
        body{ -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
        html{width:100%;}
        table {mso-table-lspace:0pt; mso-table-rspace:0pt; border-spacing:0;}
        table td {border-collapse:collapse;}
        table p{margin:0;}
        br, strong br, b br, em br, i br { line-height:100%; }
        div, p, a, li, td { -webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
        h1, h2, h3, h4, h5, h6 { line-height: 100% !important; -webkit-font-smoothing: antialiased; }
        span a { text-decoration: none !important;}
        a{ text-decoration: none !important; }
        img{height: auto !important; line-height: 100%; outline: none; text-decoration: none;  -ms-interpolation-mode:bicubic;}
        .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited,
        .yshortcuts a:hover, .yshortcuts a span { text-decoration: none !important; border-bottom: none !important;}
        /*mailChimp class*/
        .default-edit-image{
            height:20px;
        }
        ul{padding-left:10px; margin:0;}
        .tpl-repeatblock {
            padding: 0px !important;
            border: 1px dotted rgba(0,0,0,0.2);
        }
        .tpl-content{
            padding:0px !important;
        }
        @media only screen and (max-width:800px){
            table[style*="max-width:800px"]{width:100%!important; max-width:100%!important; min-width:100%!important; clear: both;}
            table[style*="max-width:800px"] img{width:100% !important; height:auto !important; max-width:100% !important;}
        }
        @media only screen and (max-width: 640px){
            /* mobile setting */
            table[class="container"]{width:100%!important; max-width:100%!important; min-width:100%!important;
                padding-left:20px!important; padding-right:20px!important; text-align: center!important; clear: both;}
            td[class="container"]{width:100%!important; padding-left:20px!important; padding-right:20px!important; clear: both;}
            table[class="full-width"]{width:100%!important; max-width:100%!important; min-width:100%!important; clear: both;}
            table[class="full-width-center"] {width: 100%!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
            table[class="force-240-center"]{width:240px !important; clear: both; margin:0 auto; float:none;}
            table[class="auto-center"] {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
            *[class="auto-center-all"]{width: auto!important; max-width:75%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
            *[class="auto-center-all"] * {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
            table[class="col-3"],table[class="col-3-not-full"]{width:30.35%!important; max-width:100%!important;}
            table[class="col-2"]{width:47.3%!important; max-width:100%!important;}
            *[class="full-block"]{width:100% !important; display:block !important; clear: both; padding-top:10px; padding-bottom:10px;}
            /* image */
            td[class="image-full-width"] img{width:100% !important; height:auto !important; max-width:100% !important;}
            /* helper */
            table[class="space-w-20"]{width:3.57%!important; max-width:20px!important; min-width:3.5% !important;}
            table[class="space-w-20"] td:first-child{width:3.5%!important; max-width:20px!important; min-width:3.5% !important;}
            table[class="space-w-25"]{width:4.45%!important; max-width:25px!important; min-width:4.45% !important;}
            table[class="space-w-25"] td:first-child{width:4.45%!important; max-width:25px!important; min-width:4.45% !important;}
            table[class="space-w-30"] td:first-child{width:5.35%!important; max-width:30px!important; min-width:5.35% !important;}
            table[class="fix-w-20"]{width:20px!important; max-width:20px!important; min-width:20px!important;}
            table[class="fix-w-20"] td:first-child{width:20px!important; max-width:20px!important; min-width:20px !important;}
            *[class="h-10"]{display:block !important;  height:10px !important;}
            *[class="h-20"]{display:block !important;  height:20px !important;}
            *[class="h-30"]{display:block !important; height:30px !important;}
            *[class="h-40"]{display:block !important;  height:40px !important;}
            *[class="remove-640"]{display:none !important;}
            *[class="text-left"]{text-align:left !important;}
            *[class="clear-pad"]{padding:0 !important;}
        }
        @media only screen and (max-width: 479px){
            /* mobile setting */
            table[class="container"]{width:100%!important; max-width:100%!important; min-width:124px!important;
                padding-left:15px!important; padding-right:15px!important; text-align: center!important; clear: both;}
            td[class="container"]{width:100%!important; padding-left:15px!important; padding-right:15px!important; text-align: center!important; clear: both;}
            table[class="full-width"],table[class="full-width-479"]{width:100%!important; max-width:100%!important; min-width:124px!important; clear: both;}
            table[class="full-width-center"] {width: 100%!important; max-width:100%!important; min-width:124px!important; text-align: center!important; clear: both; margin:0 auto; float:none;}
            *[class="auto-center-all"]{width: 100%!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
            *[class="auto-center-all"] * {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
            table[class="col-3"]{width:100%!important; max-width:100%!important; text-align: center!important; clear: both;}
            table[class="col-3-not-full"]{width:30.35%!important; max-width:100%!important; }
            table[class="col-2"]{width:100%!important; max-width:100%!important; text-align: center!important; clear: both;}
            *[class="full-block-479"]{display:block !important; width:100% !important; clear: both; padding-top:10px; padding-bottom:10px; }
            /* image */
            td[class="image-full-width"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:124px !important;}
            td[class="image-min-80"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:80px !important;}
            td[class="image-min-100"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:100px !important;}
            /* halper */
            table[class="space-w-20"]{width:100%!important; max-width:100%!important; min-width:100% !important;}
            table[class="space-w-20"] td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
            table[class="space-w-25"]{width:100%!important; max-width:100%!important; min-width:100% !important;}
            table[class="space-w-25"] td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
            table[class="space-w-30"]{width:100%!important; max-width:100%!important; min-width:100% !important;}
            table[class="space-w-30"] td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
            *[class="remove-479"]{display:none !important;}
            table[width="595"]{width:100% !important;}
            img{max-width:280px !important;}
            .resize-font, .resize-font *{
                font-size: 37px !important;
                line-height: 48px !important;
            }
        }

        td ul{list-style: initial; margin:0; padding-left:20px;}

        @media only screen and (max-width: 640px){ .image-100-percent{ width:100%!important; height: auto !important; max-width: 100% !important; min-width: 124px !important;}}td ul{list-style: initial; margin:0; padding-left:20px;}body{background-color:#efefef;} .default-edit-image{height:20px;} tr.tpl-repeatblock , tr.tpl-repeatblock > td{ display:block !important;} .tpl-repeatblock {padding: 0px !important;border: 1px dotted rgba(0,0,0,0.2);} table[width="595"]{width:100% !important;}a img{ border: 0 !important;}
        a:active{color:initial } a:visited{color:initial }
        .tpl-content{padding:0 !important;}
        .full-mb,*[fix="full-mb"]{width:100%!important;} .auto-mb,*[fix="auto-mb"]{width:auto!important;}
    </style>
    <!--[if gte mso 15]>
    <style type="text/css">
        a{text-decoration: none !important;}
        body { font-size: 0; line-height: 0; }
        tr { font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px; }
        table { font-size:1px; line-height:0; mso-margin-top-alt:1px; }
        body,table,td,span,a,font{font-family: Arial, Helvetica, sans-serif !important;}
        a img{ border: 0 !important;}
    </style>
    <![endif]-->
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
</head>
<body  style="font-size:12px; width:100%; height:100%;">
<table id="mainStructure" width="800" class="full-width" align="center" border="0" cellspacing="0" cellpadding="0" style="background-color: #efefef; width: 800px; max-width: 800px; outline: rgb(239, 239, 239) solid 1px; box-shadow: rgb(224, 224, 224) 0px 0px 5px; margin: 0px auto;">
    <!-- START LAYOUT-13 ( FULL-IMAGE / TEXT ) -->
    <tr>
        <td valign="top" align="center" style="background-color:#ffffff;" class="container" bgcolor="#ffffff">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; min-width: 600px; padding-left: 20px; padding-right: 20px; width: 600px; margin: 0px auto;">
                <tbody>
                <tr>
                    <td valign="top" align="center">
                        <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="full-width" style="width: 560px; margin: 0px auto;">
                            <!-- start space -->
                            <tbody>
                            <tr>
                                <td valign="top" height="1" style="height: 1px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                                </td>
                            </tr>
                            <!-- end space -->
                            <tr>
                                <td valign="top" align="center">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin: 0px auto;">
                                        <!-- start image -->
                                        <tbody>
                                        <tr dup="0">
                                            <td valign="top" align="center">
                                                <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width-center" style="margin: 0px auto;">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center" valign="top" class="image-full-width" width="600" style="width: 600px;">
                                                            <img src="{{ URL::asset('img/email-logo.jpg') }}" width="600" style="max-width: 600px; display: block !important; width: 600px; height: auto;" alt="image" border="0" hspace="0" vspace="0" height="auto">
                                                        </td>
                                                    </tr>
                                                    <!-- start space -->
                                                    <tr>
                                                        <td valign="top" height="2" style="height: 2px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                                                        </td>
                                                    </tr>
                                                    <!-- end space -->
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- end image -->
                                        <!-- start title -->
                                        <!-- end title -->
                                        <!-- start description -->
                                        <!-- end description -->
                                        <!-- start content -->
                                        <!-- end content -->
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- start space -->
                            <tr>
                                <td valign="top" height="1" style="height: 1px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                                </td>
                            </tr>
                            <!-- end space -->
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end container -->
        </td>
    </tr>
    <!-- END LAYOUT-13 ( FULL-IMAGE / TEXT ) -->
    <!-- START LAYOUT-11 ( 2-COL IMAGE / TEXT / BUTTON ) -->
    <tr>
        <td align="center" valign="top" class="container" style="background-color: #086ec1;" bgcolor="#086ec1">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #086ec1; min-width: 600px; width: 600px; margin: 0px auto;">
                <tbody>
                <tr>
                    <td valign="top" align="center">
                        <!-- start col left -->
                        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="full-width" style="margin: 0px auto;">
                            <!-- start space -->
                            <tbody>
                            <tr>
                                <td valign="top" height="26" style="height: 26px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                            </tr>
                            <!-- end space -->
                            <!-- start group heading -->
                            <tr dup="0">
                                <td valign="top" class="clear-pad" style="padding-left:20px; padding-right:20px;">
                                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;">
                                        <!-- start title -->
                                        <tbody>
                                        <tr dup="0">
                                            <td valign="top">
                                                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center" style="font-size: 26px; color: #333333; font-weight: normal; text-align: center; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 34px;">
                              <span style="color: #ffffff; word-break: break-word; line-height: 38px; font-size: 30px; font-weight: bold; font-family: Roboto, arial, sans-serif;">
                                  @if(isset($payment->type) && $payment->type == 'comprador' && isset($payment->msj['title']))
                                    <font face="Roboto, arial, sans-serif">{{$payment->msj['title']}}</font>
                                  @else
                                      <font face="Roboto, arial, sans-serif">Producto vendido, ¡Felicitaciones!</font>
                                  @endif
                              </span>
                                                            <br style="font-size: 26px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                        </td>
                                                    </tr>
                                                    <!-- start space -->
                                                    <tr>
                                                        <td valign="top" height="25" style="height: 25px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                                    </tr>
                                                    <!-- end space -->
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- end title -->
                                        <!-- start content -->
                                        <!-- end content -->
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- end group heading -->
                            <!-- start group 2-col -->
                            <!-- end group 2-col -->
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end container -->
        </td>
    </tr>
    <!--END LAYOUT-11 ( 2-COL IMAGE / TEXT / BUTTON ) -->
    <!--START LAYOUT-18 ( CONTACT US / ABOUT US )  -->
    <tr>
        <td align="center" valign="top" class="container" style="background-color: #fff;" bgcolor="#ffffff">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; min-width: 600px; padding-left: 20px; padding-right: 20px; width: 600px; margin: 0px auto;">
                <!-- start content container-->
                <tbody>
                <tr>
                    <td valign="top">
                        <table width="560" align="center" border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 560px; margin: 0px auto;">
                            <!-- start space -->
                            <tbody>
                            <tr>
                                <td valign="top" height="50" style="height: 50px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                            </tr>
                            <!-- end space -->
                            <tr>
                                <td valign="top">
                                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;">
                                        <tbody>
                                        <tr>
                                            <td valign="top" align="center">

                                                <table width="265" align="left" border="0" cellpadding="0" cellspacing="0" class="col-2" style="width: 265px;mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                    <!-- start title -->
                                                    <tbody>
                                                    <tr dup="0">
                                                        <td valign="top" align="left">
                                                            <table width="100%" align="left" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" style="font-size: 18px; color: #333333; font-weight: normal; text-align: left; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 26px;">
                                    <span style="color: #000000; word-break: break-word; line-height: 28px; font-size: 20px; font-weight: bold; font-family: Roboto, arial, sans-serif;">
                                        @if(isset($payment->msj['details']))
                                            <font face="Roboto, arial, sans-serif">{{$payment->msj['details']}}</font>
                                        @else
                                            <font face="Roboto, arial, sans-serif">Detalles de la venta</font>
                                        @endif
                                    </span>
                                                                        <br style="font-size: 18px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                    </td>
                                                                </tr>
                                                                <!-- start space -->
                                                                <tr>
                                                                    <td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                                                </tr>
                                                                <!-- end space -->
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <!-- end title -->
                                                    <!-- start content -->
                                                    <tr dup="0">
                                                        <td valign="top">
                                                            <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="left" style="font-size: 14px; color: #888888; font-weight: normal; text-align: left; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 22px;">
                                    <span style="color: #000000; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                    <font face="'Open Sans', Arial, Helvetica, sans-serif">Pedido:</font>
                                    </span> #{{ $payment->id }}<br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                    <span style="color: #000000; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                    <font face="'Open Sans', Arial, Helvetica, sans-serif">Fecha:</font>
                                    </span>{{ ' '.$payment->date_created }}<br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                        <span style="color: #000000; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                    <font face="'Open Sans', Arial, Helvetica, sans-serif">Hora:</font>
                                    </span> {{ $payment->hour_created }}

                                    @if(isset($payment->msj['mercadopago']) && $payment->msj['mercadopago'] != null)
                                                                            <br><span style="color: #000000; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                    <font face="'Open Sans', Arial, Helvetica, sans-serif">Método de pago:</font>
                                    </span> Mercado Pago
                                        <br><span style="color: #000000; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                            <font face="'Open Sans', Arial, Helvetica, sans-serif">id mercadopago:</font>
                                        </span> {{ $payment->mp_payment_id  }}</td>
                                    @else
                                                                        <br><span style="color: #000000; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                    <font face="'Open Sans', Arial, Helvetica, sans-serif">Metodo de pago:</font>
                                    </span> Efectivo
                                    @endif
                                                                </tr>
                                                                <!-- start space -->
                                                                <tr>
                                                                    <td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                                                </tr>
                                                                <!-- end space -->
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <!-- end content -->
                                                    </tbody>
                                                </table>
                                                <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                <td valign="top" >
                                                <![endif]-->
                                                <table width="30" border="0" cellpadding="0" cellspacing="0" align="left" class="space-w-25" style="min-width: 30px; height: 1px; border-spacing: 0px; width: 30px;mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                    <tbody>
                                                    <tr>
                                                        <td height="1" width="30" class="h-30" style="display: block; width: 30px; height: 1px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                <td valign="top" >
                                                <![endif]-->
                                @if(isset($payment->type) && $payment->type == 'comprador')
                                                    <table width="265" align="right" border="0" cellpadding="0" cellspacing="0" class="col-2" style="width: 265px;mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                        <!-- start title -->
                                                        <tbody>
                                                        <tr dup="0">
                                                            <td valign="top" align="left">
                                                                <table width="100%" align="left" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td align="left" style="font-size: 18px; color: #333333; font-weight: normal; text-align: left; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 26px;">
                                                                            <span style="color: #000000; word-break: break-word; line-height: 28px; font-size: 20px; font-weight: bold; font-family: Roboto, arial, sans-serif;">
                                    <font face="Roboto, arial, sans-serif">Detalles del vendedor</font>
                                    </span>
                                                                            <br style="font-size: 18px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                        </td>
                                                                    </tr>
                                                                    <!-- start space -->
                                                                    <tr>
                                                                        <td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- end space -->
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <!-- end title -->
                                                        <!-- start content -->
                                                        <tr dup="0">
                                                            <td valign="top">
                                                                <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td align="left" style="font-size: 14px; color: #888888; font-weight: normal; text-align: left; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 22px;">
                                                                            <span style="color: #000000; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                            <font face="'Open Sans', Arial, Helvetica, sans-serif">Nombre comercial:</font>
                                                                            </span>@if(!empty($payment->shop->business_name)) {{$payment->shop->business_name}} @else - @endif
                                                                            <br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                            <span style="color: #000000; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                            <font face="'Open Sans', Arial, Helvetica, sans-serif">Teléfono:</font>
                                                                            </span>@if(!empty($payment->shop->phone)) {{$payment->shop->phone}} @else - @endif
                                                                            <br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                            <span style="color: #000000; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                            <font face="'Open Sans', Arial, Helvetica, sans-serif">Dirección:</font>
                                                                            </span>@if(!empty($payment->shop->address)) {{$payment->shop->address}} @else - @endif <br />
                                                                            <span style="color: #000000; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                            <font face="'Open Sans', Arial, Helvetica, sans-serif">Email:</font>
                                                                            </span>@if(!empty($payment->shop->email)) {{$payment->shop->email}} @else - @endif
                                                                            <br><br>
                                                                        </td>
                                                                    </tr>
                                                                    <!-- start space -->
                                                                    <tr>
                                                                        <td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                                                    </tr>
                                                                    <!-- end space -->
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>

                                @else
                                    <table width="265" align="right" border="0" cellpadding="0" cellspacing="0" class="col-2" style="width: 265px;mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                        <!-- start title -->
                                        <tbody>
                                        <tr dup="0">
                                            <td valign="top" align="left">
                                                <table width="100%" align="left" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                    <tbody>
                                                    <tr>
                                                        <td align="left" style="font-size: 18px; color: #333333; font-weight: normal; text-align: left; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 26px;">
                                                            <span style="color: #000000; word-break: break-word; line-height: 28px; font-size: 20px; font-weight: bold; font-family: Roboto, arial, sans-serif;">
                                    <font face="Roboto, arial, sans-serif">Comprador</font>
                                    </span>
                                                            <br style="font-size: 18px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                        </td>
                                                    </tr>
                                                    <!-- start space -->
                                                    <tr>
                                                        <td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                                    </tr>
                                                    <!-- end space -->
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- end title -->
                                        <!-- start content -->
                                        <tr dup="0">
                                            <td valign="top">
                                                <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0" style="mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                    <tbody>
                                                    <tr>
                                                        <td align="left" style="font-size: 14px; color: #888888; font-weight: normal; text-align: left; font-family: 'Open Sans', Arial, Helvetica, sans-serif; word-break: break-word; line-height: 22px;">
                                                            <span style="color: #000000; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                            <font face="'Open Sans', Arial, Helvetica, sans-serif">Nombre: </font>
                                                            </span>@if(!empty($payment->user->full_name)) {{$payment->user->full_name}} @else - @endif
                                                            <br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                            <span style="color: #000000; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                            <font face="'Open Sans', Arial, Helvetica, sans-serif">Teléfono: </font>
                                                            </span>@if(!empty($payment->user->phone)) {{$payment->user->phone}} @else - @endif
                                                            <br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                            <span style="color: #000000; line-height: 22px; font-size: 14px; font-weight: bold; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                            <font face="'Open Sans', Arial, Helvetica, sans-serif">Email: </font>
                                                            </span>@if(!empty($payment->user->email)) {{$payment->user->email}} @else - @endif
                                                            <br><br>
                                                        </td>
                                                    </tr>
                                                    <!-- start space -->
                                                    <tr>
                                                        <td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                                    </tr>
                                                    <!-- end space -->
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>

                                        @endif
                                    <!-- end content -->
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- start space -->
                            <tr>
                                <td valign="top" height="30" style="height: 30px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                            </tr>
                            <!-- end space -->
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end container -->
        </td>
    </tr>
    <!-- END LAYOUT-18 ( CONTACT US / ABOUT US ) -->
    <!--START LAYOUT-15 ( 2-COL TABLE PRICE ) -->
    <tr>
        <td align="center" valign="top" class="container" style="background-color: #ffffff;" bgcolor="#ffffff">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; min-width: 600px; width: 600px; margin: 0px auto;">
                <tbody>
                <tr>
                    <td valign="top" align="center">
                        <!-- start col left -->
                        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="full-width" style="margin: 0px auto;">
                            <!-- start space -->
                            <tbody>
                            <tr>
                                <td valign="top" height="2" style="height: 2px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                                </td>
                            </tr>
                            <!-- end space -->
                            <!-- start group heading -->
                            <tr dup="0">
                                <td valign="top" class="clear-pad" style="padding-left:20px; padding-right:20px;">
                                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;">
                                        <!-- start title -->
                                        <tbody>
                                        <!-- end title -->
                                        <!-- start content -->
                                        <tr dup="0">
                                            <td valign="top">
                                                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center" style="font-size: 14px; color: #888888; font-weight: normal; font-family: 'Open Sans', Arial, Helvetica, sans-serif; text-align: center; word-break: break-word; line-height: 22px;">
                                                            <table class="m_-2184356662000553917td" cellspacing="0" cellpadding="6" style="color: #737373; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1" width="100%" data-mce-selected="1">
                                                                <thead style="font-size: 14px; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                                                <tr style="font-size: 14px; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                                                    <th class="m_-2184356662000553917td" scope="col" style="color: #737373; text-align: left; padding: 12px; font-size: 14px; font-weight: bold; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                        <span style="color: #000000; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: bold; font-family: Roboto, arial, sans-serif;">
                                        <font face="Roboto, arial, sans-serif">Producto</font>
                                        </span>
                                                                    </th>
                                                                    <th class="m_-2184356662000553917td" scope="col" style="color: #737373; text-align: left; padding: 12px; font-size: 14px; font-weight: bold; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                        <span style="color: #000000; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: bold; font-family: Roboto, arial, sans-serif;">
                                        <font face="Roboto, arial, sans-serif">Cantidad</font>
                                        </span>
                                                                    </th>
                                                                    <th class="m_-2184356662000553917td" scope="col" style="color: #737373; text-align: left; padding: 12px; font-size: 14px; font-weight: bold; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                        <span style="color: #000000; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: bold; font-family: Roboto, arial, sans-serif;">
                                        <font face="Roboto, arial, sans-serif">Precio</font>
                                        </span>
                                                                    </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody style="font-size: 14px; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                                                <tr class="m_-2184356662000553917order_item" style="font-size: 14px; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                                                    <td class="m_-2184356662000553917td" style="color: #737373; text-align: left; vertical-align: middle; padding: 12px; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" width="" align="left">
                                                                        <img src="{{ url($payment->product->images[0]->image) }}" width="" style="float: left; max-width: 82px; display: block !important; font-size: 0px; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" alt="image" border="0" hspace="0" vspace="0" height="auto">
                                                                        <span style="float: left; margin-top: 30px; margin-left: 15px; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                                        <font face="Roboto, arial, sans-serif">
                                        <font face="Roboto, arial, sans-serif" style="font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">{{ $payment->product->name }}</font>
                                        </font>
                                        </span>
                                                                        <br style="font-size: 14px; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                                                        <span style="word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                                        <small style="font-size: 10.5px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                                        </small>
                                        </span>
                                                                    </td>
                                                                    <td class="m_-2184356662000553917td" style="color: #737373; text-align: left; vertical-align: middle; padding: 12px; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" align="left">
                                        <span style="word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                                        <font face="Roboto, arial, sans-serif">
                                        <font face="Roboto, arial, sans-serif" style="font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">1</font>
                                        </font>
                                        </span>
                                                                    </td>
                                                                    <td class="m_-2184356662000553917td" style="color: #737373; text-align: left; vertical-align: middle; padding: 12px; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" align="left">
                                        <span class="m_-2184356662000553917woocommerce-Price-amount                                                                                     amount" style="word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                                        <font face="Roboto, arial, sans-serif">
                                        <font face="Roboto, arial, sans-serif" style="font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">{{ '$ '.$payment->product->price }}</font>
                                        </font>
                                        </span>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                                <tfoot style="font-size: 14px; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                                                <tr style="font-size: 14px; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                                                    <th class="m_-2184356662000553917td" scope="row" colspan="2" style="color: #737373; text-align: left; padding: 12px; font-size: 14px; font-weight: bold; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
                                        <span style="color: #000000; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: bold; font-family: Roboto, arial, sans-serif;">
                                        <font face="Roboto, arial, sans-serif">Total:</font>
                                        </span>
                                                                    </th>
                                                                    <td class="m_-2184356662000553917td" style="color: #737373; text-align: left; padding: 12px; font-size: 14px; font-weight: 400; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" align="left">
                                        <span class="m_-2184356662000553917woocommerce-Price-amount m_-2184356662000553917amount" style="word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                                        <font face="Roboto, arial, sans-serif">{{ '$ '.$payment->product->price }}</font>
                                        </span>
                                                                    </td>
                                                                </tr>
                                                                </tfoot>
                                                            </table>
                                                            <div id="mceResizeHandlen" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="margin: 0px; padding: 0px; left: 296.5px; top: 4.5px; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                            </div>
                                                            <!--[if (gte mso 9)|(IE)]>
                                                            </td>
                                                            <td valign="top" >
                                                            <![endif]-->
                                                            <div id="mceResizeHandlee" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="margin: 0px; padding: 0px; left: 588.5px; top: 102px; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                            </div>
                                                            <!--[if (gte mso 9)|(IE)]>
                                                            </td>
                                                            <td valign="top" >
                                                            <![endif]-->
                                                            <div id="mceResizeHandles" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="margin: 0px; padding: 0px; left: 296.5px; top: 199.5px; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                            </div>
                                                            <!--[if (gte mso 9)|(IE)]>
                                                            </td>
                                                            <td valign="top" >
                                                            <![endif]-->
                                                            <div id="mceResizeHandlew" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="margin: 0px; padding: 0px; left: 4.5px; top: 102px; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                            </div>
                                                            <!--[if (gte mso 9)|(IE)]>
                                                            </td>
                                                            <td valign="top" >
                                                            <![endif]-->
                                                            <div id="mceResizeHandlenw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="margin: 0px; padding: 0px; left: 4.5px; top: 4.5px; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                            </div>
                                                            <!--[if (gte mso 9)|(IE)]>
                                                            </td>
                                                            <td valign="top" >
                                                            <![endif]-->
                                                            <div id="mceResizeHandlene" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="margin: 0px; padding: 0px; left: 588.5px; top: 4.5px; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                            </div>
                                                            <!--[if (gte mso 9)|(IE)]>
                                                            </td>
                                                            <td valign="top" >
                                                            <![endif]-->
                                                            <div id="mceResizeHandlese" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="margin: 0px; padding: 0px; left: 588.5px; top: 199.5px; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                            </div>
                                                            <!--[if (gte mso 9)|(IE)]>
                                                            </td>
                                                            <td valign="top" >
                                                            <![endif]-->
                                                            <div id="mceResizeHandlesw" data-mce-bogus="all" class="mce-resizehandle" unselectable="true" style="margin: 0px; padding: 0px; left: 4.5px; top: 199.5px; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                            </div>
                                                            <!--[if (gte mso 9)|(IE)]>
                                                            </td>
                                                            <td valign="top" >
                                                            <![endif]-->
                                                        </td>
                                                    </tr>
                                                    <!-- start space -->
                                                    <tr>
                                                        <td valign="top" height="40" style="height: 40px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                                    </tr>
                                                    <!-- end space -->
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- end content -->
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- end group heading -->
                            <!-- start group 2-col -->
                            <!-- end group 2-col -->
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end container -->
        </td>
    </tr>
    <!--END LAYOUT-15 ( 2-COL TABLE PRICE ) -->
    <!-- START LAYOUT-11 ( 2-COL IMAGE / TEXT / BUTTON ) -->
    <tr>
        <td align="center" valign="top" class="container" style="background-color: #ffffff;" bgcolor="#ffffff">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; min-width: 600px; width: 600px; margin: 0px auto;">
                <tbody>
                <tr>
                    <td valign="top" align="center">
                        <!-- start col left -->
                        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="full-width" style="margin: 0px auto;">
                            <!-- start space -->
                            <tbody>
                            <tr>
                                <td valign="top" height="3" style="height: 3px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                                </td>
                            </tr>
                            <!-- end space -->
                            <!-- start group heading -->
                            <tr dup="0">
                                <td valign="top" class="clear-pad" style="padding-left:20px; padding-right:20px;">
                                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;">
                                        <!-- start title -->
                                        <tbody>
                                        <tr dup="0">
                                            <td valign="top">
                                                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;">
                                                    <tbody>
                                                    <tr>
                                                        <td align="left" style="font-size: 26px; color: #333333; font-weight: normal; font-family: 'Open Sans', Arial, Helvetica, sans-serif; text-align: left; word-break: break-word; line-height: 34px;">
                                                            <div style="text-align: left; font-size: 26px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                              <span style="color: #000000; word-break: break-word; line-height: 28px; font-size: 20px; font-weight: bold; font-family: Roboto, arial, sans-serif;">
                              <font face="Roboto, arial, sans-serif">Forma de pago y entrega</font>
                              </span>
                                                                <br style="font-size: 26px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <!-- start space -->
                                                    <tr>
                                                        <td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                                    </tr>
                                                    <!-- end space -->
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- end title -->
                                        <!-- start content -->
                                        <tr dup="0">
                                            <td valign="top">
                                                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;">
                                                    <tbody>
                                                    <tr>
                                                        <td align="left" style="font-size: 14px; color: #888888; font-weight: normal; font-family: 'Open Sans', Arial, Helvetica, sans-serif; text-align: left; word-break: break-word; line-height: 22px;">
                                                            <div style="text-align: left; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                              @if(isset($payment->msj))
                                                                    <span style="color: #000000; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                              <font face="Roboto, arial, sans-serif">{{$payment->msj['method']}}</font>
                                                                    </span>
                              @elseif(isset($payment->msj2))
                                                                    <span style="color: #000000; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                              <font face="Roboto, arial, sans-serif">{{$payment->msj2['method']}}</font>
                                                                    </span>
                              @else
                                                                    <span style="color: #000000; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                              <font face="Roboto, arial, sans-serif">El comprador ha realizado el pago a través de Mercado Pago.&nbsp;
                                  <span style="color: #000000; line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                              <font face="Roboto, arial, sans-serif">En instantes recibirá un email con los detalles del mismo.</font>
                              </span>&nbsp;</font>
                              </span>
                                                                    <br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                    <span style="color: #000000; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                              <font face="Roboto, arial, sans-serif">Contacte al comprador para coordinar la entrega.</font>
                              </span>
                              @endif
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <td>
                                                        <div style="">
                                                            @if(isset($payment->msj['clasification']))
                                                                <br />
                                                                <span style="color: #000000; word-break: break-word; line-height: 28px; font-size: 20px; font-weight: bold; font-family: Roboto, arial, sans-serif;">
                                                                          <font face="Roboto, arial, sans-serif">Calificar</font>
                                                                          </span>
                                                            <br />
                                                            <br />
                                                                <span style="color: #000000; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                                                                              <font face="Roboto, arial, sans-serif">
                                                                                  {{$payment->msj['clasification']}}
                                                                              </font>
                                                                            </span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                    <!-- start space -->
                                                    <tr>
                                                        <td valign="top" height="40" style="height: 40px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                                    </tr>
                                                    <!-- end space -->
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- end content -->
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- end group heading -->
                            <!-- start group 2-col -->
                            <!-- end group 2-col -->
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end container -->
        </td>
    </tr>
    <!--END LAYOUT-11 ( 2-COL IMAGE / TEXT / BUTTON ) -->
    <!-- START LAYOUT-11 ( 2-COL IMAGE / TEXT / BUTTON ) -->
    <tr>
        <td align="center" valign="top" class="container" style="background-color: #ffffff;" bgcolor="#ffffff">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; min-width: 600px; width: 600px; margin: 0px auto;">
                <tbody>
                <tr>
                    <td valign="top" align="center">
                        <!-- start col left -->
                        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="full-width" style="margin: 0px auto;">
                            <!-- start space -->
                            <tbody>
                            <tr>
                                <td valign="top" height="20" style="height: 20px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                            </tr>
                            <!-- end space -->
                            <!-- start group heading -->
                            <tr dup="0">
                                <td valign="top" class="clear-pad" style="padding-left:20px; padding-right:20px;">
                                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;">
                                        <!-- start title -->
                                        <tbody>
                                        <!-- end title -->
                                        <!-- start content -->
                                        <tr dup="0">
                                            <td valign="top">
                                                <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center" style="font-size: 14px; color: #888888; font-weight: normal; font-family: 'Open Sans', Arial, Helvetica, sans-serif; text-align: center; word-break: break-word; line-height: 22px;">
                                                            <div style="text-align: center; font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                @if(empty($payment->type))
                              <span style="color: #000000; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                              <font face="Roboto, arial, sans-serif">Ver esta venta en el <span style="color: #3366ff; line-height: 22px; font-size: 14px; font-weight: bold; font-family: Roboto, arial, sans-serif;">
                              <font face="Roboto, arial, sans-serif">
                              <a herf="#" target="_blank" style="border-style: none; text-decoration: none !important; line-height: 22px; font-size: 14px; font-weight: bold; font-family: Roboto, arial, sans-serif;" border="0">
                                  <font face="Roboto, arial, sans-serif"> <a href="https://panel.blueshop.com.ar/payments/{{  $payment->id }}">panel de administración.</a></font>
                              </a> </font>
                              </span>
                              </font>
                              </span>
                                                                @endif
                                                                <br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                <br style="font-size: 14px; font-weight: 400; font-family: 'Open Sans', Arial, Helvetica, sans-serif;">
                                                                <span style="color: #000000; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                                                                    <font face="Roboto, arial, sans-serif">¡Muchas gracias por formar parte de <a href="http://blueshop.com.ar">BlueShop!</a></font>
                              </span>
                                                                    <br><br><br><br>
                                                                    <span style="color: #000000; word-break: break-word; line-height: 22px; font-size: 14px; font-weight: 400; font-family: Roboto, arial, sans-serif;">
                                                                    <font face="Roboto, arial, sans-serif"><small>No responda a este email. Por dudas o comentarios escribir a contacto@blueshop.com.ar</small></a></font>
                              </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <!-- start space -->
                                                    <tr>
                                                        <td valign="top" height="40" style="height: 40px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                                                    </tr>
                                                    <!-- end space -->
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <!-- end content -->
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- end group heading -->
                            <!-- start group 2-col -->
                            <!-- end group 2-col -->
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end container -->
        </td>
    </tr>
    <!--END LAYOUT-11 ( 2-COL IMAGE / TEXT / BUTTON ) -->
    <!--START LAYOUT-18 ( CONTACT US / ABOUT US )  -->
    <tr>
        <td align="center" valign="top" class="container" style="background-color: #fff;" bgcolor="#ffffff">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; min-width: 600px; padding-left: 20px; padding-right: 20px; width: 600px; margin: 0px auto;">
                <!-- start content container-->
                <tbody>
                <tr>
                    <td valign="top">
                        <table width="560" align="center" border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 560px; margin: 0px auto;">
                            <!-- start space -->
                            <tbody>
                            <tr>
                                <td valign="top" height="50" style="height: 50px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                            </tr>
                            <!-- end space -->
                            <tr>
                                <td valign="top">
                                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;">
                                        <tbody>
                                        <tr>
                                            <td valign="top" align="center">

                                                <table width="265" align="left" border="0" cellpadding="0" cellspacing="0" class="col-2" style="width: 265px;mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                    <!-- start title -->
                                                    <tbody>
                                                    <!-- end title -->
                                                    <!-- start content -->
                                                    <!-- end content -->
                                                    </tbody>
                                                </table>
                                                <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                <td valign="top" >
                                                <![endif]-->
                                                <table width="30" border="0" cellpadding="0" cellspacing="0" align="left" class="space-w-25" style="min-width: 30px; height: 1px; border-spacing: 0px; width: 30px;mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                    <tbody>
                                                    <tr>
                                                        <td height="1" width="30" class="h-30" style="display: block; width: 30px; height: 1px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <!--[if (gte mso 9)|(IE)]>
                                                </td>
                                                <td valign="top" >
                                                <![endif]-->
                                                <table width="265" align="right" border="0" cellpadding="0" cellspacing="0" class="col-2" style="width: 265px;mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                    <!-- start title -->
                                                    <tbody>
                                                    <!-- end title -->
                                                    <!-- start content -->
                                                    <!-- end content -->
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!-- start space -->
                            <tr>
                                <td valign="top" height="30" style="height: 30px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                            </tr>
                            <!-- end space -->
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- end container -->
        </td>
    </tr>
    <!-- END LAYOUT-18 ( CONTACT US / ABOUT US ) -->
</table>
</body>
</html>