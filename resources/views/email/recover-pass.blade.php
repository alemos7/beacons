<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="format-detection" content="telephone=no"/>
    <title>Blueshop</title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700,300|Roboto:400,300,700&subset=latin,cyrillic,greek" rel="stylesheet" type="text/css">
    <style type="text/css">

        /* Resets: see reset.css for details */
        .ReadMsgBody { width: 100%; background-color: #ffffff;}
        .ExternalClass {width: 100%; background-color: #ffffff;}
        .ExternalClass, .ExternalClass p, .ExternalClass span,
        .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
        #outlook a{ padding:0;}
        body{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0;}
        body{ -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
        html{width:100%;}
        table {mso-table-lspace:0pt; mso-table-rspace:0pt; border-spacing:0;}
        table td {border-collapse:collapse;}
        table p{margin:0;}
        br, strong br, b br, em br, i br { line-height:100%; }
        div, p, a, li, td { -webkit-text-size-adjust:none; -ms-text-size-adjust:none;}
        h1, h2, h3, h4, h5, h6 { line-height: 100% !important; -webkit-font-smoothing: antialiased; }
        span a { text-decoration: none !important;}
        a{ text-decoration: none !important; }
        img{height: auto !important; line-height: 100%; outline: none; text-decoration: none;  -ms-interpolation-mode:bicubic;}
        .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited,
        .yshortcuts a:hover, .yshortcuts a span { text-decoration: none !important; border-bottom: none !important;}
        /*mailChimp class*/
        .default-edit-image{
            height:20px;
        }
        ul{padding-left:10px; margin:0;}
        .tpl-repeatblock {
            padding: 0px !important;
            border: 1px dotted rgba(0,0,0,0.2);
        }
        .tpl-content{
            padding:0px !important;
        }
        @media only screen and (max-width:800px){
            table[style*="max-width:800px"]{width:100%!important; max-width:100%!important; min-width:100%!important; clear: both;}
            table[style*="max-width:800px"] img{width:100% !important; height:auto !important; max-width:100% !important;}
        }
        @media only screen and (max-width: 640px){
            /* mobile setting */
            table[class="container"]{width:100%!important; max-width:100%!important; min-width:100%!important;
                padding-left:20px!important; padding-right:20px!important; text-align: center!important; clear: both;}
            td[class="container"]{width:100%!important; padding-left:20px!important; padding-right:20px!important; clear: both;}
            table[class="full-width"]{width:100%!important; max-width:100%!important; min-width:100%!important; clear: both;}
            table[class="full-width-center"] {width: 100%!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
            table[class="force-240-center"]{width:240px !important; clear: both; margin:0 auto; float:none;}
            table[class="auto-center"] {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
            *[class="auto-center-all"]{width: auto!important; max-width:75%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
            *[class="auto-center-all"] * {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
            table[class="col-3"],table[class="col-3-not-full"]{width:30.35%!important; max-width:100%!important;}
            table[class="col-2"]{width:47.3%!important; max-width:100%!important;}
            *[class="full-block"]{width:100% !important; display:block !important; clear: both; padding-top:10px; padding-bottom:10px;}
            /* image */
            td[class="image-full-width"] img{width:100% !important; height:auto !important; max-width:100% !important;}
            /* helper */
            table[class="space-w-20"]{width:3.57%!important; max-width:20px!important; min-width:3.5% !important;}
            table[class="space-w-20"] td:first-child{width:3.5%!important; max-width:20px!important; min-width:3.5% !important;}
            table[class="space-w-25"]{width:4.45%!important; max-width:25px!important; min-width:4.45% !important;}
            table[class="space-w-25"] td:first-child{width:4.45%!important; max-width:25px!important; min-width:4.45% !important;}
            table[class="space-w-30"] td:first-child{width:5.35%!important; max-width:30px!important; min-width:5.35% !important;}
            table[class="fix-w-20"]{width:20px!important; max-width:20px!important; min-width:20px!important;}
            table[class="fix-w-20"] td:first-child{width:20px!important; max-width:20px!important; min-width:20px !important;}
            *[class="h-10"]{display:block !important;  height:10px !important;}
            *[class="h-20"]{display:block !important;  height:20px !important;}
            *[class="h-30"]{display:block !important; height:30px !important;}
            *[class="h-40"]{display:block !important;  height:40px !important;}
            *[class="remove-640"]{display:none !important;}
            *[class="text-left"]{text-align:left !important;}
            *[class="clear-pad"]{padding:0 !important;}
        }
        @media only screen and (max-width: 479px){
            /* mobile setting */
            table[class="container"]{width:100%!important; max-width:100%!important; min-width:124px!important;
                padding-left:15px!important; padding-right:15px!important; text-align: center!important; clear: both;}
            td[class="container"]{width:100%!important; padding-left:15px!important; padding-right:15px!important; text-align: center!important; clear: both;}
            table[class="full-width"],table[class="full-width-479"]{width:100%!important; max-width:100%!important; min-width:124px!important; clear: both;}
            table[class="full-width-center"] {width: 100%!important; max-width:100%!important; min-width:124px!important; text-align: center!important; clear: both; margin:0 auto; float:none;}
            *[class="auto-center-all"]{width: 100%!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
            *[class="auto-center-all"] * {width: auto!important; max-width:100%!important;  text-align: center!important; clear: both; margin:0 auto; float:none;}
            table[class="col-3"]{width:100%!important; max-width:100%!important; text-align: center!important; clear: both;}
            table[class="col-3-not-full"]{width:30.35%!important; max-width:100%!important; }
            table[class="col-2"]{width:100%!important; max-width:100%!important; text-align: center!important; clear: both;}
            *[class="full-block-479"]{display:block !important; width:100% !important; clear: both; padding-top:10px; padding-bottom:10px; }
            /* image */
            td[class="image-full-width"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:124px !important;}
            td[class="image-min-80"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:80px !important;}
            td[class="image-min-100"] img{width:100% !important; height:auto !important; max-width:100% !important; min-width:100px !important;}
            /* halper */
            table[class="space-w-20"]{width:100%!important; max-width:100%!important; min-width:100% !important;}
            table[class="space-w-20"] td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
            table[class="space-w-25"]{width:100%!important; max-width:100%!important; min-width:100% !important;}
            table[class="space-w-25"] td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
            table[class="space-w-30"]{width:100%!important; max-width:100%!important; min-width:100% !important;}
            table[class="space-w-30"] td:first-child{width:100%!important; max-width:100%!important; min-width:100% !important;}
            *[class="remove-479"]{display:none !important;}
            table[width="595"]{width:100% !important;}
            img{max-width:280px !important;}
            .resize-font, .resize-font *{
                font-size: 37px !important;
                line-height: 48px !important;
            }
        }

        td ul{list-style: initial; margin:0; padding-left:20px;}

        @media only screen and (max-width: 640px){ .image-100-percent{ width:100%!important; height: auto !important; max-width: 100% !important; min-width: 124px !important;}}td ul{list-style: initial; margin:0; padding-left:20px;}body{background-color:#efefef;} .default-edit-image{height:20px;} tr.tpl-repeatblock , tr.tpl-repeatblock > td{ display:block !important;} .tpl-repeatblock {padding: 0px !important;border: 1px dotted rgba(0,0,0,0.2);} table[width="595"]{width:100% !important;}a img{ border: 0 !important;}
        a:active{color:initial } a:visited{color:initial }
        .tpl-content{padding:0 !important;}
        .full-mb,*[fix="full-mb"]{width:100%!important;} .auto-mb,*[fix="auto-mb"]{width:auto!important;}
    </style>
    <!--[if gte mso 15]>
    <style type="text/css">
        a{text-decoration: none !important;}
        body { font-size: 0; line-height: 0; }
        tr { font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px; }
        table { font-size:1px; line-height:0; mso-margin-top-alt:1px; }
        body,table,td,span,a,font{font-family: Arial, Helvetica, sans-serif !important;}
        a img{ border: 0 !important;}
    </style>
    <![endif]-->
    <!--[if gte mso 9]><xml><o:OfficeDocumentSettings><o:AllowPNG/><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
</head>
<body  style="font-size:12px; width:100%; height:100%;">
<table id="mainStructure" width="800" class="full-width" align="center" border="0" cellspacing="0" cellpadding="0" style="background-color: #efefef; width: 800px; max-width: 800px; outline: rgb(239, 239, 239) solid 1px; box-shadow: rgb(224, 224, 224) 0px 0px 5px; margin: 0px auto;"><!-- START LAYOUT-13 ( FULL-IMAGE / TEXT ) -->
    <tr>
        <td valign="top" align="center" style="background-color:#ffffff;" class="container" bgcolor="#ffffff">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; min-width: 600px; padding-left: 20px; padding-right: 20px; width: 600px; margin: 0px auto;">
                <tbody>
                <tr>
                    <td valign="top" align="center">
                        <table width="560" border="0" cellspacing="0" cellpadding="0" align="center" class="full-width" style="width: 560px; margin: 0px auto;"><!-- start space -->
                            <tbody>
                            <tr>
                                <td valign="top" height="1" style="height: 1px; font-size: 0px; line-height: 0; border-collapse: collapse;"></td>
                            </tr><!-- end space -->
                            <tr>
                                <td valign="top" align="center">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="margin: 0px auto;"><!-- start image --><tbody><tr dup="0"><td valign="top" align="center">
                                                <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width-center" style="margin: 0px auto;"><tbody><tr><td align="center" valign="top" class="image-full-width" width="600" style="width: 600px;">
                                                            <img src="{{ URL::asset('img/email-logo.jpg') }}" width="600" style="max-width: 600px; display: block !important; width: 600px; height: auto;" alt="image" border="0" hspace="0" vspace="0" height="auto"></td>
                                                    </tr><!-- start space --><tr><td valign="top" height="2" style="height: 2px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                                                        </td>
                                                    </tr><!-- end space --></tbody></table></td>
                                        </tr><!-- end image --><!-- start title --><!-- end title --><!-- start description --><!-- end description --><!-- start content --><!-- end content --></tbody></table></td>
                            </tr><!-- start space --><tr><td valign="top" height="1" style="height: 1px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                                </td>
                            </tr><!-- end space --></tbody></table></td>
                </tr></tbody></table><!-- end container --></td>
    </tr><!-- END LAYOUT-13 ( FULL-IMAGE / TEXT ) --><!-- START LAYOUT-11 ( 2-COL IMAGE / TEXT / BUTTON ) --><tr><td align="center" valign="top" class="container" style="background-color: #086ec1;" bgcolor="#086ec1">
            <!-- start container -->
            <table class="m_-5206834744422432224inner-body" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;background-color:#ffffff;margin:0 auto;padding:0;width:570px">
                <tbody><tr>
                    <td class="m_-5206834744422432224content-cell" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;padding:35px">
                        <h1 style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#2f3133;font-size:19px;font-weight:bold;margin-top:0;text-align:left">Hola</h1>
                        <pre style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box"><code>Le adjuntamos el enlace para recuperar su contraseña.</code></pre>
                        <table class="m_-5206834744422432224action" align="center" width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;margin:30px auto;padding:0;text-align:center;width:100%"><tbody><tr>
                                <td align="center" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box"><tbody><tr>
                                            <td align="center" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
                                                <table border="0" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box"><tbody><tr>
                                                        <td style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
                                                            <a href="{{$actionUrl}}" class="m_-5206834744422432224button m_-5206834744422432224button-blue" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;border-radius:3px;color:#fff;display:inline-block;text-decoration:none;background-color:#3097d1;border-top:10px solid #3097d1;border-right:18px solid #3097d1;border-bottom:10px solid #3097d1;border-left:18px solid #3097d1" target="_blank">Recuperar contraseña</a>
                                                        </td>
                                                    </tr></tbody></table>
                                            </td>
                                        </tr></tbody></table>
                                </td>
                            </tr></tbody></table>
                        <pre style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box"><code>Si usted no inicio esta accion, simplemente desestime este email

Atentamente, Blueshop</code></pre>
                        <table class="m_-5206834744422432224subcopy" width="100%" cellpadding="0" cellspacing="0" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;border-top:1px solid #edeff2;margin-top:25px;padding-top:25px"><tbody><tr>
                                <td style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box">
                                    <p style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#74787e;line-height:1.5em;margin-top:0;text-align:left;font-size:12px">Si tiene problemas haciendo click en el boton "Recuperar contraseña" puede copiar y pegar
                                        el siguiente enlace en su navegador: <a href="{{$actionUrl}}" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#3869d4" target="_blank" ></a><a href="{{$actionUrl}}" style="font-family:Avenir,Helvetica,sans-serif;box-sizing:border-box;color:#3869d4" target="_blank">{{$actionUrl}}</a></p>
                                </td>
                            </tr></tbody></table>
                    </td>
                </tr>
                </tbody></table></td>
                </tr></table><!-- end container --><!--END LAYOUT-11 ( 2-COL IMAGE / TEXT / BUTTON ) --><!--START LAYOUT-18 ( CONTACT US / ABOUT US )  --><tr><td align="center" valign="top" class="container" style="background-color: #fff;" bgcolor="#ffffff">
            <!-- start container -->
            <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #ffffff; min-width: 600px; padding-left: 20px; padding-right: 20px; width: 600px; margin: 0px auto;"><!-- start content container--><tbody><tr><td valign="top">
                        <table width="560" align="center" border="0" cellpadding="0" cellspacing="0" class="full-width" style="width: 560px; margin: 0px auto;"><!-- start space --><tbody><tr><td valign="top" height="50" style="height: 50px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                            </tr><!-- end space --><tr><td valign="top">
                                    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="margin: 0px auto;"><tbody><tr><td valign="top" align="center">

                                                <table width="265" align="left" border="0" cellpadding="0" cellspacing="0" class="col-2" style="width: 265px;mso-table-lspace:0pt; mso-table-rspace:0pt;"><!-- start title --><tbody><!-- end title --><!-- start content --><!-- end content --></tbody></table><!--[if (gte mso 9)|(IE)]></td><td valign="top" ><![endif]--><table width="30" border="0" cellpadding="0" cellspacing="0" align="left" class="space-w-25" style="min-width: 30px; height: 1px; border-spacing: 0px; width: 30px;mso-table-lspace:0pt; mso-table-rspace:0pt;"><tbody><tr><td height="1" width="30" class="h-30" style="display: block; width: 30px; height: 1px; font-size: 0px; line-height: 0; border-collapse: collapse;">
                                                        </td>
                                                    </tr></tbody></table><!--[if (gte mso 9)|(IE)]></td><td valign="top" ><![endif]--><table width="265" align="right" border="0" cellpadding="0" cellspacing="0" class="col-2" style="width: 265px;mso-table-lspace:0pt; mso-table-rspace:0pt;"><!-- start title --><tbody><!-- end title --><!-- start content --><!-- end content --></tbody></table></td>
                                        </tr></tbody></table></td>
                            </tr><!-- start space --><tr><td valign="top" height="30" style="height: 30px; font-size: 0px; line-height: 0; border-collapse: collapse;">&nbsp;</td>
                            </tr><!-- end space --></tbody></table></td>
                </tr></tbody></table><!-- end container --></td>
    <!-- END LAYOUT-18 ( CONTACT US / ABOUT US ) --></body>
</html>