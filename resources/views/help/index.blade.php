@extends('layouts.panel')

@section('head')
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('lib/trumbowyg/ui/trumbowyg.css') }}"/>
@endsection

@section('content-panel') 
	@include('help.header')
	
  <form action="{{ url('help') }}" method="POST">
    {{ csrf_field() }}
    <div class="form-group">
      <div class="col-xs-12">
        <textarea class="trumbowyg-editor" name="help">{{ $help }}</textarea>
      </div>
    </div>

    <div class="form-group">
      <div class="col-xs-12">
        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span>  {{ trans('process.update') }} </button> 
      </div>
    </div>
  </form>
@endsection

@section('footer') 
    <script src="{{ URL::asset('lib/trumbowyg/trumbowyg.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('lib/trumbowyg/langs/es_ar.min.js') }}" type="text/javascript"></script>
@endsection