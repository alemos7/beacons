@section('containt-title') {{ trans('data.help') }} @endsection

@section('menu-help') class="active" @endsection

@section('breadcrumbs') 
    {{-- <li><a href="{{ url('shops') }}" >{{ trans('data.help') }}</a></li> --}}
    @yield('help-breadcrumbs')
@endsection