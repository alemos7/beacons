@extends('layouts.panel')

@section('word-breadcrumbs')
    @if($word->id)
        <li><a href="{{ url('words/'.$word->id) }}">{{ trans('process.update') }}</a></li>
    @else
        <li><a href="{{ url('words/create') }}">{{ trans('process.create') }}</a></li>
    @endif
@endsection

@section('content-panel')
    @include('word.header')
    <form role="form" method="POST" action="{{ url('words/'.$word->id) }}" class="form-horizontal group-border-dashed">

        @if($word->id != '')
            <input type='hidden'name='_method' value='PUT'>
        @endif

        {{ csrf_field() }}

        <div class="form-group">
            <label class="col-sm-3 control-label">{{ trans('models.word') }}</label>
            <div class="col-sm-6">
                <input type="text" placeholder="{{ trans('models.word') }}" name="word" autocomplete="off"  class="form-control input-sm" maxlength="100" value="{{ old('word',$word->word) }}" data-parsley-maxlength="100" required autofocus>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-primary"> @if($word->id)<span class="glyphicon glyphicon-edit"></span>  {{ trans('process.update') }} @else <span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}  @endif</button> 
                <a href="{{ url('/words') }}" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span>   {{ trans('process.back') }}</a> 
            </div>
        </div>
    </form>
@endsection