<hr/>
<table class="table table-striped table-condensed" id="words-datatable">
    <thead>
        <tr>
            <th class="col col-xs-2 text-center" >{{ trans('data.id') }}</th>
            <th class="col col-xs-3" >{{ trans('models.word') }}</th>
            <th class="col col-xs-2">{{ trans('data.action') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($words as $word)
        <tr>
            <td class="text-center">{{ $word->id }}</td>
            <td>
                <span data-toggle="tooltip" title="{{ $word->word }}">{{ str_limit($word->word,30) }}</span>
            </td>
            <td>
                <form action='{{ url('words/'.$word->id) }}' method='POST' id="word-delete-{{ $word->id }}">
                    <input type="hidden" name="_method" value="DELETE">
                    {{ csrf_field()}} 
                    <a class="icon mdi mdi-edit btn btn-primary" href="{{ url('words/'.$word->id) }}" data-toggle="tooltip" title="{{ trans("process.update") }}"></a>
                    <a data-modal="full-danger"  class="btn btn-danger md-trigger " onclick="formSelected='word-delete-{{ $word->id }}'" data-toggle="tooltip" title="{{ trans("process.delete") }}">
                        <span class="icon mdi mdi-delete"></span>          
                    </a>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>