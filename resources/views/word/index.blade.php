@extends('layouts.panel')

@section('content-panel')
    @include('word.header')
    <a href="{{ url('words/create') }}"  class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>  {{ trans('process.create') }}</a>
    @include('word.list')
@endsection

@section('footer')
    <script>$('#word-datatable').DataTable();</script>
@endsection