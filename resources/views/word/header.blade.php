@section('containt-title') {{ trans_choice('models.words', 2) }} @endsection

@section('menu-palabras') class="active" @endsection

@section('breadcrumbs') 
    <li><a href="{{ url('words') }}" >{{ trans('models.list',['model'=>trans('models.words')] ) }}</a></li>
    @yield('word-breadcrumbs')
@endsection